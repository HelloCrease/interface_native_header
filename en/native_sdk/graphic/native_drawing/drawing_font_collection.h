/*
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef C_INCLUDE_DRAWING_FONT_COLLECTION_H
#define C_INCLUDE_DRAWING_FONT_COLLECTION_H

/**
 * @addtogroup Drawing
 * @{
 *
 * @brief Provides the functions for 2D graphics rendering, text drawing, and image display.
 * This module uses the physical pixel unit, px.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 *
 * @since 8
 * @version 1.0
 */

/**
 * @file drawing_font_collection.h
 *
 * @brief Declares the functions related to the font collection in the drawing module.
 *
 * File to include: native_drawing/drawing_font_collection.h
 * @library libnative_drawing.so
 * @since 8
 * @version 1.0
 */

#include "drawing_text_declaration.h"

#ifdef __cplusplus
extern "C" {
#endif
/**
 * @brief Creates an {@link OH_Drawing_FontCollection} object.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @return Returns the pointer to the {@link OH_Drawing_FontCollection} object created.
 * The {@link OH_Drawing_FontCollection} object created by this function can be used by only one
 * {@link OH_Drawing_TypographyCreate} object. To share an {@link OH_Drawing_FontCollection} object among multiple
 * {@link OH_Drawing_TypographyCreate} objects, use {@link OH_Drawing_CreateSharedFontCollection} to create it.
 * @since 8
 * @version 1.0
 */
OH_Drawing_FontCollection* OH_Drawing_CreateFontCollection(void);

/**
 * @brief Destroys an <b>OH_Drawing_FontCollection</b> object and reclaims the memory occupied by the object.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_FontCollection Pointer to an <b>OH_Drawing_FontCollection</b> object.
 * @since 8
 * @version 1.0
 */
void OH_Drawing_DestroyFontCollection(OH_Drawing_FontCollection*);

/**
 * @brief Disables the alternate fonts.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_FontCollection Pointer to an {@link OH_Drawing_FontCollection} object.
 * @since 12
 * @version 1.0
 */
void OH_Drawing_DisableFontCollectionFallback(OH_Drawing_FontCollection* fontCollection);

/**
 * @brief Disables the system fonts.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_FontCollection Pointer to an {@link OH_Drawing_FontCollection} object.
 * @since 12
 * @version 1.0
 */
void OH_Drawing_DisableFontCollectionSystemFont(OH_Drawing_FontCollection* fontCollection);

/**
 * @brief Creates an {@link OH_Drawing_FontCollection} object.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @return Returns the pointer to the {@link OH_Drawing_FontCollection} object created.
 * @since 12
 * @version 1.0
 */
OH_Drawing_FontCollection* OH_Drawing_CreateSharedFontCollection(void);

/**
 * @brief Clears the font cache.
 * The font cache has a memory limit and a clearing mechanism. It occupies limited memory.
 * You are not advised to clear it unless otherwise required.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_FontCollection Pointer to an {@link OH_Drawing_FontCollection} object.
 * @since 12
 * @version 1.0
 */
void OH_Drawing_ClearFontCaches(OH_Drawing_FontCollection*);
#ifdef __cplusplus
}
#endif
/** @} */
#endif
