/*
 * Copyright (c) 2024 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef C_INCLUDE_DRAWING_ROUND_RECT_H
#define C_INCLUDE_DRAWING_ROUND_RECT_H

/**
 * @addtogroup Drawing
 * @{
 *
 * @brief Provides the functions for 2D graphics rendering, text drawing, and image display.
 * This module uses the physical pixel unit, px.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 *
 * @since 8
 * @version 1.0
 */

/**
 * @file drawing_round_rect.h
 *
 * @brief Declares the functions related to the rounded rectangle in the drawing module.
 *
 * File to include: native_drawing/drawing_round_rect.h
 * @library libnative_drawing.so
 * @since 11
 * @version 1.0
 */

#include "drawing_error_code.h"
#include "drawing_types.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief Enumerates the corner positions of a rounded rectangle.
 *
 * @since 12
 * @version 1.0
 */
typedef enum OH_Drawing_CornerPos {
    /**
     * Top left corner of the rounded rectangle.
     */
    CORNER_POS_TOP_LEFT,
    /**
     * Top right corner of the rounded rectangle.
     */
    CORNER_POS_TOP_RIGHT,
    /**
     * Bottom right corner of the rounded rectangle.
     */
    CORNER_POS_BOTTOM_RIGHT,
    /**
     * Bottom left corner of the rounded rectangle.
     */
    CORNER_POS_BOTTOM_LEFT,
} OH_Drawing_CornerPos;

/**
 * @brief Creates an <b>OH_Drawing_RoundRect</b> object.
 *
 * Error codes may be generated in the call. You can view the error code by calling {@link OH_Drawing_ErrorCodeGet}.
 * If the operation is successful, <b>OH_DRAWING_SUCCESS</b> is returned.
 * If <b>OH_Drawing_Rect</b> is NULL, {@link OH_DRAWING_ERROR_INVALID_PARAMETER} is returned.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Rect Pointer to an <b>OH_Drawing_Rect</b> object.
 * @param xRad Radius of the rounded corner on the X axis.
 * @param yRad Radius of the rounded corner on the Y axis.
 * @return Returns the pointer to the <b>OH_Drawing_RoundRect</b> object created.
 * @since 11
 * @version 1.0
 */
OH_Drawing_RoundRect* OH_Drawing_RoundRectCreate(const OH_Drawing_Rect*, float xRad, float yRad);

/**
 * @brief Sets the radii of the specified rounded corner in a rounded rectangle.
 *
 * Error codes may be generated in the call. You can view the error code by calling {@link OH_Drawing_ErrorCodeGet}.
 * If the operation is successful, <b>OH_DRAWING_SUCCESS</b> is returned.
 * If <b>OH_Drawing_RoundRect</b> is NULL, <b>OH_DRAWING_ERROR_INVALID_PARAMETER</b> is returned.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_RoundRect Pointer to an <b>OH_Drawing_RoundRect</b> object.
 * @param pos Position of the rounded corner. For details about the available options, see {@link OH_Drawing_CornerPos}.
 * @param OH_Drawing_Corner_Radii {@link OH_Drawing_Corner_Radii} struct, including the radii on the X axis and Y axis.
 * @since 12
 * @version 1.0
 */
void OH_Drawing_RoundRectSetCorner(OH_Drawing_RoundRect*, OH_Drawing_CornerPos pos, OH_Drawing_Corner_Radii);

/**
 * @brief Obtains the radii of the specified rounded corner in a rounded rectangle.
 *
 * Error codes may be generated in the call. You can view the error code by calling {@link OH_Drawing_ErrorCodeGet}.
 * If the operation is successful, <b>OH_DRAWING_SUCCESS</b> is returned.
 * If <b>OH_Drawing_RoundRect</b> is NULL, <b>OH_DRAWING_ERROR_INVALID_PARAMETER</b> is returned.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_RoundRect Pointer to an <b>OH_Drawing_RoundRect</b> object.
 * @param pos Position of the rounded corner. For details about the available options, see {@link OH_Drawing_CornerPos}.
 * @return Returns an {@link OH_Drawing_Corner_Radii} struct, including the radii on the X axis and Y axis.
 * @since 12
 * @version 1.0
 */
OH_Drawing_Corner_Radii OH_Drawing_RoundRectGetCorner(OH_Drawing_RoundRect*, OH_Drawing_CornerPos pos);

/**
 * @brief Destroys an <b>OH_Drawing_RoundRect</b> object and reclaims the memory occupied by the object.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_RoundRect Pointer to an <b>OH_Drawing_RoundRect</b> object.
 * @since 11
 * @version 1.0
 */
void OH_Drawing_RoundRectDestroy(OH_Drawing_RoundRect*);

/**
 * @brief Translates a rounded rectangle by an offset along the X axis and Y axis.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param roundRect Pointer to an {@link OH_Drawing_Point2D} object.
 * @param dx X offset.
 * @param dy Y offset.
 * @return Returns either of the following result codes:
 *         <b>OH_DRAWING_SUCCESS</b> if the operation is successful.
 *         <b>OH_DRAWING_ERROR_INVALID_PARAMETER</b> if <b>roundRect</b> is NULL.
 * @since 12
 * @version 1.0
 */
OH_Drawing_ErrorCode OH_Drawing_RoundRectOffset(OH_Drawing_RoundRect* roundRect, float dx, float dy);
#ifdef __cplusplus
}
#endif
/** @} */
#endif
