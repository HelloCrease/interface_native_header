/*
 * Copyright (c) 2023-2024 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef C_INCLUDE_DRAWING_RECT_H
#define C_INCLUDE_DRAWING_RECT_H

/**
 * @addtogroup Drawing
 * @{
 *
 * @brief Provides the functions for 2D graphics rendering, text drawing, and image display.
 * This module uses the physical pixel unit, px.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 *
 * @since 8
 * @version 1.0
 */

/**
 * @file drawing_rect.h
 *
 * @brief Declares the functions related to the rectangle in the drawing module.
 *
 * File to include: native_drawing/drawing_rect.h
 * @library libnative_drawing.so
 * @since 11
 * @version 1.0
 */

#include "drawing_types.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief Creates an <b>OH_Drawing_Rect</b> object, without sorting the coordinates passed in.
 * This means that the coordinates of the upper left corner of the rectangle can be greater than those of
 * the lower right corner.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param left X coordinate of the upper left corner of the rectangle.
 * @param top Y coordinate of the upper left corner of the rectangle.
 * @param right X coordinate of the lower right corner of the rectangle.
 * @param bottom Y coordinate of the lower right corner of the rectangle.
 * @return Returns the pointer to the <b>OH_Drawing_Rect</b> object created.
 * @since 11
 * @version 1.0
 */
OH_Drawing_Rect* OH_Drawing_RectCreate(float left, float top, float right, float bottom);

/**
 * @brief Obtains the height of a rectangle.
 * The height is calculated by using the Y coordinate of the lower right corner of the rectangle minus
 * the Y coordinate of the upper left corner.
 *
 * Error codes may be generated in the call. You can view the error code by calling {@link OH_Drawing_ErrorCodeGet}.
 * If the operation is successful, <b>OH_DRAWING_SUCCESS</b> is returned.
 * If <b>OH_Drawing_Rect</b> is NULL, {@link OH_DRAWING_ERROR_INVALID_PARAMETER} is returned.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Rect Pointer to an <b>OH_Drawing_Rect</b> object.
 * @return Returns the height.
 * @since 12
 * @version 1.0
 */
float OH_Drawing_RectGetHeight(OH_Drawing_Rect*);

/**
 * @brief Obtains the width of a rectangle.
 * The width is calculated by using the X coordinate of the lower right corner of the rectangle minus
 * the X coordinate of the upper left corner.
 *
 * Error codes may be generated in the call. You can view the error code by calling {@link OH_Drawing_ErrorCodeGet}.
 * If the operation is successful, <b>OH_DRAWING_SUCCESS</b> is returned.
 * If <b>OH_Drawing_Rect</b> is NULL, {@link OH_DRAWING_ERROR_INVALID_PARAMETER} is returned.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Rect Pointer to an <b>OH_Drawing_Rect</b> object.
 * @return Returns the width.
 * @since 12
 * @version 1.0
 */
float OH_Drawing_RectGetWidth(OH_Drawing_Rect*);

/**
 * @brief Obtains the X coordinate of the upper left corner of a rectangle.
 *
 * Error codes may be generated in the call. You can view the error code by calling {@link OH_Drawing_ErrorCodeGet}.
 * If the operation is successful, <b>OH_DRAWING_SUCCESS</b> is returned.
 * If <b>OH_Drawing_Rect</b> is NULL, {@link OH_DRAWING_ERROR_INVALID_PARAMETER} is returned.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Rect Pointer to an <b>OH_Drawing_Rect</b> object.
 * @return Returns the X coordinate of the upper left corner of the rectangle.
 * @since 12
 * @version 1.0
 */
float OH_Drawing_RectGetLeft(OH_Drawing_Rect*);

/**
 * @brief Obtains the Y coordinate of the upper left corner of a rectangle.
 *
 * Error codes may be generated in the call. You can view the error code by calling {@link OH_Drawing_ErrorCodeGet}.
 * If the operation is successful, <b>OH_DRAWING_SUCCESS</b> is returned.
 * If <b>OH_Drawing_Rect</b> is NULL, {@link OH_DRAWING_ERROR_INVALID_PARAMETER} is returned.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Rect Pointer to an <b>OH_Drawing_Rect</b> object.
 * @return Returns the Y coordinate of the upper left corner of the rectangle.
 * @since 12
 * @version 1.0
 */
float OH_Drawing_RectGetTop(OH_Drawing_Rect*);

/**
 * @brief Obtains the X coordinate of the lower right corner of a rectangle.
 *
 * Error codes may be generated in the call. You can view the error code by calling {@link OH_Drawing_ErrorCodeGet}.
 * If the operation is successful, <b>OH_DRAWING_SUCCESS</b> is returned.
 * If <b>OH_Drawing_Rect</b> is NULL, {@link OH_DRAWING_ERROR_INVALID_PARAMETER} is returned.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Rect Pointer to an <b>OH_Drawing_Rect</b> object.
 * @return Returns the X coordinate of the lower right corner of the rectangle.
 * @since 12
 * @version 1.0
 */
float OH_Drawing_RectGetRight(OH_Drawing_Rect*);

/**
 * @brief Obtains the Y coordinate of the lower right corner of a rectangle.
 *
 * Error codes may be generated in the call. You can view the error code by calling {@link OH_Drawing_ErrorCodeGet}.
 * If the operation is successful, <b>OH_DRAWING_SUCCESS</b> is returned.
 * If <b>OH_Drawing_Rect</b> is NULL, {@link OH_DRAWING_ERROR_INVALID_PARAMETER} is returned.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Rect Pointer to an <b>OH_Drawing_Rect</b> object.
 * @return Returns the Y coordinate of the lower right corner of the rectangle.
 * @since 12
 * @version 1.0
 */
float OH_Drawing_RectGetBottom(OH_Drawing_Rect*);

/**
 * @brief Checks whether two rectangles intersect and if yes, sets <b>rect</b> to the area of intersection.
 *
 * Error codes may be generated in the call. You can view the error code by calling {@link OH_Drawing_ErrorCodeGet}.
 * If the operation is successful, <b>OH_DRAWING_SUCCESS</b> is returned.
 * If either <b>rect</b> or <b>other</b> is NULL, <b>OH_DRAWING_ERROR_INVALID_PARAMETER</b> is returned.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param rect Pointer to the first rectangle, which is an <b>OH_Drawing_Rect</b> object.
 * @param other Pointer to the second rectangle, which is an <b>OH_Drawing_Rect</b> object.
 * @return Returns <b>true</b> if they intersect (<b>rect</b> is set to the intersection area);
 * returns <b>false</b> otherwise (<b>rect</b> remains unchanged).
 * @since 12
 * @version 1.0
 */
bool OH_Drawing_RectIntersect(OH_Drawing_Rect* rect, const OH_Drawing_Rect* other);

/**
 * @brief Obtains the union of two rectangles.
 *
 * Error codes may be generated in the call. You can view the error code by calling {@link OH_Drawing_ErrorCodeGet}.
 * If the operation is successful, <b>OH_DRAWING_SUCCESS</b> is returned.
 * If either <b>rect</b> or <b>other</b> is NULL, <b>OH_DRAWING_ERROR_INVALID_PARAMETER</b> is returned.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param rect Pointer to the first rectangle, which is an <b>OH_Drawing_Rect</b> object.
 * @param other Pointer to the second rectangle, which is an <b>OH_Drawing_Rect</b> object.
 * @return Returns <b>true</b> if the union is obtained; returns <b>false</b> otherwise.
 * The possible failure cause is that at least one of the parameters <b>rect</b> and <b>other</b> is NULL
 * or the size of the rectangle specified by <b>other</b> is empty.
 * @since 12
 * @version 1.0
 */
bool OH_Drawing_RectJoin(OH_Drawing_Rect* rect, const OH_Drawing_Rect* other);

/**
 * @brief Sets the horizontal coordinate of the upper left corner of a rectangle.
 *
 * Error codes may be generated in the call. You can view the error code by calling {@link OH_Drawing_ErrorCodeGet}.
 * If the operation is successful, <b>OH_DRAWING_SUCCESS</b> is returned.
 * If <b>rect</b> is NULL, <b>OH_DRAWING_ERROR_INVALID_PARAMETER</b> is returned.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param rect Pointer to an <b>OH_Drawing_Rect</b> object.
 * @param left X coordinate of the upper left corner of the rectangle.
 * @since 12
 * @version 1.0
 */
void OH_Drawing_RectSetLeft(OH_Drawing_Rect* rect, float left);

/**
 * @brief Sets the vertical coordinate of the upper left corner of a rectangle.
 *
 * Error codes may be generated in the call. You can view the error code by calling {@link OH_Drawing_ErrorCodeGet}.
 * If the operation is successful, <b>OH_DRAWING_SUCCESS</b> is returned.
 * If <b>rect</b> is NULL, <b>OH_DRAWING_ERROR_INVALID_PARAMETER</b> is returned.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param rect Pointer to an <b>OH_Drawing_Rect</b> object.
 * @param top Y coordinate of the upper left corner of the rectangle.
 * @since 12
 * @version 1.0
 */
void OH_Drawing_RectSetTop(OH_Drawing_Rect* rect, float top);

/**
 * @brief Sets the horizontal coordinate of the lower right corner of a rectangle.
 *
 * Error codes may be generated in the call. You can view the error code by calling {@link OH_Drawing_ErrorCodeGet}.
 * If the operation is successful, <b>OH_DRAWING_SUCCESS</b> is returned.
 * If <b>rect</b> is NULL, <b>OH_DRAWING_ERROR_INVALID_PARAMETER</b> is returned.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param rect Pointer to an <b>OH_Drawing_Rect</b> object.
 * @param right X coordinate of the lower right corner of the rectangle.
 * @since 12
 * @version 1.0
 */
void OH_Drawing_RectSetRight(OH_Drawing_Rect* rect, float right);

/**
 * @brief Sets the vertical coordinate of the lower right corner of a rectangle.
 *
 * Error codes may be generated in the call. You can view the error code by calling {@link OH_Drawing_ErrorCodeGet}.
 * If the operation is successful, <b>OH_DRAWING_SUCCESS</b> is returned.
 * If <b>rect</b> is NULL, <b>OH_DRAWING_ERROR_INVALID_PARAMETER</b> is returned.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param rect Pointer to an <b>OH_Drawing_Rect</b> object.
 * @param bottom Y coordinate of the lower right corner of the rectangle.
 * @since 12
 * @version 1.0
 */
void OH_Drawing_RectSetBottom(OH_Drawing_Rect* rect, float bottom);

/**
 * @brief Copies a source rectangle to create a new one.
 *
 * Error codes may be generated in the call. You can view the error code by calling {@link OH_Drawing_ErrorCodeGet}.
 * If the operation is successful, <b>OH_DRAWING_SUCCESS</b> is returned.
 * If either <b>sRect</b> or <b>dRect</b> is NULL, <b>OH_DRAWING_ERROR_INVALID_PARAMETER</b> is returned.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param sRect Pointer to a source rectangle, which is an <b>OH_Drawing_Rect</b> object.
 * @param dRect Pointer to a destination rectangle, which is an <b>OH_Drawing_Rect</b> object.
 * @since 12
 * @version 1.0
 */
void OH_Drawing_RectCopy(OH_Drawing_Rect* sRect, OH_Drawing_Rect* dRect);

/**
 * @brief Destroys an <b>OH_Drawing_Rect</b> object and reclaims the memory occupied by the object.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Rect Pointer to an <b>OH_Drawing_Rect</b> object.
 * @since 11
 * @version 1.0
 */
void OH_Drawing_RectDestroy(OH_Drawing_Rect*);

#ifdef __cplusplus
}
#endif
/** @} */
#endif
