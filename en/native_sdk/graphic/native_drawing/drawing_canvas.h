/*
 * Copyright (c) 2021-2024 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef C_INCLUDE_DRAWING_H
#define C_INCLUDE_DRAWING_H

/**
 * @addtogroup Drawing
 * @{
 *
 * @brief Provides the functions for 2D graphics rendering, text drawing, and image display.
 * This module uses the physical pixel unit, px.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 *
 * @since 8
 * @version 1.0
 */

/**
 * @file drawing_canvas.h
 *
 * @brief Declares the functions related to the canvas in the drawing module.
 * By default, the canvas has a black brush with anti-aliasing enabled and without any other style.
 * This brush takes effect only when no brush or pen is proactively set in the canvas.
 *
 * File to include: native_drawing/drawing_canvas.h
 * @library libnative_drawing.so
 * @since 8
 * @version 1.0
 */

#include "drawing_error_code.h"
#include "drawing_types.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief Enumerates the constraint types of the source rectangle.
 *
 * @since 12
 * @version 1.0
 */
typedef enum OH_Drawing_SrcRectConstraint {
    /** The source rectangle must be completely contained in the image. */
    STRICT_SRC_RECT_CONSTRAINT,
    /** The source rectangle can be partly outside the image. */
    FAST_SRC_RECT_CONSTRAINT,
} OH_Drawing_SrcRectConstraint;

/**
 * @brief Creates an <b>OH_Drawing_Canvas</b> object.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @return Returns the pointer to the <b>OH_Drawing_Canvas</b> object created.
 * @since 8
 * @version 1.0
 */
OH_Drawing_Canvas* OH_Drawing_CanvasCreate(void);

/**
 * @brief Destroys an <b>OH_Drawing_Canvas</b> object and reclaims the memory occupied by the object.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Canvas Pointer to an <b>OH_Drawing_Canvas</b> object.
 * @since 8
 * @version 1.0
 */
void OH_Drawing_CanvasDestroy(OH_Drawing_Canvas*);

/**
 * @brief Binds a bitmap to a canvas so that the content drawn on the canvas is output to the bitmap.
 * (This process is called CPU rendering.)
 *
 * Error codes may be generated in the call. You can view the error code by calling {@link OH_Drawing_ErrorCodeGet}.
 * If the operation is successful, <b>OH_DRAWING_SUCCESS</b> is returned.
 * If either <b>OH_Drawing_Canvas</b> or <b>OH_Drawing_Bitmap</b> is NULL,
 * <b>OH_DRAWING_ERROR_INVALID_PARAMETER</b> is returned.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Canvas Pointer to an <b>OH_Drawing_Canvas</b> object.
 * @param OH_Drawing_Bitmap Pointer to an <b>OH_Drawing_Bitmap</b> object.
 * @since 8
 * @version 1.0
 */
void OH_Drawing_CanvasBind(OH_Drawing_Canvas*, OH_Drawing_Bitmap*);

/**
 * @brief Attaches a pen to a canvas so that the canvas can use the style and color of the pen to outline a shape.
 *
 * Error codes may be generated in the call. You can view the error code by calling {@link OH_Drawing_ErrorCodeGet}.
 * If the operation is successful, <b>OH_DRAWING_SUCCESS</b> is returned.
 * If either <b>OH_Drawing_Canvas</b> or <b>OH_Drawing_Pen</b> is NULL,
 * <b>OH_DRAWING_ERROR_INVALID_PARAMETER</b> is returned.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Canvas Pointer to an <b>OH_Drawing_Canvas</b> object.
 * @param OH_Drawing_Pen Pointer to an <b>OH_Drawing_Pen</b> object.
 * @since 8
 * @version 1.0
 */
void OH_Drawing_CanvasAttachPen(OH_Drawing_Canvas*, const OH_Drawing_Pen*);

/**
 * @brief Detaches the pen from a canvas so that the canvas can no longer use the style and color of the pen
 * to outline a shape.
 *
 * Error codes may be generated in the call. You can view the error code by calling {@link OH_Drawing_ErrorCodeGet}.
 * If the operation is successful, <b>OH_DRAWING_SUCCESS</b> is returned.
 * If <b>OH_Drawing_Canvas</b> is NULL, <b>OH_DRAWING_ERROR_INVALID_PARAMETER</b> is returned.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Canvas Pointer to an <b>OH_Drawing_Canvas</b> object.
 * @since 8
 * @version 1.0
 */
void OH_Drawing_CanvasDetachPen(OH_Drawing_Canvas*);

/**
 * @brief Attaches a brush to a canvas so that the canvas can use the style and color of the brush to fill in a shape.
 *
 * Error codes may be generated in the call. You can view the error code by calling {@link OH_Drawing_ErrorCodeGet}.
 * If the operation is successful, <b>OH_DRAWING_SUCCESS</b> is returned.
 * If either <b>OH_Drawing_Canvas</b> or <b>OH_Drawing_Brush</b> is NULL,
 * <b>OH_DRAWING_ERROR_INVALID_PARAMETER</b> is returned.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Canvas Pointer to an <b>OH_Drawing_Canvas</b> object.
 * @param OH_Drawing_Brush Pointer to an <b>OH_Drawing_Brush</b> object.
 * @since 8
 * @version 1.0
 */
void OH_Drawing_CanvasAttachBrush(OH_Drawing_Canvas*, const OH_Drawing_Brush*);

/**
 * @brief Detaches the brush from a canvas so that the canvas can no longer use the previously set brush
 * to fill in a shape.
 *
 * Error codes may be generated in the call. You can view the error code by calling {@link OH_Drawing_ErrorCodeGet}.
 * If the operation is successful, <b>OH_DRAWING_SUCCESS</b> is returned.
 * If <b>OH_Drawing_Canvas</b> is NULL, <b>OH_DRAWING_ERROR_INVALID_PARAMETER</b> is returned.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Canvas Pointer to an <b>OH_Drawing_Canvas</b> object.
 * @since 8
 * @version 1.0
 */
void OH_Drawing_CanvasDetachBrush(OH_Drawing_Canvas*);

/**
 * @brief Saves the current canvas status (canvas matrix) to the top of the stack.
 *
 * Error codes may be generated in the call. You can view the error code by calling {@link OH_Drawing_ErrorCodeGet}.
 * If the operation is successful, <b>OH_DRAWING_SUCCESS</b> is returned.
 * If <b>OH_Drawing_Canvas</b> is NULL, <b>OH_DRAWING_ERROR_INVALID_PARAMETER</b> is returned.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Canvas Pointer to an <b>OH_Drawing_Canvas</b> object.
 * @since 8
 * @version 1.0
 */
void OH_Drawing_CanvasSave(OH_Drawing_Canvas*);

/**
 * @brief Saves the matrix and cropping region, and allocates a bitmap for subsequent drawing. If you call
 * {@link OH_Drawing_CanvasRestore}, the changes made to the matrix and clipping region are discarded,
 * and the bitmap is drawn.
 *
 * Error codes may be generated in the call. You can view the error code by calling {@link OH_Drawing_ErrorCodeGet}.
 * If the operation is successful, <b>OH_DRAWING_SUCCESS</b> is returned.
 * If <b>OH_Drawing_Canvas</b> is NULL, <b>OH_DRAWING_ERROR_INVALID_PARAMETER</b> is returned.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Canvas Pointer to an {@link OH_Drawing_Canvas} object.
 * @param OH_Drawing_Rect Pointer to an {@link OH_Drawing_Rect} object, which is used to limit the size of
 * the graphics layer. If NULL is passed in, the size is not limited.
 * @param OH_Drawing_Brush Pointer to an {@link OH_Drawing_Brush} object. The alpha value, filter effect, and blend mode
 * of the brush are applied when the bitmap is drawn. If NULL is passed in, no effect is applied.
 * @since 12
 * @version 1.0
 */
void OH_Drawing_CanvasSaveLayer(OH_Drawing_Canvas*, const OH_Drawing_Rect*, const OH_Drawing_Brush*);

/**
 * @brief Restores the canvas status (canvas matrix) saved on the top of the stack.
 *
 * Error codes may be generated in the call. You can view the error code by calling {@link OH_Drawing_ErrorCodeGet}.
 * If the operation is successful, <b>OH_DRAWING_SUCCESS</b> is returned.
 * If <b>OH_Drawing_Canvas</b> is NULL, <b>OH_DRAWING_ERROR_INVALID_PARAMETER</b> is returned.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Canvas Pointer to an <b>OH_Drawing_Canvas</b> object.
 * @since 8
 * @version 1.0
 */
void OH_Drawing_CanvasRestore(OH_Drawing_Canvas*);

/**
 * @brief Obtains the number of canvas statuses (canvas matrices) saved in the stack.
 *
 * Error codes may be generated in the call. You can view the error code by calling {@link OH_Drawing_ErrorCodeGet}.
 * If the operation is successful, <b>OH_DRAWING_SUCCESS</b> is returned.
 * If <b>OH_Drawing_Canvas</b> is NULL, <b>OH_DRAWING_ERROR_INVALID_PARAMETER</b> is returned.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Canvas Pointer to an <b>OH_Drawing_Canvas</b> object.
 * @return Returns a 32-bit value that describes the number of canvas statuses (canvas matrices).
 * The initial number is <b>1</b>.
 * @since 11
 * @version 1.0
 */
uint32_t OH_Drawing_CanvasGetSaveCount(OH_Drawing_Canvas*);

/**
 * @brief Restores to a given number of canvas statuses (canvas matrices).
 *
 * Error codes may be generated in the call. You can view the error code by calling {@link OH_Drawing_ErrorCodeGet}.
 * If the operation is successful, <b>OH_DRAWING_SUCCESS</b> is returned.
 * If <b>OH_Drawing_Canvas</b> is NULL, <b>OH_DRAWING_ERROR_INVALID_PARAMETER</b> is returned.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Canvas Pointer to an <b>OH_Drawing_Canvas</b> object.
 * @param saveCount Number of canvas statuses (canvas matrices).
 * If the value is less than or equal to 1, the canvas is restored to the initial state.
 * If the value is greater than the number of canvas statuses that have been saved, no operation is performed.
 * @since 11
 * @version 1.0
 */
void OH_Drawing_CanvasRestoreToCount(OH_Drawing_Canvas*, uint32_t saveCount);

/**
 * @brief Draws a line segment.
 *
 * Error codes may be generated in the call. You can view the error code by calling {@link OH_Drawing_ErrorCodeGet}.
 * If the operation is successful, <b>OH_DRAWING_SUCCESS</b> is returned.
 * If <b>OH_Drawing_Canvas</b> is NULL, <b>OH_DRAWING_ERROR_INVALID_PARAMETER</b> is returned.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Canvas Pointer to an <b>OH_Drawing_Canvas</b> object.
 * @param x1 X coordinate of the start point of the line segment.
 * @param y1 Y coordinate of the start point of the line segment.
 * @param x2 X coordinate of the end point of the line segment.
 * @param y2 Y coordinate of the end point of the line segment.
 * @since 8
 * @version 1.0
 */
void OH_Drawing_CanvasDrawLine(OH_Drawing_Canvas*, float x1, float y1, float x2, float y2);

/**
 * @brief Draws a path.
 *
 * Error codes may be generated in the call. You can view the error code by calling {@link OH_Drawing_ErrorCodeGet}.
 * If the operation is successful, <b>OH_DRAWING_SUCCESS</b> is returned.
 * If either <b>OH_Drawing_Canvas</b> or <b>OH_Drawing_Path</b> is NULL,
 * <b>OH_DRAWING_ERROR_INVALID_PARAMETER</b> is returned.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Canvas Pointer to an <b>OH_Drawing_Canvas</b> object.
 * @param OH_Drawing_Path Pointer to an <b>OH_Drawing_Path</b> object.
 * @since 8
 * @version 1.0
 */
void OH_Drawing_CanvasDrawPath(OH_Drawing_Canvas*, const OH_Drawing_Path*);

/**
 * @brief Draws a portion of a pixel map onto a specified area of the canvas.
 *
 * Error codes may be generated in the call. You can view the error code by calling {@link OH_Drawing_ErrorCodeGet}.
 * If the operation is successful, <b>OH_DRAWING_SUCCESS</b> is returned.
 * If any of <b>OH_Drawing_Canvas</b>, <b>OH_Drawing_PixelMap</b>, and <b>dst</b> is NULL,
 * <b>OH_DRAWING_ERROR_INVALID_PARAMETER</b> is returned.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Canvas Pointer to an {@link OH_Drawing_Canvas} object.
 * @param OH_Drawing_PixelMap Pointer to an {@link OH_Drawing_PixelMap} object.
 * @param src Pointer to a rectangle on the pixel map. If NULL is passed in, it refers to the entire pixel map.
 * @param dst Pointer to a rectangle on the canvas.
 * @param OH_Drawing_SamplingOptions Pointer to an {@link OH_Drawing_SamplingOptions} object.
 * If NULL is passed in, the default sampling options are used.
 * @since 12
 * @version 1.0
 */
void OH_Drawing_CanvasDrawPixelMapRect(OH_Drawing_Canvas*, OH_Drawing_PixelMap*, const OH_Drawing_Rect* src,
    const OH_Drawing_Rect* dst, const OH_Drawing_SamplingOptions*);

/**
 * @brief Draws a background filled with a brush.
 *
 * Error codes may be generated in the call. You can view the error code by calling {@link OH_Drawing_ErrorCodeGet}.
 * If the operation is successful, <b>OH_DRAWING_SUCCESS</b> is returned.
 * If either <b>OH_Drawing_Canvas</b> or <b>OH_Drawing_Brush</b> is NULL,
 * <b>OH_DRAWING_ERROR_INVALID_PARAMETER</b> is returned.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Canvas Pointer to an <b>OH_Drawing_Canvas</b> object.
 * @param OH_Drawing_Brush Pointer to an <b>OH_Drawing_Brush</b> object.
 * @since 12
 * @version 1.0
 */
void OH_Drawing_CanvasDrawBackground(OH_Drawing_Canvas*, const OH_Drawing_Brush*);

/**
 * @brief Draws a region.
 *
 * Error codes may be generated in the call. You can view the error code by calling {@link OH_Drawing_ErrorCodeGet}.
 * If the operation is successful, <b>OH_DRAWING_SUCCESS</b> is returned.
 * If either <b>OH_Drawing_Canvas</b> or <b>OH_Drawing_Region</b> is NULL,
 * <b>OH_DRAWING_ERROR_INVALID_PARAMETER</b> is returned.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Canvas Pointer to an <b>OH_Drawing_Canvas</b> object.
 * @param OH_Drawing_Region Pointer to an <b>OH_Drawing_Region</b> object.
 * @since 12
 * @version 1.0
 */
void OH_Drawing_CanvasDrawRegion(OH_Drawing_Canvas*, const OH_Drawing_Region*);

/**
 * @brief Enumerates the modes of drawing multiple points.
 * The modes include discrete points, line segments, and open polygons.
 *
 * @since 12
 * @version 1.0
 */
typedef enum OH_Drawing_PointMode {
    /**
     * Draws each point separately.
     */
    POINT_MODE_POINTS,
    /**
     * Draws every two points as a line segment.
     */
    POINT_MODE_LINES,
     /**
     * Draws an array of points as an open polygon.
     */
    POINT_MODE_POLYGON,
} OH_Drawing_PointMode;

/**
 * @brief Draws a point.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param canvas Pointer to an {@link OH_Drawing_Canvas} object.
 * @param point Pointer to an {@link OH_Drawing_Point2D} object.
 * @return Returns either of the following result codes:
 * <b>OH_DRAWING_SUCCESS</b> if the operation is successful.
 * <b>OH_DRAWING_ERROR_INVALID_PARAMETER</b> if either <b>canvas</b> or <b>point</b> is NULL.
 * @since 12
 * @version 1.0
 */
OH_Drawing_ErrorCode OH_Drawing_CanvasDrawPoint(OH_Drawing_Canvas* canvas, const OH_Drawing_Point2D* point);

/**
 * @brief Draws multiple points. You can draw a single point, a line segment, or an open polygon.
 *
 * Error codes may be generated in the call. You can view the error code by calling {@link OH_Drawing_ErrorCodeGet}.
 * If the operation is successful, <b>OH_DRAWING_SUCCESS</b> is returned.
 * If either <b>OH_Drawing_Canvas</b> or <b>OH_Drawing_Point2D</b> is NULL or <b>count</b> is <b>0</b>,
 * <b>OH_DRAWING_ERROR_INVALID_PARAMETER</b> is returned.  If <b>mode</b> is not set to one of the enumerated values,
 * <b>OH_DRAWING_ERROR_PARAMETER_OUT_OF_RANGE</b> is returned.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Canvas Pointer to an <b>OH_Drawing_Canvas</b> object.
 * @param mode Mode of drawing multiple points.
 * For details about the available options, see {@link OH_Drawing_PointMode}.
 * @param count Number of vertices, that is, the number of vertices in the vertex array.
 * @param OH_Drawing_Point2D Pointer to an array holding the vertices.
 * @since 12
 * @version 1.0
 */
void OH_Drawing_CanvasDrawPoints(OH_Drawing_Canvas*, OH_Drawing_PointMode mode,
    uint32_t count, const OH_Drawing_Point2D*);

/**
 * @brief Draws a bitmap. A bitmap, also referred to as a dot matrix image, a pixel map image, or a grid image,
 * includes single points called pixels (image elements).
 *
 * Error codes may be generated in the call. You can view the error code by calling {@link OH_Drawing_ErrorCodeGet}.
 * If the operation is successful, <b>OH_DRAWING_SUCCESS</b> is returned.
 * If either <b>OH_Drawing_Canvas</b> or <b>OH_Drawing_Bitmap</b> is NULL,
 * <b>OH_DRAWING_ERROR_INVALID_PARAMETER</b> is returned.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Canvas Pointer to an <b>OH_Drawing_Canvas</b> object.
 * @param OH_Drawing_Bitmap Pointer to an <b>OH_Drawing_Bitmap</b> object.
 * @param left X coordinate of the upper left corner of the bitmap.
 * @param top Y coordinate of the upper left corner of the bitmap.
 * @since 11
 * @version 1.0
 */
void OH_Drawing_CanvasDrawBitmap(OH_Drawing_Canvas*, const OH_Drawing_Bitmap*, float left, float top);

/**
 * @brief Draws a portion of a bitmap onto a specified area of the canvas.
 *
 * Error codes may be generated in the call. You can view the error code by calling {@link OH_Drawing_ErrorCodeGet}.
 * If the operation is successful, <b>OH_DRAWING_SUCCESS</b> is returned.
 * If any of <b>OH_Drawing_Canvas</b>, <b>OH_Drawing_Bitmap</b>, and <b>dst</b> is NULL,
 * <b>OH_DRAWING_ERROR_INVALID_PARAMETER</b> is returned.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Canvas Pointer to an {@link OH_Drawing_Canvas} object.
 * @param OH_Drawing_Bitmap Pointer to an {@link OH_Drawing_Bitmap} object.
 * @param src Pointer to a rectangle on the bitmap. If NULL is passed in, it refers to the entire bitmap.
 * @param dst Pointer to a rectangle on the canvas.
 * @param OH_Drawing_SamplingOptions Pointer to an {@link OH_Drawing_SamplingOptions} object.
 * If NULL is passed in, the default sampling options are used.
 * @since 12
 * @version 1.0
 */
void OH_Drawing_CanvasDrawBitmapRect(OH_Drawing_Canvas*, const OH_Drawing_Bitmap*, const OH_Drawing_Rect* src,
    const OH_Drawing_Rect* dst, const OH_Drawing_SamplingOptions*);

/**
 * @brief Sets the matrix status for a canvas.
 *
 * Error codes may be generated in the call. You can view the error code by calling {@link OH_Drawing_ErrorCodeGet}.
 * If the operation is successful, <b>OH_DRAWING_SUCCESS</b> is returned.
 * If either <b>OH_Drawing_Canvas</b> or <b>OH_Drawing_Matrix</b> is NULL,
 * <b>OH_DRAWING_ERROR_INVALID_PARAMETER</b> is returned.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Canvas Pointer to an {@link OH_Drawing_Canvas} object.
 * @param OH_Drawing_Matrix Pointer to an {@link OH_Drawing_Matrix} object,
 * which is obtained by calling {@link OH_Drawing_MatrixCreate}.
 * @since 12
 * @version 1.0
 */
void OH_Drawing_CanvasSetMatrix(OH_Drawing_Canvas*, OH_Drawing_Matrix*);

/**
 * @brief Resets the matrix of a canvas to an identity matrix.
 *
 * Error codes may be generated in the call. You can view the error code by calling {@link OH_Drawing_ErrorCodeGet}.
 * If the operation is successful, <b>OH_DRAWING_SUCCESS</b> is returned.
 * If <b>OH_Drawing_Canvas</b> is NULL, <b>OH_DRAWING_ERROR_INVALID_PARAMETER</b> is returned.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Canvas Pointer to an {@link OH_Drawing_Canvas} object.
 * @since 12
 * @version 1.0
 */
void OH_Drawing_CanvasResetMatrix(OH_Drawing_Canvas*);

/**
 * @brief Draws a portion of an image onto a specified area of the canvas.
 * The area selected by the source rectangle is scaled and translated to the destination rectangle.
 *
 * Error codes may be generated in the call. You can view the error code by calling {@link OH_Drawing_ErrorCodeGet}.
 * If the operation is successful, <b>OH_DRAWING_SUCCESS</b> is returned.
 * If any of <b>OH_Drawing_Canvas</b>, <b>OH_Drawing_Image</b>, <b>src</b>, and <b>dst</b> is NULL,
 * <b>OH_DRAWING_ERROR_INVALID_PARAMETER</b> is returned.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Canvas Pointer to an {@link OH_Drawing_Canvas} object.
 * @param OH_Drawing_Image Pointer to an {@link OH_Drawing_Image} object.
 * @param src Pointer to a source rectangle, which is an {@link OH_Drawing_Rect} object.
 * @param dst Pointer to a destination rectangle, which is an {@link OH_Drawing_Rect} object.
 * @param OH_Drawing_SamplingOptions Pointer to an {@link OH_Drawing_SamplingOptions} object.
 * If NULL is passed in, the default sampling options are used.
 * @param OH_Drawing_SrcRectConstraint Constraint type.
 * For details about the available options, see {@link OH_Drawing_SrcRectConstraint}.
 * @since 12
 * @version 1.0
 */
void OH_Drawing_CanvasDrawImageRectWithSrc(OH_Drawing_Canvas*, const OH_Drawing_Image*,
    const OH_Drawing_Rect* src, const OH_Drawing_Rect* dst, const OH_Drawing_SamplingOptions*,
    OH_Drawing_SrcRectConstraint);

/**
 * @brief Draws an image onto a specified area of the canvas.
 *
 * Error codes may be generated in the call. You can view the error code by calling {@link OH_Drawing_ErrorCodeGet}.
 * If the operation is successful, <b>OH_DRAWING_SUCCESS</b> is returned.
 * If any of <b>OH_Drawing_Canvas</b>, <b>OH_Drawing_Image</b>, and <b>dst</b> is NULL,
 * <b>OH_DRAWING_ERROR_INVALID_PARAMETER</b> is returned.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Canvas Pointer to an {@link OH_Drawing_Canvas} object.
 * @param OH_Drawing_Image Pointer to an {@link OH_Drawing_Image} object.
 * @param dst Pointer to an {@link OH_Drawing_Rect} object.
 * @param OH_Drawing_SamplingOptions Pointer to an {@link OH_Drawing_SamplingOptions} object.
 * If NULL is passed in, the default sampling options are used.
 * @since 12
 * @version 1.0
 */
void OH_Drawing_CanvasDrawImageRect(OH_Drawing_Canvas*, OH_Drawing_Image*,
    OH_Drawing_Rect* dst, OH_Drawing_SamplingOptions*);

/**
 * @brief Enumerates the modes of interpreting the geometry of a given vertex.
 *
 * @since 12
 * @version 1.0
 */
typedef enum OH_Drawing_VertexMode {
    /**
     * Draws a triangle list. Specifically, a list of isolated triangles are drawn using every three vertices.
     * If the number of vertices is not a multiple of 3, the extra vertices will be ignored.
     */
    VERTEX_MODE_TRIANGLES,
    /**
     * Draws a triangle strip. Specifically, the first triangle is drawn between the first 3 vertices,
     * and all subsequent triangles use the previous 2 vertices plus the next additional vertex.
     */
    VERTEX_MODE_TRIANGLESSTRIP,
    /**
     * Draws a triangle fan. A triangle fan is similar to a triangle strip, except that all the triangles share
     * one vertex (the first vertex).
     */
    VERTEX_MODE_TRIANGLEFAN,
} OH_Drawing_VertexMode;

/**
 * @brief Draws a triangular grid described by a vertex array.
 *
 * Error codes may be generated in the call. You can view the error code by calling {@link OH_Drawing_ErrorCodeGet}.
 * If the operation is successful, <b>OH_DRAWING_SUCCESS</b> is returned.
 * If either <b>OH_Drawing_Canvas</b> or <b>positions</b> is null, <b>vertexCount</b> is less than 3,
 * or <b>indexCount</b> is less than 3 but not 0, <b>OH_DRAWING_ERROR_INVALID_PARAMETER</b> is returned.
 * If either <b>vertexMmode</b> or <b>mode</b> is not set to one of the enumerated values,
 * <b>OH_DRAWING_ERROR_PARAMETER_OUT_OF_RANGE</b> is returned.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Canvas Pointer to an <b>OH_Drawing_Canvas</b> object.
 * @param vertexMmode Vertex drawing mode. For details about the available options, see {@link OH_Drawing_VertexMode}.
 * @param vertexCount Number of elements in the vertex array. The value must be greater than or equal to 3.
 * @param positions Pointer to the array that holds the position of every vertex. The array cannot be null and
 * its length must be equal to the value of <b>vertexCount</b>.
 * @param texs Pointer to the array that holds the texture space coordinate corresponding to each vertex.
 * The array can be null. If the array is not null, its length must be equal to the value of <b>vertexCount</b>.
 * @param colors Pointer to the array that holds the color corresponding to each vertex.
 * It is used for interpolation in a triangle. The array can be null. If the array is not null,
 * its length must be equal to the value of <b>vertexCount</b>.
 * @param indexCount Number of indexes. The value can be 0 or a value greater than or equal to 3.
 * @param indices Pointer to the array that holds the index of each vertex. The array can be null.
 * If the array is not null, its length must be equal to the value of <b>indexCount</b>.
 * @param mode Blend mode. For details about the available options, see {@link OH_Drawing_BlendMode}.
 * @since 12
 * @version 1.0
 */
void OH_Drawing_CanvasDrawVertices(OH_Drawing_Canvas*, OH_Drawing_VertexMode vertexMmode,
    int32_t vertexCount, const OH_Drawing_Point2D* positions, const OH_Drawing_Point2D* texs,
    const uint32_t* colors, int32_t indexCount, const uint16_t* indices, OH_Drawing_BlendMode mode);

/**
 * @brief Copies pixel data from a canvas to a specified address. This function cannot be used for recorded canvases.
 *
 * Error codes may be generated in the call. You can view the error code by calling {@link OH_Drawing_ErrorCodeGet}.
 * If the operation is successful, <b>OH_DRAWING_SUCCESS</b> is returned.
 * If any of <b>OH_Drawing_Canvas</b>, <b>OH_Drawing_Image_Info</b>, and <b>dstPixels</b> is NULL,
 * <b>OH_DRAWING_ERROR_INVALID_PARAMETER</b> is returned.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Canvas Pointer to an {@link OH_Drawing_Canvas} object.
 * @param OH_Drawing_Image_Info Pointer to an {@link OH_Drawing_Image_Info} object.
 * @param dstPixels Pointer to the start address for storing the pixel data.
 * @param dstRowBytes Size of pixels per row.
 * @param srcX X-axis offset of the pixels on the canvas, in px.
 * @param srcY Y-axis offset of the pixels on the canvas, in px.
 * @return Returns <b>true</b> if the pixel data is copied to the start address of the storage;
 * returns <b>false</b> otherwise.
 * @since 12
 * @version 1.0
 */
bool OH_Drawing_CanvasReadPixels(OH_Drawing_Canvas*, OH_Drawing_Image_Info*,
    void* dstPixels, uint32_t dstRowBytes, int32_t srcX, int32_t srcY);

/**
 * @brief Copies pixel data from a canvas to an image. This function cannot be used for recorded canvases.
 *
 * Error codes may be generated in the call. You can view the error code by calling {@link OH_Drawing_ErrorCodeGet}.
 * If the operation is successful, <b>OH_DRAWING_SUCCESS</b> is returned.
 * If either <b>OH_Drawing_Canvas</b> or <b>OH_Drawing_Bitmap</b> is NULL,
 * <b>OH_DRAWING_ERROR_INVALID_PARAMETER</b> is returned.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Canvas Pointer to an {@link OH_Drawing_Canvas} object.
 * @param OH_Drawing_Bitmap Pointer to an {@link OH_Drawing_Bitmap} object.
 * @param srcX X-axis offset of the pixels on the canvas, in px.
 * @param srcY Y-axis offset of the pixels on the canvas, in px.
 * @return Returns <b>true</b> if the pixel data is copied to the image; returns <b>false</b> otherwise.
 * @since 12
 * @version 1.0
 */
bool OH_Drawing_CanvasReadPixelsToBitmap(OH_Drawing_Canvas*, OH_Drawing_Bitmap*, int32_t srcX, int32_t srcY);

/**
 * @brief Checks whether the region that can be drawn is empty after cropping.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param canvas Pointer to an {@link OH_Drawing_Canvas} object.
 * @param isClipEmpty Pointer to the variable that specifies whether the region is empty.
 * The value <b>true</b> means that the region is empty, and <b>false</b> means the opposite.
 * @return Returns either of the following result codes:
 * <b>OH_DRAWING_SUCCESS</b> if the operation is successful.
 * <b>OH_DRAWING_ERROR_INVALID_PARAMETER</b> if either <b>canvas</b> or <b>isClipEmpty</b> is NULL.
 * @since 12
 * @version 1.0
 */
OH_Drawing_ErrorCode OH_Drawing_CanvasIsClipEmpty(OH_Drawing_Canvas* canvas, bool* isClipEmpty);

/**
 * @brief Obtains the image information of a canvas.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param canvas Pointer to an {@link OH_Drawing_Canvas} object.
 * @param imageInfo Pointer to an {@link OH_Drawing_Image_Info} object.
 * @return Returns either of the following result codes:
 * <b>OH_DRAWING_SUCCESS</b> if the operation is successful.
 * <b>OH_DRAWING_ERROR_INVALID_PARAMETER</b> if either <b>canvas</b> or <b>imageInfo</b> is NULL.
 * @since 12
 * @version 1.0
 */
OH_Drawing_ErrorCode OH_Drawing_CanvasGetImageInfo(OH_Drawing_Canvas* canvas, OH_Drawing_Image_Info* imageInfo);

/**
 * @brief Draws a rectangle.
 *
 * Error codes may be generated in the call. You can view the error code by calling {@link OH_Drawing_ErrorCodeGet}.
 * If the operation is successful, <b>OH_DRAWING_SUCCESS</b> is returned.
 * If either <b>OH_Drawing_Canvas</b> or <b>OH_Drawing_Rect</b> is NULL,
 * <b>OH_DRAWING_ERROR_INVALID_PARAMETER</b> is returned.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Canvas Pointer to an <b>OH_Drawing_Canvas</b> object.
 * @param OH_Drawing_Rect Pointer to an <b>OH_Drawing_Rect</b> object.
 * @since 11
 * @version 1.0
 */
void OH_Drawing_CanvasDrawRect(OH_Drawing_Canvas*, const OH_Drawing_Rect*);

/**
 * @brief Draws a circle.
 *
 * Error codes may be generated in the call. You can view the error code by calling {@link OH_Drawing_ErrorCodeGet}.
 * If the operation is successful, <b>OH_DRAWING_SUCCESS</b> is returned.
 * If either <b>OH_Drawing_Canvas</b> or <b>OH_Drawing_Point</b> is NULL,
 * <b>OH_DRAWING_ERROR_INVALID_PARAMETER</b> is returned.
 * If <b>radius</b> is less than or equal to 0, <b>OH_DRAWING_ERROR_PARAMETER_OUT_OF_RANGE</b> is returned.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Canvas Pointer to an <b>OH_Drawing_Canvas</b> object.
 * @param OH_Drawing_Point Pointer to an <b>OH_Drawing_Point</b> object, which indicates the center of the circle.
 * @param radius Radius of the circle.
 * @since 11
 * @version 1.0
 */
void OH_Drawing_CanvasDrawCircle(OH_Drawing_Canvas*, const OH_Drawing_Point*, float radius);

/**
 * @brief Fills the entire canvas with the specified color and blend mode.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param canvas Pointer to an {@link OH_Drawing_Canvas} object.
 * @param color Color.
 * @param blendMode Blend mode.
 * @return Returns either of the following result codes:
 * <b>OH_DRAWING_SUCCESS</b> if the operation is successful.
 * <b>OH_DRAWING_ERROR_INVALID_PARAMETER</b> if <b>canvas</b> is NULL.
 * <b>OH_DRAWING_ERROR_PARAMETER_OUT_OF_RANGE</b> if <b>blendMode</b> is not set to one of the enumerated values.
 * @since 12
 * @version 1.0
 */
OH_Drawing_ErrorCode OH_Drawing_CanvasDrawColor(OH_Drawing_Canvas* canvas, uint32_t color,
    OH_Drawing_BlendMode blendMode);

/**
 * @brief Draws an oval.
 *
 * Error codes may be generated in the call. You can view the error code by calling {@link OH_Drawing_ErrorCodeGet}.
 * If the operation is successful, <b>OH_DRAWING_SUCCESS</b> is returned.
 * If either <b>OH_Drawing_Canvas</b> or <b>OH_Drawing_Rect</b> is NULL,
 * <b>OH_DRAWING_ERROR_INVALID_PARAMETER</b> is returned.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Canvas Pointer to an <b>OH_Drawing_Canvas</b> object.
 * @param OH_Drawing_Rect Pointer to an <b>OH_Drawing_Rect</b> object.
 * @since 11
 * @version 1.0
 */
void OH_Drawing_CanvasDrawOval(OH_Drawing_Canvas*, const OH_Drawing_Rect*);

/**
 * @brief Draws an arc.
 *
 * Error codes may be generated in the call. You can view the error code by calling {@link OH_Drawing_ErrorCodeGet}.
 * If the operation is successful, <b>OH_DRAWING_SUCCESS</b> is returned.
 * If either <b>OH_Drawing_Canvas</b> or <b>OH_Drawing_Rect</b> is NULL,
 * <b>OH_DRAWING_ERROR_INVALID_PARAMETER</b> is returned.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Canvas Pointer to an <b>OH_Drawing_Canvas</b> object.
 * @param OH_Drawing_Rect Pointer to an <b>OH_Drawing_Rect</b> object.
 * @param startAngle Start angle of the arc.
 * @param sweepAngle Sweep angle of the arc.
 * @since 11
 * @version 1.0
 */
void OH_Drawing_CanvasDrawArc(OH_Drawing_Canvas*, const OH_Drawing_Rect*, float startAngle, float sweepAngle);

/**
 * @brief Draws a rounded rectangle.
 *
 * Error codes may be generated in the call. You can view the error code by calling {@link OH_Drawing_ErrorCodeGet}.
 * If the operation is successful, <b>OH_DRAWING_SUCCESS</b> is returned.
 * If either <b>OH_Drawing_Canvas</b> or <b>OH_Drawing_RoundRect</b> is NULL,
 * <b>OH_DRAWING_ERROR_INVALID_PARAMETER</b> is returned.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Canvas Pointer to an <b>OH_Drawing_Canvas</b> object.
 * @param OH_Drawing_RoundRect Pointer to an <b>OH_Drawing_RoundRect</b> object.
 * @since 11
 * @version 1.0
 */
void OH_Drawing_CanvasDrawRoundRect(OH_Drawing_Canvas*, const OH_Drawing_RoundRect*);

/**
 * @brief Draws a single character.
 * If the typeface of the current font does not support the character to draw, the system typeface is used to
 * draw the character.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param canvas Pointer to an {@link OH_Drawing_Canvas} object.
 * @param str Pointer to the single character to draw. A string can be passed in, but only the first character
 * in the string is parsed and drawn in UTF-8 encoding.
 * @param font Pointer to an {@link OH_Drawing_Font} object.
 * @param x X coordinate of the left point of the character baseline.
 * @param y Y coordinate of the left point of the character baseline.
 * @return Returns either of the following result codes:
 * <b>OH_DRAWING_SUCCESS</b> if the operation is successful.
 * <b>OH_DRAWING_ERROR_INVALID_PARAMETER</b> if at least one of the parameters <b>canvas</b>, <b>str</b>,
 * or <b>font</b> is NULL, or the length of <b>str</b> is <b>0</b>.
 * @since 12
 * @version 1.0
 */
OH_Drawing_ErrorCode OH_Drawing_CanvasDrawSingleCharacter(OH_Drawing_Canvas* canvas, const char* str,
    const OH_Drawing_Font* font, float x, float y);

/**
 * @brief Draws a text blob.
 * If the typeface used to construct <b>OH_Drawing_TextBlob</b> does not support a character,
 * that character will not be drawn.
 *
 * Error codes may be generated in the call. You can view the error code by calling {@link OH_Drawing_ErrorCodeGet}.
 * If the operation is successful, <b>OH_DRAWING_SUCCESS</b> is returned.
 * If either <b>OH_Drawing_Canvas</b> or <b>OH_Drawing_TextBlob</b> is NULL,
 * <b>OH_DRAWING_ERROR_INVALID_PARAMETER</b> is returned.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Canvas Pointer to an <b>OH_Drawing_Canvas</b> object.
 * @param OH_Drawing_TextBlob Pointer to an <b>OH_Drawing_TextBlob</b> object.
 * @param x X coordinate of the left point of the text baseline.
 * @param y Y coordinate of the left point of the text baseline.
 * @since 11
 * @version 1.0
 */
void OH_Drawing_CanvasDrawTextBlob(OH_Drawing_Canvas*, const OH_Drawing_TextBlob*, float x, float y);

/**
 * @brief Enumerates the canvas clipping modes.
 *
 * @since 11
 * @version 1.0
 */
typedef enum OH_Drawing_CanvasClipOp {
    /**
     * Clips a specified area. That is, the difference set is obtained.
     */
    DIFFERENCE,
    /**
     * Retains a specified area. That is, the intersection is obtained.
     */
    INTERSECT,
} OH_Drawing_CanvasClipOp;

/**
 * @brief Clips a rectangle.
 *
 * Error codes may be generated in the call. You can view the error code by calling {@link OH_Drawing_ErrorCodeGet}.
 * If the operation is successful, <b>OH_DRAWING_SUCCESS</b> is returned.
 * If either <b>OH_Drawing_Canvas</b> or <b>OH_Drawing_Rect</b> is NULL,
 * <b>OH_DRAWING_ERROR_INVALID_PARAMETER</b> is returned. If <b>clipOp</b> is not set to one of the enumerated values,
 * <b>OH_DRAWING_ERROR_PARAMETER_OUT_OF_RANGE</b> is returned.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Canvas Pointer to an <b>OH_Drawing_Canvas</b> object.
 * @param OH_Drawing_Rect Pointer to an <b>OH_Drawing_Rect</b> object.
 * @param clipOp Clip mode. For details about the available options, see @{link OH_Drawing_CanvasClipOp}.
 * @param doAntiAlias Whether to enable anti-aliasing. The value <b>true</b> means to enable anti-aliasing,
 * and <b>false</b> means the opposite.
 * @since 11
 * @version 1.0
 */
void OH_Drawing_CanvasClipRect(OH_Drawing_Canvas*, const OH_Drawing_Rect*,
    OH_Drawing_CanvasClipOp clipOp, bool doAntiAlias);

/**
 * @brief Clips a rounded rectangle.
 *
 * Error codes may be generated in the call. You can view the error code by calling {@link OH_Drawing_ErrorCodeGet}.
 * If the operation is successful, <b>OH_DRAWING_SUCCESS</b> is returned.
 * If either <b>OH_Drawing_Canvas</b> or <b>OH_Drawing_RoundRect</b> is NULL,
 * <b>OH_DRAWING_ERROR_INVALID_PARAMETER</b> is returned. If <b>clipOp</b> is not set to one of the enumerated values,
 * <b>OH_DRAWING_ERROR_PARAMETER_OUT_OF_RANGE</b> is returned.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Canvas Pointer to an <b>OH_Drawing_Canvas</b> object.
 * @param OH_Drawing_RoundRect Pointer to an <b>OH_Drawing_RoundRect</b> object.
 * @param clipOp Clip mode. For details about the available options, see @{link OH_Drawing_CanvasClipOp}.
 * @param doAntiAlias Whether to perform anti-aliasing. The value <b>true</b> means to perform anti-aliasing,
 * and <b>false</b> means the opposite.
 * @since 12
 * @version 1.0
 */
void OH_Drawing_CanvasClipRoundRect(OH_Drawing_Canvas*, const OH_Drawing_RoundRect*,
    OH_Drawing_CanvasClipOp clipOp, bool doAntiAlias);

/**
 * @brief Clips a path.
 *
 * Error codes may be generated in the call. You can view the error code by calling {@link OH_Drawing_ErrorCodeGet}.
 * If the operation is successful, <b>OH_DRAWING_SUCCESS</b> is returned.
 * If either <b>OH_Drawing_Canvas</b> or <b>OH_Drawing_Path</b> is NULL,
 * <b>OH_DRAWING_ERROR_INVALID_PARAMETER</b> is returned. If <b>clipOp</b> is not set to one of the enumerated values,
 * <b>OH_DRAWING_ERROR_PARAMETER_OUT_OF_RANGE</b> is returned.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Canvas Pointer to an <b>OH_Drawing_Canvas</b> object.
 * @param OH_Drawing_Path Pointer to an <b>OH_Drawing_Path</b> object.
 * @param clipOp Clip mode. For details about the available options, see @{link OH_Drawing_CanvasClipOp}.
 * @param doAntiAlias Whether to enable anti-aliasing. The value <b>true</b> means to enable anti-aliasing,
 * and <b>false</b> means the opposite.
 * @since 11
 * @version 1.0
 */
void OH_Drawing_CanvasClipPath(OH_Drawing_Canvas*, const OH_Drawing_Path*,
    OH_Drawing_CanvasClipOp clipOp, bool doAntiAlias);

/**
 * @brief Clips a rectangle.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param canvas Pointer to an {@link OH_Drawing_Canvas} object.
 * @param region Pointer to an {@link OH_Drawing_Region} object.
 * @param clipOp Clipping mode.
 * @return Returns either of the following result codes:
 * <b>OH_DRAWING_SUCCESS</b> if the operation is successful.
 * <b>OH_DRAWING_ERROR_INVALID_PARAMETER</b> if either <b>canvas</b> or <b>region</b> is NULL.
 * <b>OH_DRAWING_ERROR_PARAMETER_OUT_OF_RANGE</b> if <b>clipOp</b> is not set to one of the enumerated values.
 * @since 12
 * @version 1.0
 */
OH_Drawing_ErrorCode OH_Drawing_CanvasClipRegion(OH_Drawing_Canvas* canvas, const OH_Drawing_Region* region,
    OH_Drawing_CanvasClipOp clipOp);

/**
 * @brief Rotates a canvas by a given angle. A positive number indicates a clockwise rotation,
 * and a negative number indicates a counterclockwise rotation.
 *
 * Error codes may be generated in the call. You can view the error code by calling {@link OH_Drawing_ErrorCodeGet}.
 * If the operation is successful, <b>OH_DRAWING_SUCCESS</b> is returned.
 * If <b>OH_Drawing_Canvas</b> is NULL, <b>OH_DRAWING_ERROR_INVALID_PARAMETER</b> is returned.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Canvas Pointer to an <b>OH_Drawing_Canvas</b> object.
 * @param degrees Angle to rotate.
 * @param px X coordinate of the rotation point.
 * @param py Y coordinate of the rotation point.
 * @since 11
 * @version 1.0
 */
void OH_Drawing_CanvasRotate(OH_Drawing_Canvas*, float degrees, float px, float py);

/**
 * @brief Translates a canvas by a given distance.
 *
 * Error codes may be generated in the call. You can view the error code by calling {@link OH_Drawing_ErrorCodeGet}.
 * If the operation is successful, <b>OH_DRAWING_SUCCESS</b> is returned.
 * If <b>OH_Drawing_Canvas</b> is NULL, <b>OH_DRAWING_ERROR_INVALID_PARAMETER</b> is returned.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Canvas Pointer to an <b>OH_Drawing_Canvas</b> object.
 * @param dx Distance to translate on the X axis.
 * @param dy Distance to translate on the Y axis.
 * @since 11
 * @version 1.0
 */
void OH_Drawing_CanvasTranslate(OH_Drawing_Canvas*, float dx, float dy);

/**
 * @brief Scales a canvas.
 *
 * Error codes may be generated in the call. You can view the error code by calling {@link OH_Drawing_ErrorCodeGet}.
 * If the operation is successful, <b>OH_DRAWING_SUCCESS</b> is returned.
 * If <b>OH_Drawing_Canvas</b> is NULL, <b>OH_DRAWING_ERROR_INVALID_PARAMETER</b> is returned.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Canvas Pointer to an <b>OH_Drawing_Canvas</b> object.
 * @param sx Scale factor on the X axis.
 * @param sy Scale factor on the Y axis.
 * @since 11
 * @version 1.0
 */
void OH_Drawing_CanvasScale(OH_Drawing_Canvas*, float sx, float sy);

/**
 * @brief Skews a canvas.
 * This API premultiplies the current canvas matrix by a skew transformation matrix and applies the resulting matrix to
 * the canvas. The skew transformation matrix is as follows:
 * |1 sx 0|
 * |sy 1 0|
 * | 0 0 1 |
 *
 * Error codes may be generated in the call. You can view the error code by calling {@link OH_Drawing_ErrorCodeGet}.
 * If the operation is successful, <b>OH_DRAWING_SUCCESS</b> is returned.
 * If <b>OH_Drawing_Canvas</b> is NULL, <b>OH_DRAWING_ERROR_INVALID_PARAMETER</b> is returned.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Canvas Pointer to an <b>OH_Drawing_Canvas</b> object.
 * @param sx Amount of tilt on the X axis.
 * A positive number tilts the drawing rightwards along the positive direction of the Y axis,
 * and a negative number tilts the drawing leftwards along the positive direction of the Y axis.
 * @param sy Amount of tilt on the Y axis.
 * A positive number tilts the drawing downwards along the positive direction of the X axis,
 * and a negative number tilts the drawing upwards along the positive direction of the X axis.
 * @since 12
 * @version 1.0
 */
void OH_Drawing_CanvasSkew(OH_Drawing_Canvas*, float sx, float sy);

/**
 * @brief Clears a canvas by using a given color.
 *
 * Error codes may be generated in the call. You can view the error code by calling {@link OH_Drawing_ErrorCodeGet}.
 * If the operation is successful, <b>OH_DRAWING_SUCCESS</b> is returned.
 * If <b>OH_Drawing_Canvas</b> is NULL, <b>OH_DRAWING_ERROR_INVALID_PARAMETER</b> is returned.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Canvas Pointer to an <b>OH_Drawing_Canvas</b> object.
 * @param color Color, which is a 32-bit (ARGB) variable.
 * @since 8
 * @version 1.0
 */
void OH_Drawing_CanvasClear(OH_Drawing_Canvas*, uint32_t color);

/**
 * @brief Obtains the canvas width.
 *
 * Error codes may be generated in the call. You can view the error code by calling {@link OH_Drawing_ErrorCodeGet}.
 * If the operation is successful, <b>OH_DRAWING_SUCCESS</b> is returned.
 * If <b>OH_Drawing_Canvas</b> is NULL, <b>OH_DRAWING_ERROR_INVALID_PARAMETER</b> is returned.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Canvas Pointer to an {@link OH_Drawing_Canvas} object.
 * @return Returns the canvas width, in px.
 * @since 12
 * @version 1.0
 */
int32_t OH_Drawing_CanvasGetWidth(OH_Drawing_Canvas*);

/**
 * @brief Obtains the canvas width.
 *
 * Error codes may be generated in the call. You can view the error code by calling {@link OH_Drawing_ErrorCodeGet}.
 * If the operation is successful, <b>OH_DRAWING_SUCCESS</b> is returned.
 * If <b>OH_Drawing_Canvas</b> is NULL, <b>OH_DRAWING_ERROR_INVALID_PARAMETER</b> is returned.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Canvas Pointer to an {@link OH_Drawing_Canvas} object.
 * @return Returns the canvas height, in px.
 * @since 12
 * @version 1.0
 */
int32_t OH_Drawing_CanvasGetHeight(OH_Drawing_Canvas*);

/**
 * @brief Obtains the bounds of the cropping region of a canvas. This function cannot be used for recorded canvases.
 *
 * Error codes may be generated in the call. You can view the error code by calling {@link OH_Drawing_ErrorCodeGet}.
 * If the operation is successful, <b>OH_DRAWING_SUCCESS</b> is returned.
 * If either <b>OH_Drawing_Canvas</b> or <b>OH_Drawing_Rect</b> is NULL,
 * <b>OH_DRAWING_ERROR_INVALID_PARAMETER</b> is returned.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Canvas Pointer to an {@link OH_Drawing_Canvas} object.
 * @param OH_Drawing_Rect Pointer to an {@link OH_Drawing_Rect} object,
 * which is obtained by calling {@link OH_Drawing_RectCreate}.
 * @since 12
 * @version 1.0
 */
void OH_Drawing_CanvasGetLocalClipBounds(OH_Drawing_Canvas*, OH_Drawing_Rect*);

/**
 * @brief Obtains the 3x3 matrix of a canvas.
 *
 * Error codes may be generated in the call. You can view the error code by calling {@link OH_Drawing_ErrorCodeGet}.
 * If the operation is successful, <b>OH_DRAWING_SUCCESS</b> is returned.
 * If either <b>OH_Drawing_Canvas</b> or <b>OH_Drawing_Matrix</b> is NULL,
 * <b>OH_DRAWING_ERROR_INVALID_PARAMETER</b> is returned.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Canvas Pointer to an {@link OH_Drawing_Canvas} object.
 * @param OH_Drawing_Matrix Pointer to an {@link OH_Drawing_Matrix} object,
 * which is obtained by calling {@link OH_Drawing_MatrixCreate}.
 * @since 12
 * @version 1.0
 */
void OH_Drawing_CanvasGetTotalMatrix(OH_Drawing_Canvas*, OH_Drawing_Matrix*);

/**
 * @brief Preconcats the existing matrix with the passed-in matrix.
 * The drawing operation triggered before this function is called is not affected.
 *
 * Error codes may be generated in the call. You can view the error code by calling {@link OH_Drawing_ErrorCodeGet}.
 * If the operation is successful, <b>OH_DRAWING_SUCCESS</b> is returned.
 * If either <b>OH_Drawing_Canvas</b> or <b>OH_Drawing_Matrix</b> is NULL,
 * <b>OH_DRAWING_ERROR_INVALID_PARAMETER</b> is returned.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Canvas Pointer to an <b>{@link OH_Drawing_Canvas}</b> object.
 * @param OH_Drawing_Matrix Pointer to an {@link OH_Drawing_Matrix} object.
 * @since 12
 * @version 1.0
 */
void OH_Drawing_CanvasConcatMatrix(OH_Drawing_Canvas*, OH_Drawing_Matrix*);

/**
 * @brief Enumerates the canvas shadow flags.
 *
 * @since 12
 * @version 1.0
 */
typedef enum OH_Drawing_CanvasShadowFlags {
    /**
     * There is no shadow flag.
     */
    SHADOW_FLAGS_NONE,
    /**
     * The occluding object is transparent.
     */
    SHADOW_FLAGS_TRANSPARENT_OCCLUDER,
    /**
     * No analysis on the shadows is required.
     */
    SHADOW_FLAGS_GEOMETRIC_ONLY,
    /**
     * All the preceding shadow flags are used.
     */
    SHADOW_FLAGS_ALL,
} OH_Drawing_CanvasShadowFlags;

/**
 * @brief Draws an offset spot shadow and uses a given path to outline the ambient shadow.
 *
 * Error codes may be generated in the call. You can view the error code by calling {@link OH_Drawing_ErrorCodeGet}.
 * If the operation is successful, <b>OH_DRAWING_SUCCESS</b> is returned.
 * If either <b>OH_Drawing_Canvas</b> or <b>OH_Drawing_Path</b> is NULL,
 * <b>OH_DRAWING_ERROR_INVALID_PARAMETER</b> is returned. If <b>flag</b> is not set to one of the enumerated values,
 * <b>OH_DRAWING_ERROR_PARAMETER_OUT_OF_RANGE</b> is returned.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Canvas Pointer to an {@link OH_Drawing_Canvas} object.
 * @param OH_Drawing_Path Pointer to an {@link OH_Drawing_Path} object, which is used to generate the shadow.
 * @param planeParams Value of the function that returns the Z-axis of the occluding object from the canvas based on
 * the x-axis and y-axis.
 * @param devLightPos Position of the light relative to the canvas.
 * @param lightRadius Radius of the light.
 * @param ambientColor Color of the ambient shadow.
 * @param spotColor Color of the spot shadow.
 * @param flag Shadow flag. For details about the available options, see {@link OH_Drawing_CanvasShadowFlags}.
 * @since 12
 * @version 1.0
 */
void OH_Drawing_CanvasDrawShadow(OH_Drawing_Canvas*, OH_Drawing_Path*, OH_Drawing_Point3D planeParams,
    OH_Drawing_Point3D devLightPos, float lightRadius, uint32_t ambientColor, uint32_t spotColor,
    OH_Drawing_CanvasShadowFlags flag);

/**
 * @brief Draws an {@link OH_Drawing_RecordCmd} object.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param canvas Pointer to an {@link OH_Drawing_Canvas} object. Only a canvas of the recording type is supported.
 * @param recordCmd Pointer to the {@link OH_Drawing_RecordCmd} object.
 * @return Returns either of the following result codes:
 * <b>OH_DRAWING_SUCCESS</b> if the operation is successful.
 * <b>OH_DRAWING_ERROR_INVALID_PARAMETER</b> if either <b>canvas</b> or <b>recordCmd</b> is NULL.
 * @since 13
 * @version 1.0
 */
OH_Drawing_ErrorCode OH_Drawing_CanvasDrawRecordCmd(OH_Drawing_Canvas* canvas, OH_Drawing_RecordCmd* recordCmd);

#ifdef __cplusplus
}
#endif
/** @} */
#endif
