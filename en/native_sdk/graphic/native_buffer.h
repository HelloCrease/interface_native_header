/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef NDK_INCLUDE_NATIVE_BUFFER_H_
#define NDK_INCLUDE_NATIVE_BUFFER_H_

/**
 * @addtogroup OH_NativeBuffer
 * @{
 *
 * @brief Provides the capabilities of <b>NativeBuffer</b>. Using the functions provided by this module,
 * you can apply for, use, and release the shared memory, and query its attributes.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeBuffer
 * @since 9
 * @version 1.0
 */

/**
 * @file native_buffer.h
 *
 * @brief Declares the functions for obtaining and using <b>NativeBuffer</b>.
 *
 * File to include: <native_buffer/native_buffer.h>
 * @library libnative_buffer.so
 * @syscap SystemCapability.Graphic.Graphic2D.NativeBuffer
 * @since 9
 * @version 1.0
 */

#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief Provides the declaration of an <b>OH_NativeBuffer</b> struct.
 * @since 9
 */
struct OH_NativeBuffer;

/**
 * @brief Provides the declaration of an <b>OH_NativeBuffer</b> struct.
 * @since 9
 */
typedef struct OH_NativeBuffer OH_NativeBuffer;

/**
 * @brief Enumerates the <b>OH_NativeBuffer</b> usages.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeBuffer
 * @since 10
 * @version 1.0
 */
typedef enum OH_NativeBuffer_Usage {
    /**
     * Read by the CPU.
     */
    NATIVEBUFFER_USAGE_CPU_READ = (1ULL << 0),
    /**
     * Write by the CPU.
     */
    NATIVEBUFFER_USAGE_CPU_WRITE = (1ULL << 1),
    /**
     * Direct memory access to the buffer.
     */
    NATIVEBUFFER_USAGE_MEM_DMA = (1ULL << 3),
    /**
     * GPU writable.
     * @since 12
     */
    NATIVEBUFFER_USAGE_HW_RENDER = (1ULL << 8),
    /**
     * GPU readable.
     * @since 12
     */
    NATIVEBUFFER_USAGE_HW_TEXTURE = (1ULL << 9),
    /**
     * Direct mapping of CPU.
     * @since 12
     */
    NATIVEBUFFER_USAGE_CPU_READ_OFTEN = (1ULL << 16),
    /**
     * 512-byte alignment.
     * @since 12
     */
    NATIVEBUFFER_USAGE_ALIGNMENT_512 = (1ULL << 18),
} OH_NativeBuffer_Usage;

/**
 * @brief Enumerates the <b>OH_NativeBuffer</b> formats.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeBuffer
 * @since 10
 * @version 1.0
 */
typedef enum OH_NativeBuffer_Format {
    /**
     * CLUT8.
     * @since 12
     */
    NATIVEBUFFER_PIXEL_FMT_CLUT8 = 0,
    /**
     * CLUT1.
     * @since 12
     */
    NATIVEBUFFER_PIXEL_FMT_CLUT1,
    /**
     * CLUT4.
     * @since 12
     */
    NATIVEBUFFER_PIXEL_FMT_CLUT4,
    /**
     * RGB565.
     */
    NATIVEBUFFER_PIXEL_FMT_RGB_565 = 3,
    /**
     * RGBA5658.
     */
    NATIVEBUFFER_PIXEL_FMT_RGBA_5658,
    /**
     * RGBX4444.
     */
    NATIVEBUFFER_PIXEL_FMT_RGBX_4444,
    /**
     * RGBA4444.
     */
    NATIVEBUFFER_PIXEL_FMT_RGBA_4444,
    /**
     * RGB444.
     */
    NATIVEBUFFER_PIXEL_FMT_RGB_444,
    /**
     * RGBX5551.
     */
    NATIVEBUFFER_PIXEL_FMT_RGBX_5551,
    /**
     * RGBA5551.
     */
    NATIVEBUFFER_PIXEL_FMT_RGBA_5551,
    /**
     * RGB555.
     */
    NATIVEBUFFER_PIXEL_FMT_RGB_555,
    /**
     * RGBX8888.
     */
    NATIVEBUFFER_PIXEL_FMT_RGBX_8888,
    /**
     * RGBA8888.
     */
    NATIVEBUFFER_PIXEL_FMT_RGBA_8888,
    /**
     * RGB888.
     */
    NATIVEBUFFER_PIXEL_FMT_RGB_888,
    /**
     * BGR565.
     */
    NATIVEBUFFER_PIXEL_FMT_BGR_565,
    /**
     * BGRX4444.
     */
    NATIVEBUFFER_PIXEL_FMT_BGRX_4444,
    /**
     * BGRA4444.
     */
    NATIVEBUFFER_PIXEL_FMT_BGRA_4444,
    /**
     * BGRX5551.
     */
    NATIVEBUFFER_PIXEL_FMT_BGRX_5551,
    /**
     * BGRA5551.
     */
    NATIVEBUFFER_PIXEL_FMT_BGRA_5551,
    /**
     * BGRX8888.
     */
    NATIVEBUFFER_PIXEL_FMT_BGRX_8888,
    /**
     * BGRA8888.
     */
    NATIVEBUFFER_PIXEL_FMT_BGRA_8888,
    /**
     * YUV422 interleaved.
     * @since 12
     */
    NATIVEBUFFER_PIXEL_FMT_YUV_422_I,
    /**
     * YCbCr422 semi-planar.
     * @since 12
     */
    NATIVEBUFFER_PIXEL_FMT_YCBCR_422_SP,
    /**
     * YCrCb422 semi-planar.
     * @since 12
     */
    NATIVEBUFFER_PIXEL_FMT_YCRCB_422_SP,
    /**
     * YCbCr420 semi-planar.
     * @since 12
     */
    NATIVEBUFFER_PIXEL_FMT_YCBCR_420_SP,
    /**
     * YCrCb420 semi-planar.
     * @since 12
     */
    NATIVEBUFFER_PIXEL_FMT_YCRCB_420_SP,
    /**
     * YCbCr422 planar.
     * @since 12
     */
    NATIVEBUFFER_PIXEL_FMT_YCBCR_422_P,
    /**
     * YCrCb422 planar.
     * @since 12
     */
    NATIVEBUFFER_PIXEL_FMT_YCRCB_422_P,
    /**
     * YCbCr420 planar.
     * @since 12
     */
    NATIVEBUFFER_PIXEL_FMT_YCBCR_420_P,
    /**
     * YCrCb420 planar.
     * @since 12
     */
    NATIVEBUFFER_PIXEL_FMT_YCRCB_420_P,
    /**
     * YUYV422 packed.
     * @since 12
     */
    NATIVEBUFFER_PIXEL_FMT_YUYV_422_PKG,
    /**
     * UYVY422 packed.
     * @since 12
     */
    NATIVEBUFFER_PIXEL_FMT_UYVY_422_PKG,
    /**
     * YVYU422 packed.
     * @since 12
     */
    NATIVEBUFFER_PIXEL_FMT_YVYU_422_PKG,
    /**
     * VYUY422 packed.
     * @since 12
     */
    NATIVEBUFFER_PIXEL_FMT_VYUY_422_PKG,
    /**
     * RGBA_1010102 packed.
     * @since 12
     */
    NATIVEBUFFER_PIXEL_FMT_RGBA_1010102,
    /**
     * YCBCR420 semi-planar 10-bit packed.
     * @since 12
     */
    NATIVEBUFFER_PIXEL_FMT_YCBCR_P010,
    /**
     * YCRCB420 semi-planar 10-bit packed.
     * @since 12
     */
    NATIVEBUFFER_PIXEL_FMT_YCRCB_P010,
    /**
     * Raw 10-bit packed.
     * @since 12
     */
    NATIVEBUFFER_PIXEL_FMT_RAW10,
    /**
     * Vender mask.
     * @since 12
     */
    NATIVEBUFFER_PIXEL_FMT_VENDER_MASK = 0X7FFF0000,
    /**
     * Invalid format.
     */
    NATIVEBUFFER_PIXEL_FMT_BUTT = 0X7FFFFFFF
} OH_NativeBuffer_Format;

/**
 * @brief Enumerates the color spaces of an <b>OH_NativeBuffer</b> instance.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeBuffer
 * @since 11
 * @version 1.0
 */
typedef enum OH_NativeBuffer_ColorSpace {
    /** No color space is available. */
    OH_COLORSPACE_NONE,
    /**
     * The color gamut is BT601_P, the transfer function is BT709, the conversion matrix is BT601_P,
     * and the data range is RANGE_FULL.
     */
    OH_COLORSPACE_BT601_EBU_FULL,
    /**
     * The color gamut is BT601_N, the transfer function is BT709, the conversion matrix is BT601_N,
     * and the data range is RANGE_FULL.
     */
    OH_COLORSPACE_BT601_SMPTE_C_FULL,
    /**
     * The color gamut is BT709, the transfer function is BT709, the conversion matrix is BT709,
     * and the data range is RANGE_FULL.
     */
    OH_COLORSPACE_BT709_FULL,
    /**
     * The color gamut is BT2020, the transfer function is HLG, the conversion matrix is BT2020,
     * and the data range is RANGE_FULL.
     */
    OH_COLORSPACE_BT2020_HLG_FULL,
    /**
     * The color gamut is BT2020, the transfer function is PQ, the conversion matrix is BT2020,
     * and the data range is RANGE_FULL.
     */
    OH_COLORSPACE_BT2020_PQ_FULL,
    /**
     * The color gamut is BT601_P, the transfer function is BT709, the conversion matrix is BT601_P,
     * and the data range is RANGE_LIMITED.
     */
    OH_COLORSPACE_BT601_EBU_LIMIT,
    /**
     * The color gamut is BT601_N, the transfer function is BT709, the conversion matrix is BT601_N,
     * and the data range is RANGE_LIMITED.
     */
    OH_COLORSPACE_BT601_SMPTE_C_LIMIT,
    /**
     * The color gamut is BT709, the transfer function is BT709, the conversion matrix is BT709,
     * and the data range is RANGE_LIMITED.
     */
    OH_COLORSPACE_BT709_LIMIT,
    /**
     * The color gamut is BT2020, the transfer function is HLG, the conversion matrix is BT2020,
     * and the data range is RANGE_LIMITED.
     */
    OH_COLORSPACE_BT2020_HLG_LIMIT,
    /**
     * The color gamut is BT2020, the transfer function is PQ, the conversion matrix is BT2020,
     * and the data range is RANGE_LIMITED.
     */
    OH_COLORSPACE_BT2020_PQ_LIMIT,
    /**
     * The color gamut is SRGB, the transfer function is SRGB, the conversion matrix is BT601_N,
     * and the data range is RANGE_FULL.
     */
    OH_COLORSPACE_SRGB_FULL,
    /**
     * The color gamut is P3_D65, the transfer function is SRGB, the conversion matrix is P3,
     * and the data range is RANGE_FULL.
     */
    OH_COLORSPACE_P3_FULL,
    /**
     * The color gamut is P3_D65, the transfer function is HLG, the conversion matrix is P3,
     * and the data range is RANGE_FULL.
     */
    OH_COLORSPACE_P3_HLG_FULL,
    /**
     * The color gamut is P3_D65, the transfer function is PQ, the conversion matrix is P3,
     * and the data range is RANGE_FULL.
     */
    OH_COLORSPACE_P3_PQ_FULL,
    /**
     * The color gamut is ADOBERGB, the transfer function is ADOBERGB, the conversion matrix is ADOBERGB,
     * and the data range is RANGE_FULL.
     */
    OH_COLORSPACE_ADOBERGB_FULL,
    /**
     * The color gamut is SRGB, the transfer function is SRGB, the conversion matrix is BT601_N,
     * and the data range is RANGE_LIMITED.
     */
    OH_COLORSPACE_SRGB_LIMIT,
    /**
     * The color gamut is P3_D65, the transfer function is SRGB, the conversion matrix is P3,
     * and the data range is RANGE_LIMITED.
     */
    OH_COLORSPACE_P3_LIMIT,
    /**
     * The color gamut is P3_D65, the transfer function is HLG, the conversion matrix is P3,
     * and the data range is RANGE_LIMITED.
     */
    OH_COLORSPACE_P3_HLG_LIMIT,
    /**
     * The color gamut is P3_D65, the transfer function is PQ, the conversion matrix is P3,
     * and the data range is RANGE_LIMITED.
     */
    OH_COLORSPACE_P3_PQ_LIMIT,
    /**
     * The color gamut is ADOBERGB, the transfer function is ADOBERGB, the conversion matrix is ADOBERGB,
     * and the data range is RANGE_LIMITED.
     */
    OH_COLORSPACE_ADOBERGB_LIMIT,
    /** The color gamut is SRGB, and the transfer function is LINEAR. */
    OH_COLORSPACE_LINEAR_SRGB,
    /** It is equivalent to <b>OH_COLORSPACE_LINEAR_SRGB</b>. */
    OH_COLORSPACE_LINEAR_BT709,
    /** The color gamut is P3_D65, and the transfer function is LINEAR. */
    OH_COLORSPACE_LINEAR_P3,
    /** The color gamut is BT2020, and the transfer function is LINEAR. */
    OH_COLORSPACE_LINEAR_BT2020,
    /** It is equivalent to <b>OH_COLORSPACE_SRGB_FULL</b>. */
    OH_COLORSPACE_DISPLAY_SRGB,
    /** It is equivalent to <b>OH_COLORSPACE_P3_FULL</b>. */
    OH_COLORSPACE_DISPLAY_P3_SRGB,
    /** It is equivalent to <b>OH_COLORSPACE_P3_HLG_FULL</b>.
    OH_COLORSPACE_DISPLAY_P3_HLG,
    /** It is equivalent to <b>OH_COLORSPACE_P3_PQ_FULL</b>. */
    OH_COLORSPACE_DISPLAY_P3_PQ,
    /**
     * The color gamut is BT2020, the transfer function is SRGB, the conversion matrix is BT2020,
     * and the data range is RANGE_FULL.
     */
    OH_COLORSPACE_DISPLAY_BT2020_SRGB,
    /** It is equivalent to <b>OH_COLORSPACE_BT2020_HLG_FULL</b>. */
    OH_COLORSPACE_DISPLAY_BT2020_HLG,
    /** It is equivalent to <b>OH_COLORSPACE_BT2020_PQ_FULL</b>. */
    OH_COLORSPACE_DISPLAY_BT2020_PQ,
} OH_NativeBuffer_ColorSpace;

/**
 * @brief Enumerates the transform types of an <b>OH_NativeBuffer</b> instance.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeBuffer
 * @since 12
 * @version 1.0
 */
typedef enum OH_NativeBuffer_TransformType {
    /** No rotation. */
    NATIVEBUFFER_ROTATE_NONE = 0,
    /** Rotates by 90 degrees. */
    NATIVEBUFFER_ROTATE_90,
    /** Rotates by 180 degrees. */
    NATIVEBUFFER_ROTATE_180,
    /** Rotates by 270 degrees. */
    NATIVEBUFFER_ROTATE_270,
    /** Flips horizontally. */
    NATIVEBUFFER_FLIP_H,
    /** Flips vertically. */
    NATIVEBUFFER_FLIP_V,
    /** Flips horizontally and rotates by 90 degrees. */
    NATIVEBUFFER_FLIP_H_ROT90,
    /** Flips vertically and rotates by 90 degrees. */
    NATIVEBUFFER_FLIP_V_ROT90,
    /** Flips horizontally and rotates by 180 degrees. */
    NATIVEBUFFER_FLIP_H_ROT180,
    /** Flips vertically and rotates by 180 degrees. */
    NATIVEBUFFER_FLIP_V_ROT180,
    /** Flips horizontally and rotates by 270 degrees. */
    NATIVEBUFFER_FLIP_H_ROT270,
    /** Flips vertically and rotates by 270 degrees. */
    NATIVEBUFFER_FLIP_V_ROT270,
} OH_NativeBuffer_TransformType;

/**
 * @brief Enumerates the color gamuts of an <b>OH_NativeBuffer</b> instance.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeBuffer
 * @since 12
 * @version 1.0
 */
typedef enum OH_NativeBuffer_ColorGamut {
    /** Default gamut. */
    NATIVEBUFFER_COLOR_GAMUT_NATIVE = 0,
    /** Standard BT.601 color gamut. */
    NATIVEBUFFER_COLOR_GAMUT_STANDARD_BT601 = 1,
    /** Standard BT.709 color gamut. */
    NATIVEBUFFER_COLOR_GAMUT_STANDARD_BT709 = 2,
    /** DCI P3 color gamut. */
    NATIVEBUFFER_COLOR_GAMUT_DCI_P3 = 3,
    /** SRGB color gamut. */
    NATIVEBUFFER_COLOR_GAMUT_SRGB = 4,
    /** Adobe RGB color gamut. */
    NATIVEBUFFER_COLOR_GAMUT_ADOBE_RGB = 5,
    /** Display P3 color gamut. */
    NATIVEBUFFER_COLOR_GAMUT_DISPLAY_P3 = 6,
    /** BT.2020 color gamut. */
    NATIVEBUFFER_COLOR_GAMUT_BT2020 = 7,
    /** BT.2100 PQ color gamut. */
    NATIVEBUFFER_COLOR_GAMUT_BT2100_PQ = 8,
    /** BT.2100 HLG color gamut format. */
    NATIVEBUFFER_COLOR_GAMUT_BT2100_HLG = 9,
    /** Display BT.2020 color gamut. */
    NATIVEBUFFER_COLOR_GAMUT_DISPLAY_BT2020 = 10,
} OH_NativeBuffer_ColorGamut;

/**
 * @brief Enumerates the error codes.
 * @since 12
 */
typedef enum OHNativeErrorCode {
    /** The operation is successful. */
    NATIVE_ERROR_OK = 0,
    /** An input parameter is invalid. */
    NATIVE_ERROR_INVALID_ARGUMENTS = 40001000,
    /** You do not have the permission to perform the operation. */
    NATIVE_ERROR_NO_PERMISSION = 40301000,
    /** No buffer is available. */
    NATIVE_ERROR_NO_BUFFER = 40601000,
    /** The consumer does not exist. */
    NATIVE_ERROR_NO_CONSUMER = 41202000,
    /** Not initialized. */
    NATIVE_ERROR_NOT_INIT = 41203000,
    /** The consumer is connected. */
    NATIVE_ERROR_CONSUMER_CONNECTED = 41206000,
    /** The buffer status does not meet the expectation. */
    NATIVE_ERROR_BUFFER_STATE_INVALID = 41207000,
    /** The buffer is already in the buffer queue. */
    NATIVE_ERROR_BUFFER_IN_CACHE = 41208000,
    /** The queue is full. */
    NATIVE_ERROR_BUFFER_QUEUE_FULL = 41209000,
    /** The buffer is not in the buffer queue. */
    NATIVE_ERROR_BUFFER_NOT_IN_CACHE = 41210000,
    /** The consumer is disconnected. */
    NATIVE_ERROR_CONSUMER_DISCONNECTED = 41211000,
    /** No listener is registered on the consumer side. */
    NATIVE_ERROR_CONSUMER_NO_LISTENER_REGISTERED = 41212000,
    /** The device or platform does not support the operation. */
    NATIVE_ERROR_UNSUPPORTED = 50102000,
    /** Unknown error. Check the log. */
    NATIVE_ERROR_UNKNOWN = 50002000,
    /** Failed to call the HDI. */
    NATIVE_ERROR_HDI_ERROR = 50007000,
    /** Cross-process communication failed. */
    NATIVE_ERROR_BINDER_ERROR = 50401000,
    /** The EGL environment is abnormal. */
    NATIVE_ERROR_EGL_STATE_UNKNOWN = 60001000,
    /** Failed to call the EGL APIs. */
    NATIVE_ERROR_EGL_API_FAILED = 60002000,
} OHNativeErrorCode;
/**
 * @brief Defines the <b>OH_NativeBuffer</b> attribute configuration, which is used when you apply for
 * a new <b>OH_NativeBuffer</b> instance or query the attributes of an existing instance.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeBuffer
 * @since 9
 * @version 1.0
 */
typedef struct OH_NativeBuffer_Config {
    /**
     * Width, in pixels.
     */
    int32_t width;
    /**
     * Height, in pixels.
     */
    int32_t height;
    /**
     * Pixel map format.
     */
    int32_t format;
    /**
     * Description of the buffer usage.
     */
    int32_t usage;
    /**
     * Output parameter. Stride of the local window buffer, in bytes.
     * @since 10
     */
    int32_t stride;
} OH_NativeBuffer_Config;

/**
 * @brief Defines the plane information of an image.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeBuffer
 * @since 12
 * @version 1.0
 */
typedef struct OH_NativeBuffer_Plane {
    /**
     * Offset of the image plane, in bytes.
     */
    uint64_t offset;
    /**
     * Distance from the first value in an image row to the first value in the next row, in bytes.
     */
    uint32_t rowStride;
    /**
     * Distance from the first value in an image column to the first value in the next column, in bytes.
     */
    uint32_t columnStride;
} OH_NativeBuffer_Plane;

/**
 * @brief Defines the plane information of images in an <b>OH_NativeBuffer</b> instance.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeBuffer
 * @since 12
 * @version 1.0
 */
typedef struct OH_NativeBuffer_Planes {
    /**
     * Number of planes.
     */
    uint32_t planeCount;
    /**
     * Array holding the plane information of each image.
     */
    OH_NativeBuffer_Plane planes[4];
} OH_NativeBuffer_Planes;

/**
 * @brief Enumerates the <b>OH_NativeBuffer</b> image standards.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeBuffer
 * @since 12
 * @version 1.0
 */
typedef enum OH_NativeBuffer_MetadataType {
    /**
     * Video HLG.
     */
    OH_VIDEO_HDR_HLG,
    /**
     * Video HDR10.
     */
    OH_VIDEO_HDR_HDR10,
    /**
     * Video HDR Vivid.
     */
    OH_VIDEO_HDR_VIVID
} OH_NativeBuffer_MetadataType;

/**
 * @brief Defines the X and Y coordinates of the primary color.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeBuffer
 * @since 12
 * @version 1.0
 */
typedef struct OH_NativeBuffer_ColorXY {
    /**
     * X coordinate of the primary color.
     */
    float x;
    /**
     * Y coordinate of the primary color.
     */
    float y;
} OH_NativeBuffer_ColorXY;

/**
 * @brief Defines the SMPTE ST 2086 static metadata.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeBuffer
 * @since 12
 * @version 1.0
 */
typedef struct OH_NativeBuffer_Smpte2086 {
    /**
     * Red primary color.
     */
    OH_NativeBuffer_ColorXY displaPrimaryRed;
    /**
     * Green primary color.
     */
    OH_NativeBuffer_ColorXY displaPrimaryGreen;
    /**
     * Blue primary color.
     */
    OH_NativeBuffer_ColorXY displaPrimaryBlue;
    /**
     * White point.
     */
    OH_NativeBuffer_ColorXY whitePoint;
    /**
     * Maximum luminance.
     */
    float maxLuminance;
    /**
     * Minimum luminance.
     */
    float minLuminance;
} OH_NativeBuffer_Smpte2086;

/**
 * @brief Defines the CTA-861.3 static metadata.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeBuffer
 * @since 12
 * @version 1.0
 */
typedef struct OH_NativeBuffer_Cta861 {
    /**
     * Maximum content light level (MaxCLL).
     */
    float maxContentLightLevel;
    /**
     * Maximum frame average light level (MaxFALLL).
     */
    float maxFrameAverageLightLevel;
} OH_NativeBuffer_Cta861;

/**
 * @brief Defines the HDR static metadata.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeBuffer
 * @since 12
 * @version 1.0
 */
typedef struct OH_NativeBuffer_StaticMetadata {
    /**
     * SMPTE ST 2086 static metadata.
     */
    OH_NativeBuffer_Smpte2086 smpte2086;
    /**
     * CTA-861.3 static metadata.
     */
    OH_NativeBuffer_Cta861 cta861;
} OH_NativeBuffer_StaticMetadata;

/**
 * @brief Enumerates the keys that specify the HDR metadata of an <b>OH_NativeBuffer</b> instance.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeBuffer
 * @since 12
 * @version 1.0
 */
typedef enum OH_NativeBuffer_MetadataKey {
    /**
     * Metadata type. For details about the available options, see {@link OH_NativeBuffer_MetadataType}.
     * <b>size</b> indicates the size of <b>OH_NativeBuffer_MetadataType</b>.
     */
    OH_HDR_METADATA_TYPE,
    /**
     * Static metadata. For details about the available options, see {@link OH_NativeBuffer_StaticMetadata}.
     * <b>size</b> indicates the size of <b>OH_NativeBuffer_StaticMetadata</b>.
     */
    OH_HDR_STATIC_METADATA,
    /**
     * Dynamic metadata. For details about the available options, see the SEI byte stream in the video stream.
     * The value range of <b>size</b> is 1-3000.
     */
    OH_HDR_DYNAMIC_METADATA
} OH_NativeBuffer_MetadataKey;

/**
 * @brief Creates an <b>OH_NativeBuffer</b> instance based on an <b>OH_NativeBuffer_Config</b> struct.
 * A new <b>OH_NativeBuffer</b> instance is created each time this function is called. \n
 * This function must be used in pair with {@link OH_NativeBuffer_Unreference}. Otherwise, memory leak occurs. \n
 * This function is not thread-safe. \n
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeBuffer
 * @param config Pointer to an <b>OH_NativeBuffer_Config</b> instance.
 * @return Returns the pointer to the <b>OH_NativeBuffer</b> instance created if the operation is successful;
 * returns <b>NULL</b> otherwise.
 * @since 9
 * @version 1.0
 */
OH_NativeBuffer* OH_NativeBuffer_Alloc(const OH_NativeBuffer_Config* config);

/**
 * @brief Increases the reference count of an <b>OH_NativeBuffer</b> instance by 1. \n
 * This function must be used in pair with {@link OH_NativeBuffer_Unreference}. Otherwise, memory leak occurs. \n
 * This function is not thread-safe. \n
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeBuffer
 * @param buffer Pointer to an <b>OH_NativeBuffer</b> instance.
 * @return Returns <b>0</b> if the operation is successful;
 * returns an error code defined in {@link OHNativeErrorCode} otherwise.
 * @since 9
 * @version 1.0
 */
int32_t OH_NativeBuffer_Reference(OH_NativeBuffer *buffer);

/**
 * @brief Decreases the reference count of an <b>OH_NativeBuffer</b> instance by 1 and
 * when the reference count reaches 0, destroys the instance. \n
 * This function is not thread-safe. \n
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeBuffer
 * @param buffer Pointer to an <b>OH_NativeBuffer</b> instance.
 * @return Returns <b>0</b> if the operation is successful;
 * returns an error code defined in {@link OHNativeErrorCode} otherwise.
 * @since 9
 * @version 1.0
 */
int32_t OH_NativeBuffer_Unreference(OH_NativeBuffer *buffer);

/**
 * @brief Obtains the attributes of an <b>OH_NativeBuffer</b> instance. \n
 * This function is not thread-safe. \n
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeBuffer
 * @param buffer Pointer to an <b>OH_NativeBuffer</b> instance.
 * @param config Pointer to an <b>OH_NativeBuffer_Config</b> instance, which is used to
 * receive the attributes of <b>OH_NativeBuffer</b>.
 * @since 9
 * @version 1.0
 */
void OH_NativeBuffer_GetConfig(OH_NativeBuffer *buffer, OH_NativeBuffer_Config* config);

/**
 * @brief Maps the ION memory corresponding to an <b>OH_NativeBuffer</b> instance to the process address space. \n
 * This function must be used in pair with {@link OH_NativeBuffer_Unmap}. \n
 * This function is not thread-safe. \n
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeBuffer
 * @param buffer Pointer to an <b>OH_NativeBuffer</b> instance.
 * @param virAddr Double pointer to the address of the virtual memory.
 * @return Returns <b>0</b> if the operation is successful;
 * returns an error code defined in {@link OHNativeErrorCode} otherwise.
 * @since 9
 * @version 1.0
 */

int32_t OH_NativeBuffer_Map(OH_NativeBuffer *buffer, void **virAddr);

/**
 * @brief Unmaps the ION memory corresponding to an <b>OH_NativeBuffer</b> instance from the process address space. \n
 * This function is not thread-safe. \n
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeBuffer
 * @param buffer Pointer to an <b>OH_NativeBuffer</b> instance.
 * @return Returns <b>0</b> if the operation is successful;
 * returns an error code defined in {@link OHNativeErrorCode} otherwise.
 * @since 9
 * @version 1.0
 */
int32_t OH_NativeBuffer_Unmap(OH_NativeBuffer *buffer);

/**
 * @brief Obtains the sequence number of an <b>OH_NativeBuffer</b> instance. \n
 * This function is not thread-safe. \n
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeBuffer
 * @param buffer Pointer to an <b>OH_NativeBuffer</b> instance.
 * @return Returns the unique sequence number of the <b>OH_NativeBuffer</b> instance.
 * @since 9
 * @version 1.0
 */
uint32_t OH_NativeBuffer_GetSeqNum(OH_NativeBuffer *buffer);

/**
 * @brief Sets the color space for an <b>OH_NativeBuffer</b> instance. \n
 * This function is not thread-safe. \n
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeBuffer
 * @param buffer Pointer to an <b>OH_NativeBuffer</b> instance.
 * @param colorSpace Color space to set.
 * For details about the available options, see {@link OH_NativeBuffer_ColorSpace}.
 * @return Returns <b>0</b> if the operation is successful;
 * returns an error code defined in {@link OHNativeErrorCode} otherwise.
 * @since 11
 * @version 1.0
 */
int32_t OH_NativeBuffer_SetColorSpace(OH_NativeBuffer *buffer, OH_NativeBuffer_ColorSpace colorSpace);

/**
 * @brief Maps the multi-channel ION memory corresponding to an <b>OH_NativeBuffer</b> instance
 * to the process address space. \n
 * This function is not thread-safe. \n
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeBuffer
 * @param buffer Pointer to an <b>OH_NativeBuffer</b> instance.
 * @param virAddr Double pointer to the address of the virtual memory.
 * @param outPlanes Pointer to the plane information of all images.
 * @return Returns <b>0</b> if the operation is successful;
 * returns an error code defined in {@link OHNativeErrorCode} otherwise.
 * @since 12
 * @version 1.0
 */
int32_t OH_NativeBuffer_MapPlanes(OH_NativeBuffer *buffer, void **virAddr, OH_NativeBuffer_Planes *outPlanes);

/**
 * @brief Converts an <b>OHNativeWindowBuffer</b> instance to an <b>OH_NativeBuffer</b> instance. \n
 * This function is not thread-safe. \n
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeBuffer
 * @param nativeWindowBuffer Pointer to an <b>OHNativeWindowBuffer</b> instance.
 * @param buffer Pointer to an <b>OH_NativeBuffer</b> instance.
 * @return Returns <b>0</b> if the operation is successful;
 * returns an error code defined in {@link OHNativeErrorCode} otherwise.
 * @since 12
 * @version 1.0
 */
int32_t OH_NativeBuffer_FromNativeWindowBuffer(OHNativeWindowBuffer *nativeWindowBuffer, OH_NativeBuffer **buffer);

/**
 * @brief Obtains the color space of an <b>OH_NativeBuffer</b> instance. \n
 * This function is not thread-safe. \n
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeBuffer
 * @param buffer Pointer to an <b>OH_NativeBuffer</b> instance.
 * @param colorSpace Pointer to the color space.
 * For details about the available options, see {@link OH_NativeBuffer_ColorSpace}.
 * @return Returns <b>0</b> if the operation is successful;
 * returns an error code defined in {@link OHNativeErrorCode} otherwise.
 * @since 12
 * @version 1.0
 */
int32_t OH_NativeBuffer_GetColorSpace(OH_NativeBuffer *buffer, OH_NativeBuffer_ColorSpace *colorSpace);

/**
 * @brief Sets a metadata value for an <b>OH_NativeBuffer</b> instance. \n
 * This function is not thread-safe. \n
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeBuffer
 * @param buffer Pointer to an <b>OH_NativeBuffer</b> instance.
 * @param metadataKey Key of the metadata.
 * For details about the available options, see {@link OH_NativeBuffer_MetadataKey}.
 * @param size Size of the uint8_t vector.
 * For details about the available options, see {@link OH_NativeBuffer_MetadataKey}.
 * @param metaData Pointer to the uint8_t vector.
 * @return Returns <b>0</b> if the operation is successful;
 * returns an error code defined in {@link OHNativeErrorCode} otherwise.
 * @since 12
 * @version 1.0
 */
int32_t OH_NativeBuffer_SetMetadataValue(OH_NativeBuffer *buffer, OH_NativeBuffer_MetadataKey metadataKey,
    int32_t size, uint8_t *metaData);

/**
 * @brief Obtains the metadata value of an <b>OH_NativeBuffer</b> instance. \n
 * This function is not thread-safe. \n
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeBuffer
 * @param buffer Pointer to an <b>OH_NativeBuffer</b> instance.
 * @param metadataKey Key of the metadata.
 * For details about the available options, see {@link OH_NativeBuffer_MetadataKey}.
 * @param size Pointer to the size of the uint8_t vector.
 * For details about the available options, see {@link OH_NativeBuffer_MetadataKey}.
 * @param metaData Double pointer to the uint8_t vector.
 * @return Returns <b>0</b> if the operation is successful;
 * returns an error code defined in {@link OHNativeErrorCode} otherwise.
 * @since 12
 * @version 1.0
 */
int32_t OH_NativeBuffer_GetMetadataValue(OH_NativeBuffer *buffer, OH_NativeBuffer_MetadataKey metadataKey,
    int32_t *size, uint8_t **metaData);
#ifdef __cplusplus
}
#endif

/** @} */
#endif
