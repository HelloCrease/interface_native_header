/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup Sensor
 * @{
 *
 * @brief Provides APIs for sensor services to access the sensor driver.
 *
 * A sensor service can obtain a sensor driver object or agent and then call APIs provided by this object or agent to
 * access different types of sensor devices, thereby obtaining sensor information,
 * subscribing to or unsubscribing from sensor data, enabling or disabling a sensor,
 * setting the sensor data reporting mode, and setting sensor options such as the accuracy and measurement range.
 *
 * @since 2.2
 * @version 1.0
 */

/**
 * @file ISensorCallback.idl
 *
 * @brief Declares data reporting callbacks for the sensor service.
 *
 * @since 2.2
 * @version 1.0
 */

/**
 * @brief Defines the package path of the Sensor module APIs.
 *
 * @since 2.2
 * @version 1.0
 */
package ohos.hdi.sensor.v1_0;

import ohos.hdi.sensor.v1_0.SensorTypes;

/**
 * @brief Defines the callback for reporting sensor data. This callback must be registered when
 * a sensor user subscribes to sensor data. The subscriber can receive sensor data
 * only after the sensor is enabled. For details, see {@Link ISensorInterface}.
 *
 * @since 2.2
 * @version 1.0
 */
[callback] interface ISensorCallback {
    /**
    * @brief Called to report sensor data.
    *
    * @param event Indicates the sensor data reporting event. For details, see {@Link HdfSensorEvents}.
    *
    * @return Returns <b>0</b> if the operation is successful.
    * @return Returns a negative value if the operation fails.
    *
    * @since 2.2
    * @version 1.0
    */
    OnDataEvent([in] struct HdfSensorEvents event);
}
/** @} */
