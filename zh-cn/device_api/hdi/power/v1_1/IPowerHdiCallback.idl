/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup power
 * @{
 *
 * @brief 提供休眠/唤醒操作、订阅休眠/唤醒状态和运行锁管理的接口。
 *
 * 电源模块为电源服务提供的休眠/唤醒操作、订阅休眠/唤醒状态和运行锁管理的接口。
 * 服务获取此模块的对象或代理后，可以调用相关的接口对设备进行休眠/唤醒、订阅休眠/唤醒状态和管理运行锁。
 *
 * @since 3.1
 * @version 1.0
 */

 /**
 * @file IPowerHdiCallback.idl
 *
 * @brief 休眠/唤醒状态的回调。
 *
 * 电源模块为电源服务提供的订阅休眠/唤醒状态的回调。
 *
 * 模块包路径：ohos.hdi.power.v1_1
 *
 * @since 3.1
 * @version 1.0
 */

package ohos.hdi.power.v1_1;

/**
 * @brief 休眠/唤醒状态的回调。
 *
 * 服务创建此回调对象后，可以调用{@link IPowerInterface}的接口注册回调，从而订阅休眠/唤醒状态的变化。
 *
 * @since 3.1
 */
[callback] interface IPowerHdiCallback {
    /**
     * @brief 休眠状态的回调方法。
     *
     * 当设备进入休眠状态时，将通过此方法通知给服务。
     *
     * @since 3.1
     */
    OnSuspend();

    /**
     * @brief 唤醒状态的回调方法。
     *
     * 当设备进入唤醒状态时，将通过此方法通知给服务。
     *
     * @since 3.1
     */
    OnWakeup();
}
/** @} */
