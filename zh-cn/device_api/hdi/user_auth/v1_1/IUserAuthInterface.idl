/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup HdfUserAuth
 * @{
 *
 * @brief 提供用户认证驱动的标准API接口。
 *
 * 用户认证驱动为用户认证服务提供统一的访问接口。获取用户认证驱动代理后，用户认证服务可以调用相关接口注册执行器，管理用户认证凭据，
 * 完成PIN码和生物特征认证。
 *
 * @since 4.0
 */

/**
 * @file IUserAuthInterface.idl
 *
 * @brief 声明用户认证驱动的API接口。接口可用于注册执行器，管理用户认证凭据，完成PIN码和生物特征认证。
 *
 * 模块包路径：ohos.hdi.user_auth.v1_1
 *
 * 引用：
 * - ohos.hdi.user_auth.v1_0.UserAuthTypes
 * - ohos.hdi.user_auth.v1_0.IUserAuthInterface
 * - ohos.hdi.user_auth.v1_1.UserAuthTypes
 *
 * @since 4.0
 */

package ohos.hdi.user_auth.v1_1;

import ohos.hdi.user_auth.v1_0.UserAuthTypes;
import ohos.hdi.user_auth.v1_0.IUserAuthInterface;
import ohos.hdi.user_auth.v1_1.UserAuthTypes;

/**
 * @brief 声明用户认证驱动的API接口。
 *
 * @since 4.0
 * @version 1.1
 */
interface IUserAuthInterface extends ohos.hdi.user_auth.v1_0.IUserAuthInterface {
    /**
     * @brief 开始注册用户认证凭据。当注册凭据类型为口令且该用户已经注册了口令凭据时，将会更新口令凭据。
     *
     * @param userId 用户ID。
     * @param authToken 用户口令认证令牌。
     * @param param 注册凭据参数{@link EnrollParam}。
     * @param info 调度信息{@link ScheduleInfoV1_1}。
     *
     * @return 0 表示操作成功。
     * @return 非0 表示操作失败。
     */
    BeginEnrollmentV1_1([in] int userId, [in] unsigned char[] authToken, [in] struct EnrollParam param, 
        [out] struct ScheduleInfoV1_1 info);
    /**
     * @brief 开始认证用户，并生成认证方案。
     *
     * @param contextId 上下文索引。
     * @param param 认证方案{@link AuthSolution}。
     * @param scheduleInfos 调度信息{@link ScheduleInfoV1_1}。
     *
     * @return 0 表示操作成功。
     * @return 非0 表示操作失败。
     */
    BeginAuthenticationV1_1([in] unsigned long contextId, [in] struct AuthSolution param,
        [out] struct ScheduleInfoV1_1[] scheduleInfos);
    /**
     * @brief 开始用户身份识别，并生成识别方案。
     *
     * @param contextId 上下文索引。
     * @param authType 用户身份识别类型@{AuthType}。
     * @param challenge 随机挑战值，用于生成用户身份识别令牌，防止重放。
     * @param executorSensorHint 执行器传感器提示，用于找到对应认证方式的传感器，值为0时表示没有指定传感器。
     * @param scheduleInfo 调度信息{@link ScheduleInfoV1_1}。
     *
     * @return 0 表示操作成功。
     * @return 非0 表示操作失败。
     */
    BeginIdentificationV1_1([in] unsigned long contextId, [in] enum AuthType authType, [in] unsigned char[] challenge,
        [in] unsigned int executorSensorHint, [out] struct ScheduleInfoV1_1 scheduleInfo);
    /**
     * @brief 获取所有用户信息.
     *
     * @param userInfos 用户信息列表@{UserInfo}.
     *
     * @return 0 表示操作成功。
     * @return 非0 表示操作失败。
     */
    GetAllUserInfo([out] UserInfo[] userInfos);
}
/** @} */
