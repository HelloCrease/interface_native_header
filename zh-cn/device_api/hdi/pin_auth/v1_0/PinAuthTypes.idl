/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup HdfPinAuth
 * @{
 *
 * @brief 提供口令认证驱动的标准API接口。
 *
 * 口令认证驱动为口令认证服务提供统一的访问接口。获取口令认证驱动代理后，口令认证服务可以调用相关接口获取执行器，获取口令认证执行器后，
 * 口令认证服务可以调用相关接口获取执行器信息，获取凭据模版信息，注册口令，认证口令，删除口令等。
 *
 * @since 3.2
 */

/**
 * @file PinAuthTypes.idl
 *
 * @brief 定义口令认证驱动的枚举类和数据结构。
 *
 * 模块包路径：ohos.hdi.pin_auth.v1_0
 *
 * @since 3.2
 */

package ohos.hdi.pin_auth.v1_0;

/**
 * @brief 枚举用户认证凭据类型。
 *
 * @since 3.2
 * @version 1.0
 */
enum AuthType : int {
    /** 认证凭据类型为口令。 */
    PIN = 1,
    /** 认证凭据类型为人脸。 */
    FACE = 2,
    /** 认证凭据类型为指纹。 */
    FINGERPRINT = 4,
};

/**
 * @brief 枚举执行器角色。
 *
 * @since 3.2
 * @version 1.0
 */
enum ExecutorRole : int {
    /** 执行器角色为采集器，提供用户认证时的数据采集能力，需要和认证器配合完成用户认证。 */
    COLLECTOR = 1,
    /** 执行器角色为认证器，提供用户认证时数据处理能力，读取存储凭据模板信息并完成比对。 */
    VERIFIER = 2,
    /** 执行器角色为全功能执行器，可提供用户认证数据采集、处理、储存及比对能力。 */
    ALL_IN_ONE = 3,
};

/**
 * @brief 枚举执行器安全等级。
 *
 * @since 3.2
 * @version 1.0
 */
enum ExecutorSecureLevel : int {
    /** 执行器安全级别为0，关键操作在无访问控制执行环境中完成。 */
    ESL0 = 0,
    /** 执行器安全级别为1，关键操作在有访问控制的执行环境中完成。 */
    ESL1 = 1,
    /** 执行器安全级别为2，关键操作在可信执行环境中完成。 */
    ESL2 = 2,
    /** 执行器安全级别为3，关键操作在高安环境如独立安全芯片中完成。 */
    ESL3 = 3,
};

/**
 * @brief 枚举口令认证相关功能操作命令。
 *
 * @since 3.2
 * @version 1.0
 */
enum CommandId : int {
    /** 默认无效操作命令。 */
    DEFAULT = 0,
};


/**
 * @brief 执行器信息。
 *
 * @since 3.2
 * @version 1.0
 */
struct ExecutorInfo {
    /** 传感器ID，不同传感器在口令认证驱动内的唯一标识。 */
    unsigned short sensorId;
    /** 执行器类型，根据执行器支持的算法类型进行分类。 */
    unsigned int executorType;
    /** 执行器角色@{ExecutorRole}。 */
    enum ExecutorRole executorRole;
    /** 用户认证凭据类型@{AuthType}。 */
    enum AuthType authType;
    /** 执行器安全等级@{ExecutorSecureLevel}。 */
    enum ExecutorSecureLevel esl;
    /** 执行器公钥，用于校验该执行器私钥签名的信息。 */
    unsigned char[] publicKey;
    /** 其他相关信息，用于支持信息扩展。 */
    unsigned char[] extraInfo;
};

/**
 * @brief 凭据模版信息，口令模版在用户注册口令认证凭据时生成并存储，用于支持通过口令认证方式验证用户身份。
 *
 * @since 3.2
 * @version 1.0
 *
 * @deprecated 从4.0版本开始废弃，使用{@link GetPropertyType}接口代替。
 */
struct TemplateInfo {
    /** 执行器类型，根据执行器支持的算法类型进行分类。 */
    unsigned int executorType;
    /** 认证执行器的剩余冻结时间。 */
    int lockoutDuration;
    /** 认证执行器的剩余可重试次数。 */
    int remainAttempts;
    /** 其他相关信息，用于支持信息扩展。 */
    unsigned char[] extraInfo;
};
/** @} */