/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup Display
 * @{
 *
 * @brief 显示模块驱动接口定义。
 *
 * 提供给图形系统使用的驱动接口，包括图层管理、设备控制、图形硬件加速、显示内存管理和回调接口等。
 *
 * @since 1.0
 * @version 2.0
 */

/**
 * @file display_gralloc.h
 *
 * @brief 显示内存驱动接口声明。
 *
 * @since 1.0
 * @version 2.0
 */

#ifndef DISPLAY_GRALLOC_H
#define DISPLAY_GRALLOC_H
#include "display_type.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief 显示内存驱动接口结构体，定义显示内存驱动接口函数指针。
 */
typedef struct {
    /**
     * @brief 显示内存分配。
     *
     * 根据GUI图形系统传递的参数分配内存，分配的内存根据类型可分为共享内存、cache内存和非cache内存等。
     *
    * @param info 输入参数，指示申请内存AllocInfo信息.
     * @param handle 输入参数，指向申请的内存handle指针。
     *
     * @return DISPLAY_SUCCESS 表示执行成功。
     * @return 其他值表示执行失败，具体错误码查看{@link DispErrCode}。
     *
     * @since 1.0
     * @version 1.0
     */
    int32_t (*AllocMem)(const AllocInfo* info, BufferHandle** handle);

    /**
     * @brief 显示内存释放。
     *
     * @param handle 输入参数，待释放的内存handle指针。
     *
     * @return 成功返回有效地址，失败返回NULL。
     *
     * @since 1.0
     * @version 1.0
     */
    void (*FreeMem)(BufferHandle *handle);

    /**
     * @brief 显示内存映射，将内存映射到对应的进程地址空间中。
     *
     * @param handle 输入参数，待映射内存handle指针。
     *
     * @return 成功返回有效地址，失败返回NULL。
     *
     * @since 1.0
     * @version 1.0
     */
    void *(*Mmap)(BufferHandle *handle);

   /**
     * @brief YUV 内存映射。
     *
     * @param handle 输出参数，指示内存映射的输出缓存。
     * @param info 输出参数，指示内存映射的YUVDescInfo信息。
     *
     * @return 成功返回有效地址，失败返回 NULL。
     *
     * @since 3.2
     * @version 1.0
     */
    void *(*MmapYUV)(BufferHandle *handle, YUVDescInfo *info);
    /**
     * @brief 显示内存映射，将内存映射为 cache 内存到对应的进程地址空间中。
     *
     * @param handle 输出参数，待映射内存 handle 指针。
     *
     * @return 成功返回有效地址，失败返回 NULL。
     *
     * @since 1.0
     * @version 1.0
     */
    void *(*MmapCache)(BufferHandle *handle);

    /**
     * @brief 内存反映射，将内存进行反映射。
     *
     * @param handle 输出参数，待反映射内存handle指针。
     *
     * @return DISPLAY_SUCCESS 表示执行成功。
     * @return 其他值表示执行失败，具体错误码查看{@link DispErrCode}。
     *
     * @since 1.0
     * @version 1.0
     */
    int32_t (*Unmap)(BufferHandle *handle);

    /**
     * @brief 刷新cache，刷新cache里的内容到内存并且使cache里的内容无效。
     *
     * @param handle 输出参数，待刷新cache的handle指针。
     *
     * @return DISPLAY_SUCCESS 表示执行成功。
     * @return 其他值表示执行失败，具体错误码查看{@link DispErrCode}。
     *
     * @since 1.0
     * @version 1.0
     */
    int32_t (*FlushCache)(BufferHandle *handle);

    /**
     * @brief 刷新Mmap映射的cache，刷新Mmap映射的cache里的内容到内存并且使cache里的内容无效。
     *
     * @param handle 输出参数，待刷新cache的handle指针。
     *
     * @return DISPLAY_SUCCESS 表示执行成功。
     * @return 其他值表示执行失败，具体错误码查看{@link DispErrCode}。
     *
     * @since 1.0
     * @version 1.0
     */
    int32_t (*FlushMCache)(BufferHandle *handle);

    /**
     * @brief 使cache中的内容无效用以存储更新内存内容。
     *
     * @param handle 输出参数，待无效cache的handle指针。
     *
     * @return DISPLAY_SUCCESS 表示执行成功。
     * @return 其他值表示执行失败，具体错误码查看{@link DispErrCode}。
     *
     * @since 1.0
     * @version 1.0
     */
    int32_t (*InvalidateCache)(BufferHandle* handle);

    /**
     * @brief 检测给定的VerifyAllocInfo数组信息能否申请内存成功。
     *
     * @param num 输入参数，VerifyAllocInfo数组个数。
     * @param infos 输出参数，VerifyAllocInfo数组首地址。
     * @param supporteds 输出参数，supporteds数组首地址, 表示能否申请成功。
     *
     * @return DISPLAY_SUCCESS 表示执行成功。
     * @return 其他值表示执行失败，具体错误码查看{@link DispErrCode}。
     *
     * @since 1.0
     * @version 1.0
     */
    int32_t (*IsSupportedAlloc)(uint32_t num, const VerifyAllocInfo *infos, bool *supporteds);
} GrallocFuncs;

/**
 * @brief 初始化内存模块，并获取内存提供的操作接口。
 *
 * @param funcs 输出参数，内存模块操作接口指针，初始化内存模块时分配内存，调用者不需要分配内存，调用者获取该指针操作内存。
 *
 * @return DISPLAY_SUCCESS 表示执行成功。
 * @return 其他值表示执行失败，具体错误码查看{@link DispErrCode}。
 *
 * @since 1.0
 * @version 1.0
 */
int32_t GrallocInitialize(GrallocFuncs **funcs);

/**
 * @brief 取消初始化内存模块，并释放内存操作接口指针。
 *
 * @param funcs 输出参数，内存操作接口指针，用于释放内存初始化函数中分配的操作指针内存。
 *
 * @return DISPLAY_SUCCESS 表示执行成功。
 * @return 其他值表示执行失败，具体错误码查看{@link DispErrCode}。
 *
 * @since 1.0
 * @version 1.0
 */
int32_t GrallocUninitialize(GrallocFuncs *funcs);

#ifdef __cplusplus
}
#endif
#endif
/** @} */