/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup HdiNfc
 * @{
 *
 * @brief 为nfc服务提供统一的访问nfc驱动的接口。
 *
 * NFC服务通过获取的nfc驱动对象提供的API接口访问nfc驱动，包括开关NFC、初始化NFC、读写数据、配置RF参数、
 * 通过IO控制发送NCI指令给nfc驱动。
 *
 * @since 3.2
 * @version 1.0
 */

/**
 * @file INfcCallback.idl
 *
 * @brief 定义NFC回调的接口文件
 *
 * 模块包路径：ohos.hdi.nfc.v1_0
 *
 * 引用：ohos.hdi.nfc.v1_0.NfcTypes
 *
 * @since 3.2
 * @version 1.0
 */

package ohos.hdi.nfc.v1_0;

import ohos.hdi.nfc.v1_0.NfcTypes;

/**
 * @brief 用于从nfc芯片给nfc协议栈上报数据和事件的回调声明。
 *
 * @since 3.2
 * @version 1.0
 */
[callback] interface INfcCallback {
    /**
    * @brief NFC芯片上报给协议栈NFC数据的函数定义。
    * 
    * @param data NFC芯片上报给NFC协议栈的数据。
    * 
    * @since 3.2
    */
    OnData([in] List<unsigned char> data);

    /**
    * @brief NFC芯片上报给协议栈事件的函数定义。
    * 
    * @param event 上报事件的事件ID。
    * @param status NFC状态，具体定义见NfcStatus。
    * 事件ID具体见{@link NfcTypes}。
    *
    * @since 3.2
    */
    OnEvent([in] enum NfcEvent event, [in] enum NfcStatus status);
}
/** @} */