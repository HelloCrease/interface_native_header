/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup Battery
 * @{
 *
 * @brief 提供获取、订阅电池信息的接口。
 * 
 * 电池模块为电池服务提供的获取、订阅电池信息的接口。
 * 服务获取此模块的对象或代理后，可以调用相关的接口获取电池信息、订阅电池信息的变化。
 *
 * @since 3.2
 * @version 1.1
 */

/**
 * @file Types.idl
 *
 * @brief 电池信息相关数据类型。
 *
 * 电池信息中使用的数据类型，包括健康状态、充电状态、充电设备类型和电池信息结构。
 *
 * 模块包路径：ohos.hdi.battery.v2_0
 *
 * @since 3.2
 * @version 1.1
 */

package ohos.hdi.battery.v2_0;


/**
 * @brief 电池的健康状态。
 *
 * @since 3.1
 */
enum BatteryHealthState
{
    /** 表示电池健康状态未知。 */
    BATTERY_HEALTH_UNKNOWN = 0,
    /** 表示电池健康状态为正常。 */
    BATTERY_HEALTH_GOOD,
    /** 表示电池健康状态为过热。 */
    BATTERY_HEALTH_OVERHEAT,
    /** 表示电池健康状态为过压。 */
    BATTERY_HEALTH_OVERVOLTAGE,
    /** 表示电池健康状态为低温。 */
    BATTERY_HEALTH_COLD,
    /** 表示电池健康状态为耗尽。 */
    BATTERY_HEALTH_DEAD,
    /** 预留。 */
    BATTERY_HEALTH_RESERVED,
};

/**
 * @brief 电池的充电状态。
 *
 * @since 3.1
 */
enum BatteryChargeState
{
    /** 表示电池充电状态未知。 */
    CHARGE_STATE_NONE = 0,
    /** 表示电池充电状态为使能状态。 */
    CHARGE_STATE_ENABLE,
    /** 表示电池充电状态为停止状态。 */
    CHARGE_STATE_DISABLE,
    /** 表示电池充电状态为已充满状态。 */
    CHARGE_STATE_FULL,
    /** 预留。 */
    CHARGE_STATE_RESERVED,
};

/**
 * @brief 电池的充电设备类型。
 *
 * @since 3.2
 */
enum BatteryPluggedType
{
    /** 表示连接充电器类型未知。 */
    PLUGGED_TYPE_NONE = 0,
    /** 表示连接的充电器类型为交流充电器。 */
    PLUGGED_TYPE_AC,
    /** 表示连接的充电器类型为USB充电器。 */
    PLUGGED_TYPE_USB,
    /** 表示连接的充电器类型为无线充电器。 */
    PLUGGED_TYPE_WIRELESS,
    /** 预留。 */
    PLUGGED_TYPE_BUTT
};

/**
 * @brief 电池相关信息。
 *
 * @since 3.1
 */
struct BatteryInfo {
    /** 表示电池的电量百分比。 */
    int capacity;
    /** 表示电池的电压。 */
    int voltage;
    /** 表示电池的温度 */
    int temperature;
    /** 表示电池的健康状态，详情可参考{@link BatteryHealthState}。 */
    int healthState;
    /** 表示电池的充电设备类型，详情可参考{@link BatteryPluggedType}。 */
    int pluggedType;
    /** 表示电池的最大充电电流。 */
    int pluggedMaxCurrent;
    /** 表示电池的最大充电电压。 */
    int pluggedMaxVoltage;
    /** 表示电池的充电状态，详情可参考{@link BatteryChargeState}。 */
    int chargeState;
    /** 表示电池的充电次数。 */
    int chargeCounter;
    /** 表示电池的总容量。 */
    int totalEnergy;
    /** 表示电池的平均电流。 */
    int curAverage;
    /** 表示电池的实时电流。 */
    int curNow;
    /** 表示电池的剩余容量。 */
    int remainEnergy;
    /** 表示是否支持电池或者电池是否在位。 */
    byte present;
    /** 表示电池的技术型号。 */
    String technology;
    /** 事件名 */
    String uevent;
};

/**
 * @brief 定义电池充电限制类型。
 *
 * @since 3.2
 */
enum ChargingLimitType
{
    /** 限制类型：充电电流 */
    TYPE_CURRENT = 0,
    /** 限制类型：充电电压 */
    TYPE_VOLTAGE,
};

/**
 * @brief 定义电池充电电流或电压的限制。
 *
 * @since 3.2
 */
struct ChargingLimit
{
    /** 定义电池充电限制类型 */
    enum ChargingLimitType type;
     /** 限制协议描述 */
    String protocol;
     /** 选择限制类型，0-电流，1-电压 */
    int value;
};

/**
 * @brief 表示插入的充电器类型。
 *
 * @since 4.0
 */
enum ChargeType
{
  /** 未知类型 */
  CHARGE_TYPE_NONE = 0,
  /** 有线普通型 */
  CHARGE_TYPE_WIRED_NORMAL,
  /** 有线快速型 */
  CHARGE_TYPE_WIRED_QUICK,
  /** 有线超快速型 */
  CHARGE_TYPE_WIRED_SUPER_QUICK,
  /** 无线普通型 */
  CHARGE_TYPE_WIRELESS_NORMAL,
  /** 无线快速型*/
  CHARGE_TYPE_WIRELESS_QUICK,
  /** 无线超快速型 */
  CHARGE_TYPE_WIRELESS_SUPER_QUICK,
};
/** @} */