/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef NDK_INCLUDE_NATIVE_VSYNC_H_
#define NDK_INCLUDE_NATIVE_VSYNC_H_

/**
 * @addtogroup NativeVsync
 * @{
 *
 * @brief 提供NativeVsync功能。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeVsync
 * @since 9
 * @version 1.0
 */

/**
 * @file native_vsync.h
 *
 * @brief 定义获取和使用NativeVsync的相关函数。
 *
 * @library libnative_vsync.so
 * @since 9
 * @version 1.0
 */

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief 提供OH_NativeVSync结构体声明。
 * @since 9
 */
struct OH_NativeVSync;

/**
 * @brief 接口错误码说明（仅用于查询）。
 * @since 12
 */
typedef enum OHNativeErrorCode {
    /** 成功 */
    NATIVE_ERROR_OK = 0,
    /** 入参无效 */
    NATIVE_ERROR_INVALID_ARGUMENTS = 40001000,
    /** 无权限操作 */
    NATIVE_ERROR_NO_PERMISSION = 40301000,
    /** 无空闲可用的buffer */
    NATIVE_ERROR_NO_BUFFER = 40601000,
    /** 消费端不存在 */
    NATIVE_ERROR_NO_CONSUMER = 41202000,
    /** 未初始化 */
    NATIVE_ERROR_NOT_INIT = 41203000,
    /** 消费端已经被连接 */
    NATIVE_ERROR_CONSUMER_CONNECTED = 41206000,
    /** buffer状态不符合预期 */
    NATIVE_ERROR_BUFFER_STATE_INVALID = 41207000,
    /** buffer已在缓存队列中 */
    NATIVE_ERROR_BUFFER_IN_CACHE = 41208000,
    /** 队列已满 */
    NATIVE_ERROR_BUFFER_QUEUE_FULL = 41209000,
    /** buffer不在缓存队列中 */
    NATIVE_ERROR_BUFFER_NOT_IN_CACHE = 41210000,
    /** 消费端已经被断开连接 */
    NATIVE_ERROR_CONSUMER_DISCONNECTED = 41211000,
    /** 消费端未注册listener回调函数 */
    NATIVE_ERROR_CONSUMER_NO_LISTENER_REGISTERED = 41212000,
    /** 当前设备或平台不支持 */
    NATIVE_ERROR_UNSUPPORTED = 50102000,
    /** 未知错误，请查看日志 */
    NATIVE_ERROR_UNKNOWN = 50002000,
    /** HDI接口调用失败 */
    NATIVE_ERROR_HDI_ERROR = 50007000,
    /** 跨进程通信失败 */
    NATIVE_ERROR_BINDER_ERROR = 50401000,
    /** egl环境状态异常 */
    NATIVE_ERROR_EGL_STATE_UNKNOWN = 60001000,
    /** egl接口调用失败 */
    NATIVE_ERROR_EGL_API_FAILED = 60002000,
} OHNativeErrorCode;

/**
 * @brief 提供OH_NativeVSync结构体声明。
 * @since 9
 */
typedef struct OH_NativeVSync OH_NativeVSync;

/**
 * @brief VSync回调函数类型。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeVsync
 * @param timestamp VSync使用CLOCK_MONOTONIC获取的系统时间戳, 单位为纳秒。
 * @param data 用户自定义数据。
 * @since 9
 * @version 1.0
 */
typedef void (*OH_NativeVSync_FrameCallback)(long long timestamp, void *data);

/**
 * @brief 创建一个OH_NativeVSync实例，每次调用都会产生一个新的实例。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeVsync
 * @param name 表示一个名字，与创建的OH_NativeVSync实例关联。
 * @param length name的长度。
 * @return 返回一个指向OH_NativeVSync实例的指针。
 * @since 9
 * @version 1.0
 */
OH_NativeVSync* OH_NativeVSync_Create(const char* name, unsigned int length);

/**
 * @brief 销毁OH_NativeVSync实例。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeVsync
 * @param nativeVsync 一个指向OH_NativeVSync实例的指针。
 * @since 9
 * @version 1.0
 */
void OH_NativeVSync_Destroy(OH_NativeVSync* nativeVsync);

/**
 * @brief 请求下一次vsync信号，当信号到来时，调用回调函数callback。
 * 如果在同一帧内中多次调用此接口，则只会触发最后一个回调。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeVsync
 * @param nativeVsync 一个指向OH_NativeVSync实例的指针。
 * @param callback 一个OH_NativeVSync_FrameCallback类型的函数指针，当下一次vsync信号到来时会被调用。
 * @param data 一个指向用户自定义数据结构的指针，类型是void*。
 * @return 返回值为0表示执行成功，其他返回值可参考{@link OHNativeErrorCode}。
 * @since 9
 * @version 1.0
 */
int OH_NativeVSync_RequestFrame(OH_NativeVSync* nativeVsync, OH_NativeVSync_FrameCallback callback, void* data);

/**
 * @brief 请求下一次vsync信号，当信号到来时，调用回调函数callback。
 * 如果在同一帧内中多次调用此接口，每一次传入的callback都会被执行。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeVsync
 * @param nativeVsync 一个指向OH_NativeVSync实例的指针。
 * @param callback 一个OH_NativeVSync_FrameCallback类型的函数指针，当下一次vsync信号到来时会被调用。
 * @param data 一个指向用户自定义数据结构的指针，类型是void*。
 * @return 返回值为0表示执行成功，其他返回值可参考{@link OHNativeErrorCode}。
 * @since 12
 * @version 1.0
 */
int OH_NativeVSync_RequestFrameWithMultiCallback(
    OH_NativeVSync* nativeVsync, OH_NativeVSync_FrameCallback callback, void* data);

/**
 * @brief 获取vsync周期。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeVsync
 * @param nativeVsync 一个指向OH_NativeVSync实例的指针。
 * @param period 用于获取vsync周期的变量。
 * @return 返回值为0表示执行成功，其他返回值可参考{@link OHNativeErrorCode}。
 * @since 10
 * @version 1.0
 */
int OH_NativeVSync_GetPeriod(OH_NativeVSync* nativeVsync, long long* period);

/**
 * @brief 启用DVSync以提高自绘制动画场景的流畅性。
 * DVSync是Decoupled VSync的缩写，它是一种与硬件VSync解耦的帧时序管理策略。\n
 * DVSync通过提前发送带有未来时间戳的VSync信号驱动后续动画帧的提前绘制，这些帧会被帧缓冲队列缓存；DVSync通过缓存的帧减少未来可能发生的丢帧，进而提高动画场景的流畅性。\n
 * 因为DVSync需要占用空闲的自绘制帧缓冲用于缓存提前绘制的动画帧，用户需要确保至少有一个空闲的帧缓冲区，否则不建议启用此功能。\n
 * 启用DVSync后，用户需要正确响应提前发送的VSync信号，并在前一个VSync对应的动画帧完成后再请求下一个VSync，且自绘制帧需要携带与VSync一致的时间戳。\n
 * 在动画结束之后，用户需要关闭DVSync。\n
 * 在不支持DVSync的平台或者如果有另一个应用程序已经启用了DVSync，则当前的启用操作将不会生效，应用程序仍将收到正常的VSync信号。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeVsync
 * @param nativeVsync 一个指向OH_NativeVSync实例的指针。
 * @param enable 表示打开或者关闭DVSync，true表示打开，false表示关闭。
 * @return 返回值为0表示执行成功，其他返回值可参考{@link OHNativeErrorCode}。
 * @since 14
 * @version 1.0
 */
int OH_NativeVSync_DVSyncSwitch(OH_NativeVSync* nativeVsync, bool enable);
#ifdef __cplusplus
}
#endif

#endif