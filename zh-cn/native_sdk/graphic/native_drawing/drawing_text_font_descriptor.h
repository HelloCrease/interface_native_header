/*
 * Copyright (c) 2024 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef DRAWING_TEXT_FONT_DESCRIPTOR_H
#define DRAWING_TEXT_FONT_DESCRIPTOR_H

/**
 * @addtogroup Drawing
 * @{
 *
 * @brief Drawing模块提供包括2D图形渲染、文字绘制和图片显示等功能函数。
 * 本模块采用屏幕物理像素单位px。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 *
 * @since 8
 * @version 1.0
 */

/**
 * @file drawing_text_font_descriptor.h
 *
 * @brief 定义了字体信息的相关接口，比如获取字体信息，查找指定字体等。
 *
 * 引用文件"native_drawing/drawing_text_font_descriptor.h"
 * @library libnative_drawing.so
 * @since 14
 * @version 1.0
 */

#include "drawing_text_typography.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief 获取与指定字体描述符匹配的所有系统字体描述符，其中{@link OH_Drawing_FontDescriptor}的path字段不作为有效的匹配
 * 字段，其余字段不是默认值时生效，如果参数{@link OH_Drawing_FontDescriptor}的所有字段都是默认值，则获取所有系统字体描述
 * 符。如果匹配失败，返回NULL。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_FontDescriptor {@link OH_Drawing_FontDescriptor}指针。 建议使用
 * {@link OH_Drawing_CreateFontDescriptor}获得有效的{@link OH_Drawing_FontDescriptor}实例，如果自己创建
 * {@link OH_Drawing_FontDescriptor}实例，请确保不用于匹配的字段是默认值。
 * @param size_t 表示返回值数组的成员个数。
 * @return {@link OH_Drawing_FontDescriptor}数组，释放时请使用{@link OH_Drawing_DestroyFontDescriptors}。
 * @since 14
 * @version 1.0
 */
OH_Drawing_FontDescriptor* OH_Drawing_MatchFontDescriptors(OH_Drawing_FontDescriptor*, size_t*);

/**
 * @brief 释放字体描述符{@link OH_Drawing_FontDescriptor}数组。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_FontDescriptor {@link OH_Drawing_FontDescriptor}数组。
 * @param size_t {@link OH_Drawing_FontDescriptor}数组的成员个数。
 * @since 14
 * @version 1.0
 */
void OH_Drawing_DestroyFontDescriptors(OH_Drawing_FontDescriptor*, size_t);

#ifdef __cplusplus
}
#endif
/** @} */
#endif