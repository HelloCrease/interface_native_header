/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef C_INCLUDE_DRAWING_REGISTER_FONT_H
#define C_INCLUDE_DRAWING_REGISTER_FONT_H

/**
 * @addtogroup Drawing
 * @{
 *
 * @brief Drawing模块提供包括2D图形渲染、文字绘制和图片显示等功能函数。
 * 本模块采用屏幕物理像素单位px。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 *
 * @since 8
 * @version 1.0
 */

/**
 * @file drawing_register_font.h
 *
 * @brief 定义绘制模块中字体管理器相关的函数。
 *
 * 引用文件"native_drawing/drawing_register_font.h"
 * @library libnative_drawing.so
 * @since 11
 * @version 1.0
 */

#include "drawing_text_declaration.h"
#include "drawing_types.h"

#ifdef __cplusplus
extern "C" {
#endif
/**
 * @brief 用于在字体管理器中注册自定义字体，支持的字体文件格式包含：ttf、otf。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_FontCollection 指向OH_Drawing_FontCollection对象的指针。
 * @param fontFamily 指需要注册的字体的字体名称。
 * @param familySrc 指需要注册的字体文件的路径。
 * @return 返回错误代码，0为成功，1为文件不存在，2为打开文件失败，3为读取文件失败，4为寻找文件失败，5为获取大小失败，9文件损坏。
 * @since 11
 * @version 1.0
 */
uint32_t OH_Drawing_RegisterFont(OH_Drawing_FontCollection*, const char* fontFamily, const char* familySrc);

/**
 * @brief 用于在字体管理器中注册字体缓冲区。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_FontCollection 指向OH_Drawing_FontCollection对象的指针。
 * @param fontFamily 指需要注册的字体的字体名称。
 * @param fontBuffer 指需要注册的字体文件的缓冲区。
 * @param length 指需要注册的字体文件的长度。
 * @return 返回错误代码，0为成功，6为缓冲区大小为零，7为字体集合为空，9文件损坏。
 * @since 11
 * @version 1.0
 */
uint32_t OH_Drawing_RegisterFontBuffer(OH_Drawing_FontCollection*, const char* fontFamily, uint8_t* fontBuffer,
    size_t length);

#ifdef __cplusplus
}
#endif
/** @} */
#endif