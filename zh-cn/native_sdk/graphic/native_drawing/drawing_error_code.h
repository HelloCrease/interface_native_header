/*
 * Copyright (c) 2024 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef C_INCLUDE_DRAWING_ERROR_CODE_H
#define C_INCLUDE_DRAWING_ERROR_CODE_H

/**
 * @addtogroup Drawing
 * @{
 *
 * @brief Drawing模块提供包括2D图形渲染、文字绘制和图片显示等功能函数。
 * 本模块采用屏幕物理像素单位px。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 *
 * @since 8
 * @version 1.0
 */

/**
 * @file drawing_error_code.h
 *
 * @brief 声明与绘图模块中的错误码相关的函数。
 *
 * 引用文件"native_drawing/drawing_error_code.h"
 * @library libnative_drawing.so
 * @since 12
 * @version 1.0
 */

#include "drawing_types.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief 枚举本模块可能产生的错误码。
 * @since 12
 */
typedef enum OH_Drawing_ErrorCode {
    /**
     * @error 操作成功完成。
     */
    OH_DRAWING_SUCCESS = 0,
    /**
     * @error 权限校验失败。
     */
    OH_DRAWING_ERROR_NO_PERMISSION = 201,
    /**
     * @error 无效的输入参数，如参数中传入了NULL。
     */
    OH_DRAWING_ERROR_INVALID_PARAMETER = 401,
    /**
     * @error 输入参数不在有效的范围内。
     */
    OH_DRAWING_ERROR_PARAMETER_OUT_OF_RANGE = 26200001,
    /**
     * @error 内存分配失败。
     * @since 13
     */
    OH_DRAWING_ERROR_ALLOCATION_FAILED = 26200002,
} OH_Drawing_ErrorCode;

/**
 * @brief 获取本模块的错误码。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @return 函数返回错误码枚举值。
 * @since 12
 * @version 1.0
 */
OH_Drawing_ErrorCode OH_Drawing_ErrorCodeGet();

#ifdef __cplusplus
}
#endif
/** @} */
#endif