/*
 * Copyright (c) 2024 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup Preferences
 * @{
 *
 * @brief 首选项模块（Preferences）提供Key-Value键值型数据（后续简称KV数据）的处理接口，实现对轻量级KV数据的查询、修改和持久化功能。
 *
 * @syscap SystemCapability.DistributedDataManager.Preferences.Core
 *
 * @since 13
 */

/**
 * @file oh_preferences.h
 *
 * @brief 提供访问Preferences对象的接口与数据结构。
 *
 * 引用文件<database/preferences/oh_preferences.h>
 * @library libohpreferences.so
 * @syscap SystemCapability.DistributedDataManager.Preferences.Core
 *
 * @since 13
 */

#ifndef OH_PREFERENCES_H
#define OH_PREFERENCES_H

#include <cstdint>

#include "oh_preferences_value.h"
#include "oh_preferences_option.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief 定义Preferences对象类型。
 *
 * @since 13
 */
typedef struct OH_Preferences OH_Preferences;

/**
 * @brief 定义数据变更触发的回调函数类型。
 *
 * @param context 应用上下文的指针。
 * @param pairs 发生变更的KV数据的指针。
 * @param count 发生变更的KV数据的数量。
 * @see OH_PreferencesPair
 * @since 13
 */
typedef void (*OH_PreferencesDataObserver)(void *context, const OH_PreferencesPair *pairs, uint32_t count);

/**
 * @brief 打开一个Preferences实例对象并创建指向它的指针。
 * 当不再需要使用指针时，请使用{@link OH_Preferences_Close}关闭实例对象。
 *
 * @param option 指向Preferences配置选项{@link OH_PreferencesOption}的指针。
 * @param errCode 该参数作为出参使用，表示指向返回错误码的指针，详见{@link OH_Preferences_ErrCode}。
 *         若错误码为PREFERENCES_OK，表示操作成功。
 *         若错误码为PREFERENCES_ERROR_INVALID_PARAM，表示参数不合法。
 *         若错误码为PREFERENCES_ERROR_NOT_SUPPORTED，表示系统能力不支持。
 *         若错误码为PREFERENCES_ERROR_DELETE_FILE，表示删除文件失败。
 *         若错误码为PREFERENCES_ERROR_STORAGE，表示存储异常。
 *         若错误码为PREFERENCES_ERROR_MALLOC，表示内存分配失败。
 * @return 当操作成功时，返回指向打开的Preferences对象{@link OH_Preferences}实例对象的指针，失败返回空指针。
 * @see OH_Preferences
 * @see OH_PreferencesOption
 * @see OH_Preferences_ErrCode
 * @since 13
 */
OH_Preferences *OH_Preferences_Open(OH_PreferencesOption *option, int *errCode);

/**
 * @brief 关闭一个Preferences实例对象。
 *
 * @param preference 指向需要关闭的Preferences{@link OH_Preferences}实例对象的指针。
 * @return 返回执行的错误码，详见{@link OH_Preferences_ErrCode}。
 *         若错误码为PREFERENCES_OK，表示操作成功。
 *         若错误码为PREFERENCES_ERROR_INVALID_PARAM，表示参数不合法。
 *         若错误码为PREFERENCES_ERROR_STORAGE，表示存储异常。
 *         若错误码为PREFERENCES_ERROR_MALLOC，表示内存分配失败。
 * @see OH_Preferences
 * @see OH_Preferences_ErrCode
 * @since 13
 */
int OH_Preferences_Close(OH_Preferences *preference);

/**
 * @brief 获取Preferences实例对象中Key对应的整型值。
 *
 * @param preference 指向目标Preferences{@link OH_Preferences}实例对象的指针。
 * @param key 需要获取的Key的指针。
 * @param value 该参数作为出参使用，表示指向获取到的整型值的指针。
 * @return 返回执行的错误码。
 *         若错误码为PREFERENCES_OK，表示操作成功。
 *         若错误码为PREFERENCES_ERROR_INVALID_PARAM，表示参数不合法。
 *         若错误码为PREFERENCES_ERROR_STORAGE，表示存储异常。
 *         若错误码为PREFERENCES_ERROR_MALLOC，表示内存分配失败。
 *         若错误码为PREFERENCES_ERROR_KEY_NOT_FOUND，表示查询的Key不存在。
 * @see OH_Preferences
 * @see OH_Preferences_ErrCode
 * @since 13
 */
int OH_Preferences_GetInt(OH_Preferences *preference, const char *key, int *value);

/**
 * @brief 获取Preferences实例对象中Key对应的布尔值。
 *
 * @param preference 指向目标Preferences{@link OH_Preferences}实例对象的指针。
 * @param key 需要获取的Key的指针。
 * @param value 该参数作为出参使用，表示指向获取到的布尔值的指针。
 * @return 返回执行的错误码。
 *         若错误码为PREFERENCES_OK，表示操作成功。
 *         若错误码为PREFERENCES_ERROR_INVALID_PARAM，表示参数不合法。
 *         若错误码为PREFERENCES_ERROR_STORAGE，表示存储异常。
 *         若错误码为PREFERENCES_ERROR_MALLOC，表示内存分配失败。
 *         若错误码为PREFERENCES_ERROR_KEY_NOT_FOUND，表示查询的key不存在。
 * @see OH_Preferences
 * @see OH_Preferences_ErrCode
 * @since 13
 */
int OH_Preferences_GetBool(OH_Preferences *preference, const char *key, bool *value);

/**
 * @brief 获取Preferences实例对象中Key对应的字符串。
 *
 * @param preference 指向目标Preferences{@link OH_Preferences}实例对象的指针。
 * @param key 需要获取的Key的指针。
 * @param value 该参数作为出参使用，表示指向获取到的字符串的二级指针，使用完毕后需要调用释放函数{@link OH_Preferences_FreeString}释放内存。
 * @param valueLen 该参数作为出参使用，表示获取到的字符串长度的指针。
 * @return 返回执行的错误码。
 *         若错误码为PREFERENCES_OK，表示操作成功。
 *         若错误码为PREFERENCES_ERROR_INVALID_PARAM，表示参数不合法。
 *         若错误码为PREFERENCES_ERROR_STORAGE，表示存储异常。
 *         若错误码为PREFERENCES_ERROR_MALLOC，表示内存分配失败。
 *         若错误码为PREFERENCES_ERROR_KEY_NOT_FOUND，表示查询的Key不存在。
 * @see OH_Preferences
 * @see OH_Preferences_ErrCode
 * @since 13
 */
int OH_Preferences_GetString(OH_Preferences *preference, const char *key, char **value, uint32_t *valueLen);

/**
 * @brief 释放从Preferences实例对象中获取的字符串。
 *
 * @param string 需要释放的字符串指针。
 * @see OH_Preferences
 * @since 13
 */
void OH_Preferences_FreeString(char *string);

/**
 * @brief 根据Key设置Preferences实例对象中的整型值。
 *
 * @param preference 指向目标Preferences{@link OH_Preferences}实例对象的指针。
 * @param key 指向需要设置的Key的指针。
 * @param value 需要设置的整型值。
 * @return 返回执行的错误码。
 *         若错误码为PREFERENCES_OK，表示操作成功。
 *         若错误码为PREFERENCES_ERROR_INVALID_PARAM，表示参数不合法。
 *         若错误码为PREFERENCES_ERROR_STORAGE，表示存储异常。
 *         若错误码为PREFERENCES_ERROR_MALLOC，表示内存分配失败。
 * @see OH_Preferences
 * @see OH_Preferences_ErrCode
 * @since 13
 */
int OH_Preferences_SetInt(OH_Preferences *preference, const char *key, int value);

/**
 * @brief 根据Key设置Preferences实例对象中的布尔值。
 *
 * @param preference 指向目标Preferences{@link OH_Preferences}实例对象的指针。
 * @param key 指向需要设置的Key的指针。
 * @param value 需要设置的布尔值。
 * @return 返回执行的错误码。
 *         若错误码为PREFERENCES_OK，表示操作成功。
 *         若错误码为PREFERENCES_ERROR_INVALID_PARAM，表示参数不合法。
 *         若错误码为PREFERENCES_ERROR_STORAGE，表示存储异常。
 *         若错误码为PREFERENCES_ERROR_MALLOC，表示内存分配失败。
 * @see OH_Preferences
 * @see OH_Preferences_ErrCode
 * @since 13
 */
int OH_Preferences_SetBool(OH_Preferences *preference, const char *key, bool value);

/**
 * @brief 根据Key设置Preferences实例对象中的字符串。
 *
 * @param preference 指向目标Preferences{@link OH_Preferences}实例对象的指针。
 * @param key 指向需要设置的Key的指针。
 * @param value 指向需要设置的字符串指针。
 * @return 返回执行的错误码。
 *         若错误码为PREFERENCES_OK，表示操作成功。
 *         若错误码为PREFERENCES_ERROR_INVALID_PARAM，表示参数不合法。
 *         若错误码为PREFERENCES_ERROR_STORAGE，表示存储异常。
 *         若错误码为PREFERENCES_ERROR_MALLOC，表示内存分配失败。
 * @see OH_Preferences
 * @see OH_Preferences_ErrCode
 * @since 13
 */
int OH_Preferences_SetString(OH_Preferences *preference, const char *key, const char *value);

/**
 * @brief 在Preferences实例对象中删除Key对应的KV数据。
 *
 * @param preference 指向目标Preferences{@link OH_Preferences}实例对象的指针。
 * @param key 指向需要删除的Key的指针。
 * @return 返回执行的错误码。
 *         若错误码为PREFERENCES_OK，表示操作成功。
 *         若错误码为PREFERENCES_ERROR_INVALID_PARAM，表示参数不合法。
 *         若错误码为PREFERENCES_ERROR_STORAGE，表示存储异常。
 *         若错误码为PREFERENCES_ERROR_MALLOC，表示内存分配失败。
 * @see OH_Preferences
 * @see OH_Preferences_ErrCode
 * @since 13
 */
int OH_Preferences_Delete(OH_Preferences *preference, const char *key);

/**
 * @brief 对选取的Key注册数据变更订阅。订阅的Key的值发生变更后，在调用OH_Preferences_Close()后触发回调。
 *
 * @param preference 指向目标Preferences{@link OH_Preferences}实例对象的指针。
 * @param context 应用上下文的指针。
 * @param observer 订阅数据变更关联的回调函数{@link OH_PreferencesDataObserver}。
 * @param keys 需要订阅的Key数组。
 * @param keyCount 需要订阅的Key的数量。
 * @return 返回执行的错误码。
 *         若错误码为PREFERENCES_OK，表示操作成功。
 *         若错误码为PREFERENCES_ERROR_INVALID_PARAM，表示参数不合法。
 *         若错误码为PREFERENCES_ERROR_STORAGE，表示存储异常。
 *         若错误码为PREFERENCES_ERROR_MALLOC，表示内存分配失败。
 *         若错误码为PREFERENCES_ERROR_GET_DATAOBSMGRCLIENT，表示获取数据变更订阅服务失败。
 * @see OH_Preferences
 * @see OH_PreferencesDataObserver
 * @see OH_Preferences_ErrCode
 * @since 13
 */
int OH_Preferences_RegisterDataObserver(OH_Preferences *preference, void *context,
    OH_PreferencesDataObserver observer, const char *keys[], uint32_t keyCount);

/**
 * @brief 取消注册选取Key的数据变更订阅。
 *
 * @param preference 指向目标Preferences{@link OH_Preferences}实例对象的指针。
 * @param context 应用上下文的指针。
 * @param observer 订阅数据变更关联的回调函数{@link OH_PreferencesDataObserver}。
 * @param keys 需要取消订阅的Key数组。
 * @param keyCount 需要取消订阅的Key的数量。
 * @return 返回执行的错误码。
 *         若错误码为PREFERENCES_OK，表示操作成功。
 *         若错误码为PREFERENCES_ERROR_INVALID_PARAM，表示参数不合法。
 *         若错误码为PREFERENCES_ERROR_STORAGE，表示存储异常。
 *         若错误码为PREFERENCES_ERROR_MALLOC，表示内存分配失败。
 * @see OH_Preferences
 * @see OH_PreferencesDataObserver
 * @see OH_Preferences_ErrCode
 * @since 13
 */
int OH_Preferences_UnregisterDataObserver(OH_Preferences *preference, void *context,
    OH_PreferencesDataObserver observer, const char *keys[], uint32_t keyCount);

#ifdef __cplusplus
};
#endif

/** @} */
#endif // OH_PREFERENCES_H