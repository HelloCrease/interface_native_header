/*
 * Copyright (c) 2024 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup UDMF
 * @{
 *
 * @brief 统一数据管理框架旨在定义数据跨应用、跨设备以及跨平台过程中的各项标准，
 * 提供统一的OpenHarmony数据语言和标准化的数据接入与读取通路。
 *
 * @syscap SystemCapability.DistributedDataManager.UDMF.Core
 *
 * @since 12
 */

/**
 * @file uds.h
 *
 * @brief 提供标准化数据结构相关接口函数、结构体定义。
 * 引用文件：<database/udmf/uds.h>
 * @library libudmf.so
 * @syscap SystemCapability.DistributedDataManager.UDMF.Core
 * @since 12
 */

#ifndef UDS_H
#define UDS_H

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief 描述纯文本类型数据的统一数据结构。
 *
 * @since 12
 */
typedef struct OH_UdsPlainText OH_UdsPlainText;

/**
 * @brief 描述超链接类型的统一数据结构。
 *
 * @since 12
 */
typedef struct OH_UdsHyperlink OH_UdsHyperlink;

/**
 * @brief 描述超文本标记语言类型的统一数据结构。
 *
 * @since 12
 */
typedef struct OH_UdsHtml OH_UdsHtml;

/**
 * @brief 描述桌面图标类型的统一数据结构。
 *
 * @since 12
 */
typedef struct OH_UdsAppItem OH_UdsAppItem;

/**
* @brief 描述文件Uri类型的统一数据结构。
*
* @since 13
*/
typedef struct OH_UdsFileUri OH_UdsFileUri;

/**
* @brief 描述像素图片类型的统一数据结构。
*
* @since 13
*/
typedef struct OH_UdsPixelMap OH_UdsPixelMap;

/**
* @brief 描述ArrayBuffer类型的统一数据结构。
*
* @since 13
*/
typedef struct OH_UdsArrayBuffer OH_UdsArrayBuffer;

/**
 * @brief 创建纯文本类型{@link OH_UdsPlainText}指针及实例对象。
 * 当不再需要使用指针时，请使用{@link OH_UdsPlainText_Destroy}销毁实例对象，否则会导致内存泄漏。
 *
 * @return 执行成功则返回一个指向纯文本类型{@link OH_UdsPlainText}实例对象的指针，否则返回nullptr。
 * @see OH_UdsPlainText
 * @since 12
 */
OH_UdsPlainText* OH_UdsPlainText_Create();

/**
 * @brief 销毁纯文本类型数据{@link OH_UdsPlainText}指针指向的实例对象。
 *
 * @param pThis 表示指向{@link OH_UdsPlainText}实例的指针。
 * @see OH_UdsPlainText
 * @since 12
 */
void OH_UdsPlainText_Destroy(OH_UdsPlainText* pThis);

/**
 * @brief 从纯文本类型{@link OH_UdsPlainText}中获取类型ID。
 *
 * @param pThis 表示指向纯文本类型{@link OH_UdsPlainText}实例的指针。
 * @return 输入有效入参时返回类型ID的字符串指针，否则返回nullptr。
 * @see OH_UdsPlainText
 * @since 12
 */
const char* OH_UdsPlainText_GetType(OH_UdsPlainText* pThis);

/**
 * @brief 从纯文本类型{@link OH_UdsPlainText}中获取纯文本内容信息。
 *
 * @param pThis 表示指向纯文本类型{@link OH_UdsPlainText}实例的指针。
 * @return 输入有效入参时返回纯文本内容信息的字符串指针，否则返回nullptr。
 * @see OH_UdsPlainText
 * @since 12
 */
const char* OH_UdsPlainText_GetContent(OH_UdsPlainText* pThis);

/**
 * @brief 从纯文本类型{@link OH_UdsPlainText}中获取纯文本摘要信息。
 *
 * @param pThis 表示指向纯文本类型{@link OH_UdsPlainText}实例的指针。
 * @return 输入有效入参时返回纯文本摘要信息的字符串指针，否则返回nullptr。
 * @see OH_UdsPlainText
 * @since 12
 */
const char* OH_UdsPlainText_GetAbstract(OH_UdsPlainText* pThis);

/**
 * @brief 设置纯文本类型{@link OH_UdsPlainText}中的纯文本内容参数。
 *
 * @param pThis 表示指向纯文本类型{@link OH_UdsPlainText}实例的指针。
 * @param content 表示纯文本内容参数。
 * @return 返回执行的错误码。请参阅错误码定义{@link Udmf_ErrCode}。
 *         若返回UDMF_E_OK，表示执行成功。
 *         若返回UDMF_E_INVALID_PARAM，表示传入了无效参数。
 * @see OH_UdsPlainText
 * @since 12
 */
int OH_UdsPlainText_SetContent(OH_UdsPlainText* pThis, const char* content);

/**
 * @brief 设置纯文本类型{@link OH_UdsPlainText}中的纯文本摘要参数。
 *
 * @param pThis 表示指向纯文本类型{@link OH_UdsPlainText}实例的指针。
 * @param abstract 表示纯文本摘要参数。
 * @return 返回执行的错误码。请参阅错误码定义{@link Udmf_ErrCode}。
 *         若返回UDMF_E_OK，表示执行成功。
 *         若返回UDMF_E_INVALID_PARAM，表示传入了无效参数。
 * @see OH_UdsPlainText
 * @since 12
 */
int OH_UdsPlainText_SetAbstract(OH_UdsPlainText* pThis, const char* abstract);

/**
 * @brief 创建超链接类型{@link OH_UdsHyperlink}指针及实例对象。
 * 当不再需要使用指针时，请使用{@link OH_UdsHyperlink_Destroy}销毁实例对象，否则会导致内存泄漏。
 *
 * @return 执行则成功返回一个指向超链接类型{@link OH_UdsHyperlink}实例对象的指针，否则返回nullptr。
 * @see OH_UdsHyperlink
 * @since 12
 */
OH_UdsHyperlink* OH_UdsHyperlink_Create();

/**
 * @brief 销毁超链接类型{@link OH_UdsHyperlink}指针指向的实例对象。
 *
 * @param pThis 表示指向超链接类型{@link OH_UdsHyperlink}实例的指针。
 * @see OH_UdsHyperlink
 * @since 12
 */
void OH_UdsHyperlink_Destroy(OH_UdsHyperlink* pThis);

/**
 * @brief 从超链接类型{@link OH_UdsHyperlink}中获取类型ID。
 *
 * @param pThis 表示指向超链接类型{@link OH_UdsHyperlink}实例的指针。
 * @return 输入有效入参时返回类型ID的字符串指针，否则返回nullptr。
 * @see OH_UdsHyperlink
 * @since 12
 */
const char* OH_UdsHyperlink_GetType(OH_UdsHyperlink* pThis);

/**
 * @brief 从超链接类型{@link OH_UdsHyperlink}中获取URL参数。
 *
 * @param pThis 表示指向超链接类型{@link OH_UdsHyperlink}实例的指针。
 * @return 输入有效入参时返回URL参数的字符串指针，否则返回nullptr。
 * @see OH_UdsHyperlink
 * @since 12
 */
const char* OH_UdsHyperlink_GetUrl(OH_UdsHyperlink* pThis);

/**
 * @brief 从超链接类型{@link OH_UdsHyperlink}中获取描述参数。
 *
 * @param pThis 表示指向超链接类型{@link OH_UdsHyperlink}实例的指针。
 * @return 输入有效入参时返回描述参数的字符串指针，否则返回nullptr。
 * @see OH_UdsHyperlink
 * @since 12
 */
const char* OH_UdsHyperlink_GetDescription(OH_UdsHyperlink* pThis);

/**
 * @brief 设置超链接类型{@link OH_UdsHyperlink}实例中URL参数。
 *
 * @param pThis 表示指向超链接类型{@link OH_UdsHyperlink}实例的指针。
 * @param url 表示URL参数。
 * @return 返回执行的错误码。请参阅错误码定义{@link Udmf_ErrCode}。
 *         若返回UDMF_E_OK，表示执行成功。
 *         若返回UDMF_E_INVALID_PARAM，表示传入了无效参数。
 * @see OH_UdsHyperlink
 * @since 12
 */
int OH_UdsHyperlink_SetUrl(OH_UdsHyperlink* pThis, const char* url);

/**
 * @brief 设置超链接类型{@link OH_UdsHyperlink}实例中描述参数。
 *
 * @param pThis 表示指向超链接类型{@link OH_UdsHyperlink}实例的指针。
 * @param description 表示描述信息。
 * @return 返回执行的错误码。请参阅错误码定义{@link Udmf_ErrCode}。
 *         若返回UDMF_E_OK，表示执行成功。
 *         若返回UDMF_E_INVALID_PARAM，表示传入了无效参数。
 * @see OH_UdsHyperlink
 * @since 12
 */
int OH_UdsHyperlink_SetDescription(OH_UdsHyperlink* pThis, const char* description);

/**
 * @brief 创建超文本标记语言类型{@link OH_UdsHtml}指针及实例对象。
 * 当不再需要使用指针时，请使用{@link OH_UdsHtml_Destroy}销毁实例对象，否则会导致内存泄漏。
 *
 * @return 执行成功则返回一个指向超文本标记语言类型{@link OH_UdsHtml}实例对象的指针，否则返回nullptr。
 * @see OH_UdsHtml
 * @since 12
 */
OH_UdsHtml* OH_UdsHtml_Create();

/**
 * @brief 销毁超文本标记语言类型{@link OH_UdsHtml}指针指向的实例对象。
 *
 * @param pThis 表示指向超文本标记语言类型{@link OH_UdsHtml}实例的指针。
 * @see OH_UdsHtml
 * @since 12
 */
void OH_UdsHtml_Destroy(OH_UdsHtml* pThis);

/**
 * @brief 获取超文本标记语言类型{@link OH_UdsHtml}对象中类型ID。
 *
 * @param pThis 表示指向超文本标记语言类型{@link OH_UdsHtml}实例的指针。
 * @return 输入有效入参时返回类型ID的字符串指针，否则返回nullptr。
 * @see OH_UdsHtml
 * @since 12
 */
const char* OH_UdsHtml_GetType(OH_UdsHtml* pThis);

/**
 * @brief 获取超文本标记语言类型{@link OH_UdsHtml}对象中HTML格式内容参数。
 *
 * @param pThis 表示指向超文本标记语言类型{@link OH_UdsHtml}实例的指针。
 * @return 输入有效入参时返回HTML格式内容的字符串指针，否则返回nullptr。
 * @see OH_UdsHtml
 * @since 12
 */
const char* OH_UdsHtml_GetContent(OH_UdsHtml* pThis);

/**
 * @brief 获取超文本标记语言类型{@link OH_UdsHtml}对象中的纯文本内容参数。
 *
 * @param pThis 表示指向超文本标记语言类型{@link OH_UdsHtml}实例的指针。
 * @return 输入有效入参时返回纯文本内容的字符串指针，否则返回nullptr。
 * @see OH_UdsHtml
 * @since 12
 */
const char* OH_UdsHtml_GetPlainContent(OH_UdsHtml* pThis);

/**
 * @brief 设置超文本标记语言类型{@link OH_UdsHtml}中的HTML格式内容参数。
 *
 * @param pThis 表示指向超文本标记语言类型{@link OH_UdsHtml}实例的指针。
 * @param content 表示HTML格式内容参数。
 * @return 返回执行的错误码。请参阅错误码定义{@link Udmf_ErrCode}。
 *         若返回UDMF_E_OK，表示执行成功。
 *         若返回UDMF_E_INVALID_PARAM，表示传入了无效参数。
 * @see OH_UdsHtml
 * @since 12
 */
int OH_UdsHtml_SetContent(OH_UdsHtml* pThis, const char* content);

/**
 * @brief 设置超文本标记语言类型{@link OH_UdsHtml}中的纯文本内容参数。
 *
 * @param pThis 表示指向超文本标记语言类型{@link OH_UdsHtml}实例的指针。
 * @param plainContent 表示纯文本内容参数。
 * @return 返回执行的错误码。请参阅错误码定义{@link Udmf_ErrCode}。
 *         若返回UDMF_E_OK，表示执行成功。
 *         若返回UDMF_E_INVALID_PARAM，表示传入了无效参数。
 * @see OH_UdsHtml
 * @since 12
 */
int OH_UdsHtml_SetPlainContent(OH_UdsHtml* pThis, const char* plainContent);

/**
 * @brief 创建桌面图标类型{@link OH_UdsAppItem}指针及实例对象。
 * 当不再需要使用指针时，请使用{@link OH_UdsAppItem_Destroy}销毁实例对象，否则会导致内存泄漏。
 *
 * @return 执行成功返则回一个指向桌面图标类型{@link OH_UdsAppItem}实例对象的指针，否则返回nullptr。
 * @see OH_UdsAppItem
 * @since 12
 */
OH_UdsAppItem* OH_UdsAppItem_Create();

/**
 * @brief 销毁桌面图标类型{@link OH_UdsAppItem}指针指向的实例对象。
 *
 * @param pThis 表示一个指向桌面图标类型{@link OH_UdsAppItem}对象的指针。
 * @see OH_UdsAppItem
 * @since 12
 */
void OH_UdsAppItem_Destroy(OH_UdsAppItem* pThis);

/**
 * @brief 从桌面图标类型{@link OH_UdsAppItem}实例获取类型ID。
 *
 * @param pThis 表示一个指向桌面图标类型{@link OH_UdsAppItem}对象的指针。
 * @return 输入有效入参时返回类型ID的字符串指针，否则返回nullptr。
 * @see OH_UdsAppItem
 * @since 12
 */
const char* OH_UdsAppItem_GetType(OH_UdsAppItem* pThis);

/**
 * @brief 从桌面图标类型{@link OH_UdsAppItem}实例中获取应用ID。
 *
 * @param pThis 表示一个指向桌面图标类型{@link OH_UdsAppItem}对象的指针。
 * @return 输入有效入参时返回应用ID的字符串指针，否则返回nullptr。
 * @see OH_UdsAppItem
 * @since 12
 */
const char* OH_UdsAppItem_GetId(OH_UdsAppItem* pThis);

/**
 * @brief 从桌面图标类型{@link OH_UdsAppItem}实例中获取应用名称。
 *
 * @param pThis 表示一个指向桌面图标类型{@link OH_UdsAppItem}对象的指针。
 * @return 输入有效入参时返回应用名称的字符串指针，否则返回nullptr。
 * @see OH_UdsAppItem
 * @since 12
 */
const char* OH_UdsAppItem_GetName(OH_UdsAppItem* pThis);

/**
 * @brief 从桌面图标类型{@link OH_UdsAppItem}实例中获取图片ID。
 *
 * @param pThis 表示一个指向桌面图标类型{@link OH_UdsAppItem}对象的指针。
 * @return 输入有效入参时返回图片ID的字符串指针，否则返回nullptr。
 * @see OH_UdsAppItem
 * @since 12
 */
const char* OH_UdsAppItem_GetIconId(OH_UdsAppItem* pThis);

/**
 * @brief 从桌面图标类型{@link OH_UdsAppItem}实例中获取标签ID。
 *
 * @param pThis 表示一个指向桌面图标类型{@link OH_UdsAppItem}对象的指针。
 * @return 输入有效入参时返回标签ID的字符串指针，否则返回nullptr。
 * @see OH_UdsAppItem
 * @since 12
 */
const char* OH_UdsAppItem_GetLabelId(OH_UdsAppItem* pThis);

/**
 * @brief 从桌面图标类型{@link OH_UdsAppItem}实例中获取bundle名称。
 *
 * @param pThis 表示一个指向桌面图标类型{@link OH_UdsAppItem}对象的指针。
 * @return 输入有效入参时返回bundle名称的字符串指针，否则返回nullptr。
 * @see OH_UdsAppItem
 * @since 12
 */
const char* OH_UdsAppItem_GetBundleName(OH_UdsAppItem* pThis);

/**
 * @brief 从桌面图标类型{@link OH_UdsAppItem}实例中ability名称。
 *
 * @param pThis 表示一个指向桌面图标类型{@link OH_UdsAppItem}对象的指针。
 * @return 输入有效入参时返回ability名称的字符串指针，否则返回nullptr。
 * @see OH_UdsAppItem
 * @since 12
 */
const char* OH_UdsAppItem_GetAbilityName(OH_UdsAppItem* pThis);

/**
 * @brief 设置桌面图标类型{@link OH_UdsAppItem}对象的应用ID。
 *
 * @param pThis 表示一个指向桌面图标类型{@link OH_UdsAppItem}对象的指针。
 * @param appId 表示应用ID。
 * @return 返回执行的错误码。请参阅错误码定义{@link Udmf_ErrCode}。
 *         若返回UDMF_E_OK，表示执行成功。
 *         若返回UDMF_E_INVALID_PARAM，表示传入了无效参数。
 * @see OH_UdsAppItem
 * @since 12
 */
int OH_UdsAppItem_SetId(OH_UdsAppItem* pThis, const char* appId);

/**
 * @brief 设置桌面图标类型{@link OH_UdsAppItem}对象的应用名称。
 *
 * @param pThis 表示一个指向桌面图标类型{@link OH_UdsAppItem}对象的指针。
 * @param appName 表示应用名称。
 * @return 返回执行的错误码。请参阅错误码定义{@link Udmf_ErrCode}。
 *         若返回UDMF_E_OK，表示执行成功。
 *         若返回UDMF_E_INVALID_PARAM，表示传入了无效参数。
 * @see OH_UdsAppItem
 * @since 12
 */
int OH_UdsAppItem_SetName(OH_UdsAppItem* pThis, const char* appName);

/**
 * @brief 设置桌面图标类型{@link OH_UdsAppItem}对象的图片ID。
 *
 * @param pThis 表示一个指向桌面图标类型{@link OH_UdsAppItem}对象的指针。
 * @param appIconId 表示图片ID。
 * @return 返回执行的错误码。请参阅错误码定义{@link Udmf_ErrCode}。
 *         若返回UDMF_E_OK，表示执行成功。
 *         若返回UDMF_E_INVALID_PARAM，表示传入了无效参数。
 * @see OH_UdsAppItem
 * @since 12
 */
int OH_UdsAppItem_SetIconId(OH_UdsAppItem* pThis, const char* appIconId);

/**
 * @brief 设置桌面图标类型{@link OH_UdsAppItem}对象的标签ID。
 *
 * @param pThis 表示一个指向桌面图标类型{@link OH_UdsAppItem}对象的指针。
 * @param appLabelId 表示标签ID。
 * @return 返回执行的错误码。请参阅错误码定义{@link Udmf_ErrCode}。
 *         若返回UDMF_E_OK，表示执行成功。
 *         若返回UDMF_E_INVALID_PARAM，表示传入了无效参数。
 * @see OH_UdsAppItem
 * @since 12
 */
int OH_UdsAppItem_SetLabelId(OH_UdsAppItem* pThis, const char* appLabelId);

/**
 * @brief 设置桌面图标类型{@link OH_UdsAppItem}对象的bundle名称。
 *
 * @param pThis 表示一个指向桌面图标类型{@link OH_UdsAppItem}对象的指针。
 * @param bundleName 表示bundle名称。
 * @return 返回执行的错误码。请参阅错误码定义{@link Udmf_ErrCode}。
 *         若返回UDMF_E_OK，表示执行成功。
 *         若返回UDMF_E_INVALID_PARAM，表示传入了无效参数。
 * @see OH_UdsAppItem
 * @since 12
 */
int OH_UdsAppItem_SetBundleName(OH_UdsAppItem* pThis, const char* bundleName);

/**
 * @brief 设置桌面图标类型{@link OH_UdsAppItem}对象的ability名称。
 *
 * @param pThis 表示一个指向桌面图标类型{@link OH_UdsAppItem}对象的指针。
 * @param abilityName 表示ability名称。
 * @return 返回执行的错误码。请参阅错误码定义{@link Udmf_ErrCode}。
 *         若返回UDMF_E_OK，表示执行成功。
 *         若返回UDMF_E_INVALID_PARAM，表示传入了无效参数。
 * @see OH_UdsAppItem
 * @since 12
 */
int OH_UdsAppItem_SetAbilityName(OH_UdsAppItem* pThis, const char* abilityName);

/**
 * @brief 创建文件Uri类型{@link OH_UdsFileUri}的实例对象以及指向它的指针。
 * 当不再需要使用指针时，请使用{@link OH_UdsFileUri_Destroy}销毁实例对象，否则会导致内存泄漏。
 *
 * @return 执行成功则返回一个指向文件Uri类型{@link OH_UdsFileUri}实例对象的指针，否则返回nullptr。
 * @see OH_UdsFileUri
 * @since 13
 */
OH_UdsFileUri* OH_UdsFileUri_Create();

/**
 * @brief 销毁文件Uri类型{@link OH_UdsFileUri}的实例对象。
 *
 * @param pThis 表示指向文件Uri类型{@link OH_UdsFileUri}实例的指针。
 * @see OH_UdsFileUri
 * @since 13
 */
void OH_UdsFileUri_Destroy(OH_UdsFileUri* pThis);

/**
 * @brief 从文件Uri类型{@link OH_UdsFileUri}实例中获取类型ID。
 *
 * @param pThis 表示指向文件Uri类型{@link OH_UdsFileUri}实例的指针。
 * @return 输入有效入参时返回类型ID的字符串指针，否则返回nullptr。
 * @see OH_UdsFileUri
 * @since 13
 */
const char* OH_UdsFileUri_GetType(OH_UdsFileUri* pThis);

/**
 * @brief 从文件Uri类型{@link OH_UdsFileUri}实例中获取文件Uri。
 *
 * @param pThis 表示指向文件Uri类型{@link OH_UdsFileUri}实例的指针。
 * @return 输入有效入参时返回文件Uri的字符串指针，否则返回nullptr。
 * @see OH_UdsFileUri
 * @since 13
 */
const char* OH_UdsFileUri_GetFileUri(OH_UdsFileUri* pThis);

/**
 * @brief 从文件Uri类型{@link OH_UdsFileUri}实例中获取文件类型。
 *
 * @param pThis 表示指向文件Uri类型{@link OH_UdsFileUri}实例的指针。
 * @return 输入有效入参时返回文件类型的字符串指针，否则返回nullptr。
 * @see OH_UdsFileUri
 * @since 13
 */
const char* OH_UdsFileUri_GetFileType(OH_UdsFileUri* pThis);

/**
 * @brief 设置文件Uri类型{@link OH_UdsFileUri}对象的Uri信息。
 *
 * @param pThis 表示指向文件Uri类型{@link OH_UdsFileUri}实例的指针。
 * @param fileUri 表示文件Uri。
 * @return 返回执行的错误码。请参阅错误码定义{@link Udmf_ErrCode}。
 *         若返回UDMF_E_OK，表示执行成功。
 *         若返回UDMF_E_INVALID_PARAM，表示传入了无效参数。
 * @see OH_UdsFileUri
 * @see Udmf_ErrCode
 * @since 13
 */
int OH_UdsFileUri_SetFileUri(OH_UdsFileUri* pThis, const char* fileUri);

/**
 * @brief 设置文件Uri类型{@link OH_UdsFileUri}对象的文件类型。
 *
 * @param pThis 表示指向文件Uri类型{@link OH_UdsFileUri}实例的指针。
 * @param fileType 表示文件类型。
 * @return 返回执行的错误码。请参阅错误码定义{@link Udmf_ErrCode}。
 *         若返回UDMF_E_OK，表示执行成功。
 *         若返回UDMF_E_INVALID_PARAM，表示传入了无效参数。
 * @see OH_UdsFileUri
 * @see Udmf_ErrCode
 * @since 13
 */
int OH_UdsFileUri_SetFileType(OH_UdsFileUri* pThis, const char* fileType);

/**
 * @brief 创建像素图片类型{@link OH_UdsPixelMap}的实例对象以及指向它的指针。
 * 当不再需要使用指针时，请使用{@link OH_UdsPixelMap_Destroy}销毁实例对象，否则会导致内存泄漏。
 *
 * @return 执行成功则返回一个指向像素图片类型{@link OH_UdsPixelMap}实例对象的指针，否则返回nullptr。
 * @see OH_UdsPixelMap
 * @since 13
 */
OH_UdsPixelMap* OH_UdsPixelMap_Create();

/**
 * @brief 销毁像素图片类型{@link OH_UdsPixelMap}的实例对象。
 *
 * @param pThis 表示指向像素图片类型{@link OH_UdsPixelMap}实例的指针。
 * @see OH_UdsPixelMap
 * @since 13
 */
void OH_UdsPixelMap_Destroy(OH_UdsPixelMap* pThis);

/**
 * @brief 从像素图片类型{@link OH_UdsPixelMap}实例中获取类型ID。
 *
 * @param pThis 表示指向像素图片类型{@link OH_UdsPixelMap}实例的指针。
 * @return 输入有效入参时返回类型ID的字符串指针，否则返回nullptr。
 * @see OH_UdsPixelMap
 * @since 13
 */
const char* OH_UdsPixelMap_GetType(OH_UdsPixelMap* pThis);

/**
 * @brief 从像素图片类型{@link OH_UdsPixelMap}实例中获取像素图片{@link OH_PixelmapNative}实例的指针。
 *
 * @param pThis 表示指向像素图片类型{@link OH_UdsPixelMap}实例的指针。
 * @param pixelmapNative 该参数是输出参数，表示指向像素图片{@link OH_PixelmapNative}实例的指针。
 * @see OH_UdsPixelMap
 * @see OH_PixelmapNative
 * @since 13
 */
void OH_UdsPixelMap_GetPixelMap(OH_UdsPixelMap* pThis, OH_PixelmapNative* pixelmapNative);

/**
 * @brief 设置像素图片类型{@link OH_UdsPixelMap}对象的像素图片内容。
 *
 * @param pThis 表示指向像素图片类型{@link OH_UdsPixelMap}实例的指针。
 * @param pixelmapNative 表示指向像素图片{@link OH_PixelmapNative}实例的指针
 * @return 返回执行的错误码。请参阅错误码定义{@link Udmf_ErrCode}。
 *         若返回UDMF_E_OK，表示执行成功。
 *         若返回UDMF_E_INVALID_PARAM，表示传入了无效参数。
 * @see OH_UdsPixelMap
 * @see OH_PixelmapNative
 * @see Udmf_ErrCode
 * @since 13
 */
int OH_UdsPixelMap_SetPixelMap(OH_UdsPixelMap* pThis, OH_PixelmapNative* pixelmapNative);

/**
 * @brief 创建ArrayBuffer类型{@link OH_UdsArrayBuffer}的实例对象以及指向它的指针。
 * 当不再需要使用指针时，请使用{@link OH_UdsArrayBuffer_Destroy}销毁实例对象，否则会导致内存泄漏。
 *
 * @return 执行成功则返回一个指向ArrayBuffer类型{@link OH_UdsArrayBuffer}实例对象的指针，否则返回nullptr。
 * @see OH_UdsArrayBuffer
 * @since 13
 */
OH_UdsArrayBuffer* OH_UdsArrayBuffer_Create();

/**
 * @brief 销毁ArrayBuffer类型{@link OH_UdsArrayBuffer}的实例对象。
 *
 * @param buffer 表示指向ArrayBuffer类型{@link OH_UdsArrayBuffer}实例的指针。
 * @return 返回执行的错误码。请参阅错误码定义{@link Udmf_ErrCode}。
 *         若返回UDMF_E_OK，表示执行成功。
 *         若返回UDMF_E_INVALID_PARAM，表示传入了无效参数。
 * @see OH_UdsArrayBuffer
 * @see Udmf_ErrCode
 * @since 13
 */
int OH_UdsArrayBuffer_Destroy(OH_UdsArrayBuffer* buffer);

/**
 * @brief 设置ArrayBuffer类型{@link OH_UdsArrayBuffer}对象的数据内容。
 *
 * @param buffer 表示指向ArrayBuffer类型{@link OH_UdsArrayBuffer}实例的指针。
 * @param data 表示用户自定义的ArrayBuffer数据。
 * @param len 表示用户自定义的ArrayBuffer数据的大小。
 * @return 返回执行的错误码。请参阅错误码定义{@link Udmf_ErrCode}。
 *         若返回UDMF_E_OK，表示执行成功。
 *         若返回UDMF_E_INVALID_PARAM，表示传入了无效参数。
 * @see OH_UdsArrayBuffer
 * @see Udmf_ErrCode
 * @since 13
 */
int OH_UdsArrayBuffer_SetData(OH_UdsArrayBuffer* buffer, unsigned char* data, unsigned int len);

/**
 * @brief 从ArrayBuffer类型{@link OH_UdsArrayBuffer}实例中获取用户自定义的ArrayBuffer数据内容。
 *
 * @param buffer 表示指向ArrayBuffer类型{@link OH_UdsArrayBuffer}实例的指针。
 * @param data 该参数是输出参数，表示用户自定义的ArrayBuffer数据。
 * @param len 该参数是输出参数，表示用户自定义的ArrayBuffer数据的大小。
 * @return 返回执行的错误码。请参阅错误码定义{@link Udmf_ErrCode}。
 *         若返回UDMF_E_OK，表示执行成功。
 *         若返回UDMF_E_INVALID_PARAM，表示传入了无效参数。
 * @see OH_UdsArrayBuffer
 * @see Udmf_ErrCode
 * @since 13
 */
int OH_UdsArrayBuffer_GetData(OH_UdsArrayBuffer* buffer, unsigned char** data, unsigned int* len);

#ifdef __cplusplus
};
#endif

/** @} */
#endif