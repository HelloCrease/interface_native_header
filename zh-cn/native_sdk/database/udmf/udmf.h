/*
 * Copyright (c) 2024 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup UDMF
 * @{
 *
 * @brief 统一数据管理框架旨在定义数据跨应用、跨设备以及跨平台过程中的各项标准，
 * 提供统一的OpenHarmony数据语言和标准化的数据接入与读取通路。
 *
 * @since 12
 */

 /**
 * @file udmf.h
 *
 * @brief 提供访问统一数据管理框架数据的接口、数据结构、枚举类型。
 * 引用文件：<database/udmf/udmf.h>
 * @library libudmf.so
 * @syscap SystemCapability.DistributedDataManager.UDMF.Core
 *
 * @since 12
 */
#ifndef UDMF_H
#define UDMF_H

#include <inttypes.h>
#include <stdbool.h>
#include "uds.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief 统一数据对象唯一标识符最小空间长度。
 *
 * @since 12
 */
#define UDMF_KEY_BUFFER_LEN (512)

/**
 * @brief 描述UDMF数据通路枚举类型。
 *
 * @since 12
 */
typedef enum Udmf_Intention {
    /**
     * @brief 拖拽数据通路。
     */
    UDMF_INTENTION_DRAG,
    /**
     * @brief 剪贴板数据通路。
     */
    UDMF_INTENTION_PASTEBOARD,
} Udmf_Intention;

/**
 * @brief UDMF支持的设备内使用范围类型枚举。
 *
 * @since 12
 */
typedef enum Udmf_ShareOption {
    /**
     * @brief 表示不合法的使用范围类型。
     */
    SHARE_OPTIONS_INVALID,
    /**
     * @brief 表示允许在本设备同应用内使用。
     */
    SHARE_OPTIONS_IN_APP,
    /**
     * @brief 表示允许在本设备内跨应用使用。
     */
    SHARE_OPTIONS_CROSS_APP
} Udmf_ShareOption;

/**
 * @brief 定义统一数据对象数据结构。
 *
 * @since 12
 */
typedef struct OH_UdmfData OH_UdmfData;

/**
 * @brief 定义统一数据对象中记录数据的数据结构，称为数据记录。
 *
 * @since 12
 */
typedef struct OH_UdmfRecord OH_UdmfRecord;

/**
 * @brief 定义统一数据对象中的数据提供者。
 *
 * @since 13
 */
typedef struct OH_UdmfRecordProvider OH_UdmfRecordProvider;

/**
 * @brief 定义统一数据对象中数据记录的属性结构。
 *
 * @since 12
 */
typedef struct OH_UdmfProperty OH_UdmfProperty;

/**
 * @brief 创建统一数据对象{@link OH_UdmfData}指针及实例对象。
 * 当不再需要使用指针时，请使用{@link OH_UdmfData_Destroy}销毁实例对象，否则会导致内存泄漏。
 *
 * @return 执行成功则返回一个指向统一数据对象{@link OH_UdmfData}实例对象的指针，否则返回nullptr。
 * @see OH_UdmfData
 * @since 12
 */
OH_UdmfData* OH_UdmfData_Create();

/**
 * @brief 销毁统一数据对象{@link OH_UdmfData}指针指向的实例对象。
 *
 * @param pThis 表示指向统一数据对象{@link OH_UdmfData}实例的指针。
 * @see OH_UdmfData
 * @since 12
 */
void OH_UdmfData_Destroy(OH_UdmfData* pThis);

/**
 * @brief 添加一个数据记录{@link OH_UdmfRecord}到统一数据对象{@link OH_UdmfData}中。
 *
 * @param pThis 表示指向统一数据对象{@link OH_UdmfData}实例的指针。
 * @param record 表示指向统一数据记录{@link OH_UdmfRecord}实例的指针。
 * @return 返回执行的错误码。请参阅错误码定义{@link Udmf_ErrCode}。
 *         若返回UDMF_E_OK，表示执行成功。
 *         若返回UDMF_E_INVALID_PARAM，表示传入了无效参数。
 * @see OH_UdmfData
 * @see Udmf_ErrCode
 * @since 12
 */
int OH_UdmfData_AddRecord(OH_UdmfData* pThis, OH_UdmfRecord* record);

/**
 * @brief 检查统一数据对象{@link OH_UdmfData}中是否存在指定类型。
 *
 * @param pThis 表示指向统一数据对象{@link OH_UdmfData}实例的指针。
 * @param type 表示指定类型的字符串指针。
 * @return 返回查找类型的状态。返回false表示不存在指定类型，返回true表示存在指定类型。
 * @see OH_UdmfData
 * @since 12
 */
bool OH_UdmfData_HasType(OH_UdmfData* pThis, const char* type);

/**
 * @brief 获取统一数据对象{@link OH_UdmfData}中包含的所有类型结果集。
 *
 * @param pThis 表示指向统一数据对象{@link OH_UdmfData}实例的指针。
 * @param count 该参数是输出参数，结果集中的类型数量会写入该变量。
 * @return 执行成功时返回统一数据对象的类型结果集，否则返回nullptr。
 * @see OH_UdmfData
 * @since 12
 */
char** OH_UdmfData_GetTypes(OH_UdmfData* pThis, unsigned int* count);

/**
 * @brief 获取统一数据对象{@link OH_UdmfData}中包含的所有记录结果集。
 *
 * @param pThis 表示指向统一数据对象{@link OH_UdmfData}实例的指针。
 * @param count 该参数是输出参数，结果集中的记录数量会写入该变量。
 * @return 执行成功时返回统一数据记录{@link OH_UdmfRecord}结果集，否则返回nullptr。
 * @see OH_UdmfData
 * @see OH_UdmfRecord
 * @since 12
 */
OH_UdmfRecord** OH_UdmfData_GetRecords(OH_UdmfData* pThis, unsigned int* count);

/**
 * @brief 定义用于释放上下文的回调函数，统一数据提供者对象销毁时触发。
 * @param context 要释放的上下文指针。
 * @since 13
 */
typedef void (*UdmfData_Finalize)(void* context);

/**
 * @brief 创建一个统一数据提供者{@link OH_UdmfRecordProvider}指针及实例对象。
 * 当不再需要使用指针时，请使用{@link OH_UdmfRecordProvider_Destroy}销毁实例对象，否则会导致内存泄漏。
 *
 * @return 执行成功时返回一个指向统一数据提供者{@link OH_UdmfRecordProvider}实例对象的指针，否则返回nullptr。
 * @see OH_UdmfRecordProvider
 * @since 13
 */
OH_UdmfRecordProvider* OH_UdmfRecordProvider_Create();

/**
 * @brief 销毁统一数据提供者{@link OH_UdmfRecordProvider}指针指向的实例对象。
 *
 * @param provider 表示指向统一数据提供者对象{@link OH_UdmfRecordProvider}实例的指针。
 * @return 返回执行的错误码。请参阅错误码定义{@link Udmf_ErrCode}。
 *         若返回UDMF_E_OK，表示执行成功。
 *         若返回UDMF_E_INVALID_PARAM，表示传入了无效参数。
 * @see OH_UdmfRecordProvider
 * @see Udmf_ErrCode
 * @since 13
 */
int OH_UdmfRecordProvider_Destroy(OH_UdmfRecordProvider* provider);

/**
 * @brief 定义用于按类型获取数据的回调函数。
 * 当从OH_UdmfRecord中获取数据时，会触发此回调函数，得到的数据就是这个回调函数返回的数据。
 *
 * @param context 用{@link OH_UdmfRecordProvider_SetData}设置的上下文指针。
 * @param type 要获取的数据类型。详细类型信息见{@link udmf_meta.h}。
 * @return 需要返回一个标准化数据。
 * @since 13
 */
typedef void* (*OH_UdmfRecordProvider_GetData)(void* context, const char* type);

/**
 * @brief 设置统一数据提供者的数据提供回调函数。
 *
 * @param provider 指向统一数据提供者{@link OH_UdmfRecordProvider}实例对象的指针。
 * @param context 上下文指针，将作为第一个参数传入{@link OH_UdmfRecordProvider_GetData}。
 * @param callback 获取数据的回调函数。详见：{@link OH_UdmfRecordProvider_GetData}。
 * @param finalize 可选的回调函数，可以用于统一数据提供者销毁时释放上下文数据。详见：{@link UdmfData_Finalize}。
 * @return 返回执行的错误码。请参阅错误码定义{@link Udmf_ErrCode}。
 *         若返回UDMF_E_OK，表示执行成功。
 *         若返回UDMF_E_INVALID_PARAM，表示传入了无效参数。
 * @see OH_UdmfRecordProvider
 * @see OH_UdmfRecordProvider_GetData
 * @see UdmfData_Finalize Udmf_ErrCode
 * @since 13
 */
int OH_UdmfRecordProvider_SetData(OH_UdmfRecordProvider* provider, void* context,
    const OH_UdmfRecordProvider_GetData callback, const UdmfData_Finalize finalize);

/**
 * @brief 创建统一数据记录{@link OH_UdmfRecord}指针及实例对象。
 * 当不再需要使用指针时，请使用{@link OH_UdmfRecord_Destroy}销毁实例对象，否则会导致内存泄漏。
 *
 * @return 执行成功则返回一个指向统一数据记录{@link OH_UdmfRecord}实例对象的指针，否则返回nullptr。
 * @see OH_UdmfRecord
 * @since 12
 */
OH_UdmfRecord* OH_UdmfRecord_Create();

/**
 * @brief 销毁统一数据记录{@link OH_UdmfRecord}指针指向的实例对象。
 *
 * @param pThis 表示指向统一数据对象{@link OH_UdmfRecord}实例的指针。
 * @see OH_UdmfRecord
 * @since 12
 */
void OH_UdmfRecord_Destroy(OH_UdmfRecord* pThis);

/**
 * @brief 添加用户自定义的通用数据至统一数据记录{@link OH_UdmfRecord}中。
 * 对于已定义UDS的类型（比如PlainText、Link、Pixelmap等）不可使用该接口。
 *
 * @param pThis 表示指向统一数据记录{@link OH_UdmfRecord}实例的指针。
 * @param typeId 表示数据类型标识，为和系统定义的类型进行区分，建议以'ApplicationDefined'开头。
 * @param entry 表示用户自定义数据。
 * @param count 表示用户自定义数据的大小。数据大小不超过4KB。
 * @return 返回执行的错误码。请参阅错误码定义{@link Udmf_ErrCode}。
 *         若返回UDMF_E_OK，表示执行成功。
 *         若返回UDMF_E_INVALID_PARAM，表示传入了无效参数。
 * @see OH_UdmfRecord
 * @see Udmf_ErrCode
 * @since 12
 */
int OH_UdmfRecord_AddGeneralEntry(OH_UdmfRecord* pThis, const char* typeId, unsigned char* entry, unsigned int count);

/**
 * @brief 增加纯文本类型{@link OH_UdsPlainText}数据至统一数据记录{@link OH_UdmfRecord}中。
 *
 * @param pThis 表示指向统一数据记录{@link OH_UdmfRecord}实例的指针。
 * @param plainText 表示指向纯文本类型{@link OH_UdsPlainText}实例的指针。
 * @return 返回执行的错误码。请参阅错误码定义{@link Udmf_ErrCode}。
 *         若返回UDMF_E_OK，表示执行成功。
 *         若返回UDMF_E_INVALID_PARAM，表示传入了无效参数。
 * @see OH_UdmfRecord
 * @see OH_UdsPlainText
 * @see Udmf_ErrCode
 * @since 12
 */
int OH_UdmfRecord_AddPlainText(OH_UdmfRecord* pThis, OH_UdsPlainText* plainText);

/**
 * @brief 增加超链接类型{@link OH_UdsHyperlink}数据至统一数据记录{@link OH_UdmfRecord}中。
 *
 * @param pThis 表示指向统一数据记录{@link OH_UdmfRecord}实例的指针。
 * @param hyperlink 表示指向超链接类型{@link OH_UdsHyperlink}实例的指针。
 * @return 返回执行的错误码。请参阅错误码定义{@link Udmf_ErrCode}。
 *         若返回UDMF_E_OK，表示执行成功。
 *         若返回UDMF_E_INVALID_PARAM，表示传入了无效参数。
 * @see OH_UdmfRecord
 * @see OH_UdsHyperlink
 * @see Udmf_ErrCode
 * @since 12
 */
int OH_UdmfRecord_AddHyperlink(OH_UdmfRecord* pThis, OH_UdsHyperlink* hyperlink);

/**
 * @brief 增加超文本标记语言类型{@link OH_UdsHtml}数据至统一数据记录{@link OH_UdmfRecord}中。
 *
 * @param pThis 表示指向统一数据记录{@link OH_UdmfRecord}实例的指针。
 * @param html 表示指向超文本标记语言类型{@link OH_UdsHtml}实例的指针。
 * @return 返回执行的错误码。请参阅错误码定义{@link Udmf_ErrCode}。
 *         若返回UDMF_E_OK，表示执行成功。
 *         若返回UDMF_E_INVALID_PARAM，表示传入了无效参数。
 * @see OH_UdmfRecord
 * @see OH_UdsHtml
 * @see Udmf_ErrCode
 * @since 12
 */
int OH_UdmfRecord_AddHtml(OH_UdmfRecord* pThis, OH_UdsHtml* html);

/**
 * @brief 增加桌面图标类型{@link OH_UdsAppItem}数据至统一数据记录{@link OH_UdmfRecord}中。
 *
 * @param pThis 表示指向统一数据记录{@link OH_UdmfRecord}实例的指针。
 * @param appItem 表示指向桌面图标类型{@link OH_UdsAppItem}实例的指针。
 * @return 返回执行的错误码。请参阅错误码定义{@link Udmf_ErrCode}。
 *         若返回UDMF_E_OK，表示执行成功。
 *         若返回UDMF_E_INVALID_PARAM，表示传入了无效参数。
 * @see OH_UdmfRecord
 * @see OH_UdsAppItem
 * @see Udmf_ErrCode
 * @since 12
 */
int OH_UdmfRecord_AddAppItem(OH_UdmfRecord* pThis, OH_UdsAppItem* appItem);

/**
 * @brief 增加文件Uri类型{@link OH_UdsFileUri}数据至统一数据记录{@link OH_UdmfRecord}中。
 *
 * @param pThis 表示指向统一数据记录{@link OH_UdmfRecord}实例的指针。
 * @param fileUri 表示指向文件Uri类型{@link OH_UdsFileUri}实例的指针。
 * @return 返回执行的错误码。请参阅错误码定义{@link Udmf_ErrCode}。
 *         若返回UDMF_E_OK，表示执行成功。
 *         若返回UDMF_E_INVALID_PARAM，表示传入了无效参数。
 * @see OH_UdmfRecord
 * @see OH_UdsFileUri
 * @see Udmf_ErrCode
 * @since 13
 */
int OH_UdmfRecord_AddFileUri(OH_UdmfRecord* pThis, OH_UdsFileUri* fileUri);

/**
 * @brief 增加像素图片类型{@link OH_UdsPixelMap}数据至统一数据记录{@link OH_UdmfRecord}中。
 *
 * @param pThis 表示指向统一数据记录{@link OH_UdmfRecord}实例的指针。
 * @param pixelMap 表示指向像素图片类型{@link OH_UdsPixelMap}实例的指针。
 * @return 返回执行的错误码。请参阅错误码定义{@link Udmf_ErrCode}。
 *         若返回UDMF_E_OK，表示执行成功。
 *         若返回UDMF_E_INVALID_PARAM，表示传入了无效参数。
 * @see OH_UdmfRecord
 * @see OH_UdsPixelMap
 * @see Udmf_ErrCode
 * @since 13
 */
int OH_UdmfRecord_AddPixelMap(OH_UdmfRecord* pThis, OH_UdsPixelMap* pixelMap);

/**
 * @brief 增加一个ArrayBuffer类型{@link OH_UdsArrayBuffer}的数据至统一数据记录{@link OH_UdmfRecord}中。
 *
 * @param record 表示指向统一数据记录{@link OH_UdmfRecord}实例的指针。
 * @param type 表示自定义的ArrayBuffer数据的数据类型标识，不可与已有的数据类型标识重复。
 * @param buffer 表示指向ArrayBuffer类型{@link OH_UdsArrayBuffer}实例的指针。
 * @return 返回执行的错误码。请参阅错误码定义{@link Udmf_ErrCode}。
 *         若返回UDMF_E_OK，表示执行成功。
 *         若返回UDMF_E_INVALID_PARAM，表示传入了无效参数。
 * @see OH_UdmfRecord
 * @see OH_UdsArrayBuffer
 * @see Udmf_ErrCode
 * @since 13
 */
int OH_UdmfRecord_AddArrayBuffer(OH_UdmfRecord* record, const char* type, OH_UdsArrayBuffer* buffer);

/**
 * @brief 获取统一数据记录{@link OH_UdmfRecord}中所有类型的结果集。
 *
 * @param pThis 表示指向统一数据记录{@link OH_UdmfRecord}实例的指针。
 * @param count 该参数是输出参数，结果集中的类型数量会写入该变量。
 * @return 执行成功时返回类型列表，否则返回nullptr。
 * @see OH_UdmfRecord
 * @since 12
 */
char** OH_UdmfRecord_GetTypes(OH_UdmfRecord* pThis, unsigned int* count);

/**
 * @brief 获取统一数据记录{@link OH_UdmfRecord}中的特定类型的数据结果集。
 *
 * @param pThis 表示指向统一数据记录{@link OH_UdmfRecord}实例的指针。
 * @param typeId 表示数据类型标识。
 * @param entry 该参数是输出参数，结果集中数据的具体信息会写入该变量。
 * @param count 该参数是输出参数，结果集中的数据长度会写入该变量。
 * @return 返回执行的错误码。请参阅错误码定义{@link Udmf_ErrCode}。
 *         若返回UDMF_E_OK，表示执行成功。
 *         若返回UDMF_E_INVALID_PARAM，表示传入了无效参数。
 * @see OH_UdmfRecord
 * @see Udmf_ErrCode
 * @since 12
 */
int OH_UdmfRecord_GetGeneralEntry(OH_UdmfRecord* pThis, const char* typeId,
    unsigned char** entry, unsigned int* count);

/**
 * @brief 从统一数据记录{@link OH_UdmfRecord}中获取纯文本类型{@link OH_UdsPlainText}数据。
 *
 * @param pThis 表示指向统一数据记录{@link OH_UdmfRecord}实例的指针。
 * @param plainText 该参数是输出参数，表示指向纯文本类型{@link OH_UdsPlainText}实例的指针。
 * @return 返回执行的错误码。请参阅错误码定义{@link Udmf_ErrCode}。
 *         若返回UDMF_E_OK，表示执行成功。
 *         若返回UDMF_E_INVALID_PARAM，表示传入了无效参数。
 * @see OH_UdmfRecord
 * @see OH_UdsPlainText
 * @see Udmf_ErrCode
 * @since 12
 */
int OH_UdmfRecord_GetPlainText(OH_UdmfRecord* pThis, OH_UdsPlainText* plainText);

/**
 * @brief 从统一数据记录{@link OH_UdmfRecord}中获取超链接类型{@link OH_UdsHyperlink}数据。
 *
 * @param pThis 表示指向统一数据记录{@link OH_UdmfRecord}实例的指针。
 * @param hyperlink 该参数是输出参数，表示指向超链接类型{@link OH_UdsHyperlink}实例的指针。
 * @return 返回执行的错误码。请参阅错误码定义{@link Udmf_ErrCode}。
 *         若返回UDMF_E_OK，表示执行成功。
 *         若返回UDMF_E_INVALID_PARAM，表示传入了无效参数。
 * @see OH_UdmfRecord
 * @see OH_UdsHyperlink
 * @see Udmf_ErrCode
 * @since 12
 */
int OH_UdmfRecord_GetHyperlink(OH_UdmfRecord* pThis, OH_UdsHyperlink* hyperlink);

/**
 * @brief 从统一数据记录{@link OH_UdmfRecord}中获取超文本标记语言类型{@link OH_UdsHtml}数据。
 *
 * @param pThis 表示指向统一数据记录{@link OH_UdmfRecord}实例的指针。
 * @param html 该参数是输出参数，表示指向超文本标记语言类型{@link OH_UdsHtml}实例的指针。
 * @return 返回执行的错误码。请参阅错误码定义{@link Udmf_ErrCode}。
 *         若返回UDMF_E_OK，表示执行成功。
 *         若返回UDMF_E_INVALID_PARAM，表示传入了无效参数。
 * @see OH_UdmfRecord
 * @see OH_UdsHtml
 * @see Udmf_ErrCode
 * @since 12
 */
int OH_UdmfRecord_GetHtml(OH_UdmfRecord* pThis, OH_UdsHtml* html);

/**
 * @brief 从统一数据记录{@link OH_UdmfRecord}中获取桌面图标类型{@link OH_UdsAppItem}数据。
 *
 * @param pThis 表示指向统一数据记录{@link OH_UdmfRecord}实例的指针。
 * @param appItem 该参数是输出参数，表示指向桌面图标类型{@link OH_UdsAppItem}实例的指针。
 * @return 返回执行的错误码。请参阅错误码定义{@link Udmf_ErrCode}。
 *         若返回UDMF_E_OK，表示执行成功。
 *         若返回UDMF_E_INVALID_PARAM，表示传入了无效参数。
 * @see OH_UdmfRecord
 * @see OH_UdsAppItem
 * @see Udmf_ErrCode
 * @since 12
 */
int OH_UdmfRecord_GetAppItem(OH_UdmfRecord* pThis, OH_UdsAppItem* appItem);

/**
 * @brief 将指定类型的统一数据提供者{@link OH_UdmfRecordProvider}设置至统一数据记录{@link OH_UdmfRecord}中。
 *
 * @param pThis 表示指向统一数据记录{@link OH_UdmfRecord}实例的指针。
 * @param types 表示一组指定的要提供的数据类型。
 * @param count 表示指定的数据类型的数量。
 * @param provider 表示指向统一数据提供者对象{@link OH_UdmfRecordProvider}实例的指针。
 * @return 返回执行的错误码。请参阅错误码定义{@link Udmf_ErrCode}。
 *         若返回UDMF_E_OK，表示执行成功。
 *         若返回UDMF_E_INVALID_PARAM，表示传入了无效参数。
 * @see OH_UdmfRecord
 * @see OH_UdmfRecordProvider
 * @see Udmf_ErrCode
 * @since 13
 */
int OH_UdmfRecord_SetProvider(OH_UdmfRecord* pThis, const char* const* types, unsigned int count,
    OH_UdmfRecordProvider* provider);

/**
 * @brief 从统一数据记录{@link OH_UdmfRecord}中获取文件Uri类型{@link OH_UdsFileUri}数据。
 *
 * @param pThis 表示指向统一数据记录{@link OH_UdmfRecord}实例的指针。
 * @param fileUri 该参数是输出参数，表示指向文件Uri类型{@link OH_UdsFileUri}实例的指针。
 * @return 返回执行的错误码。请参阅错误码定义{@link Udmf_ErrCode}。
 *         若返回UDMF_E_OK，表示执行成功。
 *         若返回UDMF_E_INVALID_PARAM，表示传入了无效参数。
 * @see OH_UdmfRecord
 * @see OH_UdsFileUri
 * @see Udmf_ErrCode
 * @since 13
 */
int OH_UdmfRecord_GetFileUri(OH_UdmfRecord* pThis, OH_UdsFileUri* fileUri);

/**
 * @brief 从统一数据记录{@link OH_UdmfRecord}中获取像素图片类型{@link OH_UdsPixelMap}数据。
 *
 * @param pThis 表示指向统一数据记录{@link OH_UdmfRecord}实例的指针。
 * @param pixelMap 该参数是输出参数，表示指向像素图片类型{@link OH_UdsPixelMap}实例的指针。
 * @return 返回执行的错误码。请参阅错误码定义{@link Udmf_ErrCode}。
 *         若返回UDMF_E_OK，表示执行成功。
 *         若返回UDMF_E_INVALID_PARAM，表示传入了无效参数。
 * @see OH_UdmfRecord
 * @see OH_UdsPixelMap
 * @see Udmf_ErrCode
 * @since 13
 */
int OH_UdmfRecord_GetPixelMap(OH_UdmfRecord* pThis, OH_UdsPixelMap* pixelMap);

/**
 * @brief 从统一数据记录{@link OH_UdmfRecord}中获取ArrayBuffer类型{@link OH_UdsArrayBuffer}数据。
 *
 * @param record 表示指向统一数据记录{@link OH_UdmfRecord}实例的指针。
 * @param type 表示要获取的ArrayBuffer类型数据的数据类型标识。
 * @param buffer 该参数是输出参数，表示指向ArrayBuffer类型{@link OH_UdsArrayBuffer}实例的指针。
 * @return 返回执行的错误码。请参阅错误码定义{@link Udmf_ErrCode}。
 *         若返回UDMF_E_OK，表示执行成功。
 *         若返回UDMF_E_INVALID_PARAM，表示传入了无效参数。
 * @see OH_UdmfRecord
 * @see OH_UdsArrayBuffer
 * @see Udmf_ErrCode
 * @since 13
 */
int OH_UdmfRecord_GetArrayBuffer(OH_UdmfRecord* record, const char* type, OH_UdsArrayBuffer* buffer);

/**
 * @brief 从统一数据对象{@link OH_UdmfData}中获取第一个纯文本类型{@link OH_UdsPlainText}数据。
 *
 * @param data 表示指向统一数据对象{@link OH_UdmfData}实例的指针。
 * @param plainText 该参数是输出参数，表示指向纯文本类型{@link OH_UdsPlainText}实例的指针。
 * @return 返回执行的错误码。请参阅错误码定义{@link Udmf_ErrCode}。
 *         若返回UDMF_E_OK，表示执行成功。
 *         若返回UDMF_E_INVALID_PARAM，表示传入了无效参数。
 * @see OH_UdmfData
 * @see OH_UdsPlainText
 * @see Udmf_ErrCode
 * @since 13
 */
int OH_UdmfData_GetPrimaryPlainText(OH_UdmfData* data, OH_UdsPlainText* plainText);

/**
 * @brief 从统一数据对象{@link OH_UdmfData}中获取第一个超文本标记语言类型{@link OH_UdsHtml}数据。
 *
 * @param data 表示指向统一数据对象{@link OH_UdmfData}实例的指针。
 * @param html 该参数是输出参数，表示指向超文本标记语言类型{@link OH_UdsHtml}实例的指针。
 * @return 返回执行的错误码。请参阅错误码定义{@link Udmf_ErrCode}。
 *         若返回UDMF_E_OK，表示执行成功。
 *         若返回UDMF_E_INVALID_PARAM，表示传入了无效参数。
 * @see OH_UdmfData
 * @see OH_UdsHtml
 * @see Udmf_ErrCode
 * @since 13
 */
int OH_UdmfData_GetPrimaryHtml(OH_UdmfData* data, OH_UdsHtml* html);

/**
 * @brief 获取统一数据对象{@link OH_UdmfData}中包含的所有记录数量。
 *
 * @param 表示指向统一数据对象{@link OH_UdmfData}实例的指针。
 * @return 返回统一数据对象{@link OH_UdmfRecord}的数量。
 * @see OH_UdmfData
 * @since 13
 */
int OH_UdmfData_GetRecordCount(OH_UdmfData* data);

/**
 * @brief 获取统一数据对象{@link OH_UdmfData}中指定位置的数据记录。
 *
 * @param data 表示指向统一数据对象{@link OH_UdmfData}实例的指针。
 * @param index 表示要获取的统一数据记录{@link OH_UdmfRecord}在统一数据对象{@link OH_UdmfData}中的下标。
 * @return 执行成功时返回统一数据记录{@link OH_UdmfRecord}实例对象的指针，否则返回nullptr。
 * @see OH_UdmfData
 * @since 13
 */
OH_UdmfRecord* OH_UdmfData_GetRecord(OH_UdmfData* data, unsigned int index);

/**
 * @brief 检查统一数据对象{@link OH_UdmfData}是否是来自本端设备的数据。
 *
 * @param data 表示指向统一数据对象{@link OH_UdmfData}实例的指针。
 * @return 返回数据是否是来自本端设备。返回true表示来自本端设备，返回false表示来自远端设备。
 * @see OH_UdmfData
 * @since 13
 */
bool OH_UdmfData_IsLocal(OH_UdmfData* data);

/**
 * @brief 创建统一数据对象中数据记录属性{@link OH_UdmfProperty}指针及实例对象。
 * 当不再需要使用指针时，请使用{@link OH_UdmfProperty_Destroy}销毁实例对象，否则会导致内存泄漏。
 *
 * @param unifiedData 表示指向统一数据对象{@link OH_UdmfData}实例的指针。
 * @return 执行成功则返回一个指向属性{@link OH_UdmfProperty}实例对象的指针，否则返回nullptr。
 * @see OH_UdmfData
 * @see OH_UdmfProperty
 * @since 12
 */
OH_UdmfProperty* OH_UdmfProperty_Create(OH_UdmfData* unifiedData);

/**
 * @brief 销毁数据属性{@link OH_UdmfProperty}指针指向的实例对象。
 *
 * @param pThis 表示指向数据属性{@link OH_UdmfProperty}实例的指针。
 * @see OH_UdmfProperty
 * @since 12
 */
void OH_UdmfProperty_Destroy(OH_UdmfProperty* pThis);

/**
 * @brief 从数据属性{@link OH_UdmfProperty}中获取用户自定义标签值。
 *
 * @param pThis 表示指向数据属性{@link OH_UdmfProperty}实例的指针。
 * @return 执行成功时返回自定义标签值的字符串指针，否则返回nullptr。
 * @see OH_UdmfProperty
 * @since 12
 */
const char* OH_UdmfProperty_GetTag(OH_UdmfProperty* pThis);

/**
 * @brief 从数据属性{@link OH_UdmfProperty}中获取时间戳。
 *
 * @param pThis 表示指向数据属性{@link OH_UdmfProperty}实例的指针。
 * @return 返回时间戳值。
 * @see OH_UdmfProperty
 * @since 12
 */
int64_t OH_UdmfProperty_GetTimestamp(OH_UdmfProperty* pThis);

/**
 * @brief 从数据属性{@link OH_UdmfProperty}中获取设备内适用范围属性。
 *
 * @param pThis 表示指向数据属性{@link OH_UdmfProperty}实例的指针。
 * @return 返回设备内适用范围属性{@link Udmf_ShareOption}值。
 * @see OH_UdmfProperty
 * @see Udmf_ShareOption
 * @since 12
 */
Udmf_ShareOption OH_UdmfProperty_GetShareOption(OH_UdmfProperty* pThis);

/**
 * @brief 从数据属性{@link OH_UdmfProperty}中获取自定义的附加整型参数。
 *
 * @param pThis 表示指向数据属性{@link OH_UdmfProperty}实例的指针。
 * @param key 表示键值对的键。
 * @param defaultValue 用于用户自行设置获取值失败时的默认值。
 * @return 执行成功返回指定的键关联的整型值，否则返回用户设置的默认值defaultValue。
 * @see OH_UdmfProperty
 * @since 12
 */
int OH_UdmfProperty_GetExtrasIntParam(OH_UdmfProperty* pThis, const char* key, int defaultValue);

/**
 * @brief 从数据属性{@link OH_UdmfProperty}中获取自定义的附加字符串参数。
 *
 * @param pThis 表示指向数据属性{@link OH_UdmfProperty}实例的指针。
 * @param key 表示键值对的键。
 * @return 执行成功时返回指定的键关联的字符串值的指针，否则返回nullptr。
 * @see OH_UdmfProperty
 * @since 12
 */
const char* OH_UdmfProperty_GetExtrasStringParam(OH_UdmfProperty* pThis, const char* key);

/**
 * @brief 设置数据属性{@link OH_UdmfProperty}的自定义标签值。
 *
 * @param pThis 表示指向数据属性{@link OH_UdmfProperty}实例的指针。
 * @param tag 表示自定义标签值。
 * @return 返回执行的错误码。请参阅错误码定义{@link Udmf_ErrCode}。
 *         若返回UDMF_E_OK，表示执行成功。
 *         若返回UDMF_E_INVALID_PARAM，表示传入了无效参数。
 * @see OH_UdmfProperty
 * @see Udmf_ErrCode
 * @since 12
 */
int OH_UdmfProperty_SetTag(OH_UdmfProperty* pThis, const char* tag);

/**
 * @brief 设置数据属性{@link OH_UdmfProperty}的设备内适用范围{@link OH_Udmf_ShareOption}参数。
 *
 * @param pThis 表示指向数据属性{@link OH_UdmfProperty}实例的指针。
 * @param option 表示设备内适用范围{@link Udmf_ShareOption}参数。
 * @return 返回执行的错误码。请参阅错误码定义{@link Udmf_ErrCode}。
 *         若返回UDMF_E_OK，表示执行成功。
 *         若返回UDMF_E_INVALID_PARAM，表示传入了无效参数。
 * @see OH_UdmfProperty
 * @see Udmf_ShareOption
 * @see Udmf_ErrCode
 * @since 12
 */
int OH_UdmfProperty_SetShareOption(OH_UdmfProperty* pThis, Udmf_ShareOption option);

/**
 * @brief 设置数据属性{@link OH_UdmfProperty}的附加整型参数。
 *
 * @param pThis 表示指向{@link OH_UdmfRecord}实例的指针。
 * @param key 表示键值对的键。
 * @param param 表示键值对的值。
 * @return 返回执行的错误码。请参阅错误码定义{@link Udmf_ErrCode}。
 *         若返回UDMF_E_OK，表示执行成功。
 *         若返回UDMF_E_INVALID_PARAM，表示传入了无效参数。
 * @see OH_UdmfProperty
 * @see Udmf_ErrCode
 * @since 12
 */
int OH_UdmfProperty_SetExtrasIntParam(OH_UdmfProperty* pThis, const char* key, int param);

/**
 * @brief 设置数据属性{@link OH_UdmfProperty}的附加字符串参数。
 *
 * @param pThis 表示指向数据属性{@link OH_UdmfRecord}实例的指针。
 * @param key 表示键值对的键。
 * @param param 表示键值对的值。
 * @return 返回执行的错误码。请参阅错误码定义{@link Udmf_ErrCode}。
 *         若返回UDMF_E_OK，表示执行成功。
 *         若返回UDMF_E_INVALID_PARAM，表示传入了无效参数。
 * @see OH_UdmfProperty
 * @see Udmf_ErrCode
 * @since 12
 */
int OH_UdmfProperty_SetExtrasStringParam(OH_UdmfProperty* pThis,
    const char* key, const char* param);

/**
 * @brief 从统一数据管理框架数据库中获取统一数据对象{@link OH_UdmfData}数据。
 *
 * @param key 表示数据库存储的唯一标识符。
 * @param intention 表示数据通路类型{@link Udmf_Intention}。
 * @param unifiedData 该参数是输出参数，获取到的统一数据对象{@link OH_UdmfData}会写入该变量。
 * @return 返回执行的错误码。请参阅错误码定义{@link Udmf_ErrCode}。
 *         若返回UDMF_E_OK，表示执行成功。
 *         若返回UDMF_E_INVALID_PARAM，表示传入了无效参数。
 * @see OH_UdmfProperty
 * @see Udmf_Intention
 * @see Udmf_ErrCode
 * @since 12
 */
int OH_Udmf_GetUnifiedData(const char* key, Udmf_Intention intention, OH_UdmfData* unifiedData);

/**
 * @brief 从统一数据管理框架数据库中写入统一数据对象{@link OH_UdmfData}数据。
 *
 * @param intention 表示数据通路类型{@link Udmf_Intention}。
 * @param unifiedData 表示统一数据对象{@link OH_UdmfData}数据。
 * @param key表示成功将数据设置到数据库后对应数据的唯一标识符。
 * @param keyLen 表示唯一标识符参数的空间大小，内存大小不小于512字节。
 * @return 返回执行的错误码。请参阅错误码定义{@link Udmf_ErrCode}。
 *         若返回UDMF_E_OK，表示执行成功。
 *         若返回UDMF_E_INVALID_PARAM，表示传入了无效参数。
 * @see OH_UdmfProperty
 * @see Udmf_Intention
 * @see Udmf_ErrCode
 * @since 12
 */
int OH_Udmf_SetUnifiedData(Udmf_Intention intention, OH_UdmfData* unifiedData,
    char* key, unsigned int keyLen);

#ifdef __cplusplus
};
#endif

/** @} */
#endif