/*
 * Copyright (c) 2024 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup Pasteboard
 * @{
 *
 * @brief 系统剪贴板支持复制和粘贴多种类型的数据。
 * 可以使用此模块接口操作纯文本、HTML、URI、像素图片等其他类型的数据。
 *
 * @since 13
 */

/**
 * @file oh_pasteboard_err_code.h
 *
 * @brief 声明剪贴板框架错误码信息。
 * 引用文件：<database/pasteboard/oh_pasteboard_err_code.h>
 *
 * @library libpasteboard.so
 * @syscap SystemCapability.MiscServices.Pasteboard
 *
 * @since 13
 */


#ifndef OH_PASTEBOARD_ERR_CODE_H
#define OH_PASTEBOARD_ERR_CODE_H

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief 错误码信息。
 *
 * @since 13
 */
typedef enum PASTEBOARD_ErrCode {
    /**
     * @error 执行成功。
     */
    ERR_OK = 0,
    /**
     * @error 权限校验失败。
     */
    ERR_PERMISSION_ERROR = 201,
    /**
     * @error 非法参数。
     */
    ERR_INVALID_PARAMETER = 401,
    /**
     * @error 设备能力不支持。
     */
    ERR_DEVICE_NOT_SUPPORTED = 801,
    /**
     * @error 内部错误。
     */
    ERR_INNER_ERROR = 12900000,
    /**
     * @error 系统忙。
     */
    ERR_BUSY = 12900003,
} PASTEBOARD_ErrCode;
#ifdef __cplusplus
};
#endif

/** @} */
#endif
