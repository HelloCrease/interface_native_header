/*
 * Copyright (c) 2024 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup CommonEvent
 * @{
 *
 * @brief 提供订阅与退订公共事件的能力。
 *
 * @since 12
 */
/**
 * @file oh_commonevent.h
 *
 * @brief 定义公共事件订阅与退订API接口与枚举错误码。
 *
 * @library libohcommonevent.so
 * @kit BasicServicesKit
 * @syscap SystemCapability.Notification.CommonEvent
 * @since 12
 * @version 1.0
 */

#ifndef OH_COMMONEVENT_H
#define OH_COMMONEVENT_H

#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief 枚举错误码。
 *
 * @since 12
 * @version 1.0
 */
typedef enum CommonEvent_ErrCode {
    /** 成功。 */
    COMMONEVENT_ERR_OK = 0,

    /** 权限错误。 */
    COMMONEVENT_ERR_PERMISSION_ERROR = 201,

    /** 参数错误。 */
    COMMONEVENT_ERR_INVALID_PARAMETER = 401,

    /** IPC发送失败。 */
    COMMONEVENT_ERR_SENDING_REQUEST_FAILED = 1500007,

    /** 服务未初始化。 */
    COMMONEVENT_ERR_INIT_UNDONE = 1500008,

    /** 订阅者数量超过限制。 */
    COMMONEVENT_ERR_SUBSCRIBER_NUM_EXCEEDED = 1500010,

    /** 内存分配失败。 */
    COMMONEVENT_ERR_ALLOC_MEMORY_FAILED = 1500011,
} CommonEvent_ErrCode;

/**
 * @brief 提供CommonEvent_SubscribeInfo订阅者信息结构体声明。
 *
 * @since 12
 */
typedef struct CommonEvent_SubscribeInfo CommonEvent_SubscribeInfo;

/**
 * @brief 提供CommonEvent_Subscriber订阅者结构体声明。
 *
 * @since 12
 */
typedef void CommonEvent_Subscriber;

/**
 * @brief 提供CommonEvent_RcvData公共事件回调数据结构体声明。
 *
 * @since 12
 */
typedef struct CommonEvent_RcvData CommonEvent_RcvData;

/**
 * @brief 提供CommonEvent_RcvData公共事件附件信息结构体声明。
 *
 * @since 12
 */
typedef void CommonEvent_Parameters;

/**
 * @brief 提供CommonEvent_ReceiveCallback回调函数声明。
 *
 * @param data 公共事件回调数据。
 * @since 12
 */
typedef void (*CommonEvent_ReceiveCallback)(const CommonEvent_RcvData *data);

/**
 * @brief 创建订阅者信息。
 *
 * @param events 订阅的公共事件。
 * @param eventsNum 订阅的公共事件数量。
 * @return 成功则返回订阅者信息,失败则返回NULL。
 * @since 12
 */
CommonEvent_SubscribeInfo* OH_CommonEvent_CreateSubscribeInfo(const char* events[], int32_t eventsNum);

/**
 * @brief 设置订阅者权限。
 *
 * @param info 订阅者信息。
 * @param permission 权限名称。
 * @return 返回{@link COMMONEVENT_ERR_OK}表示成功；
 *         返回{@link COMMONEVENT_ERR_INVALID_PARAMETER}表示参数错误；
 * @since 12
 */
CommonEvent_ErrCode OH_CommonEvent_SetPublisherPermission(CommonEvent_SubscribeInfo* info, const char* permission);

/**
 * @brief 设置订阅者包名称。
 *
 * @param info 订阅者信息。
 * @param bundleName 包名称。
 * @return 返回{@link COMMONEVENT_ERR_OK}表示成功；
 *         返回{@link COMMONEVENT_ERR_INVALID_PARAMETER}表示参数错误；
 * @since 12
 */
CommonEvent_ErrCode OH_CommonEvent_SetPublisherBundleName(CommonEvent_SubscribeInfo* info, const char* bundleName);

/**
 * @brief 释放订阅者信息。
 *
 * @param info 订阅者信息。
 * @since 12
 */
void OH_CommonEvent_DestroySubscribeInfo(CommonEvent_SubscribeInfo* info);

/**
 * @brief 创建订阅者。
 *
 * @param info 订阅者信息。
 * @param callback 公共事件回调函数
 * @return 成功则返回订阅者,失败则返回NULL。
 * @since 12
 */
CommonEvent_Subscriber* OH_CommonEvent_CreateSubscriber(const CommonEvent_SubscribeInfo* info,
    CommonEvent_ReceiveCallback callback);

/**
 * @brief 释放订阅者。
 *
 * @param subscriber 订阅者。
 * @since 12
 */
void OH_CommonEvent_DestroySubscriber(CommonEvent_Subscriber* subscriber);

/**
 * @brief 订阅公共事件。
 *
 * @param subscriber 订阅者。
 * @return 返回{@link COMMONEVENT_ERR_OK}表示成功；
 *         返回{@link COMMONEVENT_ERR_INVALID_PARAMETER }表示参数subscriber无效；
 *         返回{@link COMMONEVENT_ERR_SENDING_REQUEST_FAILED }表示IPC请求发送失败；
 *         返回{@link COMMONEVENT_ERR_INIT_UNDONE }表示公共事件服务未初始化；
 *         返回{@link COMMONEVENT_ERR_SUBSCRIBER_NUM_EXCEEDED }表示进程订阅者数量超过200个；
 *         返回{@link COMMONEVENT_ERR_SUBSCRIBER_NUM_EXCEEDED }系统分配内存失败；
 * @since 12
 */
CommonEvent_ErrCode OH_CommonEvent_Subscribe(const CommonEvent_Subscriber* subscriber);

/**
 * @brief 退阅公共事件。
 *
 * @param subscriber 订阅者。
 * @return 返回{@link COMMONEVENT_ERR_OK}表示成功；
 *         返回{@link COMMONEVENT_ERR_INVALID_PARAMETER }表示参数subscriber无效；
 *         返回{@link COMMONEVENT_ERR_SENDING_REQUEST_FAILED }表示IPC请求发送失败；
 *         返回{@link COMMONEVENT_ERR_INIT_UNDONE }表示公共事件服务未初始化；
 * @since 12
 */
CommonEvent_ErrCode OH_CommonEvent_UnSubscribe(const CommonEvent_Subscriber* subscriber);

/**
 * @brief 获取公共事件名称。
 *
 * @param rcvData 公共事件回调数据。
 * @return 返回事件名称。
 * @since 12
 */
const char* OH_CommonEvent_GetEventFromRcvData(const CommonEvent_RcvData* rcvData);

/**
 * @brief 获取公共事件结果代码。
 *
 * @param rcvData 公共事件回调数据。
 * @return 返回公共事件结果代码。
 * @since 12
 */
int32_t OH_CommonEvent_GetCodeFromRcvData(const CommonEvent_RcvData* rcvData);

/**
 * @brief 获取公共事件自定义结果数据。
 *
 * @param rcvData 公共事件回调数据。
 * @return 返回公共事件自定义结果数据。
 * @since 12
 */
const char* OH_CommonEvent_GetDataStrFromRcvData(const CommonEvent_RcvData* rcvData);

/**
 * @brief 获取公共事件包名称。
 *
 * @param rcvData 公共事件回调数据。
 * @return 返回公共事件包名称。
 * @since 12
 */
const char* OH_CommonEvent_GetBundleNameFromRcvData(const CommonEvent_RcvData* rcvData);

/**
 * @brief 获取公共事件附加信息。
 *
 * @param rcvData 公共事件回调数据。
 * @return 返回公共事件附加信息。
 * @since 12
 */
const CommonEvent_Parameters* OH_CommonEvent_GetParametersFromRcvData(const CommonEvent_RcvData* rcvData);

/**
 * @brief 检查附加信息中是否包含键值对信息。
 *
 * @param rcvData 公共事件附加信息。
 * @param key key。
 * @return 返回true表示存在，返回false表示不存在。
 * @since 12
 */
bool OH_CommonEvent_HasKeyInParameters(const CommonEvent_Parameters* para, const char* key);

/**
 * @brief 获取附加信息中int数据信息。
 *
 * @param rcvData 公共事件附加信息。
 * @param key key。
 * @param defaultValue 默认返回值。
 * @return int数据。
 * @since 12
 */
int OH_CommonEvent_GetIntFromParameters(const CommonEvent_Parameters* para, const char* key, const int defaultValue);

/**
 * @brief 获取附加信息中int数组数据信息。
 *
 * @param rcvData 公共事件附加信息。
 * @param key key。
 * @param array 接收数据数组。
 * @return 返回数组长度。
 * @since 12
 */
int32_t OH_CommonEvent_GetIntArrayFromParameters(const CommonEvent_Parameters* para, const char* key, int** array);

/**
 * @brief 获取附加信息中long数据信息。
 *
 * @param rcvData 公共事件附加信息。
 * @param key key。
 * @param defaultValue 默认返回值。
 * @return long数据。
 * @since 12
 */
long OH_CommonEvent_GetLongFromParameters(const CommonEvent_Parameters* para, const char* key, const long defaultValue);

/**
 * @brief 获取附加信息中long数组数据信息。
 *
 * @param rcvData 公共事件附加信息。
 * @param key key。
 * @param array 接收数据数组。
 * @return 返回数组长度。
 * @since 12
 */
int32_t OH_CommonEvent_GetLongArrayFromParameters(const CommonEvent_Parameters* para, const char* key, long** array);

/**
 * @brief 获取附加信息中布尔数据信息。
 *
 * @param rcvData 公共事件附加信息。
 * @param key key。
 * @param defaultValue 默认返回值。
 * @return 布尔数据。
 * @since 12
 */
bool OH_CommonEvent_GetBoolFromParameters(const CommonEvent_Parameters* para, const char* key, const bool defaultValue);

/**
 * @brief 获取附加信息中布尔数组数据信息。
 *
 * @param rcvData 公共事件附加信息。
 * @param key key。
 * @param array 接收数据数组。
 * @return 返回数组长度。
 * @since 12
 */
int32_t OH_CommonEvent_GetBoolArrayFromParameters(const CommonEvent_Parameters* para, const char* key, bool** array);

/**
 * @brief 获取附加信息中字符数据信息。
 *
 * @param rcvData 公共事件附加信息。
 * @param key key。
 * @param defaultValue 默认返回值。
 * @return 字符数据。
 * @since 12
 */
char OH_CommonEvent_GetCharFromParameters(const CommonEvent_Parameters* para, const char* key, const char defaultValue);

/**
 * @brief 获取附加信息中字符数组数据信息。
 *
 * @param rcvData 公共事件附加信息。
 * @param key key。
 * @param array 接收数据数组。
 * @return 返回数组长度。
 * @since 12
 */
int32_t OH_CommonEvent_GetCharArrayFromParameters(const CommonEvent_Parameters* para, const char* key, char** array);

/**
 * @brief 获取附加信息中double数据信息。
 *
 * @param rcvData 公共事件附加信息。
 * @param key key。
 * @param defaultValue 默认返回值。
 * @return 字符数据。
 * @since 12
 */
double OH_CommonEvent_GetDoubleFromParameters(const CommonEvent_Parameters* para, const char* key,
    const double defaultValue);

/**
 * @brief 获取附加信息中double数组数据信息。
 *
 * @param rcvData 公共事件附加信息。
 * @param key key。
 * @param array 接收数据数组。
 * @return 返回数组长度。
 * @since 12
 */
int32_t OH_CommonEvent_GetDoubleArrayFromParameters(const CommonEvent_Parameters* para, const char* key,
    double** array);

#ifdef __cplusplus
}
#endif
#endif // OH_COMMONEVENT_H
/** @} */
