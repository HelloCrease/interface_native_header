/*
 * Copyright (c) 2021-2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup OH_NativeXComponent Native XComponent
 * @{
 *
 * @brief 描述ArkUI XComponent持有的surface和触摸事件，该事件可用于EGL/OpenGLES和媒体数据输入，并显示在ArkUI XComponent上。
 *
 * @since 8
 * @version 1.0
 */

/**
 * @file native_interface_xcomponent.h
 *
 * @brief 声明用于访问Native XComponent的API。
 *
 * @library libace_ndk.z.so
 * @since 8
 * @version 1.0
 */

#ifndef _NATIVE_INTERFACE_XCOMPONENT_H_
#define _NATIVE_INTERFACE_XCOMPONENT_H_

#include <stdint.h>
#include <stdbool.h>

#include "arkui/native_type.h"
#include "arkui/ui_input_event.h"

#include "native_interface_accessibility.h"
#include "native_xcomponent_key_event.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief 枚举API访问状态。
 *
 * @since 8
 * @version 1.0
 */
enum {
    /** 成功结果。 */
    OH_NATIVEXCOMPONENT_RESULT_SUCCESS = 0,
    /** 失败结果。 */
    OH_NATIVEXCOMPONENT_RESULT_FAILED = -1,
    /** 无效参数。 */
    OH_NATIVEXCOMPONENT_RESULT_BAD_PARAMETER = -2,
};

/**
 * @brief 触摸事件类型。
 * @since 8
 */
typedef enum {
    /** 手指按下时触发触摸事件。 */
    OH_NATIVEXCOMPONENT_DOWN = 0,
    /** 手指抬起时触发触摸事件。 */
    OH_NATIVEXCOMPONENT_UP,
    /** 手指按下状态下在屏幕上移动时触发触摸事件。 */
    OH_NATIVEXCOMPONENT_MOVE,
    /** 触摸事件取消时触发事件。 */
    OH_NATIVEXCOMPONENT_CANCEL,
    /** 无效的触摸类型。 */
    OH_NATIVEXCOMPONENT_UNKNOWN,
} OH_NativeXComponent_TouchEventType;

/**
 * @brief 触摸点工具类型
 *
 * @since 9
 * @version 1.0
 */
typedef enum {
    /** 未识别工具类型。 */
    OH_NATIVEXCOMPONENT_TOOL_TYPE_UNKNOWN = 0,
    /** 表示用手指。 */
    OH_NATIVEXCOMPONENT_TOOL_TYPE_FINGER,
    /** 表示用触笔。 */
    OH_NATIVEXCOMPONENT_TOOL_TYPE_PEN,
    /** 表示用橡皮擦。 */
    OH_NATIVEXCOMPONENT_TOOL_TYPE_RUBBER,
    /** 表示用画笔。 */
    OH_NATIVEXCOMPONENT_TOOL_TYPE_BRUSH,
    /** 表示用铅笔。 */
    OH_NATIVEXCOMPONENT_TOOL_TYPE_PENCIL,
    /** 表示用气笔。 */
    OH_NATIVEXCOMPONENT_TOOL_TYPE_AIRBRUSH,
    /** 表示用鼠标。 */
    OH_NATIVEXCOMPONENT_TOOL_TYPE_MOUSE,
    /** 表示用晶状体。 */
    OH_NATIVEXCOMPONENT_TOOL_TYPE_LENS,
} OH_NativeXComponent_TouchPointToolType;

/**
 * @brief 触摸事件源类型.
 *
 * @since 9
 * @version 1.0
 */
typedef enum {
    /** 未知的输入源类型。 */
    OH_NATIVEXCOMPONENT_SOURCE_TYPE_UNKNOWN = 0,
    /** 表示输入源生成鼠标多点触摸事件。 */
    OH_NATIVEXCOMPONENT_SOURCE_TYPE_MOUSE,
    /** 表示输入源生成一个触摸屏多点触摸事件。 */
    OH_NATIVEXCOMPONENT_SOURCE_TYPE_TOUCHSCREEN,
    /** 表示输入源生成一个触摸板多点触摸事件。 */
    OH_NATIVEXCOMPONENT_SOURCE_TYPE_TOUCHPAD,
    /** 表示输入源生成一个操纵杆多点触摸事件。 */
    OH_NATIVEXCOMPONENT_SOURCE_TYPE_JOYSTICK,
    /**
     * @brief 表示输入源生成一个键盘事件。
     *
     * @since 10
     * @version 1.0
     */
    OH_NATIVEXCOMPONENT_SOURCE_TYPE_KEYBOARD,
} OH_NativeXComponent_EventSourceType;

/**
 * @brief 鼠标事件动作.
 *
 * @since 9
 * @version 1.0
 */
typedef enum {
    /** 无效鼠标事件 。*/
    OH_NATIVEXCOMPONENT_MOUSE_NONE = 0,
    /** 鼠标按键按下时触发鼠标事件。 */
    OH_NATIVEXCOMPONENT_MOUSE_PRESS,
    /** 鼠标按键松开时触发鼠标事件。 */
    OH_NATIVEXCOMPONENT_MOUSE_RELEASE,
    /** 鼠标在屏幕上移动时触发鼠标事件。 */
    OH_NATIVEXCOMPONENT_MOUSE_MOVE,
} OH_NativeXComponent_MouseEventAction;

/**
 * @brief 鼠标事件按键。
 *
 * @since 9
 * @version 1.0
 */
typedef enum {
    /** 鼠标无按键操作时触发鼠标事件。 */
    OH_NATIVEXCOMPONENT_NONE_BUTTON = 0,
    /** 按下鼠标左键时触发鼠标事件。 */
    OH_NATIVEXCOMPONENT_LEFT_BUTTON = 0x01,
    /** 按下鼠标右键时触发鼠标事件。 */
    OH_NATIVEXCOMPONENT_RIGHT_BUTTON = 0x02,
    /** 按下鼠标中键时触发鼠标事件。 */
    OH_NATIVEXCOMPONENT_MIDDLE_BUTTON = 0x04,
    /** 按下鼠标左侧后退键时触发鼠标事件。 */
    OH_NATIVEXCOMPONENT_BACK_BUTTON = 0x08,
    /** 按下鼠标左侧前进键时触发鼠标事件。 */
    OH_NATIVEXCOMPONENT_FORWARD_BUTTON = 0x10,
} OH_NativeXComponent_MouseEventButton;

#define OH_NATIVE_XCOMPONENT_OBJ ("__NATIVE_XCOMPONENT_OBJ__")
const uint32_t OH_XCOMPONENT_ID_LEN_MAX = 128;
const uint32_t OH_MAX_TOUCH_POINTS_NUMBER = 10;

typedef struct {
    /** 手指的唯一标识符。 */
    int32_t id = 0;
    /** 触摸点相对于XComponent所在应用窗口左上角的x坐标。 */
    float screenX = 0.0;
    /** 触摸点相对于XComponent所在应用窗口左上角的y坐标。 */
    float screenY = 0.0;
    /** 触摸点相对于XComponent组件左边缘的x坐标。 */
    float x = 0.0;
    /** 触摸点相对于XComponent组件上边缘的y坐标。 */
    float y = 0.0;
    /** 触摸事件的触摸类型。 */
    OH_NativeXComponent_TouchEventType type = OH_NativeXComponent_TouchEventType::OH_NATIVEXCOMPONENT_UNKNOWN;
    /** 指垫和屏幕之间的接触面积。 */
    double size = 0.0;
    /** 当前触摸事件的压力。 */
    float force = 0.0;
    /** 当前触摸事件的时间戳。触发事件时距离系统启动的时间间隔，单位纳秒。 */
    long long timeStamp = 0;
    /** 当前点是否被按下。 */
    bool isPressed = false;
} OH_NativeXComponent_TouchPoint;

/**
 * @brief 触摸事件。
 *
 * @since 8
 * @version 1.0
 */
typedef struct {
    /** 手指的唯一标识符。 */
    int32_t id = 0;
    /** 触摸点相对于所在应用窗口左上角的x坐标。 */
    float screenX = 0.0;
    /** 触摸点相对于所在应用窗口左上角的y坐标。 */
    float screenY = 0.0;
    /** 触摸点相对于XComponent组件左边缘的x坐标。 */
    float x = 0.0;
    /** 触摸点相对于XComponent组件上边缘的y坐标。 */
    float y = 0.0;
    /** 触摸事件的触摸类型。 */
    OH_NativeXComponent_TouchEventType type = OH_NativeXComponent_TouchEventType::OH_NATIVEXCOMPONENT_UNKNOWN;
    /** 指垫和屏幕之间的接触面积。 */
    double size = 0.0;
    /** 当前触摸事件的压力。 */
    float force = 0.0;
    /** 产生当前触摸事件的设备的ID。 */
    int64_t deviceId = 0;
    /** 当前触摸事件的时间戳。触发事件时距离系统启动的时间间隔，单位纳秒。 */
    long long timeStamp = 0;
    /** 当前触摸点的数组。 */
    OH_NativeXComponent_TouchPoint touchPoints[OH_MAX_TOUCH_POINTS_NUMBER];
    /** 当前接触点的数量。 */
    uint32_t numPoints = 0;
} OH_NativeXComponent_TouchEvent;

/**
 * @brief 鼠标事件。
 *
 * @since 9
 * @version 1.0
 */
typedef struct {
    /** 点击触点相对于当前组件左上角的x轴坐标。 */
    float x;
    /** 点击触点相对于当前组件左上角的y轴坐标。 */
    float y;
    /** 点击触点相对于所在应用屏幕左上角的x轴坐标。 */
    float screenX;
    /** 点击触点相对于所在应用屏幕左上角的y轴坐标。 */
    float screenY;
    /** 当前鼠标事件的时间戳。触发事件时距离系统启动的时间间隔，单位纳秒。 */
    int64_t timestamp;
    /** 当前鼠标事件动作。 */
    OH_NativeXComponent_MouseEventAction action;
    /** 鼠标事件按键。 */
    OH_NativeXComponent_MouseEventButton button;
} OH_NativeXComponent_MouseEvent;

/**
 * @brief 提供封装的OH_NativeXComponent实例。
 *
 * @since 8
 * @version 1.0
 */
typedef struct OH_NativeXComponent OH_NativeXComponent;

/**
 * @brief 注册surface生命周期和触摸事件回调。
 *
 * @since 8
 * @version 1.0
 */
typedef struct OH_NativeXComponent_Callback {
    /** 创建surface时调用。 */
    void (*OnSurfaceCreated)(OH_NativeXComponent* component, void* window);
    /** 当surface改变时调用。 */
    void (*OnSurfaceChanged)(OH_NativeXComponent* component, void* window);
    /** 当surface被破坏时调用。 */
    void (*OnSurfaceDestroyed)(OH_NativeXComponent* component, void* window);
    /** 当触摸事件被触发时调用。 */
    void (*DispatchTouchEvent)(OH_NativeXComponent* component, void* window);
} OH_NativeXComponent_Callback;

/**
 * @brief 注册鼠标事件的回调。
 *
 * @since 9
 * @version 1.0
 */
typedef struct OH_NativeXComponent_MouseEvent_Callback {
    /** 当鼠标事件被触发时调用。 */
    void (*DispatchMouseEvent)(OH_NativeXComponent* component, void* window);
    /** 当悬停事件被触发时调用。 */
    void (*DispatchHoverEvent)(OH_NativeXComponent* component, bool isHover);
} OH_NativeXComponent_MouseEvent_Callback;

struct OH_NativeXComponent_KeyEvent;
/**
 * @brief 提供封装的OH_NativeXComponent_KeyEvent实例。
 *
 * @since 10
 * @version 1.0
 */
typedef struct OH_NativeXComponent_KeyEvent OH_NativeXComponent_KeyEvent;

/**
 * @brief 定义期望帧率范围。
 *
 * @since 11
 * @version 1.0
 */
typedef struct {
    /** 期望帧率范围最小值。 */
    int32_t min;
    /** 期望帧率范围最大值。 */
    int32_t max;
    /** 期望帧率 */
    int32_t expected;
} OH_NativeXComponent_ExpectedRateRange;

/**
 * @brief 获取ArkUI XComponent的id。
 *
 * @param component 表示指向OH_NativeXComponent实例的指针。
 * @param id 指示用于保存此OH_NativeXComponent实例的ID的字符缓冲区。
 *        请注意，空终止符将附加到字符缓冲区，因此字符缓冲区的大小应至少比真实id长度大一个单位。
 *        建议字符缓冲区的大小为[OH_XCOMPONENT_ID_LEN_MAX + 1]。
 * @param size 指示指向id长度的指针。
 * @return 返回执行的状态代码。
 * @since 8
 * @version 1.0
 */
int32_t OH_NativeXComponent_GetXComponentId(OH_NativeXComponent* component, char* id, uint64_t* size);

/**
 * @brief 获取ArkUI XComponent持有的surface的大小。
 *
 * @param component 表示指向OH_NativeXComponent实例的指针。
 * @param window 表示NativeWindow句柄。
 * @param width 指示指向当前surface宽度的指针。
 * @param height 指示指向当前surface高度的指针。
 * @return 返回执行的状态代码。
 * @since 8
 * @version 1.0
 */
int32_t OH_NativeXComponent_GetXComponentSize(
    OH_NativeXComponent* component, const void* window, uint64_t* width, uint64_t* height);

/**
 * @brief 获取ArkUI XComponent持有的surface相对其父组件左顶点的偏移量。
 *
 * @param component 表示指向OH_NativeXComponent实例的指针。
 * @param window 表示NativeWindow句柄。
 * @param x 指示指向当前surface相对于XComponent父组件左顶点x坐标的指针。
 * @param y 指示指向当前surface相对于XComponent父组件左顶点y坐标的指针。
 * @return 返回执行的状态代码。
 * @since 8
 * @version 1.0
 */
int32_t OH_NativeXComponent_GetXComponentOffset(
    OH_NativeXComponent* component, const void* window, double* x, double* y);

/**
 * @brief 获取ArkUI XComponent调度的触摸事件。
 *
 * @param component 表示指向OH_NativeXComponent实例的指针。
 * @param window 表示NativeWindow句柄。
 * @param touchEvent 指示指向当前触摸事件的指针。
 * @return 返回执行的状态代码。
 * @since 8
 * @version 1.0
 */
int32_t OH_NativeXComponent_GetTouchEvent(
    OH_NativeXComponent* component, const void* window, OH_NativeXComponent_TouchEvent* touchEvent);

/**
 * @brief 获取ArkUI XComponent触摸点工具类型。
 * @param component 表示指向OH_NativeXComponent实例的指针。
 * @param pointIndex 表示触摸点的指针索引。
 * @param toolType 表示指向工具类型的指针。
 * @return 返回执行的状态代码。
 * @since 9
 * @version 1.0
 */
int32_t OH_NativeXComponent_GetTouchPointToolType(OH_NativeXComponent* component, uint32_t pointIndex,
    OH_NativeXComponent_TouchPointToolType* toolType);

/**
 * @brief 获取ArkUI XComponent触摸点倾斜与X轴角度。
 *
 * @param component 表示指向OH_NativeXComponent实例的指针。
 * @param pointIndex 表示触摸点的指针索引。
 * @param tiltX 表示指向X倾斜度的指针。
 * @return 返回执行的状态代码。
 * @since 9
 * @version 1.0
 */
int32_t OH_NativeXComponent_GetTouchPointTiltX(OH_NativeXComponent* component, uint32_t pointIndex, float* tiltX);

/**
 * @brief 获取ArkUI XComponent触摸点倾斜与Y轴角度。
 *
 * @param component 表示指向OH_NativeXComponent实例的指针。
 * @param pointIndex 表示触摸点的指针索引。
 * @param tiltY 表示指向Y倾斜度的指针。
 * @return 返回执行的状态代码。
 * @since 9
 * @version 1.0
 */
int32_t OH_NativeXComponent_GetTouchPointTiltY(OH_NativeXComponent* component, uint32_t pointIndex, float* tiltY);

/**
 * @brief 获取ArkUI XComponent触摸点相对于应用窗口左上角的X坐标。
 *
 * @param component 表示指向OH_NativeXComponent实例的指针。
 * @param pointIndex 表示触摸点的指针索引。
 * @param windowX 表示指向触摸点相对于应用窗口左上角的X坐标的指针。
 * @return 返回执行的状态代码。
 * @since 12
 * @version 1.0
 */
int32_t OH_NativeXComponent_GetTouchPointWindowX(OH_NativeXComponent* component, uint32_t pointIndex, float* windowX);

/**
 * @brief 获取ArkUI XComponent触摸点相对于应用窗口左上角的Y坐标。
 *
 * @param component 表示指向OH_NativeXComponent实例的指针。
 * @param pointIndex 表示触摸点的指针索引。
 * @param windowY 表示指向触摸点相对于应用窗口左上角的Y坐标的指针。
 * @return 返回执行的状态代码。
 * @since 12
 * @version 1.0
 */
int32_t OH_NativeXComponent_GetTouchPointWindowY(OH_NativeXComponent* component, uint32_t pointIndex, float* windowY);

/**
 * @brief 获取ArkUI XComponent触摸点相对于应用所在屏幕左上角的X坐标。
 *
 * @param component 表示指向OH_NativeXComponent实例的指针。
 * @param pointIndex 表示触摸点的指针索引。
 * @param displayX 表示指向触摸点相对于应用所在屏幕左上角的X坐标的指针。
 * @return 返回执行的状态代码。
 * @since 12
 * @version 1.0
 */
int32_t OH_NativeXComponent_GetTouchPointDisplayX(OH_NativeXComponent* component, uint32_t pointIndex, float* displayX);

/**
 * @brief 获取ArkUI XComponent触摸点相对于应用所在屏幕左上角的Y坐标。
 *
 * @param component 表示指向OH_NativeXComponent实例的指针。
 * @param pointIndex 表示触摸点的指针索引。
 * @param displayY 表示指向触摸点相对于应用所在屏幕左上角的Y坐标的指针。
 * @return 返回执行的状态代码。
 * @since 12
 * @version 1.0
 */
int32_t OH_NativeXComponent_GetTouchPointDisplayY(OH_NativeXComponent* component, uint32_t pointIndex, float* displayY);

/**
 * @brief 获取ArkUI XComponent调度的鼠标事件。
 *
 * @param component 表示指向OH_NativeXComponent实例的指针。
 * @param window 表示NativeWindow句柄
 * @param mouseEvent 指示指向当前鼠标事件的指针。
 * @return 返回执行的状态代码。
 * @since 9
 * @version 1.0
 */
int32_t OH_NativeXComponent_GetMouseEvent(
    OH_NativeXComponent* component, const void* window, OH_NativeXComponent_MouseEvent* mouseEvent);

/**
 * @brief 为此OH_NativeXComponent实例注册回调。
 *
 * @param component 表示指向OH_NativeXComponent实例的指针。
 * @param callback 指示指向surface生命周期和触摸事件回调的指针。
 * @return 返回执行的状态代码。
 * @since 8
 * @version 1.0
 */
int32_t OH_NativeXComponent_RegisterCallback(OH_NativeXComponent* component, OH_NativeXComponent_Callback* callback);

/**
 * @brief 为此OH_NativeXComponent实例注册鼠标事件回调。
 *
 * @param component 表示指向OH_NativeXComponent实例的指针。
 * @param callback 指示指向鼠标事件回调的指针。
 * @return 返回执行的状态代码。
 * @since 9
 * @version 1.0
 */
int32_t OH_NativeXComponent_RegisterMouseEventCallback(
    OH_NativeXComponent* component, OH_NativeXComponent_MouseEvent_Callback* callback);

/**
 * @brief 为此OH_NativeXComponent实例注册获焦事件回调。
 *
 * @param component 表示指向OH_NativeXComponent实例的指针。
 * @param callback 指示指向获焦事件回调的指针。
 * @return 返回执行的状态代码。
 * @since 10
 * @version 1.0
 */
int32_t OH_NativeXComponent_RegisterFocusEventCallback(
    OH_NativeXComponent* component, void (*callback)(OH_NativeXComponent* component, void* window));

/**
 * @brief 为此OH_NativeXComponent实例注册按键事件回调。
 *
 * @param component 表示指向OH_NativeXComponent实例的指针。
 * @param callback 指示指向按键事件回调的指针。
 * @return 返回执行的状态代码。
 * @since 10
 * @version 1.0
 */
int32_t OH_NativeXComponent_RegisterKeyEventCallback(
    OH_NativeXComponent* component, void (*callback)(OH_NativeXComponent* component, void* window));

/**
 * @brief 为此OH_NativeXComponent实例注册失焦事件回调。
 *
 * @param component 表示指向OH_NativeXComponent实例的指针。
 * @param callback 指示指向失焦事件回调的指针。
 * @return 返回执行的状态代码。
 * @since 10
 * @version 1.0
 */
int32_t OH_NativeXComponent_RegisterBlurEventCallback(
    OH_NativeXComponent* component, void (*callback)(OH_NativeXComponent* component, void* window));

/**
 * @brief 获取ArkUI XComponent调度的按键事件。
 *
 * @param component 表示指向OH_NativeXComponent实例的指针。
 * @param keyEvent 表示指向当前按键事件指针的指针。
 * @return 返回执行的状态代码。
 * @since 10
 * @version 1.0
 */
int32_t OH_NativeXComponent_GetKeyEvent(OH_NativeXComponent* component, OH_NativeXComponent_KeyEvent** keyEvent);

/**
 * @brief 获取传入按键事件的动作。
 *
 * @param keyEvent 表示指向OH_NativeXComponent_KeyEvent实例的指针。
 * @param action 表示指向按键事件动作的指针。
 * @return 返回执行的状态代码。
 * @since 10
 * @version 1.0
 */
int32_t OH_NativeXComponent_GetKeyEventAction(
    OH_NativeXComponent_KeyEvent* keyEvent, OH_NativeXComponent_KeyAction* action);

/**
 * @brief 获取传入按键事件的按键码。
 *
 * @param keyEvent 表示指向OH_NativeXComponent_KeyEvent实例的指针。
 * @param code 表示指向按键事件按键码的指针。
 * @return 返回执行的状态代码。
 * @since 10
 * @version 1.0
 */
int32_t OH_NativeXComponent_GetKeyEventCode(OH_NativeXComponent_KeyEvent* keyEvent, OH_NativeXComponent_KeyCode* code);

/**
 * @brief 获取传入按键事件的事件源类型。
 *
 * @param keyEvent 表示指向OH_NativeXComponent_KeyEvent实例的指针。
 * @param sourceType 表示指向按键事件事件源类型的指针。
 * @return 返回执行的状态代码。
 * @since 10
 * @version 1.0
 */
int32_t OH_NativeXComponent_GetKeyEventSourceType(
    OH_NativeXComponent_KeyEvent* keyEvent, OH_NativeXComponent_EventSourceType* sourceType);

/**
 * @brief 获取传入按键事件的设备id。
 *
 * @param keyEvent 表示指向OH_NativeXComponent_KeyEvent实例的指针。
 * @param deviceId 表示指向按键事件设备id的指针。
 * @return 返回执行的状态代码。
 * @since 10
 * @version 1.0
 */
int32_t OH_NativeXComponent_GetKeyEventDeviceId(OH_NativeXComponent_KeyEvent* keyEvent, int64_t* deviceId);

/**
 * @brief 获取传入按键事件的时间戳。
 *
 * @param keyEvent 表示指向OH_NativeXComponent_KeyEvent实例的指针。
 * @param timeStamp 表示指向按键事件时间戳的指针。
 * @return 返回执行的状态代码。
 * @since 10
 * @version 1.0
 */
int32_t OH_NativeXComponent_GetKeyEventTimeStamp(OH_NativeXComponent_KeyEvent* keyEvent, int64_t* timeStamp);

/**
 * @brief 设置期望帧率范围。
 *
 * @param component 表示指向OH_NativeXComponent实例的指针。
 * @param range 表示指向期望帧率范围的指针
 * @return 返回执行的状态代码。
 * @since 11
 * @version 1.0
 */
int32_t OH_NativeXComponent_SetExpectedFrameRateRange(
    OH_NativeXComponent* component, OH_NativeXComponent_ExpectedRateRange* range);

/**
 * @brief 为此OH_NativeXComponent实例注册显示更新回调，并使能每帧回调此函数。
 *
 * @param component 表示指向OH_NativeXComponent实例的指针。
 * @param callback 指示指向显示更新回调的指针。
 * @return 返回执行的状态代码。
 * @since 11
 * @version 1.0
 */
int32_t OH_NativeXComponent_RegisterOnFrameCallback(OH_NativeXComponent* component,
    void (*callback)(OH_NativeXComponent* component, uint64_t timestamp, uint64_t targetTimestamp));

/**
 * @brief 为此OH_NativeXComponent实例取消注册回调函数，并关闭每帧回调此函数。
 *
 * @param component 表示指向OH_NativeXComponent实例的指针。
 * @return 返回执行的状态代码。
 * @since 11
 * @version 1.0
 */
int32_t OH_NativeXComponent_UnregisterOnFrameCallback(OH_NativeXComponent* component);

/**
 * @brief 将通过ArkUI的native接口创建出来的UI组件挂载到当前XComponent上。
 *
 * @param component 表示指向OH_NativeXComponent实例的指针。
 * @param root 指向Native接口创建的组件实例的指针。
 * @return 0 - 成功。
 *         401 - 参数异常。
 *
 * @since 12
 */
int32_t OH_NativeXComponent_AttachNativeRootNode(OH_NativeXComponent* component, ArkUI_NodeHandle root);

/**
 * @brief 将ArkUI的native组件从当前XComponent上卸载。
 *
 * @param component 表示指向OH_NativeXComponent实例的指针。
 * @param root 指向Native接口创建的组件实例的指针。
 * @return 0 - 成功。
 *         401 - 参数异常。
 *
 * @since 12
 */
int32_t OH_NativeXComponent_DetachNativeRootNode(OH_NativeXComponent* component, ArkUI_NodeHandle root);

/**
 * @brief 为此OH_NativeXComponent实例注册UI输入事件回调，并使能收到UI输入事件时回调此函数。
 *
 * @param component 表示指向OH_NativeXComponent实例的指针。
 * @param callback 指示指向UI输入事件回调的指针。
 * @param type 指示当前UI输入事件的类型。
 * @return 0 - 成功。
 *         401 - 参数异常。
 *
 * @since 12
 */
int32_t OH_NativeXComponent_RegisterUIInputEventCallback(
    OH_NativeXComponent *component,
    void (*callback)(OH_NativeXComponent *component, ArkUI_UIInputEvent *event,
                     ArkUI_UIInputEvent_Type type),
    ArkUI_UIInputEvent_Type type);

/**
 * @brief 为此OH_NativeXComponent实例注册自定义事件拦截回调，并使能在做触摸测试时回调此函数。
 *
 * @param component 表示指向OH_NativeXComponent实例的指针。
 * @param callback 指示指向自定义事件拦截回调的指针。
 * @return 0 - 成功。
 *         401 - 参数异常。
 * @since 12
 */
int32_t OH_NativeXComponent_RegisterOnTouchInterceptCallback(
    OH_NativeXComponent* component, HitTestMode (*callback)(OH_NativeXComponent* component, ArkUI_UIInputEvent* event));

/**
 * @brief 为此OH_NativeXComponent实例设置是否需要软键盘。
 * @param component 表示指向OH_NativeXComponent实例的指针。
 * @param needSoftKeyboard 表示此OH_NativeXComponent实例是否需要软键盘。
 * @return 返回执行的状态代码。
 * @since 12
 * @version 1.0
 */
int32_t OH_NativeXComponent_SetNeedSoftKeyboard(OH_NativeXComponent* component, bool needSoftKeyboard);

/**
 * @brief 为此OH_NativeXComponent实例注册surface显示回调，该回调在应用窗口已经回到前台时触发。
 *
 * @param component 表示指向OH_NativeXComponent实例的指针。
 * @param callback 指示指向surface显示回调的指针。
 * @return 返回执行的状态代码。
 * @since 12
 * @version 1.0
 */
int32_t OH_NativeXComponent_RegisterSurfaceShowCallback(
    OH_NativeXComponent* component, void (*callback)(OH_NativeXComponent* component, void* window));

/**
 * @brief 为此OH_NativeXComponent实例注册surface隐藏回调，该回调在应用窗口已经进入后台时触发。
 *
 * @param component 表示指向OH_NativeXComponent实例的指针。
 * @param callback 指示指向surface隐藏回调的指针。
 * @return 返回执行的状态代码。
 * @since 12
 * @version 1.0
 */
int32_t OH_NativeXComponent_RegisterSurfaceHideCallback(
    OH_NativeXComponent* component, void (*callback)(OH_NativeXComponent* component, void* window));

/**
 * @brief 获取ArkUI XComponent触摸事件的输入设备类型。
 *
 * @param component 表示指向OH_NativeXComponent实例的指针。
 * @param pointId 表示触摸点的id。
 * @param sourceType 指示指向返回设备类型的指针。
 * @return OH_NATIVEXCOMPONENT_RESULT_SUCCESS - 成功。
 *         OH_NATIVEXCOMPONENT_RESULT_BAD_PARAMETER - 参数异常。
 *         OH_NATIVEXCOMPONENT_RESULT_FAILED - 其他错误。
 * @since 12
 * @version 1.0
 */
int32_t OH_NativeXComponent_GetTouchEventSourceType(
    OH_NativeXComponent* component, int32_t pointId, OH_NativeXComponent_EventSourceType* sourceType);

/**
 * @brief 基于Native接口创建的组件实例获取OH_NativeXComponent类型的指针。
 *
 * @param node 指向Native接口创建的组件实例的指针。
 * @return 表示指向OH_NativeXComponent实例的指针。
 * @since 12
 * @version 1.0
 */
OH_NativeXComponent* OH_NativeXComponent_GetNativeXComponent(ArkUI_NodeHandle node);

/**
 * @brief 获取ArkUI XComponent的ArkUI_AccessibilityProvider指针。
 *
 * @param component 表示指向OH_NativeXComponent实例的指针。
 * @param provider 表示指向ArkUI_AccessibilityProvider实例的指针。
 * @return OH_NATIVEXCOMPONENT_RESULT_SUCCESS - 成功。
 *         OH_NATIVEXCOMPONENT_RESULT_BAD_PARAMETER - 参数异常。
 *         OH_NATIVEXCOMPONENT_RESULT_FAILED - 其他错误。
 * @since 13
 */
int32_t OH_NativeXComponent_GetNativeAccessibilityProvider(
    OH_NativeXComponent* component, ArkUI_AccessibilityProvider** handle);

/**
 * @brief 为此OH_NativeXComponent实例注册有返回值的按键事件回调。
 *
 * @param component 表示指向OH_NativeXComponent实例的指针。
 * @param callback 表示指向有返回值的按键事件回调的指针。
 * @return 返回执行的状态代码。
 *         OH_NATIVEXCOMPONENT_RESULT_SUCCESS - 回调函数注册成功。
 *         OH_NATIVEXCOMPONENT_RESULT_BAD_PARAMETER - 传入参数异常。
 * @since 14
 * @version 1.0
 */
int32_t OH_NativeXComponent_RegisterKeyEventCallbackWithResult(
    OH_NativeXComponent* component, bool (*callback)(OH_NativeXComponent* component, void* window));

#ifdef __cplusplus
};
#endif
#endif // _NATIVE_INTERFACE_XCOMPONENT_H_
