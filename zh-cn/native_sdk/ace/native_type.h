/*
 * Copyright (c) 2024 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup ArkUI_NativeModule
 * @{
 *
 * @brief 提供ArkUI在Native侧的UI能力，如UI组件创建销毁、树节点操作，属性设置，事件监听等。
 *
 * @since 12
 */

/**
 * @file native_type.h
 *
 * @brief 提供NativeModule公共的类型定义。
 *
 * @library libace_ndk.z.so
 * @syscap SystemCapability.ArkUI.ArkUI.Full
 * @since 12
 */

#ifndef ARKUI_NATIVE_TYPE_H
#define ARKUI_NATIVE_TYPE_H

#include <stdint.h>
#include "drawable_descriptor.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief 提供ArkUI native组件实例对象定义。
 *
 * @since 12
 */
struct ArkUI_Node;

/**
 * @brief 提供ArkUI在native侧的自定义弹窗控制器对象定义。
 *
 * @since 12
 */
struct ArkUI_NativeDialog;

/**
 * @brief 约束尺寸，组件布局时，进行尺寸范围限制。
 *
 * @since 12
 */
typedef struct ArkUI_LayoutConstraint ArkUI_LayoutConstraint;

/**
 * @brief 定义组件绘制上下文类型结构。
 *
 * @since 12
 */
typedef struct ArkUI_DrawContext ArkUI_DrawContext;

/**
 * @brief 定义ArkUI native组件实例对象指针定义。
 *
 * @since 12
 */
typedef struct ArkUI_Node* ArkUI_NodeHandle;

/**
 * @brief 定义ArkUI在Native侧的自定义弹窗控制器对象指针。
 *
 * @since 12
 */
typedef struct ArkUI_NativeDialog* ArkUI_NativeDialogHandle;

/**
 * @brief 定义FlowItem分组配置信息。
 *
 * @since 12
 */
typedef struct ArkUI_WaterFlowSectionOption ArkUI_WaterFlowSectionOption;

/**
 * @brief 定义ListItemSwipeActionOption方法内Item的配置信息。
 *
 * @since 12
 */
typedef struct ArkUI_ListItemSwipeActionItem ArkUI_ListItemSwipeActionItem;

/**
 * @brief 定义ListItemSwipeActionOption方法的配置信息。
 *
 * @since 12
 */
typedef struct ArkUI_ListItemSwipeActionOption ArkUI_ListItemSwipeActionOption;

/**
 * @brief 提供ArkUI native UI的上下文实例对象定义。
 *
 * @since 12
 */
struct ArkUI_Context;

/**
 * @brief 定义ArkUI native UI的上下文实例对象指针定义。
 *
 * @since 12
 */
typedef struct ArkUI_Context* ArkUI_ContextHandle;

/**
 * @brief 定义ArkUI NodeContent实例在Native侧的实例对象指针定义。
 *
 * @since 12
 */
typedef struct ArkUI_NodeContent* ArkUI_NodeContentHandle;

/**
 * @brief 指定设置在相对容器中子组件的对齐规则。
 *
 * @since 12
 */
typedef struct ArkUI_AlignmentRuleOption ArkUI_AlignmentRuleOption;

/**
 * @brief guideLine参数，用于定义guideline的id、方向和位置。
 *
 * @since 12
 */
typedef struct ArkUI_GuidelineOption ArkUI_GuidelineOption;

/**
 * @brief barrier参数，用于定义barrier的id、方向和生成时所依赖的组件。
 *
 * @since 12
 */
typedef struct ArkUI_BarrierOption ArkUI_BarrierOption;

/**
 * @brief 定义图片帧信息。
 *
 * @since 12
 */
typedef struct ArkUI_ImageAnimatorFrameInfo ArkUI_ImageAnimatorFrameInfo;

/**
 * @brief 定义List的ChildrenMainSize类信息。
 *
 * @since 12
 */
typedef struct ArkUI_ListChildrenMainSize ArkUI_ListChildrenMainSize;

/**
 * @brief 事件回调类型。
 *
 * @since 12
 */
typedef struct {
    /** 自定义类型。*/
    void* userData;
    /** 事件回调。*/
    void (*callback)(void* userData);
} ArkUI_ContextCallback;

/**
 * @brief ArkUI在native侧的数字类型定义。
 *
 * @since 12
 */
typedef union {
    /** 浮点类型。*/
    float f32;
    /** 有符号整型。*/
    int32_t i32;
    /** 无符号整型。*/
    uint32_t u32;
} ArkUI_NumberValue;

/**
 * @brief 定义布局对齐枚举值。
 *
 * @since 12
 */
typedef enum {
    /** 顶部起始。 */
    ARKUI_ALIGNMENT_TOP_START = 0,
    /** 顶部居中。*/
    ARKUI_ALIGNMENT_TOP,
    /** 顶部尾端。*/
    ARKUI_ALIGNMENT_TOP_END,
    /** 起始端纵向居中。*/
    ARKUI_ALIGNMENT_START,
    /** 横向和纵向居中。*/
    ARKUI_ALIGNMENT_CENTER,
    /** 尾端纵向居中。*/
    ARKUI_ALIGNMENT_END,
    /** 底部起始端。*/
    ARKUI_ALIGNMENT_BOTTOM_START,
    /** 底部横向居中。*/
    ARKUI_ALIGNMENT_BOTTOM,
    /** 底部尾端。*/
    ARKUI_ALIGNMENT_BOTTOM_END,
} ArkUI_Alignment;

/**
 * @brief 定义图片重复铺设枚举值。
 *
 * @since 12
 */
typedef enum {
    /** 不重复。 */
    ARKUI_IMAGE_REPEAT_NONE = 0,
    /** 在X轴方向重复。 */
    ARKUI_IMAGE_REPEAT_X,
    /** 在Y轴方向重复。 */
    ARKUI_IMAGE_REPEAT_Y,
    /** 在X轴和Y轴方向重复。 */
    ARKUI_IMAGE_REPEAT_XY,
} ArkUI_ImageRepeat;

/**
 * @brief 定义字体样式枚举值。
 *
 * @since 12
 */
typedef enum {
    /** 标准字体样式。 */
    ARKUI_FONT_STYLE_NORMAL = 0,
    /** 斜体字体样式。 */
    ARKUI_FONT_STYLE_ITALIC
} ArkUI_FontStyle;

/**
 * @brief 定义字体粗细/字重枚举值。
 *
 * @since 12
 */
typedef enum {
    /** 100 */
    ARKUI_FONT_WEIGHT_W100 = 0,
    /** 200 */
    ARKUI_FONT_WEIGHT_W200,
    /** 300 */
    ARKUI_FONT_WEIGHT_W300,
    /** 400 */
    ARKUI_FONT_WEIGHT_W400,
    /** 500 */
    ARKUI_FONT_WEIGHT_W500,
    /** 600 */
    ARKUI_FONT_WEIGHT_W600,
    /** 700 */
    ARKUI_FONT_WEIGHT_W700,
    /** 800 */
    ARKUI_FONT_WEIGHT_W800,
    /** 900 */
    ARKUI_FONT_WEIGHT_W900,
    /** 字体较粗。 */
    ARKUI_FONT_WEIGHT_BOLD,
    /** 字体粗细正常 */
    ARKUI_FONT_WEIGHT_NORMAL,
    /** 字体非常粗。 */
    ARKUI_FONT_WEIGHT_BOLDER,
    /** 字体较细。 */
    ARKUI_FONT_WEIGHT_LIGHTER,
    /** 字体粗细适中。 */
    ARKUI_FONT_WEIGHT_MEDIUM,
    /** 字体粗细正常 */
    ARKUI_FONT_WEIGHT_REGULAR,
} ArkUI_FontWeight;

/**
 * @brief 定义字体水平对齐样式枚举值。
 *
 * @since 12
 */
typedef enum {
    /** 水平对齐首部。 */
    ARKUI_TEXT_ALIGNMENT_START = 0,
    /** 水平居中对齐。 */
    ARKUI_TEXT_ALIGNMENT_CENTER,
    /** 水平对齐尾部。 */
    ARKUI_TEXT_ALIGNMENT_END,
    /** 双端对齐。 */
    ARKUI_TEXT_ALIGNMENT_JUSTIFY,
} ArkUI_TextAlignment;

/**
 * @brief 定义单行文本输入法回车键类型枚举值。
 *
 * @since 12
 */
typedef enum {
    /** 显示为开始样式。 */
    ARKUI_ENTER_KEY_TYPE_GO = 2,
    /** 显示为搜索样式。 */
    ARKUI_ENTER_KEY_TYPE_SEARCH = 3,
    /** 显示为发送样式。 */
    ARKUI_ENTER_KEY_TYPE_SEND,
    /** 显示为下一个样式。 */
    ARKUI_ENTER_KEY_TYPE_NEXT,
    /** 显示为完成样式。 */
    ARKUI_ENTER_KEY_TYPE_DONE,
    /** 显示为上一个样式。 */
    ARKUI_ENTER_KEY_TYPE_PREVIOUS,
    /** 显示为换行样式。 */
    ARKUI_ENTER_KEY_TYPE_NEW_LINE,
} ArkUI_EnterKeyType;

/**
 * @brief 定义单行文本输入法类型枚举值。
 *
 * @since 12
 */
typedef enum {
    /** 基本输入模式。 */
    ARKUI_TEXTINPUT_TYPE_NORMAL = 0,
    /** 纯数字模式。 */
    ARKUI_TEXTINPUT_TYPE_NUMBER = 2,
    /** 电话号码输入模式。 */
    ARKUI_TEXTINPUT_TYPE_PHONE_NUMBER = 3,
    /** 邮箱地址输入模式。 */
    ARKUI_TEXTINPUT_TYPE_EMAIL = 5,
    /** 密码输入模式。 */
    ARKUI_TEXTINPUT_TYPE_PASSWORD = 7,
    /** 纯数字密码输入模式。 */
    ARKUI_TEXTINPUT_TYPE_NUMBER_PASSWORD = 8,
    /** 锁屏应用密码输入模式。 */
    ARKUI_TEXTINPUT_TYPE_SCREEN_LOCK_PASSWORD = 9,
    /** 用户名输入模式。 */
    ARKUI_TEXTINPUT_TYPE_USER_NAME = 10,
    /** 新密码输入模式。 */
    ARKUI_TEXTINPUT_TYPE_NEW_PASSWORD = 11,
    /** 带小数点的数字输入模式。 */
    ARKUI_TEXTINPUT_TYPE_NUMBER_DECIMAL = 12,
} ArkUI_TextInputType;

/**
 * @brief 定义多行文本输入法类型枚举值。
 *
 * @since 12
 */
typedef enum {
    /** 基本输入模式。 */
    ARKUI_TEXTAREA_TYPE_NORMAL = 0,
    /** 纯数字模式。 */
    ARKUI_TEXTAREA_TYPE_NUMBER = 2,
    /** 电话号码输入模式。 */
    ARKUI_TEXTAREA_TYPE_PHONE_NUMBER = 3,
    /** 邮箱地址输入模式。 */
    ARKUI_TEXTAREA_TYPE_EMAIL = 5,
} ArkUI_TextAreaType;

/**
 * @brief 定义清除按钮样式枚举值。
 *
 * @since 12
 */
typedef enum {
    /** 清除按钮常显样式。*/
    ARKUI_CANCELBUTTON_STYLE_CONSTANT = 0,
    /** 清除按钮常隐样式。*/
    ARKUI_CANCELBUTTON_STYLE_INVISIBLE,
    /** 清除按钮输入样式。*/
    ARKUI_CANCELBUTTON_STYLE_INPUT,
} ArkUI_CancelButtonStyle;

/**
 * @brief 定义XComponent类型枚举值。
 *
 * @since 12
 */
typedef enum {
    /** 用于EGL/OpenGLES和媒体数据写入，开发者定制绘制内容单独显示在屏幕上。*/
    ARKUI_XCOMPONENT_TYPE_SURFACE = 0,
    /** 用于EGL/OpenGLES和媒体数据写入，开发者定制绘制内容和XComponent组件内容合成后展示在屏幕上。*/
    ARKUI_XCOMPONENT_TYPE_TEXTURE = 2,
} ArkUI_XComponentType;

/**
 * @brief 定义进度条类型枚举值。
 *
 * @since 12
 */
typedef enum {
    /** 线性样式。*/
    ARKUI_PROGRESS_TYPE_LINEAR = 0,
    /** 环形无刻度样式。*/
    ARKUI_PROGRESS_TYPE_RING,
    /** 圆形样式。*/
    ARKUI_PROGRESS_TYPE_ECLIPSE,
    /** 唤醒有刻度样式。*/
    ARKUI_PROGRESS_TYPE_SCALE_RING,
    /** 胶囊样式。*/
    ARKUI_PROGRESS_TYPE_CAPSULE,
}ArkUI_ProgressType;

/**
 * @brief 定义装饰线类型枚举值。
 *
 * @since 12
 */
typedef enum {
    /** 不使用装饰线。*/
    ARKUI_TEXT_DECORATION_TYPE_NONE = 0,
    /** 文字下划线修饰。*/
    ARKUI_TEXT_DECORATION_TYPE_UNDERLINE,
    /** 文字上划线修饰。*/
    ARKUI_TEXT_DECORATION_TYPE_OVERLINE,
    /** 穿过文本的修饰线。*/
    ARKUI_TEXT_DECORATION_TYPE_LINE_THROUGH,
} ArkUI_TextDecorationType;

/**
 * @brief 定义装饰线样式枚举值。
 *
 * @since 12
 */
typedef enum {
    /** 单实线。*/
    ARKUI_TEXT_DECORATION_STYLE_SOLID = 0,
    /** 双实线。*/
    ARKUI_TEXT_DECORATION_STYLE_DOUBLE,
    /** 点线。*/
    ARKUI_TEXT_DECORATION_STYLE_DOTTED,
    /** 虚线。*/
    ARKUI_TEXT_DECORATION_STYLE_DASHED,
    /** 波浪线。*/
    ARKUI_TEXT_DECORATION_STYLE_WAVY,
} ArkUI_TextDecorationStyle;

/**
 * @brief 定义文本大小写枚举值。
 *
 * @since 12
 */
typedef enum {
    /** 保持原有大小写。*/
    ARKUI_TEXT_CASE_NORMAL = 0,
    /** 文本全小写。*/
    ARKUI_TEXT_CASE_LOWER,
    /** 文本全大写。*/
    ARKUI_TEXT_CASE_UPPER,
} ArkUI_TextCase;

/**
 * @brief 定义文本复制黏贴模式枚举值。
 *
 * @since 12
 */
typedef enum {
    /** 不支持复制。*/
    ARKUI_COPY_OPTIONS_NONE = 0,
    /** 支持应用内复制。*/
    ARKUI_COPY_OPTIONS_IN_APP,
    /** 支持设备内复制。*/
    ARKUI_COPY_OPTIONS_LOCAL_DEVICE,
    /** 支持跨设备复制。*/
    ARKUI_COPY_OPTIONS_CROSS_DEVICE,
} ArkUI_CopyOptions;

/**
 * @brief 定义阴影类型枚举值。
 *
 * @since 12
 */
typedef enum {
    /** 颜色。*/
    ARKUI_SHADOW_TYPE_COLOR = 0,
    /** 模糊。*/
    ARKUI_SHADOW_TYPE_BLUR
} ArkUI_ShadowType;

/**
 * @brief 定义滑动选择文本选择器输入类型。
 *
 * @since 12
 */
typedef enum {
    /** 单列数据选择器。*/
    ARKUI_TEXTPICKER_RANGETYPE_SINGLE = 0,
    /** 多列数据选择器。*/
    ARKUI_TEXTPICKER_RANGETYPE_MULTI,
    /** 支持图片资源的单列数据选择器。*/
    ARKUI_TEXTPICKER_RANGETYPE_RANGE_CONTENT,
    /** 支持联动的多列数据选择器。*/
    ARKUI_TEXTPICKER_RANGETYPE_CASCADE_RANGE_CONTENT,
} ArkUI_TextPickerRangeType;

/**
 * @brief 定义单列滑动数据选择器支持图片资源的输入结构体。
 *
 * @since 12
 */
typedef struct {
    /** 图片资源。*/
    const char* icon;
    /** 文本信息。*/
    const char* text;
} ARKUI_TextPickerRangeContent;

/**
 * @brief 定义多列带联动能力的滑动数据选择器的输入结构体。
 *
 * @since 12
 */
typedef struct {
    /** 文本信息。*/
    const char* text;
    /** 联动数据。*/
    const ARKUI_TextPickerRangeContent* children;
    /** 联动数据数组大小。*/
    int32_t size;
} ARKUI_TextPickerCascadeRangeContent;

/**
 * @brief 定义无障碍复选框状态类型枚举值。
 *
 * @since 12
 */
typedef enum {
    /** 复选框未被选中。*/
    ARKUI_ACCESSIBILITY_UNCHECKED = 0,
    /** 复选框被选中。*/
    ARKUI_ACCESSIBILITY_CHECKED,
} ArkUI_AccessibilityCheckedState;

/**
 * @brief 定义无障碍操作类型。
 *
 * @since 12
 */
typedef enum {
    /** 点击操作。*/
    ARKUI_ACCESSIBILITY_ACTION_CLICK = 1 << 0,
    /** 长按操作。*/
    ARKUI_ACCESSIBILITY_ACTION_LONG_CLICK = 1 << 1,
    /** 剪切操作。*/
    ARKUI_ACCESSIBILITY_ACTION_CUT = 1 << 2,
    /** 复制操作。*/
    ARKUI_ACCESSIBILITY_ACTION_COPY = 1 << 3,
    /** 粘贴操作。*/
    ARKUI_ACCESSIBILITY_ACTION_PASTE = 1 << 4,
} ArkUI_AccessibilityActionType;

/**
 * @brief 定义组件无障碍状态。
 *
 * @since 12
 */
typedef struct ArkUI_AccessibilityState ArkUI_AccessibilityState;

/**
 * @brief 定义组件无障碍信息值。
 *
 * @since 12
 */
typedef struct ArkUI_AccessibilityValue ArkUI_AccessibilityValue;

/**
 * @brief 定义边缘滑动效果枚举值。
 *
 * @since 12
 */
typedef enum {
    /** 弹性物理动效，滑动到边缘后可以根据初始速度或通过触摸事件继续滑动一段距离，松手后回弹。*/
    ARKUI_EDGE_EFFECT_SPRING = 0,
    /** 阴影效果，滑动到边缘后会有圆弧状的阴影。*/
    ARKUI_EDGE_EFFECT_FADE,
    /** 滑动到边缘后无效果。*/
    ARKUI_EDGE_EFFECT_NONE,
} ArkUI_EdgeEffect;

/**
 * @brief 定义边缘效果生效边缘的方向枚举值。
 *
 * @since 14
 */
typedef enum {
    /** 起始边生效。*/
    ARKUI_EFFECT_EDGE_START = 1,
    /** 末尾边生效。*/
    ARKUI_EFFECT_EDGE_END = 2,
} ArkUI_EffectEdge;

/**
 * @brief 定义Scroll组件排列方向枚举值。
 *
 * @since 12
 */
typedef enum {
    /** 仅支持竖直方向滚动。*/
    ARKUI_SCROLL_DIRECTION_VERTICAL = 0,
    /** 仅支持水平方向滚动。*/
    ARKUI_SCROLL_DIRECTION_HORIZONTAL,
    /** 禁止滚动。*/
    ARKUI_SCROLL_DIRECTION_NONE = 3,
} ArkUI_ScrollDirection;

/**
 * @brief 定义列表项滚动结束对齐效果枚举值。
 *
 * @since 12
 */
typedef enum {
    /** 默认无项目滚动对齐效果。*/
    ARKUI_SCROLL_SNAP_ALIGN_NONE = 0,
    /** 视图中的第一项将在列表的开头对齐。*/
    ARKUI_SCROLL_SNAP_ALIGN_START,
    /** 视图中的中间项将在列表中心对齐。*/
    ARKUI_SCROLL_SNAP_ALIGN_CENTER,
    /** 视图中的最后一项将在列表末尾对齐。*/
    ARKUI_SCROLL_SNAP_ALIGN_END,
} ArkUI_ScrollSnapAlign;

/**
 * @brief 定义滚动条状态枚举值。
 *
 * @since 12
 */
typedef enum {
    /** 不显示。*/
    ARKUI_SCROLL_BAR_DISPLAY_MODE_OFF = 0,
    /** 按需显示(触摸时显示，2s后消失)。*/
    ARKUI_SCROLL_BAR_DISPLAY_MODE_AUTO,
    /** 常驻显示。*/
    ARKUI_SCROLL_BAR_DISPLAY_MODE_ON,
} ArkUI_ScrollBarDisplayMode;

/**
 * @brief 定义滚动方向和List组件排列方向枚举值。
 *
 * @since 12
 */
typedef enum {
    /** 仅支持竖直方向滚动。*/
    ARKUI_AXIS_VERTICAL = 0,
    /** 仅支持水平方向滚动。*/
    ARKUI_AXIS_HORIZONTAL,
} ArkUI_Axis;

/**
 * @brief 定义列表是否吸顶和吸底枚举值。
 *
 * @since 12
 */
typedef enum {
    /** ListItemGroup的header不吸顶，footer不吸底。*/
    ARKUI_STICKY_STYLE_NONE = 0,
    /** ListItemGroup的header吸顶，footer不吸底。*/
    ARKUI_STICKY_STYLE_HEADER = 1,
    /** ListItemGroup的footer吸底，header不吸顶。*/
    ARKUI_STICKY_STYLE_FOOTER = 2,
    /** ListItemGroup的footer吸底，header吸顶。*/
    ARKUI_STICKY_STYLE_BOTH = 3,
} ArkUI_StickyStyle;


/**
 * @brief 边框线条样式枚举值。
 *
 * @since 12
 */
typedef enum {
    /** 显示为一条实线。 */
    ARKUI_BORDER_STYLE_SOLID = 0,
    /** 显示为一系列短的方形虚线。*/
    ARKUI_BORDER_STYLE_DASHED,
    /** 显示为一系列圆点。*/
    ARKUI_BORDER_STYLE_DOTTED,
} ArkUI_BorderStyle;

/**
 * @brief 触摸测试控制枚举值。
 *
 * @since 12
 */
typedef enum {
    /** 默认触摸测试效果。 */
    ARKUI_HIT_TEST_MODE_DEFAULT = 0,
    /** 自身响应触摸测试。*/
    ARKUI_HIT_TEST_MODE_BLOCK,
    /** 自身和子节点都响应触摸测试。*/
    ARKUI_HIT_TEST_MODE_TRANSPARENT,
    /** 自身不响应触摸测试。*/
    ARKUI_HIT_TEST_MODE_NONE
} ArkUI_HitTestMode;

/**
 * @brief 阴影效果枚举值。
 *
 * @since 12
 */
typedef enum {
    /** 超小阴影。 */
    ARKUI_SHADOW_STYLE_OUTER_DEFAULT_XS = 0,
    /** 小阴影。*/
    ARKUI_SHADOW_STYLE_OUTER_DEFAULT_SM,
    /** 中阴影。*/
    ARKUI_SHADOW_STYLE_OUTER_DEFAULT_MD,
    /** 大阴影。*/
    ARKUI_SHADOW_STYLE_OUTER_DEFAULT_LG,
    /** 浮动小阴影。*/
    ARKUI_SHADOW_STYLE_OUTER_FLOATING_SM,
    /** 浮动中阴影。*/
    ARKUI_SHADOW_STYLE_OUTER_FLOATING_MD,
} ArkUI_ShadowStyle;

/**
 * @brief 动画曲线枚举值。
 *
 * @since 12
 */
typedef enum {
    /** 动画从头到尾的速度都是相同。 */
    ARKUI_CURVE_LINEAR = 0,
    /** 动画以低速开始，然后加快，在结束前变慢。 */
    ARKUI_CURVE_EASE,
    /** 动画以低速开始。 */
    ARKUI_CURVE_EASE_IN,
    /** 动画以低速结束。 */
    ARKUI_CURVE_EASE_OUT,
    /** 动画以低速开始和结束。 */
    ARKUI_CURVE_EASE_IN_OUT,
    /** 动画标准曲线。 */
    ARKUI_CURVE_FAST_OUT_SLOW_IN,
    /** 动画减速曲线。 */
    ARKUI_CURVE_LINEAR_OUT_SLOW_IN,
    /** 动画加速曲线。 */
    ARKUI_CURVE_FAST_OUT_LINEAR_IN,
    /** 动画急缓曲线。 */
    ARKUI_CURVE_EXTREME_DECELERATION,
    /** 动画锐利曲线。 */
    ARKUI_CURVE_SHARP,
    /** 动画节奏曲线。 */
    ARKUI_CURVE_RHYTHM,
    /** 动画平滑曲线。 */
    ARKUI_CURVE_SMOOTH,
    /** 动画阻尼曲线。 */
    ARKUI_CURVE_FRICTION,
} ArkUI_AnimationCurve;

/**
 * @brief Swiper导航点箭头枚举值。
 *
 * @since 12
 */
typedef enum {
    /** 不显示swiper中导航点箭头。 */
    ARKUI_SWIPER_ARROW_HIDE = 0,
    /** 显示swiper中导航点箭头。 */
    ARKUI_SWIPER_ARROW_SHOW,
    /** 在hover状态下显示swiper中导航点箭头。 */
    ARKUI_SWIPER_ARROW_SHOW_ON_HOVER,
} ArkUI_SwiperArrow;

/**
 * @brief Swiper组件和父组件的嵌套滚动模式。
 *
 * @since 12
 */
typedef enum {
    /** Swiper只自身滚动，不与父组件联动。 */
    ARKUI_SWIPER_NESTED_SRCOLL_SELF_ONLY = 0,
    /** Swiper自身先滚动，自身滚动到边缘以后父组件滚动。父组件滚动到边缘以后，如果父组件有边缘效果，则父组件触发边缘效果，否则Swiper触发边缘效果。 */
    ARKUI_SWIPER_NESTED_SRCOLL_SELF_FIRST,
} ArkUI_SwiperNestedScrollMode;

/**
 * @brief 定义无障碍辅助服务模式。
 *
 * @since 12
 */
typedef enum {
    /** 根据组件不同会转换为“enabled”或者“disabled”。 */
    ARKUI_ACCESSIBILITY_MODE_AUTO = 0,
    /** 当前组件可被无障碍辅助服务所识别。*/
    ARKUI_ACCESSIBILITY_MODE_ENABLED,
    /** 当前组件不可被无障碍辅助服务所识别。*/
    ARKUI_ACCESSIBILITY_MODE_DISABLED,
    /** 当前组件及其所有子组件不可被无障碍辅助服务所识别。*/
    ARKUI_ACCESSIBILITY_MODE_DISABLED_FOR_DESCENDANTS,
} ArkUI_AccessibilityMode;

/**
 * @brief 定义组件支持设置文本是否可复制粘贴。
 *
 * @since 12
 */
typedef enum {
    /** 不支持复制。 */
    ARKUI_TEXT_COPY_OPTIONS_NONE = 0,
    /** 支持应用内复制。*/
    ARKUI_TEXT_COPY_OPTIONS_IN_APP,
    /** 支持设备内复制。*/
    ARKUI_TEXT_COPY_OPTIONS_LOCAL_DEVICE,
    /** 支持跨设备复制。*/
    ARKUI_TEXT_COPY_OPTIONS_CROSS_DEVICE,
} ArkUI_TextCopyOptions;


/**
 * @brief 定义文本自适应高度的方式。
 *
 * @since 12
 */
typedef enum {
    /** 设置文本高度自适应方式为以MaxLines优先。 */
    ARKUI_TEXT_HEIGHT_ADAPTIVE_POLICY_MAX_LINES_FIRST = 0,
    /** 设置文本高度自适应方式为以缩小字体优先。*/
    ARKUI_TEXT_HEIGHT_ADAPTIVE_POLICY_MIN_FONT_SIZE_FIRST,
    /** 设置文本高度自适应方式为以布局约束（高度）优先。*/
    ARKUI_TEXT_HEIGHT_ADAPTIVE_POLICY_LAYOUT_CONSTRAINT_FIRST,
} ArkUI_TextHeightAdaptivePolicy;


/**
 * @brief 定义嵌套滚动选项。
 *
 * @since 12
 */
typedef enum {
    /** 只自身滚动，不与父组件联动。 */
    ARKUI_SCROLL_NESTED_MODE_SELF_ONLY = 0,
    /** 自身先滚动，自身滚动到边缘以后父组件滚动。父组件滚动到边缘以后
    如果父组件有边缘效果，则父组件触发边缘效果，否则子组件触发边缘效果。*/
    ARKUI_SCROLL_NESTED_MODE_SELF_FIRST,
    /** 父组件先滚动，父组件滚动到边缘以后自身滚动。
    身滚动到边缘后，如果有边缘效果，会触发自身的边缘效果，否则触发父组件的边缘效果。*/
    ARKUI_SCROLL_NESTED_MODE_PARENT_FIRST,
    /** 自身和父组件同时滚动，自身和父组件都到达边缘以后
    如果自身有边缘效果，则自身触发边缘效果，否则父组件触发边缘效果。*/
    ARKUI_SCROLL_NESTED_MODE_PARALLEL,
} ArkUI_ScrollNestedMode;


/**
 * @brief 定义滚动到的边缘位置。
 *
 * @since 12
 */
typedef enum {
    /** 竖直方向上边缘。*/
    ARKUI_SCROLL_EDGE_TOP = 0,
    /** 竖直方向下边缘。*/
    ARKUI_SCROLL_EDGE_BOTTOM,
    /** 水平方向起始位置。*/
    ARKUI_SCROLL_EDGE_START,
    /** 水平方向末尾位置。*/
    ARKUI_SCROLL_EDGE_END,
} ArkUI_ScrollEdge;

/**
 * @brief 滚动到具体item时的对齐方式。
 *
 * @since 12
 */
typedef enum {
    /** 首部对齐。指定item首部与容器首部对齐。*/
    ARKUI_SCROLL_ALIGNMENT_START = 0,
    /** 居中对齐。指定item主轴方向居中对齐于容器。*/
    ARKUI_SCROLL_ALIGNMENT_CENTER,
    /** 尾部对齐。指定item尾部与容器尾部对齐。*/
    ARKUI_SCROLL_ALIGNMENT_END,
    /** 自动对齐。若指定item完全处于显示区，不做调整。否则依照滑动距离最短的原则，将指定item首部对齐或尾部对齐于容器,使指定item完全处于显示区。*/
    ARKUI_SCROLL_ALIGNMENT_AUTO,
} ArkUI_ScrollAlignment;

/**
 * @brief 定义当前滚动状态。
 *
 * @since 12
 */
typedef enum {
    /** 空闲状态。使用控制器提供的方法控制滚动时触发，拖动滚动条滚动时触发。*/
    ARKUI_SCROLL_STATE_IDLE = 0,
    /** 滚动状态。使用手指拖动容器滚动时触发。*/
    ARKUI_SCROLL_STATE_SCROLL,
    /** 惯性滚动状态。快速划动松手后进行惯性滚动和划动到边缘回弹时触发。*/
    ARKUI_SCROLL_STATE_FLING,
} ArkUI_ScrollState;

/**
 * @brief 定义滑块形状。
 *
 * @since 12
 */
typedef enum {
    /** 使用默认滑块（圆形）。*/
    ARKUI_SLIDER_BLOCK_STYLE_DEFAULT = 0,
    /** 使用图片资源作为滑块。*/
    ARKUI_SLIDER_BLOCK_STYLE_IMAGE,
    /** 使用自定义形状作为滑块。*/
    ARKUI_SLIDER_BLOCK_STYLE_SHAPE,
} ArkUI_SliderBlockStyle;

/**
 * @brief 定义滑动条滑动方向。
 *
 * @since 12
 */
typedef enum {
    /** 方向为纵向。*/
    ARKUI_SLIDER_DIRECTION_VERTICAL = 0,
    /** 方向为横向。*/
    ARKUI_SLIDER_DIRECTION_HORIZONTAL,
} ArkUI_SliderDirection;

/**
 * @brief 定义滑块与滑轨显示样式。
 *
 * @since 12
 */
typedef enum {
    /** 滑块在滑轨上。*/
    ARKUI_SLIDER_STYLE_OUT_SET = 0,
    /** 滑块在滑轨内。*/
    ARKUI_SLIDER_STYLE_IN_SET,
    /** 无滑块。 */
    ARKUI_SLIDER_STYLE_NONE,
} ArkUI_SliderStyle;

/**
 * @brief 定义CheckBox组件形状。
 *
 * @since 12
 */
typedef enum {
    /** 圆形。*/
    ArkUI_CHECKBOX_SHAPE_CIRCLE = 0,
    /** 圆角方形。*/
    ArkUI_CHECKBOX_SHAPE_ROUNDED_SQUARE,
} ArkUI_CheckboxShape;

/**
 * @brief 定义动画播放模式。
 *
 * @since 12
 */
typedef enum {
    /** 动画正向播放。*/
    ARKUI_ANIMATION_PLAY_MODE_NORMAL = 0,
    /** 动画反向播放。*/
    ARKUI_ANIMATION_PLAY_MODE_REVERSE,
    /** 动画在奇数次（1、3、5...）正向播放，在偶数次（2、4、6...）反向播放。*/
    ARKUI_ANIMATION_PLAY_MODE_ALTERNATE,
    /** 动画在奇数次（1、3、5...）反向播放，在偶数次（2、4、6...）正向播放。*/
    ARKUI_ANIMATION_PLAY_MODE_ALTERNATE_REVERSE,
} ArkUI_AnimationPlayMode;

/**
 * @brief 定义图片宽高样式。
 *
 * @since 12
 */
typedef enum {
    /** 保持原图的比例不变。*/
    ARKUI_IMAGE_SIZE_AUTO = 0,
    /** 默认值，保持宽高比进行缩小或者放大，使得图片两边都大于或等于显示边界。*/
    ARKUI_IMAGE_SIZE_COVER,
    /** 保持宽高比进行缩小或者放大，使得图片完全显示在显示边界内。*/
    ARKUI_IMAGE_SIZE_CONTAIN,
} ArkUI_ImageSize;

/**
 * @brief 定义取色模式。
 *
 * @since 12
 */
typedef enum {
    /** 不使用取色模糊。*/
    ARKUI_ADAPTIVE_COLOR_DEFAULT = 0,
    /** 使用取色模糊。*/
    ARKUI_ADAPTIVE_COLOR_AVERAGE,
} ArkUI_AdaptiveColor;

/**
 * @brief 定义深浅色模式。
 *
 * @since 12
 */
typedef enum {
    /** 跟随系统深浅色模式。*/
    ARKUI_COLOR_MODE_SYSTEM = 0,
    /** 固定使用浅色模式。*/
    ARKUI_COLOR_MODE_LIGHT,
    /** 固定使用深色模式。 */
    ARKUI_COLOR_MODE_DARK,
} ArkUI_ColorMode;

/**
 * @brief 定义系统深浅色模式。
 *
 * @since 12
 */
typedef enum {
    /** 浅色模式 */
    ARKUI_SYSTEM_COLOR_MODE_LIGHT = 0,
    /** 深色模式 */
    ARKUI_SYSTEM_COLOR_MODE_DARK,
} ArkUI_SystemColorMode;

/**
 * @brief 定义背景模糊样式。
 *
 * @since 12
 */
typedef enum {
    /** 轻薄材质模糊。 */
    ARKUI_BLUR_STYLE_THIN = 0,
    /** 普通厚度材质模糊。 */
    ARKUI_BLUR_STYLE_REGULAR,
    /** 厚材质模糊。 */
    ARKUI_BLUR_STYLE_THICK,
    /** 近距景深模糊。 */
    ARKUI_BLUR_STYLE_BACKGROUND_THIN,
    /** 中距景深模糊。 */
    ARKUI_BLUR_STYLE_BACKGROUND_REGULAR,
    /** 远距景深模糊。 */
    ARKUI_BLUR_STYLE_BACKGROUND_THICK,
    /** 超远距景深模糊。 */
    ARKUI_BLUR_STYLE_BACKGROUND_ULTRA_THICK,
    /** 关闭模糊。 */
    ARKUI_BLUR_STYLE_NONE,
    /** 组件超轻薄材质模糊。 */
    ARKUI_BLUR_STYLE_COMPONENT_ULTRA_THIN,
    /** 组件轻薄材质模糊。 */
    ARKUI_BLUR_STYLE_COMPONENT_THIN,
    /** 组件普通材质模糊。 */
    ARKUI_BLUR_STYLE_COMPONENT_REGULAR,
    /** 组件厚材质模糊。 */
    ARKUI_BLUR_STYLE_COMPONENT_THICK,
    /** 组件超厚材质模糊。 */
    ARKUI_BLUR_STYLE_COMPONENT_ULTRA_THICK,
} ArkUI_BlurStyle;

/**
 * @brief 定义垂直对齐方式。
 *
 * @since 12
 */
typedef enum {
    /** 顶部对齐。 */
    ARKUI_VERTICAL_ALIGNMENT_TOP = 0,
    /** 居中对齐，默认对齐方式。 */
    ARKUI_VERTICAL_ALIGNMENT_CENTER,
    /** 底部对齐。 */
    ARKUI_VERTICAL_ALIGNMENT_BOTTOM,
} ArkUI_VerticalAlignment;

/**
 * @brief 定义语言方向对齐方式。
 *
 * @since 12
 */
typedef enum {
    /** 按照语言方向起始端对齐。 */
    ARKUI_HORIZONTAL_ALIGNMENT_START = 0,
    /** 居中对齐，默认对齐方式。 */
    ARKUI_HORIZONTAL_ALIGNMENT_CENTER,
    /** 按照语言方向末端对齐。 */
    ARKUI_HORIZONTAL_ALIGNMENT_END,
} ArkUI_HorizontalAlignment;

/**
 * @brief 定义文本超长时的显示方式。
 *
 * @since 12
 */
typedef enum {
    /** 文本超长时不裁剪显示。 */
    ARKUI_TEXT_OVERFLOW_NONE = 0,
    /** 文本超长时进行裁剪显示。 */
    ARKUI_TEXT_OVERFLOW_CLIP,
    /** 文本超长时显示不下的文本用省略号代替。 */
    ARKUI_TEXT_OVERFLOW_ELLIPSIS,
    /** 文本超长时以跑马灯的方式展示。 */
    ARKUI_TEXT_OVERFLOW_MARQUEE,
} ArkUI_TextOverflow;

/**
 * @brief 定义图片基于文本的对齐方式。
 *
 * @since 12
 */
typedef enum {
    /** 图片下边沿与文本BaseLine对齐。*/
    ARKUI_IMAGE_SPAN_ALIGNMENT_BASELINE = 0,
    /** 图片下边沿与文本下边沿对齐。*/
    ARKUI_IMAGE_SPAN_ALIGNMENT_BOTTOM,
    /** 图片中间与文本中间对齐。*/
    ARKUI_IMAGE_SPAN_ALIGNMENT_CENTER,
    /** 图片上边沿与文本上边沿对齐。 */
    ARKUI_IMAGE_SPAN_ALIGNMENT_TOP,
} ArkUI_ImageSpanAlignment;

/**
 * @brief 定义image填充效果。
 *ImageSpanAlignment
 * @since 12
 */
typedef enum {
    /** 保持宽高比进行缩小或者放大，使得图片完全显示在显示边界内。 */
    ARKUI_OBJECT_FIT_CONTAIN = 0,
    /** 保持宽高比进行缩小或者放大，使得图片两边都大于或等于显示边界。*/
    ARKUI_OBJECT_FIT_COVER,
    /** 自适应显示。*/
    ARKUI_OBJECT_FIT_AUTO,
    /** 不保持宽高比进行放大缩小，使得图片充满显示边界。*/
    ARKUI_OBJECT_FIT_FILL,
    /** 保持宽高比显示，图片缩小或者保持不变。*/
    ARKUI_OBJECT_FIT_SCALE_DOWN,
    /** 保持原有尺寸显示。*/
    ARKUI_OBJECT_FIT_NONE,
    /** 图片大小不变，在image组件中顶部起始端对齐。 */
    ARKUI_OBJECT_FIT_NONE_AND_ALIGN_TOP_START,
    /** 图片大小不变，在image组件中顶部横向居中对齐。*/
    ARKUI_OBJECT_FIT_NONE_AND_ALIGN_TOP,
    /** 图片大小不变，在image组件中顶部尾端对齐。*/
    ARKUI_OBJECT_FIT_NONE_AND_ALIGN_TOP_END,
    /** 图片大小不变，在image组件中起始端纵向居中对齐。*/
    ARKUI_OBJECT_FIT_NONE_AND_ALIGN_START,
    /** 图片大小不变，在image组件中横向和纵向居中对齐。*/
    ARKUI_OBJECT_FIT_NONE_AND_ALIGN_CENTER,
    /** 图片大小不变，在image组件中尾端纵向居中对齐。*/
    ARKUI_OBJECT_FIT_NONE_AND_ALIGN_END,
    /** 图片大小不变，在image组件中底部起始端对齐。*/
    ARKUI_OBJECT_FIT_NONE_AND_ALIGN_BOTTOM_START,
    /** 图片大小不变，在image组件中底部横向居中对齐。*/
    ARKUI_OBJECT_FIT_NONE_AND_ALIGN_BOTTOM,
    /** 图片大小不变，在image组件中底部尾端对齐。*/
    ARKUI_OBJECT_FIT_NONE_AND_ALIGN_BOTTOM_END,
} ArkUI_ObjectFit;

/**
 * @brief 定义图片插值效果。
 *
 * @since 12
 */
typedef enum {
    /** 不使用图片插值。*/
    ARKUI_IMAGE_INTERPOLATION_NONE = 0,
    /** 低图片插值。*/
    ARKUI_IMAGE_INTERPOLATION_LOW,
    /** 中图片插值。*/
    ARKUI_IMAGE_INTERPOLATION_MEDIUM,
    /** 高图片插值，插值质量最高。*/
    ARKUI_IMAGE_INTERPOLATION_HIGH,
} ArkUI_ImageInterpolation;


/**
 * @brief 混合模式枚举值。
 *
 * @since 12
 */
typedef enum {
    /** 将上层图像直接覆盖到下层图像上，不进行任何混合操作。 */
    ARKUI_BLEND_MODE_NONE = 0,
    /** 将源像素覆盖的目标像素清除为完全透明。 */
    ARKUI_BLEND_MODE_CLEAR,
    /** r = s，只显示源像素。 */
    ARKUI_BLEND_MODE_SRC,
    /** r = d，只显示目标像素。 */
    ARKUI_BLEND_MODE_DST,
    /** r = s + (1 - sa) * d，将源像素按照透明度进行混合，覆盖在目标像素上。 */
    ARKUI_BLEND_MODE_SRC_OVER,
    /** r = d + (1 - da) * s，将目标像素按照透明度进行混合，覆盖在源像素上。 */
    ARKUI_BLEND_MODE_DST_OVER,
    /** r = s * da，只显示源像素中与目标像素重叠的部分。 */
    ARKUI_BLEND_MODE_SRC_IN,
    /** r = d * sa，只显示目标像素中与源像素重叠的部分。 */
    ARKUI_BLEND_MODE_DST_IN,
    /** r = s * (1 - da)，只显示源像素中与目标像素不重叠的部分。 */
    ARKUI_BLEND_MODE_SRC_OUT,
    /** r = d * (1 - sa)，只显示目标像素中与源像素不重叠的部分。 */
    ARKUI_BLEND_MODE_DST_OUT,
    /** r = s * da + d * (1 - sa)，在源像素和目标像素重叠的地方绘制源像素，在源像素和目标像素不重叠的地方绘制目标像素。
     */
    ARKUI_BLEND_MODE_SRC_ATOP,
    /** r = d * sa + s * (1 - da)，在源像素和目标像素重叠的地方绘制目标像素，在源像素和目标像素不重叠的地方绘制源像素。
     */
    ARKUI_BLEND_MODE_DST_ATOP,
    /** r = s * (1 - da) + d * (1 - sa)，只显示源像素与目标像素不重叠的部分。 */
    ARKUI_BLEND_MODE_XOR,
    /** r = min(s + d, 1)，将源像素值与目标像素值相加，并将结果作为新的像素值。 */
    ARKUI_BLEND_MODE_PLUS,
    /** r = s * d，将源像素与目标像素进行乘法运算，并将结果作为新的像素值。 */
    ARKUI_BLEND_MODE_MODULATE,
    /** r = s + d - s * d，将两个图像的像素值相加，然后减去它们的乘积来实现混合。 */
    ARKUI_BLEND_MODE_SCREEN,
    /** 根据目标像素来决定使用MULTIPLY混合模式还是SCREEN混合模式。 */
    ARKUI_BLEND_MODE_OVERLAY,
    /** rc = s + d - max(s * da, d * sa), ra = kSrcOver，当两个颜色重叠时，较暗的颜色会覆盖较亮的颜色。 */
    ARKUI_BLEND_MODE_DARKEN,
    /** rc = s + d - min(s * da, d * sa), ra =
       kSrcOver，将源图像和目标图像中的像素进行比较，选取两者中较亮的像素作为最终的混合结果。 */
    ARKUI_BLEND_MODE_LIGHTEN,
    /** 使目标像素变得更亮来反映源像素。 */
    ARKUI_BLEND_MODE_COLOR_DODGE,
    /** 使目标像素变得更暗来反映源像素。 */
    ARKUI_BLEND_MODE_COLOR_BURN,
    /** 根据源像素的值来决定目标像素变得更亮或者更暗。根据源像素来决定使用MULTIPLY混合模式还是SCREEN混合模式。 */
    ARKUI_BLEND_MODE_HARD_LIGHT,
    /** 根据源像素来决定使用LIGHTEN混合模式还是DARKEN混合模式。 */
    ARKUI_BLEND_MODE_SOFT_LIGHT,
    /** rc = s + d - 2 * (min(s * da, d * sa)), ra =
       kSrcOver，对比源像素和目标像素，亮度更高的像素减去亮度更低的像素，产生高对比度的效果。 */
    ARKUI_BLEND_MODE_DIFFERENCE,
    /** rc = s + d - two(s * d), ra = kSrcOver，对比源像素和目标像素，亮度更高的像素减去亮度更低的像素，产生柔和的效果。
     */
    ARKUI_BLEND_MODE_EXCLUSION,
    /** r = s * (1 - da) + d * (1 - sa) + s * d，将源图像与目标图像进行乘法混合，得到一张新的图像。	 */
    ARKUI_BLEND_MODE_MULTIPLY,
    /** 保留源图像的亮度和饱和度，但会使用目标图像的色调来替换源图像的色调。 */
    ARKUI_BLEND_MODE_HUE,
    /** 保留目标像素的亮度和色调，但会使用源像素的饱和度来替换目标像素的饱和度。 */
    ARKUI_BLEND_MODE_SATURATION,
    /** 保留源像素的饱和度和色调，但会使用目标像素的亮度来替换源像素的亮度。 */
    ARKUI_BLEND_MODE_COLOR,
    /** 保留目标像素的色调和饱和度，但会用源像素的亮度替换目标像素的亮度。 */
    ARKUI_BLEND_MODE_LUMINOSITY,
} ArkUI_BlendMode;

/**
 * @brief 设置容器元素内主轴方向上的布局枚举值。
 *
 * @since 12
 */
typedef enum {
    /** 元素从左到右布局。 */
    ARKUI_DIRECTION_LTR = 0,
    /** 元素从右到左布局。 */
    ARKUI_DIRECTION_RTL,
    /** 使用系统默认布局方向。 */
    ARKUI_DIRECTION_AUTO = 3,
} ArkUI_Direction;

/**
 * @brief 设置子组件在父容器交叉轴的对齐格式枚举值。
 *
 * @since 12
 */
typedef enum {
    /** 使用Flex容器中默认配置。 */
    ARKUI_ITEM_ALIGNMENT_AUTO = 0,
    /** 元素在Flex容器中，交叉轴方向首部对齐。 */
    ARKUI_ITEM_ALIGNMENT_START,
    /** 元素在Flex容器中，交叉轴方向居中对齐。 */
    ARKUI_ITEM_ALIGNMENT_CENTER,
    /** 元素在Flex容器中，交叉轴方向底部对齐。 */
    ARKUI_ITEM_ALIGNMENT_END,
    /** 元素在Flex容器中，交叉轴方向拉伸填充。 */
    ARKUI_ITEM_ALIGNMENT_STRETCH,
    /** 元素在Flex容器中，交叉轴方向文本基线对齐。 */
    ARKUI_ITEM_ALIGNMENT_BASELINE,
} ArkUI_ItemAlignment;

/**
 * @brief 前景色枚举值。
 *
 * @since 12
 */
typedef enum {
    /** 前景色为控件背景色的反色。 */
    ARKUI_COLOR_STRATEGY_INVERT = 0,
    /** 控件背景阴影色为控件背景阴影区域的平均色。 */
    ARKUI_COLOR_STRATEGY_AVERAGE,
    /** 控件背景阴影色为控件背景阴影区域的主色。 */
    ARKUI_COLOR_STRATEGY_PRIMARY,
} ArkUI_ColorStrategy;

/**
 * @brief 定义垂直方向对齐方式。
 *
 * @since 12
 */
typedef enum {
    /** 主轴方向首端对齐。 */
    ARKUI_FLEX_ALIGNMENT_START = 1,
    /** 主轴方向中心对齐。 */
    ARKUI_FLEX_ALIGNMENT_CENTER = 2,
    /** 主轴方向尾部对齐。 */
    ARKUI_FLEX_ALIGNMENT_END = 3,
    /** Flex主轴方向均匀分配弹性元素，相邻元素之间距离相同，第一个元素行首对齐，最后的元素行尾对齐。 */
    ARKUI_FLEX_ALIGNMENT_SPACE_BETWEEN = 6,
    /** Flex主轴方向均匀分配弹性元素，相邻元素之间距离相同，第一个元素到行首的距离时相邻元素间距离的一半。 */
    ARKUI_FLEX_ALIGNMENT_SPACE_AROUND = 7,
    /** Flex主轴方向均匀分配弹性元素，相邻元素之间距离、第一个元素到行首的距离和最后的元素到行尾的距离均相等。 */
    ARKUI_FLEX_ALIGNMENT_SPACE_EVENLY = 8,
} ArkUI_FlexAlignment;

/**
 * @brief 定义Flex容器的主轴方向。
 *
 * @since 12
 */
typedef enum {
    /** 主轴与行方向一致。 */
    ARKUI_FLEX_DIRECTION_ROW = 0,
    /** 主轴与列方向一致。 */
    ARKUI_FLEX_DIRECTION_COLUMN,
    /** 主轴与行方向相反。 */
    ARKUI_FLEX_DIRECTION_ROW_REVERSE,
    /** 主轴与列方向相反。 */
    ARKUI_FLEX_DIRECTION_COLUMN_REVERSE,
} ArkUI_FlexDirection;

/**
 * @brief 定义Flex行列布局模式模式。
 *
 * @since 12
 */
typedef enum {
    /** 单行/单列布局，子项不能超出容器。 */
    ARKUI_FLEX_WRAP_NO_WRAP = 0,
    /** 多行/多列布局，子项允许超出容器。 */
    ARKUI_FLEX_WRAP_WRAP,
    /** 反向多行/多列布局，子项允许超出容器。 */
    ARKUI_FLEX_WRAP_WRAP_REVERSE,
} ArkUI_FlexWrap;

/**
 * @brief 控制组件的显隐枚举值。
 *
 * @since 12
 */
typedef enum {
    /** 显示。 */
    ARKUI_VISIBILITY_VISIBLE = 0,
    /** 隐藏，但参与布局进行占位。 */
    ARKUI_VISIBILITY_HIDDEN,
    /** 隐藏，但不参与布局，不进行占位。 */
    ARKUI_VISIBILITY_NONE,
} ArkUI_Visibility;

/**
 * @brief 日历选择器与入口组件对齐方式。
 *
 * @since 12
 */
typedef enum {
    /** 选择器和入口组件左对齐方式。 */
    ARKUI_CALENDAR_ALIGNMENT_START = 0,
    /** 选择器和入口组件居中对齐方式。 */
    ARKUI_CALENDAR_ALIGNMENT_CENTER,
    /** 选择器和入口组件右对齐方式。 */
    ARKUI_CALENDAR_ALIGNMENT_END,
} ArkUI_CalendarAlignment;

/**
 * @brief 遮罩类型枚举。
 *
 * @since 12
 */
typedef enum {
    /** 矩形类型。 */
    ARKUI_MASK_TYPE_RECTANGLE = 0,
    /** 圆形类型。 */
    ARKUI_MASK_TYPE_CIRCLE,
    /** 椭圆形类型。 */
    ARKUI_MASK_TYPE_ELLIPSE,
    /** 路径类型。 */
    ARKUI_MASK_TYPE_PATH,
    /** 进度类型。 */
    ARKUI_MASK_TYPE_PROGRESS,
} ArkUI_MaskType;

/**
 * @brief 裁剪类型枚举。
 *
 * @since 12
 */
typedef enum {
    /** 矩形类型。 */
    ARKUI_CLIP_TYPE_RECTANGLE = 0,
    /** 圆形类型。 */
    ARKUI_CLIP_TYPE_CIRCLE,
    /** 椭圆形类型。 */
    ARKUI_CLIP_TYPE_ELLIPSE,
    /** 路径类型。 */
    ARKUI_CLIP_TYPE_PATH,
} ArkUI_ClipType;

/**
 * @brief 定义渐变色结构。
 *
 * @since 12
 */
typedef struct {
    /** 颜色数组。*/
    const uint32_t* colors;
    /** 位置数组。*/
    float* stops;
    /** 数组长度。*/
    int size;
} ArkUI_ColorStop;

/**
 * @brief 自定义形状。
 *
 * @since 12
 */
typedef enum {
    /** 矩形类型。 */
    ARKUI_SHAPE_TYPE_RECTANGLE = 0,
    /** 圆形类型。 */
    ARKUI_SHAPE_TYPE_CIRCLE,
    /** 椭圆形类型。 */
    ARKUI_SHAPE_TYPE_ELLIPSE,
    /** 路径类型。 */
    ARKUI_SHAPE_TYPE_PATH,
} ArkUI_ShapeType;

/**
 * @brief 定义渐变方向结构。
 *
 * @since 12
 */
typedef enum {
    /** 向左渐变。 */
    ARKUI_LINEAR_GRADIENT_DIRECTION_LEFT = 0,
    /** 向上渐变。 */
    ARKUI_LINEAR_GRADIENT_DIRECTION_TOP,
    /** 向右渐变。 */
    ARKUI_LINEAR_GRADIENT_DIRECTION_RIGHT,
    /** 向下渐变。 */
    ARKUI_LINEAR_GRADIENT_DIRECTION_BOTTOM,
    /** 向左上渐变。 */
    ARKUI_LINEAR_GRADIENT_DIRECTION_LEFT_TOP,
    /** 向左下渐变。 */
    ARKUI_LINEAR_GRADIENT_DIRECTION_LEFT_BOTTOM,
    /** 向右上渐变。 */
    ARKUI_LINEAR_GRADIENT_DIRECTION_RIGHT_TOP,
    /** 向右下渐变。 */
    ARKUI_LINEAR_GRADIENT_DIRECTION_RIGHT_BOTTOM,
    /** 不渐变。 */
    ARKUI_LINEAR_GRADIENT_DIRECTION_NONE,
    /** 自定义渐变方向. */
    ARKUI_LINEAR_GRADIENT_DIRECTION_CUSTOM,
} ArkUI_LinearGradientDirection;

/**
 * @brief 定义文本断行规则。
 *
 * @since 12
 */
typedef enum {
    /** CJK(中文、日文、韩文)文本可以在任意2个字符间断行，而Non-CJK文本（如英文等）只能在空白符处断行。 */
    ARKUI_WORD_BREAK_NORMAL = 0,
    /** 对于Non-CJK的文本，可在任意2个字符间断行。CJK(中文、日文、韩文)文本可以在任意2个字符间断行。 */
    ARKUI_WORD_BREAK_BREAK_ALL,
    /** 对于Non-CJK的文本可在任意2个字符间断行，一行文本中有断行破发点（如空白符）时，优先按破发点换行。
        CJK(中文、日文、韩文)文本可以在任意2个字符间断行 */
    ARKUI_WORD_BREAK_BREAK_WORD,
}ArkUI_WordBreak;

/**
 * @brief 定义文本省略位置。
 *
 * @since 12
 */
typedef enum {
    /** 省略行首内容。*/
    ARKUI_ELLIPSIS_MODE_START = 0,
    /** 省略行中内容。*/
    ARKUI_ELLIPSIS_MODE_CENTER,
    /** 省略行末内容。*/
    ARKUI_ELLIPSIS_MODE_END,
}ArkUI_EllipsisMode;

/**
 * @brief 定义图片渲染模式。
 *
 * @since 12
 */
typedef enum {
    /** 原色渲染模式。*/
    ARKUI_IMAGE_RENDER_MODE_ORIGINAL = 0,
    /** 黑白渲染模式。*/
    ARKUI_IMAGE_RENDER_MODE_TEMPLATE,
}ArkUI_ImageRenderMode;

/**
 * @brief 定义转场从边缘滑入和滑出的效果。
 *
 * @since 12
 */
typedef enum {
    /** 窗口的上边缘。*/
    ARKUI_TRANSITION_EDGE_TOP = 0,
    /** 窗口的下边缘。*/
    ARKUI_TRANSITION_EDGE_BOTTOM,
    /** 窗口的左边缘。*/
    ARKUI_TRANSITION_EDGE_START,
    /** 窗口的右边缘。*/
    ARKUI_TRANSITION_EDGE_END,
}ArkUI_TransitionEdge;

/**
 * @brief 在动画中定义onFinish回调的类型。
 *
 * @since 12
 */
typedef enum {
    /** 当整个动画结束并立即删除时，将触发回调。 */
    ARKUI_FINISH_CALLBACK_REMOVED = 0,
    /** 当动画在逻辑上处于下降状态，但可能仍处于其长尾状态时，将触发回调。*/
    ARKUI_FINISH_CALLBACK_LOGICALLY,
} ArkUI_FinishCallbackType;

/**
 * @brief 交叉轴方向的布局方式。
  *
 * @since 12
 */
typedef enum {
     /** ListItem在List中，交叉轴方向首部对齐。 */
    ARKUI_LIST_ITEM_ALIGNMENT_START = 0,
    /** ListItem在List中，交叉轴方向居中对齐。*/
    ARKUI_LIST_ITEM_ALIGNMENT_CENTER,
    /** ListItem在List中，交叉轴方向尾部对齐。*/
    ARKUI_LIST_ITEM_ALIGNMENT_END,
} ArkUI_ListItemAlignment;

/**
 * @brief 指定的混合模式应用于视图的内容选项.
 *
 * @since 12
 */
typedef enum {
    /** 在目标图像上按顺序混合视图的内容. */
    BLEND_APPLY_TYPE_FAST = 0,
    /** 将此组件和子组件内容绘制到离屏画布上，然后整体进行混合. */
    BLEND_APPLY_TYPE_OFFSCREEN,
} ArkUI_BlendApplyType;

/**
 * @brief 定义遮罩屏蔽区域的范围结构体。
 *
 * @since 12
 */
typedef struct {
    /** 区域在x轴的位置。 */
    float x;
    /** 区域在y轴的位置。 */
    float y;
    /** 区域宽度。 */
    float width;
    /** 区域高度。 */
    float height;
} ArkUI_Rect;

/**
 * @brief 尺寸类型，用于描述组件的宽高。
 *
 * @since 12。
 */
typedef struct {
    /** 宽度，单位为px。*/
    int32_t width;
    /** 高度，单位为px。*/
    int32_t height;
} ArkUI_IntSize;

/**
 * @brief 位置，用于描述组件的位置。
 *
 * @since 12。
 */
typedef struct {
    /** 水平方向横坐标，单位为px。*/
    int32_t x;
    /** 竖直方向纵坐标，单位为px。*/
    int32_t y;
} ArkUI_IntOffset;

/**
 * @brief 外边距属性，用于描述组件的外边距属性。
 *
 * @since 12。
 */
typedef struct {
    /** 上外边距，单位为vp。*/
    float top;
    /** 右外边距，单位为vp。*/
    float right;
    /** 下外边距，单位为vp。*/
    float bottom;
    /** 左外边距，单位为vp。*/
    float left;
} ArkUI_Margin;

/**
 * @brief 定义组件的单位模式。
 *
 * @since 12
 */
typedef enum {
    /** 默认，字体类单位为FP，非字体类单位为VP。*/
    ARKUI_LENGTH_METRIC_UNIT_DEFAULT = -1,
    /** 单位为PX。 */
    ARKUI_LENGTH_METRIC_UNIT_PX = 0,
    /** 单位为VP。 */
    ARKUI_LENGTH_METRIC_UNIT_VP,
    /** 单位为FP。 */
    ARKUI_LENGTH_METRIC_UNIT_FP
} ArkUI_LengthMetricUnit;

/**
 * @brief 定义自动填充类型。
 *
 * @since 12
 */
typedef enum {
    /** 【用户名】在已启用密码保险箱的情况下，支持用户名的自动保存和自动填充。 */
    ARKUI_TEXTINPUT_CONTENT_TYPE_USER_NAME = 0,
    /** 【密码】在已启用密码保险箱的情况下，支持密码的自动保存和自动填充。 */
    ARKUI_TEXTINPUT_CONTENT_TYPE_PASSWORD,
    /** 【新密码】在已启用密码保险箱的情况下，支持自动生成新密码。 */
    ARKUI_TEXTINPUT_CONTENT_TYPE_NEW_PASSWORD,
    /** 【详细地址】在已启用情景化自动填充的情况下，支持详细地址的自动保存和自动填充。*/
    ARKUI_TEXTINPUT_CONTENT_TYPE_FULL_STREET_ADDRESS,
    /** 【门牌号】在已启用情景化自动填充的情况下，支持门牌号的自动保存和自动填充。*/
    ARKUI_TEXTINPUT_CONTENT_TYPE_HOUSE_NUMBER,
    /** 【区/县】在已启用情景化自动填充的情况下，支持区/县的自动保存和自动填充。*/
    ARKUI_TEXTINPUT_CONTENT_TYPE_DISTRICT_ADDRESS,
    /** 【市】在已启用情景化自动填充的情况下，支持市的自动保存和自动填充。*/
    ARKUI_TEXTINPUT_CONTENT_TYPE_CITY_ADDRESS,
    /** 【省】在已启用情景化自动填充的情况下，支持省的自动保存和自动填充。*/
    ARKUI_TEXTINPUT_CONTENT_TYPE_PROVINCE_ADDRESS,
    /** 【国家】在已启用情景化自动填充的情况下，支持国家的自动保存和自动填充。*/
    ARKUI_TEXTINPUT_CONTENT_TYPE_COUNTRY_ADDRESS,
    /** 【姓名】在已启用情景化自动填充的情况下，支持姓名的自动保存和自动填充。*/
    ARKUI_TEXTINPUT_CONTENT_TYPE_PERSON_FULL_NAME,
    /** 【姓氏】在已启用情景化自动填充的情况下，支持姓氏的自动保存和自动填充。*/
    ARKUI_TEXTINPUT_CONTENT_TYPE_PERSON_LAST_NAME,
    /** 【名字】在已启用情景化自动填充的情况下，支持名字的自动保存和自动填充。*/
    ARKUI_TEXTINPUT_CONTENT_TYPE_PERSON_FIRST_NAME,
    /** 【手机号】在已启用情景化自动填充的情况下，支持手机号的自动保存和自动填充。*/
    ARKUI_TEXTINPUT_CONTENT_TYPE_PHONE_NUMBER,
    /** 【国家代码】在已启用情景化自动填充的情况下，支持国家代码的自动保存和自动填充。*/
    ARKUI_TEXTINPUT_CONTENT_TYPE_PHONE_COUNTRY_CODE,
    /** 【包含国家代码的手机号】在已启用情景化自动填充的情况下，支持包含国家代码的手机号的自动保存和自动填充。*/
    ARKUI_TEXTINPUT_CONTENT_TYPE_FULL_PHONE_NUMBER,
    /** 【邮箱地址】在已启用情景化自动填充的情况下，支持邮箱地址的自动保存和自动填充。*/
    ARKUI_TEXTINPUT_CONTENT_EMAIL_ADDRESS,
    /** 【银行卡号】在已启用情景化自动填充的情况下，支持银行卡号的自动保存和自动填充。*/
    ARKUI_TEXTINPUT_CONTENT_TYPE_BANK_CARD_NUMBER,
    /** 【身份证号】在已启用情景化自动填充的情况下，支持身份证号的自动保存和自动填充。*/
    ARKUI_TEXTINPUT_CONTENT_TYPE_ID_CARD_NUMBER,
    /** 【昵称】在已启用情景化自动填充的情况下，支持昵称的自动保存和自动填充。*/
    ARKUI_TEXTINPUT_CONTENT_TYPE_NICKNAME,
    /** 【无街道地址】在已启用情景化自动填充的情况下，支持无街道地址的自动保存和自动填充。*/
    ARKUI_TEXTINPUT_CONTENT_TYPE_DETAIL_INFO_WITHOUT_STREET,
    /** 【标准地址】在已启用情景化自动填充的情况下，支持标准地址的自动保存和自动填充。*/
    ARKUI_TEXTINPUT_CONTENT_TYPE_FORMAT_ADDRESS,
}ArkUI_TextInputContentType;

/**
 * @brief 定义屏障线的方向。
 *
 * @since 12
 */
typedef enum {
    /** 屏障在其所有referencedId的最左侧。*/
    ARKUI_BARRIER_DIRECTION_START = 0,
    /** 屏障在其所有referencedId的最右侧。*/
    ARKUI_BARRIER_DIRECTION_END,
    /** 屏障在其所有referencedId的最上方。*/
    ARKUI_BARRIER_DIRECTION_TOP,
    /** 屏障在其所有referencedId的最下方。*/
    ARKUI_BARRIER_DIRECTION_BOTTOM
} ArkUI_BarrierDirection;


/**
 * @brief 定义链的风格。
 *
 * @since 12
 */
typedef enum {
    /**组件在约束锚点间均匀分布。 */
    ARKUI_RELATIVE_LAYOUT_CHAIN_STYLE_SPREAD = 0,
    /**除首尾2个子组件的其他组件在约束锚点间均匀分布。 */
    ARKUI_RELATIVE_LAYOUT_CHAIN_STYLE_SPREAD_INSIDE,
    /**链内子组件无间隙。 */
    ARKUI_RELATIVE_LAYOUT_CHAIN_STYLE_PACKED,
} ArkUI_RelativeLayoutChainStyle;

/**
 * @brief 定义输入框风格。
 *
 * @since 12
 */
typedef enum {
    /** 默认风格，光标宽1.5vp，光标高度与文本选中底板高度和字体大小相关。*/
    ARKUI_TEXTINPUT_STYLE_DEFAULT = 0,
    /** 内联输入风格。文本选中底板高度与输入框高度相同。 */
    ARKUI_TEXTINPUT_STYLE_INLINE
} ArkUI_TextInputStyle;

/**
 * @brief 定义文本识别的实体类型。
 *
 * @since 12
 */
typedef enum {
    /** 电话号码。*/
    ARKUI_TEXT_DATA_DETECTOR_TYPE_PHONE_NUMBER = 0,
    /** 链接。 */
    ARKUI_TEXT_DATA_DETECTOR_TYPE_URL,
    /** 邮箱。 */
    ARKUI_TEXT_DATA_DETECTOR_TYPE_EMAIL,
    /** 地址。 */
    ARKUI_TEXT_DATA_DETECTOR_TYPE_ADDRESS,
} ArkUI_TextDataDetectorType;

/**
 * @brief 定义按钮样式枚举值。
 *
 * @since 12
 */
typedef enum {
    /** 普通按钮，默认不带圆角。*/
    ARKUI_BUTTON_TYPE_NORMAL = 0,
    /** 胶囊型按钮，圆角默认为高度的一半。*/
    ARKUI_BUTTON_TYPE_CAPSULE,
    /** 圆形按钮。*/
    ARKUI_BUTTON_TYPE_CIRCLE,
} ArkUI_ButtonType;

typedef enum {
    /** 保持动画终态的内容大小，并且内容始终与组件保持中心对齐。*/
    ARKUI_RENDER_FIT_CENTER = 0,
    /** 保持动画终态的内容大小，并且内容始终与组件保持顶部中心对齐。 */
    ARKUI_RENDER_FIT_TOP,
    /** 保持动画终态的内容大小，并且内容始终与组件保持底部中心对齐。 */
    ARKUI_RENDER_FIT_BOTTOM,
    /** 保持动画终态的内容大小，并且内容始终与组件保持左侧对齐。 */
    ARKUI_RENDER_FIT_LEFT,
    /** 保持动画终态的内容大小，并且内容始终与组件保持右侧对齐。 */
    ARKUI_RENDER_FIT_RIGHT,
    /** 保持动画终态的内容大小，并且内容始终与组件保持左上角对齐。 */
    ARKUI_RENDER_FIT_TOP_LEFT,
    /** 保持动画终态的内容大小，并且内容始终与组件保持右上角对齐。 */
    ARKUI_RENDER_FIT_TOP_RIGHT,
    /** 保持动画终态的内容大小，并且内容始终与组件保持左下角对齐。 */
    ARKUI_RENDER_FIT_BOTTOM_LEFT,
    /** 保持动画终态的内容大小，并且内容始终与组件保持右下角对齐。 */
    ARKUI_RENDER_FIT_BOTTOM_RIGHT,
    /** 不考虑动画终态内容的宽高比，并且内容始终缩放到组件的大小。 */
    ARKUI_RENDER_FIT_RESIZE_FILL,
    /** 保持动画终态内容的宽高比进行缩小或放大，使内容完整显示在组件内，且与组件保持中心对齐。 */
    ARKUI_RENDER_FIT_RESIZE_CONTAIN,
    /** 保持动画终态内容的宽高比进行缩小或放大，使内容完整显示在组件内。当组件宽方向有剩余时，内容与组件保持左侧对齐，当组件高方向有剩余时，内容与组件保持顶部对齐。 */
    ARKUI_RENDER_FIT_RESIZE_CONTAIN_TOP_LEFT,
    /** 保持动画终态内容的宽高比进行缩小或放大，使内容完整显示在组件内。当组件宽方向有剩余时，内容与组件保持右侧对齐，当组件高方向有剩余时，内容与组件保持底部对齐。 */
    ARKUI_RENDER_FIT_RESIZE_CONTAIN_BOTTOM_RIGHT,
    /** 保持动画终态内容的宽高比进行缩小或放大，使内容两边都大于或等于组件两边，且与组件保持中心对齐，显示内容的中间部分。 */
    ARKUI_RENDER_FIT_RESIZE_COVER,
    /** 保持动画终态内容的宽高比进行缩小或放大，使内容的两边都恰好大于或等于组件两边。当内容宽方向有剩余时，内容与组件保持左侧对齐，显示内容的左侧部分。当内容高方向有剩余时，内容与组件保持顶部对齐，显示内容的顶侧部分。 */
    ARKUI_RENDER_FIT_RESIZE_COVER_TOP_LEFT,
    /** 保持动画终态内容的宽高比进行缩小或放大，使内容的两边都恰好大于或等于组件两边。当内容宽方向有剩余时，内容与组件保持右侧对齐，显示内容的右侧部分。当内容高方向有剩余时，内容与组件保持底部对齐，显示内容的底侧部分。 */
    ARKUI_RENDER_FIT_RESIZE_COVER_BOTTOM_RIGHT
} ArkUI_RenderFit;

typedef enum {
    /** 跟随系统深浅色模式。 */
    ARKUI_THEME_COLOR_MODE_SYSTEM = 0,
    /** 固定使用浅色模式。 */
    ARKUI_THEME_COLOR_MODE_LIGHT,
    /** 固定使用深色模式。 */
    ARKUI_THEME_COLOR_MODE_DARK
} ArkUI_ThemeColorMode;

/**
 * @brief 定义 Swiper 组件的导航指示器类型。
 *
 * @since 12
 */
typedef enum {
    /** 圆点指示器类型。*/
    ARKUI_SWIPER_INDICATOR_TYPE_DOT,
    /** 数字指示器类型。*/
    ARKUI_SWIPER_INDICATOR_TYPE_DIGIT,
} ArkUI_SwiperIndicatorType;


/**
 * @brief 动画播放模式。
 *
 * @since 12
 */
typedef enum {
    /**动画正向循环播放。*/
    ARKUI_ANIMATION_DIRECTION_NORMAL = 0,
    /**动画反向循环播放。*/
    ARKUI_ANIMATION_DIRECTION_REVERSE,
    /**动画交替循环播放，奇数次正向播放，偶数次反向播放。*/
    ARKUI_ANIMATION_DIRECTION_ALTERNATE,
    /**动画反向交替循环播放，奇数次反向播放，偶数次正向播放。*/
    ARKUI_ANIMATION_DIRECTION_ALTERNATE_REVERSE,
} ArkUI_AnimationDirection;

/**
 * @brief 动画执行后是否恢复到初始状态，动画执行后，动画结束时的状态（在最后一个关键帧中定义）将保留。
 *
 * @since 12
 */
typedef enum {
    /**在动画执行之前和之后都不会应用任何样式到目标上。*/
    ARKUI_ANIMATION_FILL_NONE = 0,
    /**在动画结束后，目标将保留动画结束时的状态（在最后一个关键帧中定义）。*/
    ARKUI_ANIMATION_FILL_FORWARDS,
    /**动画将在animation-delay期间应用第一个关键帧中定义的值。*/
    ARKUI_ANIMATION_FILL_BACKWARDS,
    /**动画将遵循forwards和backwards的规则，从而在两个方向上扩展动画属性。*/
    ARKUI_ANIMATION_FILL_BOTH,
} ArkUI_AnimationFill;

/**
 * @brief 定义 Swiper 组件的主轴方向上元素排列的模式。
 *
 * @since 12
 */
typedef enum {
    /** Swiper滑动一页的宽度为Swiper组件自身的宽度。*/
    ARKUI_SWIPER_DISPLAY_MODE_STRETCH,
    /** Swiper滑动一页的宽度为视窗内最左侧子组件的宽度。*/
    ARKUI_SWIPER_DISPLAY_MODE_AUTO_LINEAR,
} ArkUI_SwiperDisplayModeType;

/**
 * @brief 定义 Listitem 组件SwipeAction方法的显隐模式。
 *
 * @since 12
 */
typedef enum {
    /** 收起状态，当ListItem与主轴方向相反滑动时操作项处于隐藏状态。*/
    ARKUI_LIST_ITEM_SWIPE_ACTION_STATE_COLLAPSED = 0,
    /** 收起状态，当ListItem与主轴方向相反滑动时操作项处于显示状态。*/
    ARKUI_LIST_ITEM_SWIPE_ACTION_STATE_EXPANDED,
    /** 长距离状态，当ListItem进入长距删除区后删除ListItem的状态。*/
    ARKUI_LIST_ITEM_SWIPE_ACTION_STATE_ACTIONING,
} ArkUI_ListItemSwipeActionState;

/**
 * @brief 定义 Listitem 组件SwipeAction方法的滚动模式。
 *
 * @since 12
 */
typedef enum {
    /** ListItem划动距离超过划出组件大小后可以继续划动。 */
    ARKUI_LIST_ITEM_SWIPE_EDGE_EFFECT_SPRING = 0,
    /** ListItem划动距离不能超过划出组件大小。 */
    ARKUI_LIST_ITEM_SWIPE_EDGE_EFFECT_NONE,
} ArkUI_ListItemSwipeEdgeEffect;

/**
 * @brief 定义帧动画的播放状态。
 *
 * @since 12
*/
typedef enum {
    /** 动画初始状态。 */
    ARKUI_ANIMATION_STATUS_INITIAL,
    /** 动画处于播放状态。*/
    ARKUI_ANIMATION_STATUS_RUNNING,
    /** 动画处于暂停状态。*/
    ARKUI_ANIMATION_STATUS_PAUSED,
    /** 动画处于停止状态。*/
    ARKUI_ANIMATION_STATUS_STOPPED,
} ArkUI_AnimationStatus;

/**
 * @brief 定义帧动画组件在动画开始前和结束后的状态。
 *
 * @since 12
*/
typedef enum {
    /** 动画未执行时不会将任何样式应用于目标，动画播放完成之后恢复初始默认状态。*/
    ARKUI_ANIMATION_FILL_MODE_NONE,
    /** 目标将保留动画执行期间最后一个关键帧的状态。*/
    ARKUI_ANIMATION_FILL_MODE_FORWARDS,
    /** 动画将在应用于目标时立即应用第一个关键帧中定义的值，并在delay期间保留此值。*/
    ARKUI_ANIMATION_FILL_MODE_BACKWARDS,
    /** 动画将遵循Forwards和Backwards的规则，从而在两个方向上扩展动画属性。*/
    ARKUI_ANIMATION_FILL_MODE_BOTH,
} ArkUI_AnimationFillMode;

/**
 * @brief 定义错误码枚举值。
 *
 * @since 12
*/
typedef enum {
    /** 无错误。*/
    ARKUI_ERROR_CODE_NO_ERROR = 0,
    /** 参数错误。*/
    ARKUI_ERROR_CODE_PARAM_INVALID = 401,
    /** 组件不支持特定的属性或者事件。*/
    ARKUI_ERROR_CODE_ATTRIBUTE_OR_EVENT_NOT_SUPPORTED = 106102,
    /** 对应的操作不支持ArkTS创建的节点。*/
    ARKUI_ERROR_CODE_NOT_SUPPROTED_FOR_ARKTS_NODE = 106103,
    /** 懒加载适配器未绑定到组件上。*/
    ARKUI_ERROR_CODE_NODE_ADAPTER_NONE_HOST = 106104,
    /** 适配器已存在。*/
    ARKUI_ERROR_CODE_NODE_ADAPTER_EXIST_IN_HOST = 106105,
    /** 对应节点已存在子节点，无法添加适配器。*/
    ARKUI_ERROR_CODE_NODE_ADAPTER_CHILD_NODE_EXIST = 106106,
    /** 组件事件中参数长度超限。*/
    ARKUI_ERROR_CODE_NODE_EVENT_PARAM_INDEX_OUT_OF_RANGE = 106107,
    /** 组件事件中不存在该数据。*/
    ARKUI_ERROR_CODE_NODE_EVENT_PARAM_INVALID = 106108,
    /** 组件事件不支持返回值。*/
    ARKUI_ERROR_CODE_NODE_EVENT_NO_RETURN = 106109,
    /** 传入的索引值非法。*/
    ARKUI_ERROR_CODE_NODE_INDEX_INVALID = 106200,
    /** 查询路由导航信息失败。*/
    ARKUI_ERROR_CODE_GET_INFO_FAILED = 106201,
    /** 传入的buffer size异常。*/
    ARKUI_ERROR_CODE_BUFFER_SIZE_ERROR = 106202,
    /** 非滚动类容器。 */
    ARKUI_ERROR_CODE_NON_SCROLLABLE_CONTAINER = 180001,
    /** 存储区大小不足 */
    ARKUI_ERROR_CODE_BUFFER_SIZE_NOT_ENOUGH = 180002,
    /** 无效的属性字符串 */
    ARKUI_ERROR_CODE_INVALID_STYLED_STRING = 180101,
} ArkUI_ErrorCode;

/**
 * @brief 定义滚动来源枚举值。
 *
 * @since 12
*/
typedef enum {
    /** 手指拖动。*/
    ARKUI_SCROLL_SOURCE_DRAG = 0,
    /** 手指拖动后的惯性滚动。*/
    ARKUI_SCROLL_SOURCE_FLING,
    /** 在过界时执行EdgeEffect.Spring边缘特效。*/
    ARKUI_SCROLL_SOURCE_EDGE_EFFECT,
    /** 除了拖动以外的其他用户输入，如鼠标滚轮、键盘事件等。*/
    ARKUI_SCROLL_SOURCE_OTHER_USER_INPUT,
    /** 拖动滚动条。*/
    ARKUI_SCROLL_SOURCE_SCROLL_BAR,
    /** 拖动滚动条后的惯性滚动。*/
    ARKUI_SCROLL_SOURCE_SCROLL_BAR_FLING,
    /** 滚动控制器引起的无动画的滚动。*/
    ARKUI_SCROLL_SOURCE_SCROLLER,
    /** 滚动控制器引起的带动画的滚动。*/
    ARKUI_SCROLL_SOURCE_ANIMATION,
} ArkUI_ScrollSource;

/**
 * @brief 定义扩展安全区域的枚举值。
 *
 * @since 12
*/
typedef enum {
    /** 系统默认非安全区域，包括状态栏、导航栏。*/
    ARKUI_SAFE_AREA_TYPE_SYSTEM = 1,
    /** 设备的非安全区域，例如刘海屏或挖孔屏区域。*/
    ARKUI_SAFE_AREA_TYPE_CUTOUT = 1 << 1,
    /** 软键盘区域。*/
    ARKUI_SAFE_AREA_TYPE_KEYBOARD = 1 << 2,
} ArkUI_SafeAreaType;

/**
 * @brief 定义扩展安全区域的方向的枚举值。
 *
 * @since 12
*/
typedef enum {
    /** 上方区域。*/
    ARKUI_SAFE_AREA_EDGE_TOP = 1,
    /** 下方区域。*/
    ARKUI_SAFE_AREA_EDGE_BOTTOM = 1 << 1,
    /** 前部区域。*/
    ARKUI_SAFE_AREA_EDGE_START = 1 << 2,
    /** 尾部区域。*/
    ARKUI_SAFE_AREA_EDGE_END = 1 << 3,
} ArkUI_SafeAreaEdge;

/**
 * @brief 系统字体变更事件定义。
 *
 * @since 12
 */
typedef struct ArkUI_SystemFontStyleEvent ArkUI_SystemFontStyleEvent;

/**
 * @brief 定义组件转场时的平移效果对象。
 *
 * @since 12
 */
typedef struct {
    /** 横向的平移距离。*/
    float x;
    /** 纵向的平移距离。*/
    float y;
    /** 竖向的平移距离。*/
    float z;
} ArkUI_TranslationOptions;

/**
 * @brief 定义组件转场时的缩放效果对象。
 *
 * @since 12
 */
typedef struct {
    /** 横向放大倍数（或缩小比例）。*/
    float x;
    /** 纵向放大倍数（或缩小比例）。*/
    float y;
    /** 当前为二维显示，该参数无效 。*/
    float z;
    /** 变换中心点x轴坐标。*/
    float centerX;
    /** 变换中心点y轴坐标。*/
    float centerY;
} ArkUI_ScaleOptions;

/**
 * @brief 定义组件转场时的旋转效果对象。
 *
 * @since 12
 */
typedef struct {
    /** 横向的旋转向量分量。*/
    float x;
    /** 纵向的旋转向量分量。*/
    float y;
    /** 竖向的旋转向量分量。*/
    float z;
    /** 旋转角度。*/
    float angle;
    /** 变换中心点x轴坐标。*/
    float centerX;
    /** 变换中心点y轴坐标。*/
    float centerY;
    /** z轴锚点，即3D旋转中心点的z轴分量。*/
    float centerZ;
    /** 视距，即视点到z=0平面的距离。*/
    float perspective;
} ArkUI_RotationOptions;

/**
 * @brief 自定义段落组件的测量信息。
 *
 * @since 12
 */
typedef struct ArkUI_CustomSpanMeasureInfo ArkUI_CustomSpanMeasureInfo;

/**
 * @brief 自定义段落组件的度量指标。
 *
 * @since 12
 */
typedef struct ArkUI_CustomSpanMetrics ArkUI_CustomSpanMetrics;

/**
 * @brief 自定义段落组件的绘制信息。
 *
 * @since 12
 */
typedef struct ArkUI_CustomSpanDrawInfo ArkUI_CustomSpanDrawInfo;

/**
 * @brief 定义NavDestination组件的状态。
 *
 * @since 12
*/
typedef enum {
    /** NavDestination组件显示。*/
    ARKUI_NAV_DESTINATION_STATE_ON_SHOW = 0,
    /** NavDestination组件隐藏。*/
    ARKUI_NAV_DESTINATION_STATE_ON_HIDE = 1,
    /** NavDestination从组件树上挂载。*/
    ARKUI_NAV_DESTINATION_STATE_ON_APPEAR = 2,
    /** NavDestination从组件树上卸载。*/
    ARKUI_NAV_DESTINATION_STATE_ON_DISAPPEAR = 3,
    /** NavDestination组件显示之前。*/
    ARKUI_NAV_DESTINATION_STATE_ON_WILL_SHOW = 4,
    /** NavDestination组件隐藏之前。*/
    ARKUI_NAV_DESTINATION_STATE_ON_WILL_HIDE = 5,
    /** NavDestination挂载到组件树之前。*/
    ARKUI_NAV_DESTINATION_STATE_ON_WILL_APPEAR = 6,
    /** NavDestination从组件树上卸载之前。*/
    ARKUI_NAV_DESTINATION_STATE_ON_WILL_DISAPPEAR = 7,
    /** NavDestination从组件返回。*/
    ARKUI_NAV_DESTINATION_STATE_ON_BACK_PRESS = 100,
} ArkUI_NavDestinationState;

/**
 * @brief 定义Router Page的状态。
 *
 * @since 12
*/
typedef enum {
    /** Router Page即将创建。*/
    ARKUI_ROUTER_PAGE_STATE_ON_WILL_APPEAR = 0,
    /** Router Page即将销毁。*/
    ARKUI_ROUTER_PAGE_STATE_ON_WILL_DISAPPEAR = 1,
    /** Router Page显示。*/
    ARKUI_ROUTER_PAGE_STATE_ON_SHOW = 2,
    /** Router Page隐藏。*/
    ARKUI_ROUTER_PAGE_STATE_ON_HIDE = 3,
    /** Router Page返回时。*/
    ARKUI_ROUTER_PAGE_STATE_ON_BACK_PRESS = 4,
} ArkUI_RouterPageState;

/**
 * @brief 定义 Swiper 组件的导航指示器风格。
 *
 * @since 12
 */
typedef struct ArkUI_SwiperIndicator ArkUI_SwiperIndicator;

/**
* @brief 定义文本组件支持的属性字符串的数据对象。
*
* @since 14
*/
typedef struct ArkUI_StyledString_Descriptor ArkUI_StyledString_Descriptor;

/**
* @brief 创建约束尺寸。
*
* @since 12
*/
ArkUI_LayoutConstraint* OH_ArkUI_LayoutConstraint_Create();

/**
* @brief 约束尺寸深拷贝。
*
* @param Constraint 约束尺寸。
* @return 新的约束尺寸指针。
* @since 12
*/
ArkUI_LayoutConstraint* OH_ArkUI_LayoutConstraint_Copy(const ArkUI_LayoutConstraint* Constraint);

/**
* @brief 销毁约束尺寸指针。
*
* @param Constraint 约束尺寸。
* @since 12
*/
void* OH_ArkUI_LayoutConstraint_Dispose(ArkUI_LayoutConstraint* Constraint);

/**
* @brief 通过约束尺寸获取最大宽度，单位为px。
*
* @param Constraint 约束尺寸。
* @return  最大宽度。
* @since 12
*/
int32_t OH_ArkUI_LayoutConstraint_GetMaxWidth(const ArkUI_LayoutConstraint* Constraint);

/**
* @brief 通过约束尺寸获取最小宽度，单位为px。
*
* @param Constraint 约束尺寸。
* @return  最小宽度。
* @since 12
*/
int32_t OH_ArkUI_LayoutConstraint_GetMinWidth(const ArkUI_LayoutConstraint* Constraint);

/**
* @brief 通过约束尺寸获取最大高度，单位为px。
*
* @param Constraint 约束尺寸。
* @return  最大高度。
* @since 12
*/
int32_t OH_ArkUI_LayoutConstraint_GetMaxHeight(const ArkUI_LayoutConstraint* Constraint);

/**
* @brief 通过约束尺寸获取最小高度，单位为px。
*
* @param Constraint 约束尺寸。
* @return  最小高度。
* @since 12
*/
int32_t OH_ArkUI_LayoutConstraint_GetMinHeight(const ArkUI_LayoutConstraint* Constraint);

/**
* @brief 通过约束尺寸获取宽度百分比基准，单位为px。
*
* @param Constraint 约束尺寸。
* @return  宽度百分比基准。
* @since 12
*/
int32_t OH_ArkUI_LayoutConstraint_GetPercentReferenceWidth(const ArkUI_LayoutConstraint* Constraint);

/**
* @brief 通过约束尺寸获取高度百分比基准，单位为px。
*
* @param Constraint 约束尺寸。
* @return  高度百分比基准。
* @since 12
*/
int32_t OH_ArkUI_LayoutConstraint_GetPercentReferenceHeight(const ArkUI_LayoutConstraint* Constraint);

/**
* @brief 设置最大宽度。
*
* @param Constraint 约束尺寸。
* @param value 最大宽度，单位为px。
* @since 12
*/
void OH_ArkUI_LayoutConstraint_SetMaxWidth(ArkUI_LayoutConstraint* Constraint, int32_t value);

/**
* @brief 设置最小宽度。
*
* @param Constraint 约束尺寸。
* @param value 最小宽度，单位为px。
* @since 12
*/
void OH_ArkUI_LayoutConstraint_SetMinWidth(ArkUI_LayoutConstraint* Constraint, int32_t value);

/**
* @brief 设置最大高度。
*
* @param Constraint 约束尺寸。
* @param value 最大高度，单位为px。
* @since 12
*/
void OH_ArkUI_LayoutConstraint_SetMaxHeight(ArkUI_LayoutConstraint* Constraint, int32_t value);

/**
* @brief 设置最小高度。
*
* @param Constraint 约束尺寸。
* @param value 最小高度，单位为px。
* @since 12
*/
void OH_ArkUI_LayoutConstraint_SetMinHeight(ArkUI_LayoutConstraint* Constraint, int32_t value);

/**
* @brief 设置宽度百分比基准。
*
* @param Constraint 约束尺寸。
* @param value 宽度百分比基准，单位为px。
* @since 12
*/
void OH_ArkUI_LayoutConstraint_SetPercentReferenceWidth(ArkUI_LayoutConstraint* Constraint, int32_t value);

/**
* @brief 设置高度百分比基准。
*
* @param Constraint 约束尺寸。
* @param value 高度百分比基准，单位为px。
* @since 12
*/
void OH_ArkUI_LayoutConstraint_SetPercentReferenceHeight(ArkUI_LayoutConstraint* Constraint, int32_t value);

/**
* @brief 获取绘制canvas指针，可以转换为图形库的OH_Drawing_Canvas指针进行绘制。
*
* @param context 绘制上下文。
* @return 用于绘制的canvas指针。
* @since 12
*/
void* OH_ArkUI_DrawContext_GetCanvas(ArkUI_DrawContext* context);

/**
* @brief 获取可绘制区域大小。
*
* @param context 绘制上下文。
* @return 可绘制区域大小。
* @since 12
*/
ArkUI_IntSize OH_ArkUI_DrawContext_GetSize(ArkUI_DrawContext* context);

/**
* @brief 创建FlowItem分组配置信息。
*
* @return FlowItem分组配置信息。
* @since 12
*/
ArkUI_WaterFlowSectionOption* OH_ArkUI_WaterFlowSectionOption_Create();

/**
* @brief 销毁FlowItem分组配置信息指针。
*
* @param option FlowItem分组配置信息。
* @since 12
*/
void OH_ArkUI_WaterFlowSectionOption_Dispose(ArkUI_WaterFlowSectionOption* option);

/**
* @brief 设置FlowItem分组配置信息数组长度。
*
* @param option FlowItem分组配置信息。
* @param size 数组长度。
* @since 12
*/
void OH_ArkUI_WaterFlowSectionOption_SetSize(ArkUI_WaterFlowSectionOption* option,
    int32_t size);

/**
* @brief 设置FlowItem分组配置信息数组长度。
*
* @param option FlowItem分组配置信息。
* @return 数组长度。如果返回-1，则返回失败。失败的原因可能是option参数异常，如空指针。
* @since 12
*/
int32_t OH_ArkUI_WaterFlowSectionOption_GetSize(ArkUI_WaterFlowSectionOption* option);

/**
* @brief 设置分组中FlowItem数量。
*
* @param option FlowItem分组配置信息。
* @param index FlowItem索引值。
* @param itemCount 分组中FlowItem数量。
* @since 12
*/
void OH_ArkUI_WaterFlowSectionOption_SetItemCount(ArkUI_WaterFlowSectionOption* option,
    int32_t index, int32_t itemCount);

/**
* @brief 通过FlowItem分组配置信息获取对应索引下的FlowItem数量。
*
* @param option FlowItem分组配置信息。
* @param index FlowItem索引值。
* @return 分组中FlowItem数量。
* @since 12
*/
int32_t OH_ArkUI_WaterFlowSectionOption_GetItemCount(ArkUI_WaterFlowSectionOption* option, int32_t index);

/**
* @brief 设置布局栅格，纵向布局时为列数，横向布局时为行数。
*
* @param option FlowItem分组配置信息。
* @param index FlowItem索引值。
* @param crossCount 布局栅格数量。
* @since 12
*/
void OH_ArkUI_WaterFlowSectionOption_SetCrossCount(ArkUI_WaterFlowSectionOption* option,
    int32_t index, int32_t crossCount);

/**
* @brief 通过FlowItem分组配置信息获取对应索引下的布局栅格数。
*
* @param option FlowItem分组配置信息。
* @param index FlowItem索引值。
* @return 布局栅格数量。
* @since 12
*/
int32_t OH_ArkUI_WaterFlowSectionOption_GetCrossCount(ArkUI_WaterFlowSectionOption* option, int32_t index);

/**
* @brief 设置分组的列间距。
*
* @param option FlowItem分组配置信息。
* @param index FlowItem索引值。
* @param columnGap 列间距。
* @since 12
*/
void OH_ArkUI_WaterFlowSectionOption_SetColumnGap(ArkUI_WaterFlowSectionOption*,
    int32_t index, float columnGap);

/**
* @brief 通过FlowItem分组配置信息获取对应索引下的分组的列间距。
*
* @param option FlowItem分组配置信息。
* @param index FlowItem索引值。
* @return 列间距。
* @since 12
*/
float OH_ArkUI_WaterFlowSectionOption_GetColumnGap(ArkUI_WaterFlowSectionOption* option, int32_t index);

/**
* @brief 设置分组的行间距。
*
* @param option FlowItem分组配置信息。
* @param index FlowItem索引值。
* @param rowGap 行间距。
* @since 12
*/
void OH_ArkUI_WaterFlowSectionOption_SetRowGap(ArkUI_WaterFlowSectionOption* option,
    int32_t index, float rowGap);

/**
* @brief 通过FlowItem分组配置信息获取对应索引下的分组的行间距。
*
* @param option FlowItem分组配置信息。
* @param index FlowItem索引值。
* @return 行间距。
* @since 12
*/
float OH_ArkUI_WaterFlowSectionOption_GetRowGap(ArkUI_WaterFlowSectionOption* option, int32_t index);

/**
* @brief 设置分组的外边距。
*
* @param option FlowItem分组配置信息。
* @param index FlowItem索引值。
* @param marginTop FlowItem上外边距。
* @param marginRight FlowItem右外边距。
* @param marginBottom FlowItem下外边距。
* @param marginLeft FlowItem左外边距。
* @since 12
*/
void OH_ArkUI_WaterFlowSectionOption_SetMargin(ArkUI_WaterFlowSectionOption* option, int32_t index,
    float marginTop, float marginRight, float marginBottom, float marginLeft);

/**
* @brief 通过FlowItem分组配置信息获取对应索引下的分组的外边距。
*
* @param option FlowItem分组配置信息。
* @param index FlowItem索引值。
* @return 外边距。
* @since 12
*/
ArkUI_Margin OH_ArkUI_WaterFlowSectionOption_GetMargin(ArkUI_WaterFlowSectionOption* option, int32_t index);

/**
* @brief 通过FlowItem分组配置信息根据flowItemIndex获取指定Item的主轴大小。
*
* @param option FlowItem分组配置信息。
* @param index FlowItem索引值。
* @param callback 根据index获取指定Item的主轴大小。
* @since 12
*/
void OH_ArkUI_WaterFlowSectionOption_RegisterGetItemMainSizeCallbackByIndex(ArkUI_WaterFlowSectionOption* option,
    int32_t index, float(*callback)(int32_t itemIndex));

/**
* @brief 通过FlowItem分组配置信息根据flowItemIndex获取指定Item的主轴大小。
*
* @param option FlowItem分组配置信息。
* @param index FlowItem索引值。
* @param userData FlowItem自定义数据。
* @param callback 根据index获取指定Item的主轴大小。
* @since 12
*/
void OH_ArkUI_WaterFlowSectionOption_RegisterGetItemMainSizeCallbackByIndexWithUserData(
    ArkUI_WaterFlowSectionOption* option, int32_t index, void* userData,
    float (*callback)(int32_t itemIndex, void* userData));

/**
* @brief 创建RelativeContaine容器内的辅助线信息。
*
* @param size 辅助线数量。
* @return 辅助线信息。
* @since 12
*/
ArkUI_GuidelineOption* OH_ArkUI_GuidelineOption_Create(int32_t size);

/**
* @brief 销毁辅助线信息。
*
* @param guideline 辅助线信息。
* @since 12
*/
void OH_ArkUI_GuidelineOption_Dispose(ArkUI_GuidelineOption* guideline);

/**
* @brief 设置辅助线的Id。
*
* @param guideline 辅助线信息。
* @param value id，必须是唯一的并且不可与容器内组件重名。
* @param index 辅助线索引值。
* @since 12
*/
void OH_ArkUI_GuidelineOption_SetId(ArkUI_GuidelineOption* guideline, const char* value, int32_t index);

/**
* @brief 设置辅助线的方向。
*
* @param guideline 辅助线信息。
* @param value 方向。
* @param index 辅助线索引值。
* @since 12
*/
void OH_ArkUI_GuidelineOption_SetDirection(ArkUI_GuidelineOption* guideline, ArkUI_Axis value, int32_t index);

/**
* @brief 设置距离容器左侧或者顶部的距离。
*
* @param guideline 辅助线信息。
* @param value 距离容器左侧或者顶部的距离。
* @param index 辅助线索引值。
* @since 12
*/
void OH_ArkUI_GuidelineOption_SetPositionStart(ArkUI_GuidelineOption* guideline, float value, int32_t index);

/**
* @brief 设置距离容器右侧或者底部的距离。
*
* @param guideline 辅助线信息。
* @param value 距离容器右侧或者底部的距离。
* @param index 辅助线索引值。
* @since 12
*/
void OH_ArkUI_GuidelineOption_SetPositionEnd(ArkUI_GuidelineOption* guideline, float value, int32_t index);

/**
* @brief 获取辅助线的Id。
*
* @param guideline 辅助线信息。
* @param index 辅助线索引值。
* @return Id。
* @since 12
*/
const char* OH_ArkUI_GuidelineOption_GetId(ArkUI_GuidelineOption* guideline, int32_t index);

/**
* @brief 获取辅助线的方向。
*
* @param guideline 辅助线信息。
* @param index 辅助线索引值。
* @return 方向。
* @since 12
*/
ArkUI_Axis OH_ArkUI_GuidelineOption_GetDirection(ArkUI_GuidelineOption* guideline, int32_t index);

/**
* @brief 获取距离容器左侧或者顶部的距离。
*
* @param guideline 辅助线信息。
* @param index 辅助线索引值。
* @return 距离容器左侧或者顶部的距离。
* @since 12
*/
float OH_ArkUI_GuidelineOption_GetPositionStart(ArkUI_GuidelineOption* guideline, int32_t index);

/**
* @brief 获取距离容器右侧或者底部的距离。
*
* @param guideline 辅助线信息。
* @param index 辅助线索引值。
* @return 距离容器右侧或者底部的距离。
* @since 12
*/
float OH_ArkUI_GuidelineOption_GetPositionEnd(ArkUI_GuidelineOption* guideline, int32_t index);

/**
* @brief 创建RelativeContaine容器内的屏障信息。
*
* @param size 屏障数量。
* @return 屏障信息。
* @since 12
*/
ArkUI_BarrierOption* OH_ArkUI_BarrierOption_Create(int32_t size);

/**
* @brief 销毁屏障信息。
*
* @param barrierStyle 屏障信息。
* @since 12
*/
void OH_ArkUI_BarrierOption_Dispose(ArkUI_BarrierOption* barrierStyle);

/**
* @brief 设置屏障的Id。
*
* @param barrierStyle 屏障信息。
* @param value id，必须是唯一的并且不可与容器内组件重名。
* @param index 屏障索引值。
* @since 12
*/
void OH_ArkUI_BarrierOption_SetId(ArkUI_BarrierOption* barrierStyle, const char* value, int32_t index);

/**
* @brief 设置屏障的方向。
*
* @param barrierStyle 屏障信息。
* @param value 方向。
* @param index 屏障索引值。
* @since 12
*/
void OH_ArkUI_BarrierOption_SetDirection(ArkUI_BarrierOption* barrierStyle, ArkUI_BarrierDirection value, int32_t index);

/**
* @brief 设置屏障的依赖的组件。
*
* @param barrierStyle 屏障信息。
* @param value 依赖的组件的Id。
* @param index 屏障索引值。
* @since 12
*/
void OH_ArkUI_BarrierOption_SetReferencedId(ArkUI_BarrierOption* barrierStyle, const char* value, int32_t index);

/**
* @brief 获取屏障的Id。
*
* @param barrierStyle 辅助线信息。
* @param index 辅助线索引值。
* @return 屏障的Id。
* @since 12
*/
const char* OH_ArkUI_BarrierOption_GetId(ArkUI_BarrierOption* barrierStyle, int32_t index);

/**
* @brief 获取屏障的方向。
*
* @param barrierStyle 辅助线信息。
* @param index 辅助线索引值。
* @return 屏障的方向。
* @since 12
*/
ArkUI_BarrierDirection OH_ArkUI_BarrierOption_GetDirection(ArkUI_BarrierOption* barrierStyle, int32_t index);

/**
* @brief 获取屏障的依赖的组件。
*
* @param barrierStyle 辅助线信息。
* @param index 辅助线索引值。
* @param referencedIndex 依赖的组件Id索引值。
* @return 屏障的依赖的组件。
* @since 12
*/
const char* OH_ArkUI_BarrierOption_GetReferencedId(ArkUI_BarrierOption* barrierStyle, int32_t index , int32_t referencedIndex);

/**
* @brief 获取屏障的依赖的组件的个数。
*
* @param barrierStyle 辅助线信息。
* @param index 辅助线索引值。
* @return 屏障的依赖的组件的个数。
* @since 12
*/
int32_t OH_ArkUI_BarrierOption_GetReferencedIdSize(ArkUI_BarrierOption* barrierStyle, int32_t index);

/**
* @brief 创建相对容器中子组件的对齐规则信息。
*
* @return 对齐规则信息。
* @since 12
*/
ArkUI_AlignmentRuleOption* OH_ArkUI_AlignmentRuleOption_Create();

/**
* @brief 销毁相对容器中子组件的对齐规则信息。
*
* @param option 相对容器中子组件的对齐规则信息。
* @since 12
*/
void OH_ArkUI_AlignmentRuleOption_Dispose(ArkUI_AlignmentRuleOption* option);

/**
* @brief 设置左对齐参数。
*
* @param option 相对容器中子组件的对齐规则信息。
* @param id 锚点的组件的id值。
* @param value 相对于锚点组件的对齐方式。
* @since 12
*/
void OH_ArkUI_AlignmentRuleOption_SetStart(ArkUI_AlignmentRuleOption* option, const char* id, ArkUI_HorizontalAlignment alignment);

/**
* @brief 设置右对齐参数。
*
* @param option 相对容器中子组件的对齐规则信息。
* @param id 锚点的组件的id值。
* @param value 相对于锚点组件的对齐方式。
* @since 12
*/
void OH_ArkUI_AlignmentRuleOption_SetEnd(ArkUI_AlignmentRuleOption* option, const char* id, ArkUI_HorizontalAlignment alignment);

/**
* @brief 设置横向居中对齐方式的参数。
*
* @param option 相对容器中子组件的对齐规则信息。
* @param id 锚点的组件的id值。
* @param value 相对于锚点组件的对齐方式
* @since 12
*/
void OH_ArkUI_AlignmentRuleOption_SetCenterHorizontal(ArkUI_AlignmentRuleOption* option, const char* id, ArkUI_HorizontalAlignment alignment);

/**
* @brief 设置顶部对齐的参数。
*
* @param option 相对容器中子组件的对齐规则信息。
* @param id 锚点的组件的id值。
* @param value 相对于锚点组件的对齐方式
* @since 12
*/
void OH_ArkUI_AlignmentRuleOption_SetTop(ArkUI_AlignmentRuleOption* option, const char* id, ArkUI_VerticalAlignment alignment);

/**
* @brief 设置底部对齐的参数。
*
* @param option 相对容器中子组件的对齐规则信息。
* @param id 锚点的组件的id值。
* @param value 相对于锚点组件的对齐方式
* @since 12
*/
void OH_ArkUI_AlignmentRuleOption_SetBottom(ArkUI_AlignmentRuleOption* option, const char* id, ArkUI_VerticalAlignment alignment);

/**
* @brief 设置纵向居中对齐方式的参数。
*
* @param option 相对容器中子组件的对齐规则信息。
* @param id 锚点的组件的id值。
* @param value 相对于锚点组件的对齐方式。
* @since 12
*/
void OH_ArkUI_AlignmentRuleOption_SetCenterVertical(ArkUI_AlignmentRuleOption* option, const char* id, ArkUI_VerticalAlignment alignment);

/**
* @brief 设置组件在锚点约束下的水平方向上偏移参数。
*
* @param option 相对容器中子组件的对齐规则信息。
* @param horizontal 水平方向上的bias值。
* @since 12
*/
void OH_ArkUI_AlignmentRuleOption_SetBiasHorizontal(ArkUI_AlignmentRuleOption* option, float horizontal);

/**
* @brief 设置组件在锚点约束下的垂直方向上偏移参数。
*
* @param option 相对容器中子组件的对齐规则信息。
* @param horizontal 垂直方向上的bias值。
* @since 12
*/
void OH_ArkUI_AlignmentRuleOption_SetBiasVertical(ArkUI_AlignmentRuleOption* option, float vertical);

/**
* @brief 获取左对齐参数的Id。
*
* @param option 相对容器中子组件的对齐规则信息。
* @return 锚点的组件的id值。
* @since 12
*/
const char* OH_ArkUI_AlignmentRuleOption_GetStartId(ArkUI_AlignmentRuleOption* option);

/**
* @brief 获取左对齐参数的对齐方式。
*
* @param option 相对容器中子组件的对齐规则信息。
* @return 参数的对齐方式。
* @since 12
*/
ArkUI_HorizontalAlignment OH_ArkUI_AlignmentRuleOption_GetStartAlignment(ArkUI_AlignmentRuleOption* option);

/**
* @brief 获取右对齐参数。
*
* @param option 相对容器中子组件的对齐规则信息。
* @return 右对齐参数id。
* @since 12
*/
const char* OH_ArkUI_AlignmentRuleOption_GetEndId(ArkUI_AlignmentRuleOption* option);

/**
* @brief 获取右对齐参数。
*
* @param option 相对容器中子组件的对齐规则信息。
* @return 右对齐参数的对齐方式。
* @since 12
*/
ArkUI_HorizontalAlignment OH_ArkUI_AlignmentRuleOption_GetEndAlignment(ArkUI_AlignmentRuleOption* option);

/**
* @brief 获取横向居中对齐方式的参数。
*
* @param option 相对容器中子组件的对齐规则信息。
* @return 横向居中对齐方式的参数的id。
* @since 12
*/
const char* OH_ArkUI_AlignmentRuleOption_GetCenterIdHorizontal(ArkUI_AlignmentRuleOption* option);

/**
* @brief 获取横向居中对齐方式的参数。
*
* @param option 相对容器中子组件的对齐规则信息。
* @return 横向居中对齐方式的参数的对齐方式。
* @since 12
*/
ArkUI_HorizontalAlignment OH_ArkUI_AlignmentRuleOption_GetCenterAlignmentHorizontal(ArkUI_AlignmentRuleOption* option);

/**
* @brief 获取顶部对齐的参数。
*
* @param option 相对容器中子组件的对齐规则信息。
* @return 顶部对齐的参数id。
* @since 12
*/
const char* OH_ArkUI_AlignmentRuleOption_GetTopId(ArkUI_AlignmentRuleOption* option);

/**
* @brief 获取顶部对齐的参数。
*
* @param option 相对容器中子组件的对齐规则信息。
* @return 顶部对齐的参数的对齐方式。
* @since 12
*/
ArkUI_VerticalAlignment OH_ArkUI_AlignmentRuleOption_GetTopAlignment(ArkUI_AlignmentRuleOption* option);

/**
* @brief 获取底部对齐的参数。
*
* @param option 相对容器中子组件的对齐规则信息。
* @return 底部对齐的参数的id。
* @since 12
*/
const char* OH_ArkUI_AlignmentRuleOption_GetBottomId(ArkUI_AlignmentRuleOption* option);

/**
* @brief 获取底部对齐的参数。
*
* @param option 相对容器中子组件的对齐规则信息。
* @return 底部对齐的参数的对齐方式。
* @since 12
*/
ArkUI_VerticalAlignment OH_ArkUI_AlignmentRuleOption_GetBottomAlignment(ArkUI_AlignmentRuleOption* option);

/**
* @brief 获取纵向居中对齐方式的参数。
*
* @param option 相对容器中子组件的对齐规则信息。
* @return 纵向居中对齐方式的参数的id。
* @since 12
*/
const char* OH_ArkUI_AlignmentRuleOption_GetCenterIdVertical(ArkUI_AlignmentRuleOption* option);

/**
* @brief 获取纵向居中对齐方式的参数。
*
* @param option 相对容器中子组件的对齐规则信息。
* @return 纵向居中对齐方式的参数的对齐方式。
* @since 12
*/
ArkUI_VerticalAlignment OH_ArkUI_AlignmentRuleOption_GetCenterAlignmentVertical(ArkUI_AlignmentRuleOption* option);

/**
* @brief 获取水平方向上的bias值。
*
* @param option 相对容器中子组件的对齐规则信息。
* @return 水平方向上的bias值。
* @since 12
*/
float OH_ArkUI_AlignmentRuleOption_GetBiasHorizontal(ArkUI_AlignmentRuleOption* option);

/**
* @brief 获取垂直方向上的bias值。
*
* @param option 相对容器中子组件的对齐规则信息。
* @return 垂直方向上的bias值。
* @since 12
*/
float OH_ArkUI_AlignmentRuleOption_GetBiasVertical(ArkUI_AlignmentRuleOption* option);
/**
 * @brief 创建 Swiper 组件的导航指示器。
 *
 * @param type 导航指示器的类型。
 * @return 导航指示器对象指针。
 * @since 12
*/
ArkUI_SwiperIndicator* OH_ArkUI_SwiperIndicator_Create(ArkUI_SwiperIndicatorType type);

/**
 * @brief 销毁Swiper组件的导航指示器指针。
 *
 * @param indicator 导航指示器对象指针。
 * @since 12
*/
void OH_ArkUI_SwiperIndicator_Dispose(ArkUI_SwiperIndicator* indicator);

/**
 * @brief 设置导航点距离 Swiper 组件左边的距离。
 *
 * @param indicator 导航指示器对象指针。
 * @param value 导航点距离Swiper组件左边的距离。
 * @since 12
*/
void OH_ArkUI_SwiperIndicator_SetStartPosition(ArkUI_SwiperIndicator* indicator, float value);

/**
 * @brief 获取导航点距离 Swiper 组件左边的距离。
 *
 * @param indicator 导航指示器对象指针。
 * @return 导航点距离Swiper组件左边的距离。
 * @since 12
*/
float OH_ArkUI_SwiperIndicator_GetStartPosition(ArkUI_SwiperIndicator* indicator);

/**
 * @brief 设置导航点距离 Swiper 组件顶部的距离。
 *
 * @param indicator 导航指示器对象指针。
 * @param value 导航点距离Swiper组件顶部的距离。
 * @since 12
*/
void OH_ArkUI_SwiperIndicator_SetTopPosition(ArkUI_SwiperIndicator* indicator, float value);

/**
 * @brief 获取导航点距离 Swiper 组件顶部的距离。
 *
 * @param indicator 导航指示器对象指针。
 * @return 导航点距离Swiper组件顶部的距离。
 * @since 12
*/
float OH_ArkUI_SwiperIndicator_GetTopPosition(ArkUI_SwiperIndicator* indicator);

/**
 * @brief 设置导航点距离 Swiper 组件右边的距离。
 *
 * @param indicator 导航指示器对象指针。
 * @param value 导航点距离Swiper组件右边的距离。
 * @since 12
*/
void OH_ArkUI_SwiperIndicator_SetEndPosition(ArkUI_SwiperIndicator* indicator, float value);

/**
 * @brief 获取导航点距离 Swiper 组件右边的距离。
 *
 * @param indicator 导航指示器对象指针。
 * @return 导航点距离Swiper组件右边的距离。
 * @since 12
*/
float OH_ArkUI_SwiperIndicator_GetEndPosition(ArkUI_SwiperIndicator* indicator);

/**
 * @brief 设置导航点距离 Swiper 组件底部的距离。
 *
 * @param indicator 导航指示器对象指针。
 * @param value 导航点距离Swiper组件底部的距离。
 * @since 12
*/
void OH_ArkUI_SwiperIndicator_SetBottomPosition(ArkUI_SwiperIndicator* indicator, float value);

/**
 * @brief 获取导航点距离 Swiper 组件底部的距离。
 *
 * @param indicator 导航指示器对象指针。
 * @return 导航点距离Swiper组件底部的距离。
 * @since 12
*/
float OH_ArkUI_SwiperIndicator_GetBottomPosition(ArkUI_SwiperIndicator* indicator);

/**
 * @brief 设置 Swiper 组件圆点导航指示器的宽。
 *
 * @param indicator 导航指示器对象指针。
 * @param value 圆点导航指示器的宽。
 * @since 12
*/
void OH_ArkUI_SwiperIndicator_SetItemWidth(ArkUI_SwiperIndicator* indicator, float value);

/**
 * @brief 获取 Swiper 组件圆点导航指示器的宽。
 *
 * @param indicator 导航指示器对象指针。
 * @return 圆点导航指示器的宽。
 * @since 12
*/
float OH_ArkUI_SwiperIndicator_GetItemWidth(ArkUI_SwiperIndicator* indicator);

/**
 * @brief 设置 Swiper 组件圆点导航指示器的高。
 *
 * @param indicator 导航指示器对象指针。
 * @param value 圆点导航指示器的高。
 * @since 12
*/
void OH_ArkUI_SwiperIndicator_SetItemHeight(ArkUI_SwiperIndicator* indicator, float value);

/**
 * @brief 获取 Swiper 组件圆点导航指示器的高。
 *
 * @param indicator 导航指示器对象指针。
 * @return 圆点导航指示器的高。
 * @since 12
*/
float OH_ArkUI_SwiperIndicator_GetItemHeight(ArkUI_SwiperIndicator* indicator);

/**
 * @brief 设置被选中的 Swiper 组件圆点导航指示器的宽。
 *
 * @param indicator 导航指示器对象指针。
 * @param value 圆点导航指示器的宽。
 * @since 12
*/
void OH_ArkUI_SwiperIndicator_SetSelectedItemWidth(ArkUI_SwiperIndicator* indicator, float value);

/**
 * @brief 获取被选中 Swiper 组件圆点导航指示器的宽。
 *
 * @param indicator 导航指示器对象指针。
 * @return 圆点导航指示器的宽。
 * @since 12
*/
float OH_ArkUI_SwiperIndicator_GetSelectedItemWidth(ArkUI_SwiperIndicator* indicator);

/**
 * @brief 设置被选中的 Swiper 组件圆点导航指示器的高。
 *
 * @param indicator 导航指示器对象指针。
 * @param value 圆点导航指示器的高。
 * @since 12
*/
void OH_ArkUI_SwiperIndicator_SetSelectedItemHeight(ArkUI_SwiperIndicator* indicator, float value);

/**
 * @brief 获取被选中 Swiper 组件圆点导航指示器的高。
 *
 * @param indicator 导航指示器对象指针。
 * @return 圆点导航指示器的高。
 * @since 12
*/
float OH_ArkUI_SwiperIndicator_GetSelectedItemHeight(ArkUI_SwiperIndicator* indicator);

/**
 * @brief 设置是否显示 Swiper 组件圆点导航指示器的蒙版样式。 
 *
 * @param indicator 导航指示器对象指针。
 * @param mask 是否显示蒙版样式，1 表示显示，0 表示不显示。
 * @since 12
*/
void OH_ArkUI_SwiperIndicator_SetMask(ArkUI_SwiperIndicator* indicator, int32_t mask);

/**
 * @brief 获取是否显示 Swiper 组件圆点导航指示器的蒙版样式。 
 *
 * @param indicator 导航指示器对象指针。
 * @return mask 1 表示显示圆点导航指示器的蒙版样式，0 表示不显示。
 * @since 12
*/
int32_t OH_ArkUI_SwiperIndicator_GetMask(ArkUI_SwiperIndicator* indicator);

/**
 * @brief 设置 Swiper 组件圆点导航指示器的颜色。 
 *
 * @param indicator 导航指示器对象指针。
 * @param color 颜色类型，0xargb格式，形如 0xFFFF0000 表示红色。
 * @since 12
*/
void OH_ArkUI_SwiperIndicator_SetColor(ArkUI_SwiperIndicator* indicator, uint32_t color);

/**
 * @brief 获取 Swiper 组件圆点导航指示器的颜色。 
 *
 * @param indicator 导航指示器对象指针。
 * @return 颜色类型，0xargb格式，形如 0xFFFF0000 表示红色。
 * @since 12
*/
uint32_t OH_ArkUI_SwiperIndicator_GetColor(ArkUI_SwiperIndicator* indicator);

/**
 * @brief 设置被选中 Swiper 组件圆点导航指示器的颜色。 
 *
 * @param indicator 导航指示器对象指针。
 * @param selectedColor 颜色类型，0xargb格式，形如 0xFFFF0000 表示红色。
 * @since 12
*/
void OH_ArkUI_SwiperIndicator_SetSelectedColor(ArkUI_SwiperIndicator* indicator, uint32_t selectedColor);

/**
 * @brief 获取被选中 Swiper 组件圆点导航指示器的颜色。 
 *
 * @param indicator 导航指示器对象指针。
 * @return 颜色类型，0xargb格式，形如 0xFFFF0000 表示红色。
 * @since 12
*/
uint32_t OH_ArkUI_SwiperIndicator_GetSelectedColor(ArkUI_SwiperIndicator* indicator);

/**
 * @brief 设置圆点导航点指示器样式下，导航点显示个数的最大值。
 *
 * @param indicator 导航指示器对象指针。
 * @param maxDisplayCount 导航点显示个数最大值，有效取值范围6-9。
 * @return 错误码。
 *         {@link ARKUI_ERROR_CODE_NO_ERROR} 成功。
 *         {@link ARKUI_ERROR_CODE_PARAM_INVALID} 如果maxDisplayCount设置范围错误, 返回错误码。
 * @since 12
*/
int32_t OH_ArkUI_SwiperIndicator_SetMaxDisplayCount(ArkUI_SwiperIndicator* indicator, int32_t maxDisplayCount);

/**
 * @brief 获取圆点导航点指示器样式下，导航点显示个数的最大值。
 *
 * @param indicator 导航指示器对象指针。
 * @return 导航点显示个数最大值，有效取值范围6-9。
 * @since 12
*/
int32_t OH_ArkUI_SwiperIndicator_GetMaxDisplayCount(ArkUI_SwiperIndicator* indicator);

/**
 * @brief 创建ListItemSwipeActionItem接口设置的配置项。
 *
 * @return ListItemSwipeActionItem配置项实例。
 * @since 12
*/
ArkUI_ListItemSwipeActionItem* OH_ArkUI_ListItemSwipeActionItem_Create();

/**
* @brief 销毁ListItemSwipeActionItem实例。
*
* @param item 要销毁的ListItemSwipeActionItem实例。
* @since 12
*/
void OH_ArkUI_ListItemSwipeActionItem_Dispose(ArkUI_ListItemSwipeActionItem* item);

/**
* @brief 设置ListItemSwipeActionItem的布局内容。
*
* @param item ListItemSwipeActionItem实例。
* @param node 布局信息。
* @since 12
*/
void OH_ArkUI_ListItemSwipeActionItem_SetContent(ArkUI_ListItemSwipeActionItem* item, ArkUI_NodeHandle node);

/**
* @brief 设置组件长距离滑动删除距离阈值。
*
* @param item ListItemSwipeActionItem实例。
* @param distance 组件长距离滑动删除距离阈值。
* @since 12
*/
void OH_ArkUI_ListItemSwipeActionItem_SetActionAreaDistance(ArkUI_ListItemSwipeActionItem* item, float distance);

/**
* @brief 获取组件长距离滑动删除距离阈值。
*
* @param item ListItemSwipeActionItem实例。
* @return 组件长距离滑动删除距离阈值。异常时返回值：0。
* @since 12
*/
float OH_ArkUI_ListItemSwipeActionItem_GetActionAreaDistance(ArkUI_ListItemSwipeActionItem* item);

/**
* @brief 设置滑动条目进入删除区域时调用的事件。
*
* @param item ListItemSwipeActionItem实例。
* @param callback 回调事件
* @since 12
*/
void OH_ArkUI_ListItemSwipeActionItem_SetOnEnterActionArea(ArkUI_ListItemSwipeActionItem* item, void (*callback)());

/**
* @brief 设置滑动条目进入删除区域时调用的事件。
*
* @param item ListItemSwipeActionItem实例。
* @param userData 用户自定义数据。
* @param callback 回调事件
* @since 12
*/
void OH_ArkUI_ListItemSwipeActionItem_SetOnEnterActionAreaWithUserData(ArkUI_ListItemSwipeActionItem* item,
    void* userData, void (*callback)(void* userData));

/**
* @brief 设置组件进入长距删除区后删除ListItem时调用的事件。
*
* @param item ListItemSwipeActionItem实例。
* @param callback 回调事件
* @since 12
*/
void OH_ArkUI_ListItemSwipeActionItem_SetOnAction(ArkUI_ListItemSwipeActionItem* item, void (*callback)());

/**
* @brief 设置组件进入长距删除区后删除ListItem时调用的事件。
*
* @param item ListItemSwipeActionItem实例。
* @param userData 用户自定义数据。
* @param callback 回调事件
* @since 12
*/
void OH_ArkUI_ListItemSwipeActionItem_SetOnActionWithUserData(ArkUI_ListItemSwipeActionItem* item,
    void* userData, void (*callback)(void* userData));

/**
* @brief 设置滑动条目退出删除区域时调用的事件。
*
* @param item ListItemSwipeActionItem实例。
* @param callback 回调事件
* @since 12
*/
void OH_ArkUI_ListItemSwipeActionItem_SetOnExitActionArea(ArkUI_ListItemSwipeActionItem* item, void (*callback)());

/**
* @brief 设置滑动条目退出删除区域时调用的事件。
*
* @param item ListItemSwipeActionItem实例。
* @param userData 用户自定义数据。
* @param callback 回调事件
* @since 12
*/
void OH_ArkUI_ListItemSwipeActionItem_SetOnExitActionAreaWithUserData(ArkUI_ListItemSwipeActionItem* item,
    void* userData, void (*callback)(void* userData));

/**
* @brief 设置列表项滑动状态变化时候触发的事件。
*
* @param item ListItemSwipeActionItem实例。
* @param callback 回调事件
*        swipeActionState 变化后的状态。
* @since 12
*/
void OH_ArkUI_ListItemSwipeActionItem_SetOnStateChange(ArkUI_ListItemSwipeActionItem* item,
    void (*callback)(ArkUI_ListItemSwipeActionState swipeActionState));

/**
* @brief 设置列表项滑动状态变化时候触发的事件。
*
* @param item ListItemSwipeActionItem实例。
* @param userData 用户自定义数据。
* @param callback 回调事件
*        swipeActionState 变化后的状态。
* @since 12
*/
void OH_ArkUI_ListItemSwipeActionItem_SetOnStateChangeWithUserData(ArkUI_ListItemSwipeActionItem* item,
    void* userData, void (*callback)(ArkUI_ListItemSwipeActionState swipeActionState, void* userData));

/**
 * @brief 创建ListItemSwipeActionOption接口设置的配置项。
 *
 * @return ListItemSwipeActionOption配置项实例。
 * @since 12
*/
ArkUI_ListItemSwipeActionOption* OH_ArkUI_ListItemSwipeActionOption_Create();

/**
* @brief 销毁ListItemSwipeActionOption实例。
*
* @param option 要销毁的ListItemSwipeActionOption实例。
* @since 12
*/
void OH_ArkUI_ListItemSwipeActionOption_Dispose(ArkUI_ListItemSwipeActionOption* option);

/**
* @brief 设置ListItemSwipeActionItem的左侧（垂直布局）或上方（横向布局）布局内容。
*
* @param option ListItemSwipeActionItem实例。
* @param builder 布局信息。
* @since 12
*/
void OH_ArkUI_ListItemSwipeActionOption_SetStart(ArkUI_ListItemSwipeActionOption* option, ArkUI_ListItemSwipeActionItem* item);

/**
* @brief 设置ListItemSwipeActionItem的右侧（垂直布局）或下方（横向布局）布局内容。
*
* @param option ListItemSwipeActionItem实例。
* @param builder 布局信息。
* @since 12
*/
void OH_ArkUI_ListItemSwipeActionOption_SetEnd(ArkUI_ListItemSwipeActionOption* option,
    ArkUI_ListItemSwipeActionItem* item);

/**
* @brief 设置滑动效果。
*
* @param option ListItemSwipeActionItem实例。
* @param edgeEffect 滑动效果。
* @since 12
*/
void OH_ArkUI_ListItemSwipeActionOption_SetEdgeEffect(ArkUI_ListItemSwipeActionOption* option,
    ArkUI_ListItemSwipeEdgeEffect edgeEffect);

/**
* @brief 获取滑动效果。
*
* @param option ListItemSwipeActionItem实例。
* @return 滑动效果。默认返回值：ARKUI_LIST_ITEM_SWIPE_EDGE_EFFECT_SPRING。
* @since 12
*/
int32_t OH_ArkUI_ListItemSwipeActionOption_GetEdgeEffect(ArkUI_ListItemSwipeActionOption* option);

/**
* @brief 滑动操作偏移量更改时调用的事件。
*
* @param option ListItemSwipeActionItem实例。
* @param callback 回调事件
*        offset 滑动偏移量。
* @since 12
*/
void OH_ArkUI_ListItemSwipeActionOption_SetOnOffsetChange(ArkUI_ListItemSwipeActionOption* option,
    void (*callback)(float offset));

/**
* @brief 滑动操作偏移量更改时调用的事件。
*
* @param option ListItemSwipeActionItem实例。
* @param userData 用户自定义数据。
* @param callback 回调事件
*        offset 滑动偏移量。
* @since 12
*/
void OH_ArkUI_ListItemSwipeActionOption_SetOnOffsetChangeWithUserData(ArkUI_ListItemSwipeActionOption* option,
    void* userData, void (*callback)(float offset, void* userData));

/**
 * @brief 创建无障碍状态。
 *
 * @return 无障碍状态对象指针。如果对象返回空指针，表示创建失败，失败的可能原因是应用地址空间满。
 * @since 12
*/
ArkUI_AccessibilityState* OH_ArkUI_AccessibilityState_Create(void);

/**
* @brief 销毁无障碍状态指针。
*
* @param state 无障碍状态对象指针。
* @since 12
*/
void OH_ArkUI_AccessibilityState_Dispose(ArkUI_AccessibilityState* state);

/**
 * @brief 设置无障碍状态是否禁用。
 *
 * @param state 无障碍状态对象指针。
 * @param isDisabled 无障碍状态是否禁用， 1表示禁用，0表示不禁用，默认为0。
 * @since 12
*/
void OH_ArkUI_AccessibilityState_SetDisabled(ArkUI_AccessibilityState* state, int32_t isDisabled);

/**
 * @brief 获取无障碍状态是否禁用。
 *
 * @param state 无障碍状态对象指针。
 * @return 是否禁用， 1表示禁用，0表示未禁用，默认为0;
 *         若state为空，返回默认值。
 * @since 12
*/
int32_t OH_ArkUI_AccessibilityState_IsDisabled(ArkUI_AccessibilityState* state);

/**
 * @brief 设置无障碍状态是否选中。
 *
 * @param state 无障碍状态对象指针。
 * @param isSelected 是否被选中， 1表示选中，0表示未选中，默认为0。
 * @since 12
*/
void OH_ArkUI_AccessibilityState_SetSelected(ArkUI_AccessibilityState* state, int32_t isSelected);

/**
 * @brief 获取无障碍状态是否选中。
 *
 * @param state 无障碍状态对象指针。
 * @return 是否被选中， 1表示选中，0表示未选中，默认为0;
 *         若state为空，返回默认值。
 * @since 12
*/
int32_t OH_ArkUI_AccessibilityState_IsSelected(ArkUI_AccessibilityState* state);

/**
 * @brief 设置无障碍状态复选框状态。
 *
 * @param state 无障碍状态对象指针。
 * @param checkedState 复选框状态，参数类型{@link ArkUI_AccessibilityCheckedState}, 默认值：ARKUI_ACCESSIBILITY_UNCHECKED。
 * @since 12
*/
void OH_ArkUI_AccessibilityState_SetCheckedState(ArkUI_AccessibilityState* state, int32_t checkedState);

/**
 * @brief 获取无障碍状态复选框状态。
 *
 * @param state 无障碍状态对象指针。
 * @return 复选框状态，参数类型{@link ArkUI_AccessibilityCheckedState}, 默认值：ARKUI_ACCESSIBILITY_UNCHECKED;
 *         若函数参数异常，返回默认值。
 * @since 12
*/
int32_t OH_ArkUI_AccessibilityState_GetCheckedState(ArkUI_AccessibilityState* state);

/**
 * @brief 创建无障碍信息。
 *
 * @return 无障碍信息对象指针。
 * @since 12
*/
ArkUI_AccessibilityValue* OH_ArkUI_AccessibilityValue_Create(void);

/**
* @brief 销毁无障碍信息指针。
*
* @param state 无障碍信息对象指针。
* @since 12
*/
void OH_ArkUI_AccessibilityValue_Dispose(ArkUI_AccessibilityValue* value);

/**
 * @brief 设置无障碍最小值信息。
 *
 * @param value 无障碍信息对象指针。
 * @param min 基于范围组件的最小值, 默认为-1。
 * @since 12
*/
void OH_ArkUI_AccessibilityValue_SetMin(ArkUI_AccessibilityValue* value, int32_t min);

/**
 * @brief 获取无障碍最小值信息。
 *
 * @param value 无障碍信息对象指针。
 * @return 基于范围组件的最小值, 默认为-1;
 *         若函数参数异常，返回-1。
 * @since 12
*/
int32_t OH_ArkUI_AccessibilityValue_GetMin(ArkUI_AccessibilityValue* value);

/**
 * @brief 设置无障碍最大值信息。
 *
 * @param value 无障碍信息对象指针。
 * @param max 基于范围组件的最大值, 默认为-1。
 * @since 12
*/
void OH_ArkUI_AccessibilityValue_SetMax(ArkUI_AccessibilityValue* value, int32_t max);

/**
 * @brief 获取无障碍最大值信息。
 *
 * @param value 无障碍信息对象指针。
 * @return 基于范围组件的最大值, 默认为-1;
 *         若函数参数异常，返回-1。
 * @since 12
*/
int32_t OH_ArkUI_AccessibilityValue_GetMax(ArkUI_AccessibilityValue* value);

/**
 * @brief 设置无障碍当前值信息。
 *
 * @param value 无障碍信息对象指针。
 * @param current 基于范围组件的当前值, 默认为-1。
 * @since 12
*/
void OH_ArkUI_AccessibilityValue_SetCurrent(ArkUI_AccessibilityValue* value, int32_t current);

/**
 * @brief 获取无障碍当前值信息。
 *
 * 
 * @param value 无障碍信息对象指针。
 * @return 基于范围组件的当前值, 默认为-1;
 *         若函数参数异常，返回-1。
 * @since 12
*/
int32_t OH_ArkUI_AccessibilityValue_GetCurrent(ArkUI_AccessibilityValue* value);

/**
 * @brief 设置无障碍文本描述信息。
 *
 * @param value 无障碍信息对象指针。
 * @param text 组件的文本描述信息, 默认为空字符串。
 * @since 12
*/
void OH_ArkUI_AccessibilityValue_SetText(ArkUI_AccessibilityValue* value, const char* text);

/**
 * @brief 获取无障碍文本描述信息。
 *
 * 
 * @param value 无障碍信息对象指针。
 * @return 组件的文本描述信息, 默认为空字符串;
 *         若函数参数异常，返回空指针。
 * @since 12
*/
const char* OH_ArkUI_AccessibilityValue_GetText(ArkUI_AccessibilityValue* value);

/**
 * @brief 使用图片路径创建帧图片信息，图片格式为svg，png和jpg。
 *
 * @param src 图片路径。
 * @return 帧图片对象指针。
 * @since 12
*/
ArkUI_ImageAnimatorFrameInfo* OH_ArkUI_ImageAnimatorFrameInfo_CreateFromString(char* src);

/**
 * @brief 使用 DrawableDescriptor 对象创建帧图片信息，图片格式为Resource和PixelMap。
 *
 * @param drawable 使用Resource或PixelMap创建的ArkUI_DrawableDescriptor对象指针。
 * @return 帧图片对象指针。
 * @since 12
*/
ArkUI_ImageAnimatorFrameInfo* OH_ArkUI_ImageAnimatorFrameInfo_CreateFromDrawableDescriptor(
    ArkUI_DrawableDescriptor* drawable);

/**
 * @brief 销毁帧图片对象指针。
 *
 * @param imageInfo 帧图片对象指针。
 * @since 12
*/
void OH_ArkUI_ImageAnimatorFrameInfo_Dispose(ArkUI_ImageAnimatorFrameInfo* imageInfo);

/**
 * @brief 设置图片宽度。
 *
 * @param imageInfo 帧图片对象指针。
 * @param width 图片宽度，单位为PX。
 * @since 12
*/
void OH_ArkUI_ImageAnimatorFrameInfo_SetWidth(ArkUI_ImageAnimatorFrameInfo* imageInfo, int32_t width);

/**
 * @brief 获取图片宽度。
 *
 * @param imageInfo 帧图片对象指针。
 * @return 图片宽度，单位为PX，imageInfo为空指针时返回0。
 * @since 12
*/
int32_t OH_ArkUI_ImageAnimatorFrameInfo_GetWidth(ArkUI_ImageAnimatorFrameInfo* imageInfo);

/**
 * @brief 设置图片高度。
 *
 * @param imageInfo 帧图片对象指针。
 * @param height 图片高度，单位为PX。
 * @since 12
*/
void OH_ArkUI_ImageAnimatorFrameInfo_SetHeight(ArkUI_ImageAnimatorFrameInfo* imageInfo, int32_t height);

/**
 * @brief 获取图片高度。
 *
 * @param imageInfo 帧图片对象指针。
 * @return 图片高度，单位为PX，imageInfo为空指针时返回0。
 * @since 12
*/
int32_t OH_ArkUI_ImageAnimatorFrameInfo_GetHeight(ArkUI_ImageAnimatorFrameInfo* imageInfo);

/**
 * @brief 设置图片相对于组件左上角的纵向坐标。
 *
 * @param imageInfo 帧图片对象指针。
 * @param top 图片相对于组件左上角的纵向坐标，单位为PX。
 * @since 12
*/
void OH_ArkUI_ImageAnimatorFrameInfo_SetTop(ArkUI_ImageAnimatorFrameInfo* imageInfo, int32_t top);

/**
 * @brief 获取图片相对于组件左上角的纵向坐标。
 *
 * @param imageInfo 帧图片对象指针。
 * @return 图片相对于组件左上角的纵向坐标，单位为PX，imageInfo为空指针时返回0。
 * @since 12
*/
int32_t OH_ArkUI_ImageAnimatorFrameInfo_GetTop(ArkUI_ImageAnimatorFrameInfo* imageInfo);

/**
 * @brief 设置图片相对于组件左上角的横向坐标。
 *
 * @param imageInfo 帧图片对象指针。
 * @param left 图片相对于组件左上角的横向坐标，单位为PX。
 * @since 12
*/
void OH_ArkUI_ImageAnimatorFrameInfo_SetLeft(ArkUI_ImageAnimatorFrameInfo* imageInfo, int32_t left);

/**
 * @brief 获取图片相对于组件左上角的横向坐标。
 *
 * @param imageInfo 帧图片对象指针。
 * @return 图片相对于组件左上角的横向坐标，单位为PX，imageInfo为空指针时返回0。
 * @since 12
*/
int32_t OH_ArkUI_ImageAnimatorFrameInfo_GetLeft(ArkUI_ImageAnimatorFrameInfo* imageInfo);

/**
 * @brief 设置图片的播放时长。
 *
 * @param imageInfo 帧图片对象指针。
 * @param duration 图片的播放时长，单位为毫秒。
 * @since 12
*/
void OH_ArkUI_ImageAnimatorFrameInfo_SetDuration(ArkUI_ImageAnimatorFrameInfo* imageInfo, int32_t duration);

/**
 * @brief 获取图片的播放时长。
 *
 * @param imageInfo 帧图片对象指针。
 * @return 图片的播放时长，单位为毫秒，imageInfo为空指针时返回0。
 * @since 12
*/
int32_t OH_ArkUI_ImageAnimatorFrameInfo_GetDuration(ArkUI_ImageAnimatorFrameInfo* imageInfo);

/**
 * @brief 创建ListChildrenMainSize接口设置的配置项。
 *
 * @return ListChildrenMainSize配置项实例。
 * @since 12
*/
ArkUI_ListChildrenMainSize* OH_ArkUI_ListChildrenMainSizeOption_Create();

/**
 * @brief 销毁ListChildrenMainSize实例。
 *
 * @param option 要销毁的ListChildrenMainSize实例。
 * @since 12
*/
void OH_ArkUI_ListChildrenMainSizeOption_Dispose(ArkUI_ListChildrenMainSize* option);

/**
 * @brief 设置List组件的ChildrenMainSizeOption默认大小。
 *
 * @param option ListChildrenMainSize实例。
 * @param defaultMainSize List下的ListItem的默认大小，单位为vp。
 * @return 错误码。
 *         {@link ARKUI_ERROR_CODE_NO_ERROR} 成功。
 *         {@link ARKUI_ERROR_CODE_PARAM_INVALID} 函数参数异常。
 * @since 12
*/
int32_t OH_ArkUI_ListChildrenMainSizeOption_SetDefaultMainSize(ArkUI_ListChildrenMainSize* option, float defaultMainSize);

/**
 * @brief 获取List组件的ChildrenMainSizeOption默认大小。
 *
 * @param option ListChildrenMainSize实例。
 * @return List下的ListItem的默认大小，默认为0，单位为vp，option为空指针时返回-1。
 * @since 12
*/
float OH_ArkUI_ListChildrenMainSizeOption_GetDefaultMainSize(ArkUI_ListChildrenMainSize* option);

/**
 * @brief 重置List组件的ChildrenMainSizeOption的数组大小。
 *
 * @param option ListChildrenMainSize实例。
 * @param totalSize 数组大小。
 * @since 12
*/
void OH_ArkUI_ListChildrenMainSizeOption_Resize(ArkUI_ListChildrenMainSize* option, int32_t totalSize);

/**
 * @brief 对List组件的ChildrenMainSizeOption数组操作大小调整。
 *
 * @param option ListChildrenMainSize实例。
 * @param index 要修改MainSize的数组起始位置。
 * @param deleteCount 要删除的MainSize数组从index开始的数量。
 * @param addCount 要添加的MainSize数组从index开始的数量。
 * @return 错误码。
 *         {@link ARKUI_ERROR_CODE_NO_ERROR} 成功。
 *         {@link ARKUI_ERROR_CODE_PARAM_INVALID} 函数参数异常。
 * @since 12
*/
int32_t OH_ArkUI_ListChildrenMainSizeOption_Splice(ArkUI_ListChildrenMainSize* option, int32_t index, int32_t deleteCount, int32_t addCount);

/**
 * @brief 更新List组件的ChildrenMainSizeOption数组的值。
 *
 * @param option ListChildrenMainSize实例。
 * @param index 要修改MainSize的数组起始位置。
 * @param mainSize 实际修改的值。
 * @return 错误码。
 *         {@link ARKUI_ERROR_CODE_NO_ERROR} 成功。
 *         {@link ARKUI_ERROR_CODE_PARAM_INVALID} 函数参数异常。
 * @since 12
*/
int32_t OH_ArkUI_ListChildrenMainSizeOption_UpdateSize(ArkUI_ListChildrenMainSize* option, int32_t index, float mainSize);

/**
 * @brief 获取List组件的ChildrenMainSizeOption数组的值。
 *
 * @param option ListChildrenMainSize实例。
 * @param index 要获取的值的下标位置。
 * @return 数组具体位置的值。若函数参数异常，返回-1。
 * @since 12
*/
float OH_ArkUI_ListChildrenMainSizeOption_GetMainSize(ArkUI_ListChildrenMainSize* option, int32_t index);

/**
 * @brief 创建自定义段落组件测量信息。
 *
 * @return CustomSpanMeasureInfo实例。
 * 如果返回空指针，可能是因为内存不足。
 * @since 12
*/
ArkUI_CustomSpanMeasureInfo* OH_ArkUI_CustomSpanMeasureInfo_Create(void);

/**
 * @brief 销毁自定义段落组件测量信息。
 *
 * @param info  自定义段落组件测量信息指针。
 * @since 12
*/
void OH_ArkUI_CustomSpanMeasureInfo_Dispose(ArkUI_CustomSpanMeasureInfo* info);

/**
 * @brief 获取自定义段落组件的父节点Text的字体大小。
 *
 * @param info  自定义段落组件测量信息指针。
 * @return 父节点Text的字体大小。若函数参数异常，返回0.0f。
 * 异常返回原因：传入参数验证失败，参数不能为空。
 * @since 12
*/
float OH_ArkUI_CustomSpanMeasureInfo_GetFontSize(ArkUI_CustomSpanMeasureInfo* info);

/**
 * @brief 创建自定义段落组件度量信息。
 *
 * @return CustomSpanMetrics实例。
 * 如果返回空指针，可能是因为内存不足。
 * @since 12
*/
ArkUI_CustomSpanMetrics* OH_ArkUI_CustomSpanMetrics_Create(void);

/**
 * @brief 销毁自定义段落组件度量信息。
 *
 * @param metrics CustomSpanMetrics实例。
 * @since 12
*/
void OH_ArkUI_CustomSpanMetrics_Dispose(ArkUI_CustomSpanMetrics* metrics);

/**
 * @brief 设置自定义段落组件的宽度。
 *
 * @param metrics CustomSpanMetrics实例。
 * @param width 宽度大小，单位为vp。
 * @return 错误码。
 *         {@link ARKUI_ERROR_CODE_NO_ERROR} 成功。
 *         {@link ARKUI_ERROR_CODE_PARAM_INVALID} 函数参数异常。
 *         异常原因：传入参数验证失败，参数不能为空。
 * @since 12
*/
int32_t OH_ArkUI_CustomSpanMetrics_SetWidth(ArkUI_CustomSpanMetrics* metrics, float width);

/**
 * @brief 设置自定义段落组件的高度。
 *
 * @param metrics CustomSpanMetrics实例。
 * @param height 高度大小，单位为vp。
 * @return 错误码。
 *         {@link ARKUI_ERROR_CODE_NO_ERROR} 成功。
 *         {@link ARKUI_ERROR_CODE_PARAM_INVALID} 函数参数异常。
 *         异常原因：传入参数验证失败，参数不能为空。
 * @since 12
*/
int32_t OH_ArkUI_CustomSpanMetrics_SetHeight(ArkUI_CustomSpanMetrics* metrics, float height);

/**
 * @brief 创建自定义段落组件绘制信息。
 *
 * @return CustomSpanDrawInfo实例。
 * 如果返回空指针，可能是因为内存不足。
 * @since 12
*/
ArkUI_CustomSpanDrawInfo* OH_ArkUI_CustomSpanDrawInfo_Create(void);

/**
 * @brief 销毁自定义段落组件绘制信息。
 *
 * @param info  自定义段落组件绘制信息指针。
 * @since 12
*/
void OH_ArkUI_CustomSpanDrawInfo_Dispose(ArkUI_CustomSpanDrawInfo* info);

/**
 * @brief 获取自定义段落组件相对于挂载组件的x轴偏移值。
 *
 * @param info  自定义段落组件绘制信息指针。
 * @return x轴偏移值。若函数参数异常，返回0.0f。
 * 异常返回原因：传入参数验证失败，参数不能为空。
 * @since 12
*/
float OH_ArkUI_CustomSpanDrawInfo_GetXOffset(ArkUI_CustomSpanDrawInfo* info);

/**
 * @brief 获取自定义段落组件相对于挂载组件的上边距。
 *
 * @param info  自定义段落组件绘制信息指针。
 * @return 上边距值。若函数参数异常，返回0.0f。
 * 异常返回原因：传入参数验证失败，参数不能为空。
 * @since 12
*/
float OH_ArkUI_CustomSpanDrawInfo_GetLineTop(ArkUI_CustomSpanDrawInfo* info);

/**
 * @brief 获取自定义段落组件相对于挂载组件的下边距。
 *
 * @param info  自定义段落组件绘制信息指针。
 * @return 下边距值。若函数参数异常，返回0.0f。
 * 异常返回原因：传入参数验证失败，参数不能为空。
 * @since 12
*/
float OH_ArkUI_CustomSpanDrawInfo_GetLineBottom(ArkUI_CustomSpanDrawInfo* info);

/**
 * @brief 获取自定义段落组件相对于挂载组件的基线偏移量。
 *
 * @param info  自定义段落组件绘制信息指针。
 * @return 基线偏移量值。若函数参数异常，返回0.0f。
 * 异常返回原因：传入参数验证失败，参数不能为空。
 * @since 12
*/
float OH_ArkUI_CustomSpanDrawInfo_GetBaseline(ArkUI_CustomSpanDrawInfo* info);
#ifdef __cplusplus
};
#endif

#endif // ARKUI_NATIVE_TYPE_H
/** @} */
