/*
 * Copyright (c) 2024 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup ArkUI_NativeModule
 * @{
 *
 * @brief 提供ArkUI在Native侧的通用按键事件能力。
 *
 * @since 14
 */

/**
 * @file native_key_event.h
 *
 * @brief 提供NativeKeyEvent相关接口定义。
 *
 * @library libace_ndk.z.so
 * @syscap SystemCapability.ArkUI.ArkUI.Full
 * @kit ArkUI
 * @since 14
 */

#ifndef ARKUI_NATIVE_KEY_EVENT_H
#define ARKUI_NATIVE_KEY_EVENT_H

#include <stdint.h>

#include "native_type.h"
#include "ui_input_event.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief 按键事件的键码
 *
 * @since 14
 */
typedef enum {
    /** 未知按键 **/
    ARKUI_KEYCODE_KEY_UNKNOWN = -1,
    /** 功能（Fn）键 **/
    ARKUI_KEYCODE_KEY_FN = 0,
    /** 功能（Home）键 **/
    ARKUI_KEYCODE_KEY_HOME = 1,
    /** 返回键 **/
    ARKUI_KEYCODE_KEY_BACK = 2,
    /** 多媒体键 播放/暂停 **/
    ARKUI_KEYCODE_KEY_MEDIA_PLAY_PAUSE = 10,
    /** 多媒体键 停止 **/
    ARKUI_KEYCODE_KEY_MEDIA_STOP = 11,
    /** 多媒体键 下一首 **/
    ARKUI_KEYCODE_KEY_MEDIA_NEXT = 12,
    /** 多媒体键 上一首 **/
    ARKUI_KEYCODE_KEY_MEDIA_PREVIOUS = 13,
    /** 多媒体键 快退 **/
    ARKUI_KEYCODE_KEY_MEDIA_REWIND = 14,
    /** 多媒体键 快进 **/
    ARKUI_KEYCODE_KEY_MEDIA_FAST_FORWARD = 15,
    /** 音量增加键 **/
    ARKUI_KEYCODE_KEY_VOLUME_UP = 16,
    /** 音量减小键 **/
    ARKUI_KEYCODE_KEY_VOLUME_DOWN = 17,
    /** 电源键 **/
    ARKUI_KEYCODE_KEY_POWER = 18,
    /** 拍照键 **/
    ARKUI_KEYCODE_KEY_CAMERA = 19,
    /** 扬声器静音键 **/
    ARKUI_KEYCODE_KEY_VOLUME_MUTE = 22,

    /** 话筒静音键 **/
    ARKUI_KEYCODE_KEY_MUTE = 23,
    /** 亮度调节按键 调亮 **/
    ARKUI_KEYCODE_KEY_BRIGHTNESS_UP = 40,
    /** 亮度调节按键 调暗 **/
    ARKUI_KEYCODE_KEY_BRIGHTNESS_DOWN = 41,
    /** 按键'0' **/
    ARKUI_KEYCODE_KEY_0 = 2000,
    /** 按键'1' **/
    ARKUI_KEYCODE_KEY_1 = 2001,
    /** 按键'2' **/
    ARKUI_KEYCODE_KEY_2 = 2002,
    /** 按键'3' **/
    ARKUI_KEYCODE_KEY_3 = 2003,
    /** 按键'4' **/
    ARKUI_KEYCODE_KEY_4 = 2004,
    /** 按键'5' **/
    ARKUI_KEYCODE_KEY_5 = 2005,
    /** 按键'6' **/
    ARKUI_KEYCODE_KEY_6 = 2006,
    /** 按键'7' **/
    ARKUI_KEYCODE_KEY_7 = 2007,
    /** 按键'8' **/
    ARKUI_KEYCODE_KEY_8 = 2008,
    /** 按键'9' **/
    ARKUI_KEYCODE_KEY_9 = 2009,
    /** 按键'*' **/
    ARKUI_KEYCODE_KEY_STAR = 2010,
    /** 按键'#' **/
    ARKUI_KEYCODE_KEY_POUND = 2011,
    /** 导航键 向上 **/
    ARKUI_KEYCODE_KEY_DPAD_UP = 2012,
    /** 导航键 向下 **/
    ARKUI_KEYCODE_KEY_DPAD_DOWN = 2013,
    /** 导航键 向左 **/
    ARKUI_KEYCODE_KEY_DPAD_LEFT = 2014,
    /** 导航键 向右 **/
    ARKUI_KEYCODE_KEY_DPAD_RIGHT = 2015,
    /** 导航键 确定键 **/
    ARKUI_KEYCODE_KEY_DPAD_CENTER = 2016,
    /** 按键'A' **/
    ARKUI_KEYCODE_KEY_A = 2017,
    /** 按键'B' **/
    ARKUI_KEYCODE_KEY_B = 2018,
    /** 按键'C' **/
    ARKUI_KEYCODE_KEY_C = 2019,
    /** 按键'D' **/
    ARKUI_KEYCODE_KEY_D = 2020,
    /** 按键'E' **/
    ARKUI_KEYCODE_KEY_E = 2021,
    /** 按键'F' **/
    ARKUI_KEYCODE_KEY_F = 2022,
    /** 按键'G' **/
    ARKUI_KEYCODE_KEY_G = 2023,
    /** 按键'H' **/
    ARKUI_KEYCODE_KEY_H = 2024,
    /** 按键'I' **/
    ARKUI_KEYCODE_KEY_I = 2025,
    /** 按键'J' **/
    ARKUI_KEYCODE_KEY_J = 2026,
    /** 按键'K' **/
    ARKUI_KEYCODE_KEY_K = 2027,
    /** 按键'L' **/
    ARKUI_KEYCODE_KEY_L = 2028,
    /** 按键'M' **/
    ARKUI_KEYCODE_KEY_M = 2029,
    /** 按键'N' **/
    ARKUI_KEYCODE_KEY_N = 2030,
    /** 按键'O' **/
    ARKUI_KEYCODE_KEY_O = 2031,
    /** 按键'P' **/
    ARKUI_KEYCODE_KEY_P = 2032,
    /** 按键'R' **/
    ARKUI_KEYCODE_KEY_Q = 2033,
    /** 按键'R' **/
    ARKUI_KEYCODE_KEY_R = 2034,
    /** 按键'S' **/
    ARKUI_KEYCODE_KEY_S = 2035,
    /** 按键'T' **/
    ARKUI_KEYCODE_KEY_T = 2036,
    /** 按键'U' **/
    ARKUI_KEYCODE_KEY_U = 2037,
    /** 按键'V' **/
    ARKUI_KEYCODE_KEY_V = 2038,
    /** 按键'W' **/
    ARKUI_KEYCODE_KEY_W = 2039,
    /** 按键'X' **/
    ARKUI_KEYCODE_KEY_X = 2040,
    /** 按键'Y' **/
    ARKUI_KEYCODE_KEY_Y = 2041,
    /** 按键'Z' **/
    ARKUI_KEYCODE_KEY_Z = 2042,
    /** 按键',' **/
    ARKUI_KEYCODE_KEY_COMMA = 2043,
    /** 按键'.' **/
    ARKUI_KEYCODE_KEY_PERIOD = 2044,
    /** 左Alt键 **/
    ARKUI_KEYCODE_KEY_ALT_LEFT = 2045,
    /** 右Alt键 **/
    ARKUI_KEYCODE_KEY_ALT_RIGHT = 2046,
    /** 左Shift键 **/
    ARKUI_KEYCODE_KEY_SHIFT_LEFT = 2047,
    /** 右Shift键 **/
    ARKUI_KEYCODE_KEY_SHIFT_RIGHT = 2048,
    /** Tab键 **/
    ARKUI_KEYCODE_KEY_TAB = 2049,
    /** 空格键 **/
    ARKUI_KEYCODE_KEY_SPACE = 2050,
    /** 符号修改器按键 **/
    ARKUI_KEYCODE_KEY_SYM = 2051,
    /** 浏览器功能键，此键用于启动浏览器应用程序。 **/
    ARKUI_KEYCODE_KEY_EXPLORER = 2052,
    /** 电子邮件功能键，此键用于启动电子邮件应用程序。 **/
    ARKUI_KEYCODE_KEY_ENVELOPE = 2053,
    /** 回车键 **/
    ARKUI_KEYCODE_KEY_ENTER = 2054,
    /** 退格键 **/
    ARKUI_KEYCODE_KEY_DEL = 2055,
    /** 按键'`' **/
    ARKUI_KEYCODE_KEY_GRAVE = 2056,
    /** 按键'-' **/
    ARKUI_KEYCODE_KEY_MINUS = 2057,
    /** 按键'=' **/
    ARKUI_KEYCODE_KEY_EQUALS = 2058,
    /** 按键'[' **/
    ARKUI_KEYCODE_KEY_LEFT_BRACKET = 2059,
    /** 按键']' **/
    ARKUI_KEYCODE_KEY_RIGHT_BRACKET = 2060,
    /** 按键'\\' **/
    ARKUI_KEYCODE_KEY_BACKSLASH = 2061,
    /** 按键';' **/
    ARKUI_KEYCODE_KEY_SEMICOLON = 2062,
    /** 按键''' (单引号) **/
    ARKUI_KEYCODE_KEY_APOSTROPHE = 2063,
    /** 按键'/' **/
    ARKUI_KEYCODE_KEY_SLASH = 2064,
    /** 按键'@' **/
    ARKUI_KEYCODE_KEY_AT = 2065,
    /** 按键'+' **/
    ARKUI_KEYCODE_KEY_PLUS = 2066,
    /** 菜单键 **/
    ARKUI_KEYCODE_KEY_MENU = 2067,
    /** 向上翻页键 **/
    ARKUI_KEYCODE_KEY_PAGE_UP = 2068,
    /** 向下翻页键 **/
    ARKUI_KEYCODE_KEY_PAGE_DOWN = 2069,
    /** ESC键 **/
    ARKUI_KEYCODE_KEY_ESCAPE = 2070,
    /** 删除键 **/
    ARKUI_KEYCODE_KEY_FORWARD_DEL = 2071,
    /** 左Ctrl键 **/
    ARKUI_KEYCODE_KEY_CTRL_LEFT = 2072,
    /** 右Ctrl键 **/
    ARKUI_KEYCODE_KEY_CTRL_RIGHT = 2073,
    /** 大写锁定键 **/
    ARKUI_KEYCODE_KEY_CAPS_LOCK = 2074,
    /** 滚动锁定键 **/
    ARKUI_KEYCODE_KEY_SCROLL_LOCK = 2075,
    /** 左元修改器键 **/
    ARKUI_KEYCODE_KEY_META_LEFT = 2076,
    /** 右元修改器键 **/
    ARKUI_KEYCODE_KEY_META_RIGHT = 2077,
    /** 功能键 **/
    ARKUI_KEYCODE_KEY_FUNCTION = 2078,
    /** 系统请求/打印屏幕键 **/
    ARKUI_KEYCODE_KEY_SYSRQ = 2079,
    /** Break/Pause键 **/
    ARKUI_KEYCODE_KEY_BREAK = 2080,
    /** 光标移动到开始键 **/
    ARKUI_KEYCODE_KEY_MOVE_HOME = 2081,
    /** 光标移动到末尾键 **/
    ARKUI_KEYCODE_KEY_MOVE_END = 2082,
    /** 插入键 **/
    ARKUI_KEYCODE_KEY_INSERT = 2083,
    /** 前进键 **/
    ARKUI_KEYCODE_KEY_FORWARD = 2084,
    /** 多媒体键 播放 **/
    ARKUI_KEYCODE_KEY_MEDIA_PLAY = 2085,
    /** 多媒体键 暂停 **/
    ARKUI_KEYCODE_KEY_MEDIA_PAUSE = 2086,
    /** 多媒体键 关闭 **/
    ARKUI_KEYCODE_KEY_MEDIA_CLOSE = 2087,
    /** 多媒体键 弹出 **/
    ARKUI_KEYCODE_KEY_MEDIA_EJECT = 2088,
    /** 多媒体键 录音 **/
    ARKUI_KEYCODE_KEY_MEDIA_RECORD = 2089,
    /** 按键'F1' **/
    ARKUI_KEYCODE_KEY_F1 = 2090,
    /** 按键'F2' **/
    ARKUI_KEYCODE_KEY_F2 = 2091,
    /** 按键'F3' **/
    ARKUI_KEYCODE_KEY_F3 = 2092,
    /** 按键'F4' **/
    ARKUI_KEYCODE_KEY_F4 = 2093,
    /** 按键'F5' **/
    ARKUI_KEYCODE_KEY_F5 = 2094,
    /** 按键'F6' **/
    ARKUI_KEYCODE_KEY_F6 = 2095,
    /** 按键'F7' **/
    ARKUI_KEYCODE_KEY_F7 = 2096,
    /** 按键'F8' **/
    ARKUI_KEYCODE_KEY_F8 = 2097,
    /** 按键'F9' **/
    ARKUI_KEYCODE_KEY_F9 = 2098,
    /** 按键'F10' **/
    ARKUI_KEYCODE_KEY_F10 = 2099,
    /** 按键'F11' **/
    ARKUI_KEYCODE_KEY_F11 = 2100,
    /** 按键'F12' **/
    ARKUI_KEYCODE_KEY_F12 = 2101,
    /** 小键盘锁 **/
    ARKUI_KEYCODE_KEY_NUM_LOCK = 2102,
    /** 小键盘按键'0' **/
    ARKUI_KEYCODE_KEY_NUMPAD_0 = 2103,
    /** 小键盘按键'1' **/
    ARKUI_KEYCODE_KEY_NUMPAD_1 = 2104,
    /** 小键盘按键'2' **/
    ARKUI_KEYCODE_KEY_NUMPAD_2 = 2105,
    /** 小键盘按键'3' **/
    ARKUI_KEYCODE_KEY_NUMPAD_3 = 2106,
    /** 小键盘按键'4' **/
    ARKUI_KEYCODE_KEY_NUMPAD_4 = 2107,
    /** 小键盘按键'5' **/
    ARKUI_KEYCODE_KEY_NUMPAD_5 = 2108,
    /** 小键盘按键'6' **/
    ARKUI_KEYCODE_KEY_NUMPAD_6 = 2109,
    /** 小键盘按键'7' **/
    ARKUI_KEYCODE_KEY_NUMPAD_7 = 2110,
    /** 小键盘按键'8' **/
    ARKUI_KEYCODE_KEY_NUMPAD_8 = 2111,
    /** 小键盘按键'9' **/
    ARKUI_KEYCODE_KEY_NUMPAD_9 = 2112,
    /** 小键盘按键'/' **/
    ARKUI_KEYCODE_KEY_NUMPAD_DIVIDE = 2113,
    /** 小键盘按键'*' **/
    ARKUI_KEYCODE_KEY_NUMPAD_MULTIPLY = 2114,
    /** 小键盘按键'-' **/
    ARKUI_KEYCODE_KEY_NUMPAD_SUBTRACT = 2115,
    /** 小键盘按键'+' **/
    ARKUI_KEYCODE_KEY_NUMPAD_ADD = 2116,
    /** 小键盘按键'.' **/
    ARKUI_KEYCODE_KEY_NUMPAD_DOT = 2117,
    /** 小键盘按键',' **/
    ARKUI_KEYCODE_KEY_NUMPAD_COMMA = 2118,
    /** 小键盘按键回车 **/
    ARKUI_KEYCODE_KEY_NUMPAD_ENTER = 2119,
    /** 小键盘按键'=' **/
    ARKUI_KEYCODE_KEY_NUMPAD_EQUALS = 2120,
    /** 小键盘按键'(' **/
    ARKUI_KEYCODE_KEY_NUMPAD_LEFT_PAREN = 2121,
    /** 小键盘按键')' **/
    ARKUI_KEYCODE_KEY_NUMPAD_RIGHT_PAREN = 2122,

    /** 虚拟多任务键 **/
    ARKUI_KEYCODE_KEY_VIRTUAL_MULTITASK = 2210,
    /** 睡眠键 **/
    ARKUI_KEYCODE_KEY_SLEEP = 2600,
    /** 日文全宽/半宽键 **/
    ARKUI_KEYCODE_KEY_ZENKAKU_HANKAKU = 2601,
    /** 102nd按键 **/
    ARKUI_KEYCODE_KEY_102ND = 2602,
    /** 日文Ro键 **/
    ARKUI_KEYCODE_KEY_RO = 2603,
    /** 日文片假名键 **/
    ARKUI_KEYCODE_KEY_KATAKANA = 2604,
    /** 日文平假名键 **/
    ARKUI_KEYCODE_KEY_HIRAGANA = 2605,
    /** 日文转换键 **/
    ARKUI_KEYCODE_KEY_HENKAN = 2606,
    /** 日语片假名/平假名键 **/
    ARKUI_KEYCODE_KEY_KATAKANA_HIRAGANA = 2607,
    /** 日文非转换键 **/
    ARKUI_KEYCODE_KEY_MUHENKAN = 2608,
    /** 换行键 **/
    ARKUI_KEYCODE_KEY_LINEFEED = 2609,
    /** 宏键 **/
    ARKUI_KEYCODE_KEY_MACRO = 2610,
    /** 数字键盘上的加号/减号键 **/
    ARKUI_KEYCODE_KEY_NUMPAD_PLUSMINUS = 2611,
    /** 扩展键 **/
    ARKUI_KEYCODE_KEY_SCALE = 2612,
    /** 日文韩语键 **/
    ARKUI_KEYCODE_KEY_HANGUEL = 2613,
    /** 日文汉语键 **/
    ARKUI_KEYCODE_KEY_HANJA = 2614,
    /** 日元键 **/
    ARKUI_KEYCODE_KEY_YEN = 2615,
    /** 停止键 **/
    ARKUI_KEYCODE_KEY_STOP = 2616,
    /** 重复键 **/
    ARKUI_KEYCODE_KEY_AGAIN = 2617,
    /** 道具键 **/
    ARKUI_KEYCODE_KEY_PROPS = 2618,
    /** 撤消键 **/
    ARKUI_KEYCODE_KEY_UNDO = 2619,
    /** 复制键 **/
    ARKUI_KEYCODE_KEY_COPY = 2620,
    /** 打开键 **/
    ARKUI_KEYCODE_KEY_OPEN = 2621,
    /** 粘贴键 **/
    ARKUI_KEYCODE_KEY_PASTE = 2622,
    /** 查找键 **/
    ARKUI_KEYCODE_KEY_FIND = 2623,
    /** 剪切键 **/
    ARKUI_KEYCODE_KEY_CUT = 2624,
    /** 帮助键 **/
    ARKUI_KEYCODE_KEY_HELP = 2625,
    /** 计算器特殊功能键，用于启动计算器应用程序 **/
    ARKUI_KEYCODE_KEY_CALC = 2626,
    /** 文件按键 **/
    ARKUI_KEYCODE_KEY_FILE = 2627,
    /** 书签键 **/
    ARKUI_KEYCODE_KEY_BOOKMARKS = 2628,
    /** 下一个按键 **/
    ARKUI_KEYCODE_KEY_NEXT = 2629,
    /** 播放/暂停键 **/
    ARKUI_KEYCODE_KEY_PLAYPAUSE = 2630,
    /** 上一个按键 **/
    ARKUI_KEYCODE_KEY_PREVIOUS = 2631,
    /** CD停止键 **/
    ARKUI_KEYCODE_KEY_STOPCD = 2632,
    /** 配置键 **/
    ARKUI_KEYCODE_KEY_CONFIG = 2634,
    /** 刷新键 **/
    ARKUI_KEYCODE_KEY_REFRESH = 2635,
    /** 退出键 **/
    ARKUI_KEYCODE_KEY_EXIT = 2636,
    /** 编辑键 **/
    ARKUI_KEYCODE_KEY_EDIT = 2637,
    /** 向上滚动键 **/
    ARKUI_KEYCODE_KEY_SCROLLUP = 2638,
    /** 向下滚动键 **/
    ARKUI_KEYCODE_KEY_SCROLLDOWN = 2639,
    /** 新建键 **/
    ARKUI_KEYCODE_KEY_NEW = 2640,
    /** 恢复键 **/
    ARKUI_KEYCODE_KEY_REDO = 2641,
    /** 关闭键 **/
    ARKUI_KEYCODE_KEY_CLOSE = 2642,
    /** 播放键 **/
    ARKUI_KEYCODE_KEY_PLAY = 2643,
    /** 低音增强键 **/
    ARKUI_KEYCODE_KEY_BASSBOOST = 2644,
    /** 打印键 **/
    ARKUI_KEYCODE_KEY_PRINT = 2645,
    /** 聊天键 **/
    ARKUI_KEYCODE_KEY_CHAT = 2646,
    /** 金融键 **/
    ARKUI_KEYCODE_KEY_FINANCE = 2647,
    /** 取消键 **/
    ARKUI_KEYCODE_KEY_CANCEL = 2648,
    /** 键盘灯光切换键 **/
    ARKUI_KEYCODE_KEY_KBDILLUM_TOGGLE = 2649,
    /** 键盘灯光调亮键 **/
    ARKUI_KEYCODE_KEY_KBDILLUM_DOWN = 2650,
    /** 键盘灯光调暗键 **/
    ARKUI_KEYCODE_KEY_KBDILLUM_UP = 2651,
    /** 发送键 **/
    ARKUI_KEYCODE_KEY_SEND = 2652,
    /** 答复键 **/
    ARKUI_KEYCODE_KEY_REPLY = 2653,
    /** 邮件转发键 **/
    ARKUI_KEYCODE_KEY_FORWARDMAIL = 2654,
    /** 保存键 **/
    ARKUI_KEYCODE_KEY_SAVE = 2655,
    /** 文件键 **/
    ARKUI_KEYCODE_KEY_DOCUMENTS = 2656,
    /** 下一个视频键 **/
    ARKUI_KEYCODE_KEY_VIDEO_NEXT = 2657,
    /** 上一个视频键 **/
    ARKUI_KEYCODE_KEY_VIDEO_PREV = 2658,
    /** 背光渐变键 **/
    ARKUI_KEYCODE_KEY_BRIGHTNESS_CYCLE = 2659,
    /** 亮度调节为0键 **/
    ARKUI_KEYCODE_KEY_BRIGHTNESS_ZERO = 2660,
    /** 显示关闭键 **/
    ARKUI_KEYCODE_KEY_DISPLAY_OFF = 2661,
    /** 游戏手柄上的各种按键 **/
    ARKUI_KEYCODE_KEY_BTN_MISC = 2662,
    /** 进入键 **/
    ARKUI_KEYCODE_KEY_GOTO = 2663,
    /** 信息查看键 **/
    ARKUI_KEYCODE_KEY_INFO = 2664,
    /** 程序键 **/
    ARKUI_KEYCODE_KEY_PROGRAM = 2665,
    /** 个人录像机(PVR)键 **/
    ARKUI_KEYCODE_KEY_PVR = 2666,
    /** 字幕键 **/
    ARKUI_KEYCODE_KEY_SUBTITLE = 2667,
    /** 全屏键 **/
    ARKUI_KEYCODE_KEY_FULL_SCREEN = 2668,
    /** 键盘 **/
    ARKUI_KEYCODE_KEY_KEYBOARD = 2669,
    /** 屏幕纵横比调节键 **/
    ARKUI_KEYCODE_KEY_ASPECT_RATIO = 2670,
    /** 端口控制键 **/
    ARKUI_KEYCODE_KEY_PC = 2671,
    /** TV键 **/
    ARKUI_KEYCODE_KEY_TV = 2672,
    /** TV键2 **/
    ARKUI_KEYCODE_KEY_TV2 = 2673,
    /** 录像机开启键 **/
    ARKUI_KEYCODE_KEY_VCR = 2674,
    /** 录像机开启键2 **/
    ARKUI_KEYCODE_KEY_VCR2 = 2675,
    /** SIM卡应用工具包（SAT）键 **/
    ARKUI_KEYCODE_KEY_SAT = 2676,
    /** CD键 **/
    ARKUI_KEYCODE_KEY_CD = 2677,
    /** 磁带键 **/
    ARKUI_KEYCODE_KEY_TAPE = 2678,
    /** 调谐器键 **/
    ARKUI_KEYCODE_KEY_TUNER = 2679,
    /** 播放器键 **/
    ARKUI_KEYCODE_KEY_PLAYER = 2680,
    /** DVD键 **/
    ARKUI_KEYCODE_KEY_DVD = 2681,
    /** 音频键 **/
    ARKUI_KEYCODE_KEY_AUDIO = 2682,
    /** 视频键 **/
    ARKUI_KEYCODE_KEY_VIDEO = 2683,
    /** 备忘录键 **/
    ARKUI_KEYCODE_KEY_MEMO = 2684,
    /** 日历键 **/
    ARKUI_KEYCODE_KEY_CALENDAR = 2685,
    /** 红色指示器 **/
    ARKUI_KEYCODE_KEY_RED = 2686,
    /** 绿色指示器 **/
    ARKUI_KEYCODE_KEY_GREEN = 2687,
    /** 黄色指示器 **/
    ARKUI_KEYCODE_KEY_YELLOW = 2688,
    /** 蓝色指示器 **/
    ARKUI_KEYCODE_KEY_BLUE = 2689,
    /** 频道向上键 **/
    ARKUI_KEYCODE_KEY_CHANNELUP = 2690,
    /** 频道向下键 **/
    ARKUI_KEYCODE_KEY_CHANNELDOWN = 2691,
    /** 末尾键 **/
    ARKUI_KEYCODE_KEY_LAST = 2692,
    /** 重启键 **/
    ARKUI_KEYCODE_KEY_RESTART = 2693,
    /** 慢速键 **/
    ARKUI_KEYCODE_KEY_SLOW = 2694,
    /** 随机播放键 **/
    ARKUI_KEYCODE_KEY_SHUFFLE = 2695,
    /** 可视电话键 **/
    ARKUI_KEYCODE_KEY_VIDEOPHONE = 2696,
    /** 游戏键 **/
    ARKUI_KEYCODE_KEY_GAMES = 2697,
    /** 放大键 **/
    ARKUI_KEYCODE_KEY_ZOOMIN = 2698,
    /** 缩小键 **/
    ARKUI_KEYCODE_KEY_ZOOMOUT = 2699,
    /** 缩放重置键 **/
    ARKUI_KEYCODE_KEY_ZOOMRESET = 2700,
    /** 文字处理键 **/
    ARKUI_KEYCODE_KEY_WORDPROCESSOR = 2701,
    /** 编辑器键 **/
    ARKUI_KEYCODE_KEY_EDITOR = 2702,
    /** 电子表格键 **/
    ARKUI_KEYCODE_KEY_SPREADSHEET = 2703,
    /** 图形编辑器键 **/
    ARKUI_KEYCODE_KEY_GRAPHICSEDITOR = 2704,
    /** 演示文稿键 **/
    ARKUI_KEYCODE_KEY_PRESENTATION = 2705,
    /** 数据库键标 **/
    ARKUI_KEYCODE_KEY_DATABASE = 2706,
    /** 新闻键 **/
    ARKUI_KEYCODE_KEY_NEWS = 2707,
    /** 语音信箱 **/
    ARKUI_KEYCODE_KEY_VOICEMAIL = 2708,
    /** 通讯簿 **/
    ARKUI_KEYCODE_KEY_ADDRESSBOOK = 2709,
    /** 通信键 **/
    ARKUI_KEYCODE_KEY_MESSENGER = 2710,
    /** 亮度切换键 **/
    ARKUI_KEYCODE_KEY_BRIGHTNESS_TOGGLE = 2711,
    /** 媒体循环键 **/
    ARKUI_KEYCODE_KEY_SPELLCHECK = 2712,
    /** 终端锁/屏幕保护程序 **/
    ARKUI_KEYCODE_KEY_COFFEE = 2713,
    /** 媒体循环键 **/
    ARKUI_KEYCODE_KEY_MEDIA_REPEAT = 2714,
    /** 图像键 **/
    ARKUI_KEYCODE_KEY_IMAGES = 2715,
    /** 按键配置键 **/
    ARKUI_KEYCODE_KEY_BUTTONCONFIG = 2716,
    /** 任务管理器 **/
    ARKUI_KEYCODE_KEY_TASKMANAGER = 2717,
    /** 日志按键 **/
    ARKUI_KEYCODE_KEY_JOURNAL = 2718,
    /** 控制面板键 **/
    ARKUI_KEYCODE_KEY_CONTROLPANEL = 2719,
    /** 应用程序选择键 **/
    ARKUI_KEYCODE_KEY_APPSELECT = 2720,
    /** 屏幕保护程序键 **/
    ARKUI_KEYCODE_KEY_SCREENSAVER = 2721,
    /** 辅助键 **/
    ARKUI_KEYCODE_KEY_ASSISTANT = 2722,
    /** 下一个键盘布局键 **/
    ARKUI_KEYCODE_KEY_KBD_LAYOUT_NEXT = 2723,
    /** 最小亮度键 **/
    ARKUI_KEYCODE_KEY_BRIGHTNESS_MIN = 2724,
    /** 最大亮度键 **/
    ARKUI_KEYCODE_KEY_BRIGHTNESS_MAX = 2725,
    /** 键盘输入Assist_Previous **/
    ARKUI_KEYCODE_KEY_KBDINPUTASSIST_PREV = 2726,
    /** 键盘输入Assist_Next **/
    ARKUI_KEYCODE_KEY_KBDINPUTASSIST_NEXT = 2727,
    /** 键盘输入Assist_Previous **/
    ARKUI_KEYCODE_KEY_KBDINPUTASSIST_PREVGROUP = 2728,
    /** 键盘输入Assist_Next **/
    ARKUI_KEYCODE_KEY_KBDINPUTASSIST_NEXTGROUP = 2729,
    /** 键盘输入Assist_Accept **/
    ARKUI_KEYCODE_KEY_KBDINPUTASSIST_ACCEPT = 2730,
    /** 键盘输入Assist_Cancel **/
    ARKUI_KEYCODE_KEY_KBDINPUTASSIST_CANCEL = 2731,
    /** 挡风玻璃除雾器开关 **/
    ARKUI_KEYCODE_KEY_FRONT = 2800,
    /** 设置键 **/
    ARKUI_KEYCODE_KEY_SETUP = 2801,
    /** 唤醒键 **/
    ARKUI_KEYCODE_KEY_WAKEUP = 2802,
    /** 发送文件按键 **/
    ARKUI_KEYCODE_KEY_SENDFILE = 2803,
    /** 删除文件按键 **/
    ARKUI_KEYCODE_KEY_DELETEFILE = 2804,
    /** 文件传输(XFER)按键 **/
    ARKUI_KEYCODE_KEY_XFER = 2805,
    /** 程序键1 **/
    ARKUI_KEYCODE_KEY_PROG1 = 2806,
    /** 程序键2 **/
    ARKUI_KEYCODE_KEY_PROG2 = 2807,
    /** MS-DOS键（微软磁盘操作系统 **/
    ARKUI_KEYCODE_KEY_MSDOS = 2808,
    /** 屏幕锁定键 **/
    ARKUI_KEYCODE_KEY_SCREENLOCK = 2809,
    /** 方向旋转显示键 **/
    ARKUI_KEYCODE_KEY_DIRECTION_ROTATE_DISPLAY = 2810,
    /** Windows循环键 **/
    ARKUI_KEYCODE_KEY_CYCLEWINDOWS = 2811,
    /** 按键 **/
    ARKUI_KEYCODE_KEY_COMPUTER = 2812,
    /** 弹出CD键 **/
    ARKUI_KEYCODE_KEY_EJECTCLOSECD = 2813,
    /** ISO键 **/
    ARKUI_KEYCODE_KEY_ISO = 2814,
    /** 移动键 **/
    ARKUI_KEYCODE_KEY_MOVE = 2815,
    /** 按键'F13' **/
    ARKUI_KEYCODE_KEY_F13 = 2816,
    /** 按键'F14' **/
    ARKUI_KEYCODE_KEY_F14 = 2817,
    /** 按键'F15' **/
    ARKUI_KEYCODE_KEY_F15 = 2818,
    /** 按键'F16' **/
    ARKUI_KEYCODE_KEY_F16 = 2819,
    /** 按键'F17' **/
    ARKUI_KEYCODE_KEY_F17 = 2820,
    /** 按键'F18' **/
    ARKUI_KEYCODE_KEY_F18 = 2821,
    /** 按键'F19' **/
    ARKUI_KEYCODE_KEY_F19 = 2822,
    /** 按键'F20' **/
    ARKUI_KEYCODE_KEY_F20 = 2823,
    /** 按键'F21' **/
    ARKUI_KEYCODE_KEY_F21 = 2824,
    /** 按键'F22' **/
    ARKUI_KEYCODE_KEY_F22 = 2825,
    /** 按键'F23' **/
    ARKUI_KEYCODE_KEY_F23 = 2826,
    /** 按键'F24' **/
    ARKUI_KEYCODE_KEY_F24 = 2827,
    /** 程序键3 **/
    ARKUI_KEYCODE_KEY_PROG3 = 2828,
    /** 程序键4 **/
    ARKUI_KEYCODE_KEY_PROG4 = 2829,
    /** 仪表板 **/
    ARKUI_KEYCODE_KEY_DASHBOARD = 2830,
    /** 挂起键 **/
    ARKUI_KEYCODE_KEY_SUSPEND = 2831,
    /** 高阶路径键 **/
    ARKUI_KEYCODE_KEY_HP = 2832,
    /** 音量键 **/
    ARKUI_KEYCODE_KEY_SOUND = 2833,
    /** 疑问按键 **/
    ARKUI_KEYCODE_KEY_QUESTION = 2834,
    /** 连接键 **/
    ARKUI_KEYCODE_KEY_CONNECT = 2836,
    /** 运动按键 **/
    ARKUI_KEYCODE_KEY_SPORT = 2837,
    /** 商城键 **/
    ARKUI_KEYCODE_KEY_SHOP = 2838,
    /** 交替键 **/
    ARKUI_KEYCODE_KEY_ALTERASE = 2839,
    /** 在可用视频之间循环输出（监视器/LCD/TV输出/等） **/
    ARKUI_KEYCODE_KEY_SWITCHVIDEOMODE = 2841,
    /** 电池按键 **/
    ARKUI_KEYCODE_KEY_BATTERY = 2842,
    /** 蓝牙按键 **/
    ARKUI_KEYCODE_KEY_BLUETOOTH = 2843,
    /** 无线局域网 **/
    ARKUI_KEYCODE_KEY_WLAN = 2844,
    /** 超宽带（UWB） **/
    ARKUI_KEYCODE_KEY_UWB = 2845,
    /** WWAN WiMAX按键 **/
    ARKUI_KEYCODE_KEY_WWAN_WIMAX = 2846,
    /** 控制所有收音机的键 **/
    ARKUI_KEYCODE_KEY_RFKILL = 2847,
    /** 向上频道键 **/
    ARKUI_KEYCODE_KEY_CHANNEL = 3001,
    /** 按键0 **/
    ARKUI_KEYCODE_KEY_BTN_0 = 3100,
    /** 按键1 **/
    ARKUI_KEYCODE_KEY_BTN_1 = 3101,
    /** 按键2 **/
    ARKUI_KEYCODE_KEY_BTN_2 = 3102,
    /** 按键3 **/
    ARKUI_KEYCODE_KEY_BTN_3 = 3103,
    /** 按键4 **/
    ARKUI_KEYCODE_KEY_BTN_4 = 3104,
    /** 按键5 **/
    ARKUI_KEYCODE_KEY_BTN_5 = 3105,
    /** 按键6 **/
    ARKUI_KEYCODE_KEY_BTN_6 = 3106,
    /** 按键7 **/
    ARKUI_KEYCODE_KEY_BTN_7 = 3107,
    /** 按键8 **/
    ARKUI_KEYCODE_KEY_BTN_8 = 3108,
    /** 按键9 **/
    ARKUI_KEYCODE_KEY_BTN_9 = 3109,
} ArkUI_KeyCode_E;

/**
 * @brief 按键的类型。
 *
 * @since 14
 */
typedef enum {
    /** 未知类型 **/
    ARKUI_KEY_EVENT_UNKNOWN = -1,
    /** 按键按下 **/
    ARKUI_KEY_EVENT_DOWN = 0,
    /** 按键松开 **/
    ARKUI_KEY_EVENT_UP = 1,
    /** 按键长按 **/
    ARKUI_KEY_EVENT_LONG_PRESS = 2,
    /** 按键点击 **/
    ARKUI_KEY_EVENT_CLICK = 3,
} ArkUI_KeyEventType_E;

/**
 * @brief 触发当前按键的输入设备类型。
 *
 * @since 14
 */
typedef enum {
    /** 未知类型 **/
    ARKUI_KEY_SOURCE_UNKNOWN = 0,
    /** 鼠标 **/
    ARKUI_KEY_SOURCE_TYPE_MOUSE = 1,
    /** 触摸屏 **/
    ARKUI_KEY_SOURCE_TYPE_TOUCH = 2,
    /** 触摸板 **/
    ARKUI_KEY_SOURCE_TYPE_TOUCH_PAD = 3,
    /** 键盘 **/
    ARKUI_KEY_SOURCE_TYPE_KEYBOARD = 4,
} ArkUI_KeySourceType_E;

/**
 * @brief 按键对应的意图。
 *
 * @since 14
 */
typedef enum {
    /** 未知意图 **/
    ARKUI_KEY_INTENSION_UNKNOWN = -1,
    /** 向上 **/
    ARKUI_KEY_INTENSION_UP = 1,
    /** 向下 **/
    ARKUI_KEY_INTENSION_DOWN = 2,
    /** 向左 **/
    ARKUI_KEY_INTENSION_LEFT = 3,
    /** 向右 **/
    ARKUI_KEY_INTENSION_RIGHT = 4,
    /** 选中 **/
    ARKUI_KEY_INTENSION_SELECT = 5,
    /** 返回 **/
    ARKUI_KEY_INTENSION_ESCAPE = 6,
    /** 后退 **/
    ARKUI_KEY_INTENSION_BACK = 7,
    /** 前进 **/
    ARKUI_KEY_INTENSION_FORWARD = 8,
    /** 菜单 **/
    ARKUI_KEY_INTENSION_MENU = 9,
    /** 主页 **/
    ARKUI_KEY_INTENSION_HOME = 10,
    /** 上一页 **/
    ARKUI_KEY_INTENSION_PAGE_UP = 11,
    /** 下一页 **/
    ARKUI_KEY_INTENSION_PAGE_DOWN = 12,
    /** 缩小 **/
    ARKUI_KEY_INTENSION_ZOOM_OUT = 13,
    /** 放大 **/
    ARKUI_KEY_INTENSION_ZOOM_IN = 14,

    /** 播放 **/
    ARKUI_KEY_INTENTION_MEDIA_PLAY_PAUSE = 100,
    /** 快进 **/
    ARKUI_KEY_INTENTION_MEDIA_FAST_FORWARD = 101,
    /** 快退 **/
    ARKUI_KEY_INTENTION_MEDIA_FAST_REWIND = 102,
    /** 快速播放 **/
    ARKUI_KEY_INTENTION_MEDIA_FAST_PLAYBACK = 103,
    /** 下一首 **/
    ARKUI_KEY_INTENTION_MEDIA_NEXT = 104,
    /** 上一首 **/
    ARKUI_KEY_INTENTION_MEDIA_PREVIOUS = 105,
    /** 静音 **/
    ARKUI_KEY_INTENTION_MEDIA_MUTE = 106,
    /** 音量增加 **/
    ARKUI_KEY_INTENTION_VOLUME_UP = 107,
    /** 音量降低 **/
    ARKUI_KEY_INTENTION_VOLUME_DOWN = 108,

    /** 接听电话 **/
    ARKUI_KEY_INTENTION_CALL = 200,
    /** 挂断电话 **/
    ARKUI_KEY_INTENTION_ENDCALL = 201,
    /** 拒接电话 **/
    ARKUI_KEY_INTENTION_REJECTCALL = 202,
    /** 拍照 **/
    ARKUI_KEY_INTENTION_CAMERA = 300,
} ArkUI_KeyIntension_E;

/**
 * @brief 获取按键的类型。
 *
 * @param event ArkUI_UIInputEvent事件指针。
 * @return ArkUI_KeyEventType_E 按键的类型。
 * @since 14
 */
ArkUI_KeyEventType_E OH_ArkUI_KeyEvent_GetType(const ArkUI_UIInputEvent* event);

/**
 * @brief 获取按键的键码。
 *
 * @param event ArkUI_UIInputEvent事件指针。
 * @return 按键的键码。
 * @since 14
 */
ArkUI_KeyCode_E OH_ArkUI_KeyEvent_GetKeyCode(const ArkUI_UIInputEvent* event);

/**
 * @brief 获取按键的键值。
 *
 * @param event ArkUI_UIInputEvent事件指针。
 * @return 按键的键值。
 * @since 14
 */
const char *OH_ArkUI_KeyEvent_GetKeyText(const ArkUI_UIInputEvent* event);

/**
 * @brief 获取当前按键的输入设备类型。
 *
 * @param event ArkUI_UIInputEvent事件指针。
 * @return ArkUI_KeySourceType_E 当前按键的输入设备类型。
 * @since 14
 */
ArkUI_KeySourceType_E OH_ArkUI_KeyEvent_GetKeySource(const ArkUI_UIInputEvent* event);

/**
 * @brief 获取当前按键的输入设备ID。
 *
 * @param event ArkUI_UIInputEvent事件指针。
 * @return 当前按键的输入设备ID。
 * @since 14
 */
int32_t OH_ArkUI_KeyEvent_GetDeviceId(const ArkUI_UIInputEvent* event);

/**
 * @brief 获取按键发生时元键（即Windows键盘的WIN键、Mac键盘的Command键）的状态。
 *
 * @param event ArkUI_UIInputEvent事件指针。
 * @return 按键发生时元键（即Windows键盘的WIN键、Mac键盘的Command键）的状态，1表示按压态，0表示未按压态。
 * @since 14
 */
int32_t OH_ArkUI_KeyEvent_GetMetaKey(const ArkUI_UIInputEvent* event);

/**
 * @brief 获取事件时间戳。触发事件时距离系统启动的时间间隔，单位：ns。
 *
 * @param event ArkUI_UIInputEvent事件指针。
 * @return 事件时间戳，单位：ns。
 * @since 14
 */
uint64_t OH_ArkUI_KeyEvent_GetTimestamp(const ArkUI_UIInputEvent* event);

/**
 * @brief 阻塞事件冒泡传递。
 *
 * @param event ArkUI_UIInputEvent事件指针。
 * @param stopPropagation 表示是否阻止事件冒泡。
 * @since 14
 */
void OH_ArkUI_KeyEvent_StopPropagation(const ArkUI_UIInputEvent* event, bool stopPropagation);

/**
 * @brief 获取功能键按压状态。报错信息请参考以下错误码。支持功能键 'Ctrl'|'Alt'|'Shift'|'Fn'，设备外接带Fn键的键盘不支持Fn键查询。
 *
 * @param event ArkUI_UIInputEvent事件指针。
 * @param uint32_t modifierKeys 应用在输入时需通过 {@link ArkUI_ModifierKeyName} 创建。
 * @return bool 是否按压。
 * @since 14
 */
bool OH_ArkUI_KeyEvent_GetModifierKeyState(const ArkUI_UIInputEvent* event, uint32_t modifierKeys);

/**
 * @brief 获取按键对应的意图。
 *
 * @param event ArkUI_UIInputEvent事件指针。
 * @return ArkUI_KeyIntension_E 按键对应的意图。
 * @since 14
 */
ArkUI_KeyIntension_E OH_ArkUI_KeyEvent_GetKeyIntensionCode(const ArkUI_UIInputEvent* event);

/**
 * @brief 获取按键的unicode码值。支持范围为非空格的基本拉丁字符：0x0021-0x007E，不支持字符为0。组合键场景下，返回当前keyEvent对应按键的unicode码值。
 *
 * @param event ArkUI_UIInputEvent事件指针。
 * @return unicode码值。
 * @since 14
 */
uint32_t OH_ArkUI_KeyEvent_GetUnicode(const ArkUI_UIInputEvent* event);

/**
 * @brief 在按键事件回调中，设置事件是否被该回调消费
 *
 * @param event ArkUI_UIInputEvent事件指针。
 * @param isConsumed 是否被消费。
 * @since 14
 */
void OH_ArkUI_KeyEvent_SetConsumed(const ArkUI_UIInputEvent* event, bool isConsumed);
#ifdef __cplusplus
};
#endif

#endif // ARKUI_NATIVE_KEY_EVENT_H