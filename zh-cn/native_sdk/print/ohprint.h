/*
 * Copyright (C) 2024 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup OH_Print
 * @{
 *
 * @brief 提供打印模块的C接口定义.
 *
 * @syscap SystemCapability.Print.PrintFramework
 * @since 12
 * @version 1.0
 */

/**
 * @file ohprint.h
 *
 * @brief 声明用于发现和连接打印机、从打印机打印文件、查询已添加打印机的列表及其中的打印机信息等API.
 *
 * @library libohprint.so
 * @kit BasicServicesKit
 * @syscap SystemCapability.Print.PrintFramework
 * @since 12
 * @version 1.0
 */

#ifndef OH_PRINT_H
#define OH_PRINT_H

#include <stdint.h>
#include <stdbool.h>

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief 枚举错误码.
 *
 * @since 12
 * @version 1.0
 */
typedef enum {
    /** 成功. */
    PRINT_ERROR_NONE = 0,
    /** 没有权限. */
    PRINT_ERROR_NO_PERMISSION = 201,
    /** 无效参数. */
    PRINT_ERROR_INVALID_PARAMETER = 401,
    /** 内部错误. */
    PRINT_ERROR_GENERIC_FAILURE = 24300001,
    /** rpc传输错误. */
    PRINT_ERROR_RPC_FAILURE = 24300002,
    /** 打印服务错误. */
    PRINT_ERROR_SERVER_FAILURE = 24300003,
    /** 无效打印扩展. */
    PRINT_ERROR_INVALID_EXTENSION = 24300004,
    /** 无效打印机. */
    PRINT_ERROR_INVALID_PRINTER = 24300005,
    /** 无效打印任务. */
    PRINT_ERROR_INVALID_PRINT_JOB = 24300006,
    /** 文件读写错误. */
    PRINT_ERROR_FILE_IO = 24300007,
    /** 未知错误. */
    PRINT_ERROR_UNKNOWN = 24300255,
} Print_ErrorCode;

/**
 * @brief 打印边距
 *
 * @since 12
 */
typedef struct {
    /** 左边距. */
    uint32_t leftMargin;
    /** 上边距. */
    uint32_t topMargin;
    /** 右边距. */
    uint32_t rightMargin;
    /** 下边距. */
    uint32_t bottomMargin;
} Print_Margin;

/**
 * @brief 纸张大小信息.
 *
 * @since 12
 */
typedef struct {
    /** 纸张id. */
    char *id;
    /** 纸张名称. */
    char *name;
    /** 纸张宽度. */
    uint32_t width;
    /** 纸张高度. */
    uint32_t height;
} Print_PageSize;

/**
 * @brief 打印文档任务的状态.
 *
 * @since 13
 */
typedef enum {
    /** 打印预览界面销毁. */
    PRINT_DOC_ADAPTER_PREVIEW_ABILITY_DESTROY = 0,
    /** 打印任务执行成功. */
    PRINT_DOC_ADAPTER_PRINT_TASK_SUCCEED = 1,
    /** 打印任务执行失败. */
    PRINT_DOC_ADAPTER_PRINT_TASK_FAIL = 2,
    /** 打印任务被取消. */
    PRINT_DOC_ADAPTER_PRINT_TASK_CANCEL = 3,
    /** 打印任务阻塞. */
    PRINT_DOC_ADAPTER_PRINT_TASK_BLOCK = 4,
    /** 预览界面点击取消按钮界面退出. */
    PRINT_DOC_ADAPTER_PREVIEW_ABILITY_DESTROY_FOR_CANCELED = 5,
    /** 预览界面点击打印按钮界面退出. */
    PRINT_DOC_ADAPTER_PREVIEW_ABILITY_DESTROY_FOR_STARTED = 6,
} Print_JobDocAdapterState;

/**
 * @brief 打印范围.
 *
 * @since 13
 */
typedef struct {
    /** 打印起始页. */
    uint32_t startPage;
    /** 打印终止页. */
    uint32_t endPage;
    /** 打印队列长度. */
    uint32_t pagesArrayLen;
    /** 打印队列指针. */
    uint32_t* pagesArray;
} Print_Range;

/**
 * @brief 打印属性结构体.
 *
 * @since 13
 */
typedef struct {
    /** 打印范围. */
    Print_Range pageRange;
    /** 打印尺寸. */
    Print_PageSize pageSize;
    /** 打印边距. */
    Print_Margin pageMargin;
    /** 打印份数. */
    uint32_t copyNumber;
    /** 单双面. */
    uint32_t duplexMode;
    /** 彩色. */
    uint32_t colorMode;
    /** 顺序打印. */
    bool isSequential;
    /** 横纵向. */
    bool isLandscape;
    /** 打印选项标识位 */
    bool hasOption;
    /** 打印选项. */
    char options[256];
} Print_PrintAttributes;

/**
 * @brief 文件回写回调.
 *
 * @param jobId 打印任务id
 * @param code 文件回写结果.
 * @since 13
 */
typedef void(*Print_WriteResultCallback)(const char *jobId, uint32_t code);

/**
 * @brief 文件开始回写回调函数.
 *
 * @param jobId 打印任务id.
 * @param fd 回写的文件句柄.
 * @param oldAttrs 用户设置打印参数变化前的参数.
 * @param newAttrs 用户设置打印参数变化后的参数.
 * @param writeCallback 使用方回写完文件后调用回调函数通知打印服务.
 * @since 13
 */
typedef void(*Print_OnStartLayoutWrite)(const char *jobId,
                                        uint32_t fd,
                                        const Print_PrintAttributes *oldAttrs,
                                        const Print_PrintAttributes *newAttrs,
                                        Print_WriteResultCallback writeCallback);

/**
 * @brief 打印任务状态回调.
 *
 * @param jobId 打印任务id.
 * @param state 当前任务状态.
 * @since 13
 */
typedef void(*Print_OnJobStateChanged)(const char *jobId, uint32_t state);

/**
 * @brief 打印文档任务回调结构体.
 *
 * @since 13
 */
typedef struct {
    /** 文件开始回写回调函数. */
    Print_OnStartLayoutWrite startLayoutWriteCb;
    /** 打印任务状态回调. */
    Print_OnJobStateChanged jobStateChangedCb;
} Print_PrintDocCallback;

/**
 * @brief 拉起打印预览界面接口.
 *
 * @permission {@code ohos.permission.PRINT}
 * @param printJobName 打印任务名称.
 * @param printDocCallback 打印文档任务回调结构体.
 * @param context 调用接口的ability的上下文.
 * @return 返回 {@link Print_ErrorCode#PRINT_ERROR_NONE} 执行成功.
 *         {@link PRINT_ERROR_NO_PERMISSION} 需要配置 {@code ohos.permission.PRINT} 权限.
 *         {@link PRINT_ERROR_RPC_FAILURE} 无法连接打印服务.
 * @syscap SystemCapability.Print.PrintFramework
 * @since 13
 */
Print_ErrorCode OH_Print_StartPrintByNative(const char *printJobName,
                                            Print_PrintDocCallback printDocCallback,
                                            void *context);

#ifdef __cplusplus
}
#endif

#endif // OH_PRINT_H
/** @} */
