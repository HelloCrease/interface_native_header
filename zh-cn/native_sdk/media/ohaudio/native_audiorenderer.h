/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup OHAudio
 * @{
 *
 * @brief 提供音频模块C接口定义。
 *
 * @syscap SystemCapability.Multimedia.Audio.Core
 *
 * @since 10
 * @version 1.0
 */

/**
 * @file native_audiorenderer.h
 *
 * @brief 声明输出类型的音频流相关接口,
 *
 * @syscap SystemCapability.Multimedia.Audio.Core
 * @since 10
 * @version 1.0
 */

#ifndef NATIVE_AUDIORENDERER_H
#define NATIVE_AUDIORENDERER_H

#include <time.h>
#include "native_audiostream_base.h"
#include "multimedia/native_audio_channel_layout.h"
#ifdef __cplusplus
extern "C" {
#endif
/**
 * @brief 释放音频流。
 *
 * @since 10
 * @syscap SystemCapability.Multimedia.Audio.Core
 * @param renderer 指向{@link OH_AudioStreamBuilder_GenerateRenderer}创建的音频流实例。
 * @return 函数返回值：
 *         {@link AUDIOSTREAM_SUCCESS} 函数执行成功。
 *         {@link AUDIOSTREAM_ERROR_INVALID_PARAM} 参数renderer为nullptr。
 *         {@link AUDIOSTREAM_ERROR_ILLEGAL_STATE} 执行状态异常。
 */
OH_AudioStream_Result OH_AudioRenderer_Release(OH_AudioRenderer* renderer);

/**
 * @brief 开始输出音频数据。
 *
 * @since 10
 * @syscap SystemCapability.Multimedia.Audio.Core
 * @param renderer 指向{@link OH_AudioStreamBuilder_GenerateRenderer}创建的音频流实例。
 * @return 函数返回值：
 *         {@link AUDIOSTREAM_SUCCESS} 函数执行成功。
 *         {@link AUDIOSTREAM_ERROR_INVALID_PARAM} 参数renderer为nullptr。
 *         {@link AUDIOSTREAM_ERROR_ILLEGAL_STATE} 执行状态异常。
 */
OH_AudioStream_Result OH_AudioRenderer_Start(OH_AudioRenderer* renderer);

/**
 * @brief 暂停音频流。
 *
 * @since 10
 * @syscap SystemCapability.Multimedia.Audio.Core
 * @param renderer 指向{@link OH_AudioStreamBuilder_GenerateRenderer}创建的音频流实例。
 * @return 函数返回值：
 *         {@link AUDIOSTREAM_SUCCESS} 函数执行成功。
 *         {@link AUDIOSTREAM_ERROR_INVALID_PARAM} 参数renderer为nullptr。
 *         {@link AUDIOSTREAM_ERROR_ILLEGAL_STATE} 执行状态异常。
 */
OH_AudioStream_Result OH_AudioRenderer_Pause(OH_AudioRenderer* renderer);

/**
 * @brief 停止音频流
 *
 * @since 10
 * @syscap SystemCapability.Multimedia.Audio.Core
 * @param renderer 指向{@link OH_AudioStreamBuilder_GenerateRenderer}创建的音频流实例。
 * @return 函数返回值：
 *         {@link AUDIOSTREAM_SUCCESS} 函数执行成功。
 *         {@link AUDIOSTREAM_ERROR_INVALID_PARAM} 参数renderer为nullptr。
 *         {@link AUDIOSTREAM_ERROR_ILLEGAL_STATE} 执行状态异常。
 */
OH_AudioStream_Result OH_AudioRenderer_Stop(OH_AudioRenderer* renderer);

/**
 * @brief 丢弃已经写入的音频数据。
 *
 * @since 10
 * @syscap SystemCapability.Multimedia.Audio.Core
 * @param renderer 指向{@link OH_AudioStreamBuilder_GenerateRenderer}创建的音频流实例。
 * @return 函数返回值：
 *         {@link AUDIOSTREAM_SUCCESS} 函数执行成功。
 *         {@link AUDIOSTREAM_ERROR_INVALID_PARAM} 参数renderer为nullptr。
 *         {@link AUDIOSTREAM_ERROR_ILLEGAL_STATE} 执行状态异常。
 */
OH_AudioStream_Result OH_AudioRenderer_Flush(OH_AudioRenderer* renderer);

/**
 * @brief 查询当前音频流状态。
 *
 * @since 10
 * @syscap SystemCapability.Multimedia.Audio.Core
 * @param renderer 指向{@link OH_AudioStreamBuilder_GenerateRenderer}创建的音频流实例。
 * @param state 指向一个用来接收音频流状态的变量(作为返回值使用)。
 * @return 函数返回值：
 *         {@link AUDIOSTREAM_SUCCESS} 函数执行成功。
 *         {@link AUDIOSTREAM_ERROR_INVALID_PARAM} 参数renderer为nullptr。
 */
OH_AudioStream_Result OH_AudioRenderer_GetCurrentState(OH_AudioRenderer* renderer,
    OH_AudioStream_State* state);

/**
 * @brief 查询当前音频流采样率。
 *
 * @since 10
 * @syscap SystemCapability.Multimedia.Audio.Core
 * @param renderer 指向{@link OH_AudioStreamBuilder_GenerateRenderer}创建的音频流实例。
 * @param rate 指向一个用来接收音频流采样率的变量(作为返回值使用)。
 * @return 函数返回值：
 *         {@link AUDIOSTREAM_SUCCESS} 函数执行成功。
 *         {@link AUDIOSTREAM_ERROR_INVALID_PARAM} 参数renderer为nullptr。
 */
OH_AudioStream_Result OH_AudioRenderer_GetSamplingRate(OH_AudioRenderer* renderer, int32_t* rate);

/**
 * @brief 查询当前音频流ID。
 *
 * @since 10
 * @syscap SystemCapability.Multimedia.Audio.Core
 * @param renderer 指向{@link OH_AudioStreamBuilder_GenerateRenderer}创建的音频流实例。
 * @param streamId 指向一个用来接收音频流ID的变量(作为返回值使用)。
 * @return 函数返回值：
 *         {@link AUDIOSTREAM_SUCCESS} 函数执行成功。
 *         {@link AUDIOSTREAM_ERROR_INVALID_PARAM} 参数renderer为nullptr。
 */
OH_AudioStream_Result OH_AudioRenderer_GetStreamId(OH_AudioRenderer* renderer, uint32_t* streamId);

/**
 * @brief 查询当前音频流通道数。
 *
 * @since 10
 * @syscap SystemCapability.Multimedia.Audio.Core
 * @param renderer 指向{@link OH_AudioStreamBuilder_GenerateRenderer}创建的音频流实例。
 * @param channelCount 指向一个用来接收音频流通道数的变量(作为返回值使用)。
 * @return 函数返回值：
 *         {@link AUDIOSTREAM_SUCCESS} 函数执行成功。
 *         {@link AUDIOSTREAM_ERROR_INVALID_PARAM} 参数renderer为nullptr。
 */
OH_AudioStream_Result OH_AudioRenderer_GetChannelCount(OH_AudioRenderer* renderer, int32_t* channelCount);

/**
 * @brief 查询当前音频流采样格式。
 *
 * @since 10
 * @syscap SystemCapability.Multimedia.Audio.Core
 * @param renderer 指向{@link OH_AudioStreamBuilder_GenerateRenderer}创建的音频流实例。
 * @param sampleFormat 指向一个用来接收音频流采样格式的变量(作为返回值使用)。
 * @return 函数返回值：
 *         {@link AUDIOSTREAM_SUCCESS} 函数执行成功。
 *         {@link AUDIOSTREAM_ERROR_INVALID_PARAM} 参数renderer为nullptr。
 */
OH_AudioStream_Result OH_AudioRenderer_GetSampleFormat(OH_AudioRenderer* renderer,
    OH_AudioStream_SampleFormat* sampleFormat);

/**
 * @brief 查询当前音频流时延模式。
 *
 * @since 10
 * @syscap SystemCapability.Multimedia.Audio.Core
 * @param renderer 指向{@link OH_AudioStreamBuilder_GenerateRenderer}创建的音频流实例。
 * @param latencyMode 指向一个用来接收音频流时延模式的变量(作为返回值使用)。
 * @return 函数返回值：
 *         {@link AUDIOSTREAM_SUCCESS} 函数执行成功。
 *         {@link AUDIOSTREAM_ERROR_INVALID_PARAM} 参数renderer为nullptr。
 */
OH_AudioStream_Result OH_AudioRenderer_GetLatencyMode(OH_AudioRenderer* renderer,
    OH_AudioStream_LatencyMode* latencyMode);

/**
 * @brief 查询当前音频流工作场景类型。
 *
 * The rendere info includes {@link OH_AudioStream_Usage} value and {@link OH_AudioStream_Content} value.
 *
 * @since 10
 * @syscap SystemCapability.Multimedia.Audio.Core
 * @param renderer 指向{@link OH_AudioStreamBuilder_GenerateRenderer}创建的音频流实例。
 * @param usage 指向一个用来接收输出类型音频流的工作场景的变量(作为返回值使用)。
 * @return 函数返回值：
 *         {@link AUDIOSTREAM_SUCCESS} 函数执行成功。
 *         {@link AUDIOSTREAM_ERROR_INVALID_PARAM} 参数renderer为nullptr。
 */
OH_AudioStream_Result OH_AudioRenderer_GetRendererInfo(OH_AudioRenderer* renderer,
    OH_AudioStream_Usage* usage);

/**
 * @brief 查询当前音频流编码类型。
 *
 * @since 10
 * @syscap SystemCapability.Multimedia.Audio.Core
 * @param renderer 指向{@link OH_AudioStreamBuilder_GenerateRenderer}创建的音频流实例。
 * @param encodingType 指向一个用来接收音频流编码类型的变量(作为返回值使用)。
 * @return 函数返回值：
 *         {@link AUDIOSTREAM_SUCCESS} 函数执行成功。
 *         {@link AUDIOSTREAM_ERROR_INVALID_PARAM} 参数renderer为nullptr。
 */
OH_AudioStream_Result OH_AudioRenderer_GetEncodingType(OH_AudioRenderer* renderer,
    OH_AudioStream_EncodingType* encodingType);

/**
 * @brief 查询自创建流以来已写入的帧数。
 *
 * @since 10
 * @syscap SystemCapability.Multimedia.Audio.Core
 * @param renderer 指向{@link OH_AudioStreamBuilder_GenerateRenderer}创建的音频流实例。
 * @param frames 指向将为帧计数设置的变量的指针(作为返回值使用)。
 * @return 函数返回值：
 *         {@link AUDIOSTREAM_SUCCESS} 函数执行成功。
 *         {@link AUDIOSTREAM_ERROR_INVALID_PARAM} 参数renderer为nullptr。
 */
OH_AudioStream_Result OH_AudioRenderer_GetFramesWritten(OH_AudioRenderer* renderer, int64_t* frames);

/**
 * @brief 获取输出音频流时间戳和位置信息。
 *
 * 该接口可以获取到音频通道实际播放位置（framePosition）以及播放到该位置时候的时间戳（timestamp），时间戳单位为纳秒。
 *
 * 该接口一般用来实现音画同步，建议频率不要太频繁，可以每分钟一次，最好不要低200ms一次。频繁调用可能会带来功耗问题，因此在能保证音画同步效果的情况下，不需要频繁的查询时间戳。
 *
 * @since 10
 * @syscap SystemCapability.Multimedia.Audio.Core
 * @param renderer 指向{@link OH_AudioStreamBuilder_GenerateRenderer}创建的音频流实例。
 * @param clockId 时钟标识符，使用：{@link #CLOCK_MONOTONIC}。
 * @param framePosition 指向要接收位置的变量的指针(作为返回值使用)。
 * @param timestamp 指向接收时间戳的变量的指针(作为返回值使用)。
 * @return 函数返回值：
 *         {@link AUDIOSTREAM_SUCCESS} 函数执行成功。
 *         {@link AUDIOSTREAM_ERROR_INVALID_PARAM}：
 *                                                 1. 参数renderer为nullptr；
 *                                                 2. 参数clockId无效。
 *         {@link AUDIOSTREAM_ERROR_ILLEGAL_STATE} 执行状态异常。
 */
OH_AudioStream_Result OH_AudioRenderer_GetTimestamp(OH_AudioRenderer* renderer, clockid_t clockId,
    int64_t* framePosition, int64_t* timestamp);

/**
 * @brief 在回调中查询帧大小，它是一个固定的长度，每次回调都要填充流。
 *
 * @since 10
 * @syscap SystemCapability.Multimedia.Audio.Core
 * @param renderer 指向{@link OH_AudioStreamBuilder_GenerateRenderer}创建的音频流实例。
 * @param frameSize 指向将为帧大小设置的变量的指针(作为返回值使用)。
 * @return 函数返回值：
 *         {@link AUDIOSTREAM_SUCCESS} 函数执行成功。
 *         {@link AUDIOSTREAM_ERROR_INVALID_PARAM} 参数renderer为nullptr。
 */
OH_AudioStream_Result OH_AudioRenderer_GetFrameSizeInCallback(OH_AudioRenderer* renderer, int32_t* frameSize);

/**
 * @brief 获取音频渲染速率。
 *
 * @since 11
 * @syscap SystemCapability.Multimedia.Audio.Core
 * @param renderer 指向{@link OH_AudioStreamBuilder_GenerateRenderer}创建的音频流实例。
 * @param speed 指向接收播放倍速值的变量的指针(作为返回值使用)。
 * @return 函数返回值：
 *         {@link AUDIOSTREAM_SUCCESS} 函数执行成功。
 *         {@link AUDIOSTREAM_ERROR_INVALID_PARAM} 参数renderer为nullptr。
 */
OH_AudioStream_Result OH_AudioRenderer_GetSpeed(OH_AudioRenderer* renderer, float* speed);

/**
 * @brief 设置音频渲染速率。
 *
 * @since 11
 * @syscap SystemCapability.Multimedia.Audio.Core
 * @param renderer 指向{@link OH_AudioStreamBuilder_GenerateRenderer}创建的音频流实例。
 * @param speed 设置播放的倍速值（倍速范围：0.25-4.0）。
 * @return 函数返回值：
 *         {@link AUDIOSTREAM_SUCCESS} 函数执行成功。
 *         {@link AUDIOSTREAM_ERROR_INVALID_PARAM} 参数renderer为nullptr。
 */
OH_AudioStream_Result OH_AudioRenderer_SetSpeed(OH_AudioRenderer* renderer, float speed);

/**
 * @brief 在当前渲染器上设置标记位置。调用此函数将覆盖已设置的标记位置。
 *
 * @since 12
 * @syscap SystemCapability.Multimedia.Audio.Core
 * @param renderer 指向{@link OH_AudioStreamBuilder_GenerateRenderer}创建的音频流实例。
 * @param samplePos 设置目标标记位置。
 * @param callback 当到达目标标记位置时回调{@link OH_AudioRenderer_OnMarkReachedCallback}。
 * @param userData 指向通过回调函数传递的应用数据指针。
 * @return 函数返回值：
 *         {@link AUDIOSTREAM_SUCCESS} 函数执行成功。
 *         {@link AUDIOSTREAM_ERROR_INVALID_PARAM}：
 *                                                 1. 参数renderer为nullptr；
 *                                                 2. 参数samplePos无效。
 *         {@link AUDIOSTREAM_ERROR_ILLEGAL_STATE} 执行状态异常。
 *         {@link AUDIOSTREAM_ERROR_SYSTEM} 出现系统错误。
 */
OH_AudioStream_Result OH_AudioRenderer_SetMarkPosition(OH_AudioRenderer* renderer, uint32_t samplePos,
    OH_AudioRenderer_OnMarkReachedCallback callback, void* userData);

/**
 * @brief 取消由{@link #OH_AudioRenderer_SetMarkPosition}设置的标记。
 *
 * @since 12
 * @syscap SystemCapability.Multimedia.Audio.Core
 * @param renderer 指向{@link OH_AudioStreamBuilder_GenerateRenderer}创建的音频流实例。
 * @return 函数返回值：
 *         {@link AUDIOSTREAM_SUCCESS} 函数执行成功。
 *         {@link AUDIOSTREAM_ERROR_INVALID_PARAM} 参数renderer为nullptr。
 */
OH_AudioStream_Result OH_AudioRenderer_CancelMark(OH_AudioRenderer* renderer);

/**
 * @brief 设置当前音频流音量值。
 * @syscap SystemCapability.Multimedia.Audio.Core
 * @since 12
 * @param renderer 指向{@link OH_AudioStreamBuilder_GenerateRenderer}创建的音频流实例。
 * @param volume 设置当前音频流音量，音量值范围[0.0, 1.0]。
 * @return 函数返回值：
 *         {@link AUDIOSTREAM_SUCCESS} 函数执行成功。
 *         {@link AUDIOSTREAM_ERROR_INVALID_PARAM}：
 *                                                 1. 参数renderer为nullptr；
 *                                                 2. 参数volume无效。
 *         {@link AUDIOSTREAM_ERROR_ILLEGAL_STATE} 执行状态异常。
 *         {@link AUDIOSTREAM_ERROR_SYSTEM} 出现系统错误。
 */
OH_AudioStream_Result OH_AudioRenderer_SetVolume(OH_AudioRenderer* renderer, float volume);

/**
 * @brief 在指定时间范围内使用渐变更改音量。
 * @syscap SystemCapability.Multimedia.Audio.Core
 * @since 12
 * @param renderer 指向{@link OH_AudioStreamBuilder_GenerateRenderer}创建的音频流实例。
 * @param volume 目标音量值，取值范围[0.0, 1.0]。
 * @param durationMs 音量渐变的持续时间，以毫秒为单位。
 * @return 函数返回值：
 *         {@link AUDIOSTREAM_SUCCESS} 函数执行成功。
 *         {@link AUDIOSTREAM_ERROR_INVALID_PARAM}：
 *                                                 1. 参数renderer为nullptr；
 *                                                 2. 参数volume无效。
 *         {@link AUDIOSTREAM_ERROR_ILLEGAL_STATE} 执行状态异常。
 *         {@link AUDIOSTREAM_ERROR_SYSTEM} 出现系统错误。
 */
OH_AudioStream_Result OH_AudioRenderer_SetVolumeWithRamp(OH_AudioRenderer* renderer, float volume, int32_t durationMs);

/**
 * @brief 获取当前音频流音量值。
 * @syscap SystemCapability.Multimedia.Audio.Core
 * @since 12
 * @param renderer 指向{@link OH_AudioStreamBuilder_GenerateRenderer}创建的音频流实例。
 * @param volume 指向一个获取当前音频流音量值的指针。
 * @return 函数返回值：
 *         {@link AUDIOSTREAM_SUCCESS} 函数执行成功。
 *         {@link AUDIOSTREAM_ERROR_INVALID_PARAM}：
 *                                                 1. 参数renderer为nullptr；
 *                                                 2. 参数volume为nullptr。
 */
OH_AudioStream_Result OH_AudioRenderer_GetVolume(OH_AudioRenderer* renderer, float* volume);

/**
 * @brief 查询当前播放音频流欠载数。
 *
 * @param renderer 指向{@link OH_AudioStreamBuilder_GenerateRenderer}创建的音频流实例。
 * @param count 指向一个用来接收音频流欠载数的变量的指针。
 * @return 函数返回值：
 *         {@link AUDIOSTREAM_SUCCESS} 函数执行成功。
 *         {@link AUDIOSTREAM_ERROR_INVALID_PARAM}：
 *                                                 1. 参数renderer为nullptr；
 *                                                 2. 参数count为nullptr。
 * @since 12
 */
OH_AudioStream_Result OH_AudioRenderer_GetUnderflowCount(OH_AudioRenderer* renderer, uint32_t* count);

/**
 * @brief 查询当前音频流声道布局。
 *
 * @since 12
 * @syscap SystemCapability.Multimedia.Audio.Core
 * @param renderer 指向{@link OH_AudioStreamBuilder_GenerateRenderer}创建的音频流实例。
 * @param channelLayout 指向一个用来接收音频流声道布局的变量的指针。
 * @return 函数返回值：
 *         {@link AUDIOSTREAM_SUCCESS} 函数执行成功。
 *         {@link AUDIOSTREAM_ERROR_INVALID_PARAM} 参数renderer为nullptr。
 */
OH_AudioStream_Result OH_AudioRenderer_GetChannelLayout(OH_AudioRenderer* renderer,
    OH_AudioChannelLayout* channelLayout);

/**
 * @brief 查询当前音频流音效模式。
 *
 * @since 12
 * @syscap SystemCapability.Multimedia.Audio.Core
 * @param renderer 指向{@link OH_AudioStreamBuilder_GenerateRenderer}创建的音频流实例。
 * @param effectMode 指向一个用来接收音频流音效模式的变量的指针。
 * @return 函数返回值：
 *         {@link AUDIOSTREAM_SUCCESS} 函数执行成功。
 *         {@link AUDIOSTREAM_ERROR_INVALID_PARAM} 参数renderer为nullptr。
 */
OH_AudioStream_Result OH_AudioRenderer_GetEffectMode(OH_AudioRenderer* renderer,
    OH_AudioStream_AudioEffectMode* effectMode);

/**
 * @brief 设置当前音频流音效模式。
 *
 * @since 12
 * @syscap SystemCapability.Multimedia.Audio.Core
 * @param renderer 指向{@link OH_AudioStreamBuilder_GenerateRenderer}创建的音频流实例。
 * @param effectMode 设置当前音频流的目标音效模式。
 * @return 函数返回值：
 *         {@link AUDIOSTREAM_SUCCESS} 函数执行成功。
 *         {@link AUDIOSTREAM_ERROR_INVALID_PARAM} 参数renderer为nullptr。
 */
OH_AudioStream_Result OH_AudioRenderer_SetEffectMode(OH_AudioRenderer* renderer,
    OH_AudioStream_AudioEffectMode effectMode);

/**
 * @brief 查询当前播放音频流是否会被其它应用录制。
 *
 * @since 12
 * @syscap SystemCapability.Multimedia.Audio.PlaybackCapture
 * @param renderer 指向{@link OH_AudioStreamBuilder_GenerateRenderer}创建的音频流实例。
 * @param privacy 用于返回当前流的内录策略。
 * @return 函数返回值：
 *         {@link AUDIOSTREAM_SUCCESS} 函数执行成功。
 *         {@link AUDIOSTREAM_ERROR_INVALID_PARAM} 参数renderer为nullptr。
 */
OH_AudioStream_Result OH_AudioRenderer_GetRendererPrivacy(OH_AudioRenderer* renderer,
    OH_AudioStream_PrivacyType* privacy);

/**
 * @brief 设置静音并发播放模式。
 *
 * @since 12
 * @syscap SystemCapability.Multimedia.Audio.Core
 * @param renderer 指向{@link OH_AudioStreamBuilder_GenerateRenderer}创建的音频流实例。
 * @param on 设置当前音频流的静音并发状态。
 *     true: 设置当前播放的音频流静音播放，并且不会打断其它音频流播放。
 *     false: 取消当前播放的音频流静音播放，音频流可根据系统焦点策略抢占焦点。
 * @return 函数返回值：
 *     {@link #AUDIOSTREAM_SUCCESS} 函数执行成功。
 *     {@link AUDIOSTREAM_ERROR_INVALID_PARAM} 参数renderer为nullptr。
 */
OH_AudioStream_Result OH_AudioRenderer_SetSilentModeAndMixWithOthers(
    OH_AudioRenderer* renderer, bool on);

/**
 * @brief 查询当前音频流是否开启静音并发播放。
 *
 * @since 12
 * @syscap SystemCapability.Multimedia.Audio.Core
 * @param renderer 指向{@link OH_AudioStreamBuilder_GenerateRenderer}创建的音频流实例。
 * @param on 用于返回当前流的静音并发状态。
 * @return 函数返回值：
 *     {@link #AUDIOSTREAM_SUCCESS} 函数执行成功。
 *     {@link AUDIOSTREAM_ERROR_INVALID_PARAM} 参数renderer为nullptr。
 */
OH_AudioStream_Result OH_AudioRenderer_GetSilentModeAndMixWithOthers(
    OH_AudioRenderer* renderer, bool* on);

/**
 * @brief 设置默认本机内置发声设备。
 *
 * 本接口仅适用于音频流类型{@link OH_AudioStream_Usage}为语音消息、VoIP语音通话或者VoIP视频通话的场景使用，
 * 以及可选的设备类型为听筒、扬声器和系统默认设备。
 *
 * 本接口允许在AudioRenderer创建以后的任何时间被调用，系统会记录应用设置的默认本机内置发声设备。在应用启动播放时，若有外接设备如蓝牙耳机/有线耳机接入，
 * 系统优先从外接设备发声；否则系统遵循应用设置的默认本机内置发声设备发声。
 *
 * @since 12
 * @syscap SystemCapability.Multimedia.Audio.Core
 * @param renderer 指向{@link OH_AudioStreamBuilder_GenerateRenderer}创建的音频流实例。
 * @param deviceType 指向{@link OH_AudioDevice_Type}用于设置发声设备类型。可设置的设备类型包括:
 *                                             AUDIO_DEVICE_TYPE_EARPIECE: 听筒
 *                                             AUDIO_DEVICE_TYPE_SPEAKER: 扬声器
 *                                             AUDIO_DEVICE_TYPE_DEFAULT: 系统默认设备
 * @return 函数返回值：
 *         {@link #AUDIOSTREAM_SUCCESS} 函数执行成功。
 *         {@link #AUDIOSTREAM_ERROR_INVALID_PARAM}:
 *                                                  1. 参数renderer为nullptr;
 *                                                  2. 参数deviceType无效。
 *         {@link #AUDIOSTREAM_ERROR_ILLEGAL_STATE} 执行状态异常。
 *         {@link #AUDIOSTREAM_ERROR_SYSTEM} 出现系统错误。
 * @since 12
 */
OH_AudioStream_Result OH_AudioRenderer_SetDefaultOutputDevice(
    OH_AudioRenderer* renderer, OH_AudioDevice_Type deviceType);

#ifdef __cplusplus
}
#endif
#endif // NATIVE_AUDIORENDERER_H
/** @} */
