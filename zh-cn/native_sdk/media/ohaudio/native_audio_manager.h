/*
 * Copyright (c) 2024 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup OHAudio
 * @{
 *
 * @brief 提供音频模块C接口定义。
 *
 * @syscap SystemCapability.Multimedia.Audio.Core
 *
 * @since 12
 * @version 1.0
 */

/**
 * @file native_audio_manager.h
 *
 * @brief 声明音频管理相关的接口。
 *
 * @library libohaudio.so
 * @syscap SystemCapability.Multimedia.Audio.Core
 * @since 12
 * @version 1.0
 */
#ifndef NATIVE_AUDIO_MANAGER_H
#define NATIVE_AUDIO_MANAGER_H

#include "native_audio_common.h"
#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief 声明音频管理器。
 * 
 * 用于管理音频管理相关功能。
 *
 * @since 12
 */
typedef struct OH_AudioManager OH_AudioManager;

/**
 * @brief 获取音频管理器。
 * 
 * 使用音频管理器相关功能，首先需要获取音频管理器实例。
 *
 * @param audioManager 指向{@link OH_AudioManager}用于接收创建的音频管理器实例。
 * @return 函数返回值:
 *         {@link AUDIOCOMMON_RESULT_SUCCESS} 函数执行成功。
 *         {@link AUDIOCOMMON_RESULT_ERROR_INVALID_PARAM}:
 *                                                        参数audioManager为nullptr。
 * @since 12
 */
OH_AudioCommon_Result OH_GetAudioManager(OH_AudioManager **audioManager);

/**
 * @brief 获取音频场景模式。
 * @param audioManager 指向{@link OH_GetAudioManager}创建的音频管理器实例：{@link OH_AudioManager}。
 * @param scene 指向{@link OH_AudioScene}用于接收返回的音频场景模式。
 * @return 函数返回值:
 *         {@link AUDIOCOMMON_RESULT_SUCCESS} 函数执行成功。
 *         {@link AUDIOCOMMON_RESULT_ERROR_INVALID_PARAM}:
 *                                                        1.参数audioManager为nullptr;
 *                                                        2.参数scene为nullptr。
 * @since 12
 */
OH_AudioCommon_Result OH_GetAudioScene(OH_AudioManager* manager, OH_AudioScene *scene);

#ifdef __cplusplus
}
#endif
/** @} */
#endif // NATIVE_AUDIO_MANAGER_H