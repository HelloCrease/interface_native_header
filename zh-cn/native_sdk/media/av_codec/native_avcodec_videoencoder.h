/*
 * Copyright (C) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup VideoEncoder
 * @{
 * 
 * @brief VideoEncoder模块提供用于视频编码的接口。
 * 
 * @syscap SystemCapability.Multimedia.VideoEncoder
 * @since 9
 */


/**
 * @file native_avcodec_videoencoder.h
 * 
 * @brief 声明用于视频编码的接口。
 * 
 * @library libnative_media_venc.so
 * @since 9
 */

#ifndef NATIVE_AVCODEC_VIDEOENCODER_H
#define NATIVE_AVCODEC_VIDEOENCODER_H

#include <stdint.h>
#include <stdio.h>
#include "native_avcodec_base.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief 配置随帧参数，当需要设置index对应帧的编码参数时，可以通过该接口设置。只在Surface模式生效。
 *
 * @syscap SystemCapability.Multimedia.Media.VideoEncoder
 * @param codec 指向视频编码实例的指针。
 * @param index 对应编码帧的index。
 * @param parameter  编码参数。
 * @param userData 调用者执行回调所依赖的数据。
 * @since 12
 */
typedef void (*OH_VideoEncoder_OnNeedInputParameter)(OH_AVCodec *codec, uint32_t index, OH_AVFormat *parameter,
                                                     void *userData);
/**
 * @brief 根据MIME类型创建视频编码器实例，推荐使用。
 * 
 * @syscap SystemCapability.Multimedia.Media.VideoEncoder
 * @param mime MIME类型描述字符串，请参阅{@link AVCODEC_MIME_TYPE}。
 * @return 成功则返回一个指向视频编码实例的指针。
 * 如果输入为不支持的编码器类型或内存不足时，则返回NULL。
 * @since 9
 * @version 1.0
 */
OH_AVCodec *OH_VideoEncoder_CreateByMime(const char *mime);

/**
 * @brief 根据视频编码器名称创建视频编码器实例。使用此接口的前提是知道编码器的确切名称，编码器的名称可以通过能力查询获取。
 * 
 * @syscap SystemCapability.Multimedia.Media.VideoEncoder
 * @param name 视频编码器名称。
 * @return 成功则返回一个指向视频编码实例的指针。
 * 如果输入是不支持编码器名称或者内存资源不足，则返回NULL。
 * @since 9
 * @version 1.0
 */
OH_AVCodec *OH_VideoEncoder_CreateByName(const char *name);

/**
 * @brief 清理编码器内部资源，销毁编码器实例。不能重复销毁。
 * 
 * @syscap SystemCapability.Multimedia.Media.VideoEncoder
 * @param codec 指向视频编码实例的指针。
 * @return 返回接口结果：\n
 *         {@link AV_ERR_OK}执行成功。\n
 *         {@link AV_ERR_NO_MEMORY}输入的编码实例内部异常，如内部出现异常空指针。\n
 *         {@link AV_ERR_INVALID_VAL}输入的codec指针为非编码器实例，或者为空指针。\n
 *         {@link AV_ERR_UNKNOWN}未知错误。\n
 *         {@AV_ERR_OPERATE_NOT_PERMIT}内部执行错误。
 * @since 9
 * @version 1.0
 */
OH_AVErrCode OH_VideoEncoder_Destroy(OH_AVCodec *codec);

/**
 * @brief 设置OH_AVCodecCallback回调函数，让应用可以响应视频编码器生成的事件。在调用OH_VideoEncoder_Prepare接口之前，必须调用此接口。
 * 
 * @syscap SystemCapability.Multimedia.Media.VideoEncoder
 * @param codec 指向视频编码实例的指针。
 * @param callback 所有回调函数的集合，请参阅{@link OH_AVCodecAsyncCallback}。
 * @param userData 调用者执行回调所依赖的数据。
 * @return 返回接口结果：\n
 *         {@link AV_ERR_OK}执行成功。\n
 *         {@link AV_ERR_NO_MEMORY}输入的编码实例内部异常，如内部出现异常空指针。\n
 *         {@link AV_ERR_INVALID_VAL}输入的codec指针为非编码器实例，或者为空指针。\n
 *         {@link AV_ERR_UNKNOWN}未知错误。\n
 *         {@AV_ERR_OPERATE_NOT_PERMIT}内部执行错误。\n
 *         {@AV_ERR_INVALID_STATE}本接口必须在OH_VideoEncoder_Prepare接口前调用，如果在其他状态时调用，则返回此错误码。
 * @deprecated since 11
 * @useinstead OH_VideoEncoder_RegisterCallback
 * @since 9
 * @version 1.0
 */
OH_AVErrCode OH_VideoEncoder_SetCallback(OH_AVCodec *codec, OH_AVCodecAsyncCallback callback, void *userData);

/**
 * @brief 注册OH_AVCodecCallback回调函数，让应用可以响应视频编码器生成的事件。在调用OH_VideoEncoder_Prepare接口之前，必须调用此接口。
 * 
 * @syscap SystemCapability.Multimedia.Media.VideoEncoder
 * @param codec 指向视频编码实例的指针。
 * @param callback 所有回调函数的集合，请参阅{@link OH_AVCodecCallback}。
 * @param userData 调用者执行回调所依赖的数据。
 * @return 返回接口结果：\n
 *         {@link AV_ERR_OK}执行成功。\n
 *         {@link AV_ERR_NO_MEMORY}输入的编码实例内部异常，如内部出现异常空指针。\n
 *         {@link AV_ERR_INVALID_VAL}输入的codec指针为非编码器实例，或者为空指针。\n
 *         {@link AV_ERR_UNKNOWN}未知错误。\n
 *         {@AV_ERR_OPERATE_NOT_PERMIT}内部执行错误。\n
 *         {@AV_ERR_INVALID_STATE}本接口必须在OH_VideoEncoder_Prepare接口前调用，如果在其他状态时调用，则返回此错误码。
 * @since 11
 */
OH_AVErrCode OH_VideoEncoder_RegisterCallback(OH_AVCodec *codec, OH_AVCodecCallback callback, void *userData);

/**
 * @brief 注册OH_AVCodecCallback输入参数回调函数，让应用可以响应视频编码器生成的事件。编码Surface模式，需要设置随帧参数时，须使用该接口。
 * 如果使用该接口，必须在{@link OH_VideoEncoder_Configure}之前调用该接口。
 *
 * @syscap SystemCapability.Multimedia.Media.VideoEncoder
 * @param codec 指向视频编码实例的指针。
 * @param onInputParameter 输入参数回调指针, 请参阅{@link OH_VideoEncoder_OnNeedInputParameter}。
 * @param userData 调用者执行回调所依赖的数据。
 * @return 返回接口结果：\n
 *         {@link AV_ERR_OK}执行成功。\n
 *         {@link AV_ERR_NO_MEMORY}输入的编码实例内部异常，如内部出现异常空指针。\n
 *         {@link AV_ERR_INVALID_VAL}输入的codec指针为非编码器实例，或者为空指针。\n
 *         {@link AV_ERR_UNKNOWN}未知错误。\n
 *         {@AV_ERR_OPERATE_NOT_PERMIT}内部执行错误。\n
 *         {@AV_ERR_INVALID_STATE}本接口必须在OH_VideoEncoder_Prepare接口前调用，如果在其他状态时调用，则返回此错误码。
 * @since 12
 */
OH_AVErrCode OH_VideoEncoder_RegisterParameterCallback(OH_AVCodec *codec,
                                                       OH_VideoEncoder_OnNeedInputParameter onInputParameter,
                                                       void *userData);

/**
 * @brief 配置视频编码器的编码参数，通常需要配置要编码的视频轨的描述信息，如宽、高、像素格式等。必须在调用OH_VideoEncoder_Prepare接口之前，调用此接口。
 * 
 * @syscap SystemCapability.Multimedia.Media.VideoEncoder
 * @param codec 指向视频编码实例的指针。
 * @param format 指向OH_AVFormat的指针，用于给出要编码的视频轨的描述。
 * @return 返回接口结果：\n
 *         {@link AV_ERR_OK}执行成功。\n
 *         {@link AV_ERR_NO_MEMORY}输入的编码实例内部异常，如内部出现异常空指针。\n
 *         {@link AV_ERR_INVALID_VAL}1. 输入的codec指针为非编码器实例，或者为空指针；2. 输入format参数不支持。\n
 *         {@link AV_ERR_UNKNOWN}未知错误。\n
 *         {@AV_ERR_OPERATE_NOT_PERMIT}内部执行错误。\n
 *         {@AV_ERR_INVALID_STATE}本接口必须在OH_VideoEncoder_Prepare接口前调用，如果在其他状态时调用，则返回此错误码。
 * @since 9
 * @version 1.0
 */
OH_AVErrCode OH_VideoEncoder_Configure(OH_AVCodec *codec, OH_AVFormat *format);

/**
 * @brief 准备编码器的内部资源，在OH_VideoEncoder_Configure接口后调用。
 * 
 * @syscap SystemCapability.Multimedia.Media.VideoEncoder
 * @param codec 指向视频编码实例的指针。
 * @return 返回接口结果：\n
 *         {@link AV_ERR_OK}执行成功。\n
 *         {@link AV_ERR_INVALID_VAL}输入的codec指针为非编码器实例，或者为空指针。\n
 *         {@AV_ERR_OPERATE_NOT_PERMIT}内部执行错误。
 * @since 9
 * @version 1.0
 */
OH_AVErrCode OH_VideoEncoder_Prepare(OH_AVCodec *codec);

/**
 * @brief 调用OH_VideoEncoder_Prepare接口成功后调用此接口启动解码器。成功启动后，编码器将开始报告注册的回调事件。
 * 
 * @syscap SystemCapability.Multimedia.Media.VideoEncoder
 * @param codec 指向视频编码实例的指针。
 * @return 返回接口结果：\n
 *         {@link AV_ERR_OK}执行成功。\n
 *         {@link AV_ERR_NO_MEMORY}输入的编码实例内部异常，如内部出现异常空指针。\n
 *         {@link AV_ERR_INVALID_VAL}输入的codec指针为非编码器实例，或者为空指针。\n
 *         {@link AV_ERR_UNKNOWN}未知错误。\n
 *         {@AV_ERR_OPERATE_NOT_PERMIT}内部执行错误。\n
 *         {@AV_ERR_INVALID_STATE}编码器状态不支持调用本接口时调用。
 * @since 9
 * @version 1.0
 */
OH_AVErrCode OH_VideoEncoder_Start(OH_AVCodec *codec);

/**
 * @brief 停止编码器，释放输入输出buffer。停止之后，可以通过调用OH_VideoEncoder_Start接口重新进入Running状态。
 * 
 * @syscap SystemCapability.Multimedia.Media.VideoEncoder
 * @param codec 指向视频编码实例的指针。
 * @return 返回接口结果：\n
 *         {@link AV_ERR_OK}执行成功。\n
 *         {@link AV_ERR_NO_MEMORY}输入的编码实例内部异常，如内部出现异常空指针。\n
 *         {@link AV_ERR_INVALID_VAL}输入的codec指针为非编码器实例，或者为空指针。\n
 *         {@link AV_ERR_UNKNOWN}未知错误。\n
 *         {@AV_ERR_OPERATE_NOT_PERMIT}内部执行错误。\n
 *         {@AV_ERR_INVALID_STATE}编码器状态不支持调用本接口时调用。
 * @since 9
 * @version 1.0
 */
OH_AVErrCode OH_VideoEncoder_Stop(OH_AVCodec *codec);

/**
 * @brief 清除编码器中缓存的输入和输出数据及参数集如H264格式的PPS/SPS。
 * 
 * 调用此接口后，以前通过异步回调上报的所有缓冲区index都将失效，请确保不要访问这些index对应的缓冲区。该接口不能连续调用。
 * @syscap SystemCapability.Multimedia.Media.VideoEncoder
 * @param codec 指向视频编码实例的指针。
 * @return 返回接口结果：\n
 *         {@link AV_ERR_OK}执行成功。\n
 *         {@link AV_ERR_NO_MEMORY}输入的编码实例内部异常，如内部出现异常空指针。\n
 *         {@link AV_ERR_INVALID_VAL}输入的codec指针为非编码器实例，或者为空指针。\n
 *         {@link AV_ERR_UNKNOWN}未知错误。\n
 *         {@AV_ERR_OPERATE_NOT_PERMIT}内部执行错误。\n
 *         {@AV_ERR_INVALID_STATE}编码器状态不支持调用本接口时调用。
 * @since 9
 * @version 1.0
 */
OH_AVErrCode OH_VideoEncoder_Flush(OH_AVCodec *codec);

/**
 * @brief 重置编码器，编码器回到初始化状态。如果要继续编码，需要再次调用OH_VideoEncoder_Configure接口配置编码器实例。
 * 
 * @syscap SystemCapability.Multimedia.Media.VideoEncoder
 * @param codec 指向视频编码实例的指针。
 * @return 返回接口结果：\n
 *         {@link AV_ERR_OK}执行成功。\n
 *         {@link AV_ERR_NO_MEMORY}输入的编码实例内部异常，如内部出现异常空指针。\n
 *         {@link AV_ERR_INVALID_VAL}输入的codec指针为非编码器实例，或者为空指针。\n
 *         {@link AV_ERR_UNKNOWN}未知错误。\n
 *         {@AV_ERR_OPERATE_NOT_PERMIT}内部执行错误。
 * @since 9
 * @version 1.0
 */
OH_AVErrCode OH_VideoEncoder_Reset(OH_AVCodec *codec);

/**
 * @brief 获取编码器输出数据的OH_AVFormat信息。请参阅{@link OH_AVFormat}。
 * 
 * 需要注意的是，返回值指向的OH_AVFormat实例的生命周期需要调用者通过调用接口OH_AVFormat_Destroy()释放。
 * @syscap SystemCapability.Multimedia.Media.VideoEncoder
 * @param codec 指向视频编码实例的指针。
 * @return 返回指向OH_AVFormat实例的指针。
 * 当输入的codec指针非编码实例，或者为空指针，则返回NULL。
 * @since 9
 * @version 1.0
 */
OH_AVFormat *OH_VideoEncoder_GetOutputDescription(OH_AVCodec *codec);

/**
 * @brief 在编码器运行时设置编码器参数。
 * 注意，此接口只有在编码器启动后才能调用。 同时，不正确的参数设置可能会导致编码失败。
 * 
 * @syscap SystemCapability.Multimedia.Media.VideoEncoder
 * @param codec 指向视频编码实例的指针。
 * @param format 指向OH_AVFormat实例的指针。
 * @return 返回接口结果：\n
 *         {@link AV_ERR_OK}执行成功。\n
 *         {@link AV_ERR_NO_MEMORY}输入的编码实例内部异常，如内部出现异常空指针。\n
 *         {@link AV_ERR_INVALID_VAL}1. 输入的codec指针为非编码器实例，或者为空指针；2. 输入format参数不支持。\n
 *         {@link AV_ERR_UNKNOWN}未知错误。\n
 *         {@AV_ERR_OPERATE_NOT_PERMIT}内部执行错误。\n
 *         {@AV_ERR_INVALID_STATE}编码器状态不支持调用本接口时调用。
 * @since 9
 * @version 1.0
 */
OH_AVErrCode OH_VideoEncoder_SetParameter(OH_AVCodec *codec, OH_AVFormat *format);

/**
 * @brief 从视频编码器获取输入surface，必须在调用OH_VideoEncoder_Prepare接口之前调用此接口。
 * 
 * @syscap SystemCapability.Multimedia.Media.VideoEncoder
 * @param codec 指向视频编码实例的指针。
 * @param window 指向OHNativeWindow实例的指针, 请参阅{@link OHNativeWindow}。
 * 应用负责管理window的生命周期，结束时调用{@link OH_NativeWindow_DestroyNativeWindow}释放。
 * @return 返回接口结果：\n
 *         {@link AV_ERR_OK}执行成功。\n
 *         {@link AV_ERR_INVALID_VAL}输入的codec指针为非编码器实例，或者为空指针。\n
 *         {@link AV_ERR_UNKNOWN}未知错误。\n
 *         {@AV_ERR_OPERATE_NOT_PERMIT}内部执行错误。
 * @since 9
 * @version 1.0
 */
OH_AVErrCode OH_VideoEncoder_GetSurface(OH_AVCodec *codec, OHNativeWindow **window);

/**
 * @brief 将处理后的输出缓冲区返回给编码器。
 * 
 * @syscap SystemCapability.Multimedia.Media.VideoEncoder
 * @param codec 指向视频编码实例的指针。
 * @param index 输出缓冲区对应的索引值。由{@link OH_AVCodecOnNewOutputData}给出。
 * @return 返回接口结果：\n
 *         {@link AV_ERR_OK}执行成功。\n
 *         {@link AV_ERR_NO_MEMORY}输入的编码实例内部异常，如内部出现异常空指针。\n
 *         {@link AV_ERR_INVALID_VAL}输入的codec指针为非编码器实例，或者为空指针。\n
 *         {@link AV_ERR_UNKNOWN}未知错误。\n
 *         {@AV_ERR_OPERATE_NOT_PERMIT}内部执行错误。\n
 *         {@AV_ERR_INVALID_STATE}编码器状态不支持调用本接口时调用。
 * @deprecated since 11
 * @useinstead OH_VideoEncoder_FreeOutputBuffer
 * @since 9
 * @version 1.0
 */
OH_AVErrCode OH_VideoEncoder_FreeOutputData(OH_AVCodec *codec, uint32_t index);

/**
 * @brief 通知视频编码器输入流已结束。建议使用此接口进行通知。该接口只在Surface模式下使用，
 * Buffer模式通过OH_AVBuffer携带EOS信息，通知输入流的结束。
 * 
 * @syscap SystemCapability.Multimedia.Media.VideoEncoder
 * @param codec 指向视频编码实例的指针。
 * @return 返回接口结果：\n
 *         {@link AV_ERR_OK}执行成功。\n
 *         {@link AV_ERR_NO_MEMORY}输入的编码实例内部异常，如内部出现异常空指针。\n
 *         {@link AV_ERR_INVALID_VAL}输入的codec指针为非编码器实例，或者为空指针。\n
 *         {@link AV_ERR_UNKNOWN}未知错误。\n
 *         {@AV_ERR_OPERATE_NOT_PERMIT}内部执行错误。\n
 *         {@AV_ERR_INVALID_STATE}编码器状态不支持调用本接口时调用。
 * @since 9
 * @version 1.0
 */
OH_AVErrCode OH_VideoEncoder_NotifyEndOfStream(OH_AVCodec *codec);

/**
 * @brief 将填入数据的输入缓冲区提交给视频编码器。
 * 
 * @syscap SystemCapability.Multimedia.Media.VideoEncoder
 * @param codec 指向视频编码实例的指针。
 * @param index 输入缓冲区对应的索引值。
 * @param attr 缓冲区中包含数据的描述信息。
 * @return 返回接口结果：\n
 *         {@link AV_ERR_OK}执行成功。\n
 *         {@link AV_ERR_NO_MEMORY}输入的编码实例内部异常，如内部出现异常空指针。\n
 *         {@link AV_ERR_INVALID_VAL}输入的codec指针为非编码器实例，或者为空指针。\n
 *         {@link AV_ERR_UNKNOWN}未知错误。\n
 *         {@AV_ERR_OPERATE_NOT_PERMIT}内部执行错误。\n
 *         {@AV_ERR_INVALID_STATE}编码器状态不支持调用本接口时调用。
 * @deprecated since 11
 * @useinstead OH_VideoEncoder_PushInputBuffer
 * @since 10
 */
OH_AVErrCode OH_VideoEncoder_PushInputData(OH_AVCodec *codec, uint32_t index, OH_AVCodecBufferAttr attr);

/**
 * @brief Buffer模式下，将index对应的OH_AVBuffer送入编码器编码。
 * 
 * @syscap SystemCapability.Multimedia.Media.VideoEncoder
 * @param codec 指向视频编码实例的指针。
 * @param index 输入缓冲区对应的索引值。由{@link OH_AVCodecOnNeedInputBuffer}给出。
 * @return 返回接口结果：\n
 *         {@link AV_ERR_OK}执行成功。\n
 *         {@link AV_ERR_NO_MEMORY}输入的编码实例内部异常，如内部出现异常空指针。\n
 *         {@link AV_ERR_INVALID_VAL}1. 输入的codec指针为非编码器实例，或者为空指针；2. 输入format参数不支持。\n
 *         {@link AV_ERR_UNKNOWN}未知错误。\n
 *         {@AV_ERR_OPERATE_NOT_PERMIT}内部执行错误。\n
 *         {@AV_ERR_INVALID_STATE}编码器状态不支持调用本接口时调用。
 * @since 11
 */
OH_AVErrCode OH_VideoEncoder_PushInputBuffer(OH_AVCodec *codec, uint32_t index);

/**
 * @brief Surface模式下，将index对应帧的编码参数送入编码器编码。
 *
 * @syscap SystemCapability.Multimedia.Media.VideoEncoder
 * @param codec 指向视频编码实例的指针。
 * @param index 输入参数缓冲区对应的索引值。由{@link OH_AVCodecOnNeedInputBuffer}给出。
 * @return 返回接口结果：\n
 *         {@link AV_ERR_OK}执行成功。\n
 *         {@link AV_ERR_NO_MEMORY}输入的编码实例内部异常，如内部出现异常空指针。\n
 *         {@link AV_ERR_INVALID_VAL}输入的codec指针为非编码器实例，或者为空指针。\n
 *         {@link AV_ERR_UNKNOWN}未知错误。\n
 *         {@AV_ERR_OPERATE_NOT_PERMIT}内部执行错误。\n
 *         {@AV_ERR_INVALID_STATE}编码器状态不支持调用本接口时调用。
 * @since 12
 */
OH_AVErrCode OH_VideoEncoder_PushInputParameter(OH_AVCodec *codec, uint32_t index);

/**
 * @brief 将处理后的index对应的OH_AVBuffer返回给编码器。调用者使用完需要及时调用此接口释放输出缓存区，否则会阻塞编码流程。
 * 
 * @syscap SystemCapability.Multimedia.Media.VideoEncoder
 * @param codec 指向视频编码实例的指针。
 * @param index 输出缓冲区对应的索引值。由{@link OH_AVCodecOnNeedInputBuffer}给出。
 * @return 返回接口结果：\n
 *         {@link AV_ERR_OK}执行成功。\n
 *         {@link AV_ERR_NO_MEMORY}输入的编码实例内部异常，如内部出现异常空指针。\n
 *         {@link AV_ERR_INVALID_VAL}
 *         1. 输入的 codec 指针为非编码器实例，或者为空指针；
 *         2. 输入format参数不支持；
 *         3. index非法或者连续给同一个index，该错误不影响后续编码流程。\n
 *         {@link AV_ERR_UNKNOWN}未知错误。\n
 *         {@AV_ERR_OPERATE_NOT_PERMIT}内部执行错误。\n
 *         {@AV_ERR_INVALID_STATE}编码器状态不支持调用本接口时调用。
 * @since 11
 */
OH_AVErrCode OH_VideoEncoder_FreeOutputBuffer(OH_AVCodec *codec, uint32_t index);

/**
 * @brief 编码器接收到的图像的描述信息，调用{@OH_VideoEncoder_Configure}后调用此接口，请参阅{@link OH_AVFormat}获取详细信息。
 * 需要注意的是，返回指针所指向的OH_AVFormat实例的生命周期需要由调用者通过调用OH_AVFormat_Destroy接口释放，请参阅{@link OH_AVFormat_Destory}。
 * 
 * @syscap SystemCapability.Multimedia.Media.VideoEncoder
 * @param codec 指向视频编码实例的指针。
 * @return 返回指向OH_AVFormat实例的指针。
 * 当codec指针非编码实例，或者为空指针，则返回NULL。
 * @since 10
 */
OH_AVFormat *OH_VideoEncoder_GetInputDescription(OH_AVCodec *codec);

/**
 * @brief 检查当前编码实例是否有效。
 * 
 * @syscap SystemCapability.Multimedia.Media.VideoEncoder
 * @param codec 指向视频编码实例的指针。
 * @param isValid 输出参数，指向布尔类型的指针。指向布尔实例的指针，
 * 如果编码器实例有效，则为true，如果编码器实例无效，则为false。建议调用者将isValid初始化为false。
 * @return 返回接口结果：\n
 *         {@link AV_ERR_OK}执行成功。\n
 *         {@link AV_ERR_INVALID_VAL}输入的codec指针为非编码器实例，或者为空指针。
 * @since 10
 */
OH_AVErrCode OH_VideoEncoder_IsValid(OH_AVCodec *codec, bool *isValid);

/**
 * @brief 视频编码器的码率控制模式。
 * 
 * @syscap SystemCapability.Multimedia.Media.VideoEncoder
 * @since 9
 * @version 1.0
 */
typedef enum OH_VideoEncodeBitrateMode {
    /** 恒定码率模式。 */
    CBR = 0,
    /** 可变码率模式。 */
    VBR = 1,
    /** 恒定质量模式。 */
    CQ = 2,
} OH_VideoEncodeBitrateMode;

#ifdef __cplusplus
}
#endif

#endif // NATIVE_AVCODEC_VIDEOENCODER_H

/** @} */