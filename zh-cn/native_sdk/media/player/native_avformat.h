/*
 * Copyright (C) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup Core
 * @{
 *
 * @brief Core模块提供用于播放框架的基础骨干能力，包含内存、错误码、格式载体等相关函数。
 * 
 * @syscap SystemCapability.Multimedia.Media.Core
 *
 * @since 9
 * @version 1.0
 */

/**
 * @file native_avformat.h
 *
 * @brief 声明了格式相关的函数和枚举。
 *
 * @since 9
 * @version 1.0
 */

#ifndef NATIVE_AVFORMAT_H
#define NATIVE_AVFORMAT_H

#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>

#ifdef __cplusplus
extern "C" {
#endif

typedef struct OH_AVFormat OH_AVFormat;

/**
 * @brief AVPixel 格式的枚举。
 * 
 * @syscap SystemCapability.Multimedia.Media.Core
 * @since 9
 * @version 1.0
 */
typedef enum OH_AVPixelFormat {
    /**
     * yuv 420 planar
     */
    AV_PIXEL_FORMAT_YUVI420 = 1,
    /**
     *  NV12. yuv 420 semiplanar
     */
    AV_PIXEL_FORMAT_NV12 = 2,
    /**
     *  NV21. yvu 420 semiplanar
     */
    AV_PIXEL_FORMAT_NV21 = 3,
    /**
     * surface格式
     */
    AV_PIXEL_FORMAT_SURFACE_FORMAT = 4,
    /**
     * RGBA8888
     */
    AV_PIXEL_FORMAT_RGBA = 5,
} OH_AVPixelFormat;

/**
 * @brief 创建一个OH_AVFormat句柄指针，用以读写数据。
 * 
 * @syscap SystemCapability.Multimedia.Media.Core
 * @return 返回OH_AVFormat实例的指针
 * @since 9
 * @version 1.0
 */
struct OH_AVFormat *OH_AVFormat_Create(void);

/**
 * @brief 销毁指定OH_AVFormat句柄资源。
 * 
 * @syscap SystemCapability.Multimedia.Media.Core
 * @param format 指向OH_AVFormat实例的指针
 * @return void
 * @since 9
 * @version 1.0
 */
void OH_AVFormat_Destroy(struct OH_AVFormat *format);

/**
 * @brief 拷贝OH_AVFormat句柄资源.
 * 
 * @syscap SystemCapability.Multimedia.Media.Core
 * @param to 接收数据的OH_AVFormat句柄指针
 * @param from 被拷贝数据的OH_AVFormat句柄指针
 * @return 返回值为TRUE表示成功，返回值为FALSE表示失败。\n
 * 可能的失败原因：1.输入参数为空指针；2.输入的OH_AVFormat参数格式校验失败。
 * @since 9
 * @version 1.0
 */
bool OH_AVFormat_Copy(struct OH_AVFormat *to, struct OH_AVFormat *from);

/**
 * @brief 向OH_AVFormat写入Int数据.
 * 
 * @syscap SystemCapability.Multimedia.Media.Core
 * @param format 指向OH_AVFormat实例的指针
 * @param key 写入数据的键值
 * @param value 写入的数据
 * @return 返回值为TRUE表示成功，返回值为FALSE表示失败。\n
 * 可能的失败原因：1.输入format为空指针；2.输入format参数格式校验失败；3.输入key为空指针。
 * @since 9
 * @version 1.0
 */
bool OH_AVFormat_SetIntValue(struct OH_AVFormat *format, const char *key, int32_t value);

/**
 * @brief 向OH_AVFormat写入Long数据。
 * 
 * @syscap SystemCapability.Multimedia.Media.Core
 * @param format 指向OH_AVFormat实例的指针
 * @param key 写入数据的键值
 * @param value 写入的数据
 * @return 返回值为TRUE表示成功，返回值为FALSE表示失败。\n
 * 可能的失败原因：1.输入format为空指针；2.输入format参数格式校验失败；3.输入key为空指针。
 * @since 9
 * @version 1.0
 */
bool OH_AVFormat_SetLongValue(struct OH_AVFormat *format, const char *key, int64_t value);

/**
 * @brief 向OH_AVFormat写入Float数据。
 * 
 * @syscap SystemCapability.Multimedia.Media.Core
 * @param format 指向OH_AVFormat实例的指针
 * @param key 写入数据的键值
 * @param value 写入的数据
 * @return 返回值为TRUE表示成功，返回值为FALSE表示失败。\n
 * 可能的失败原因：1.输入format为空指针；2.输入format参数格式校验失败；3.输入key为空指针。
 * @since 9
 * @version 1.0
 */
bool OH_AVFormat_SetFloatValue(struct OH_AVFormat *format, const char *key, float value);

/**
 * @brief 向OH_AVFormat写入Double数据。
 * 
 * @syscap SystemCapability.Multimedia.Media.Core
 * @param format 指向OH_AVFormat实例的指针
 * @param key 写入数据的键值
 * @param value 写入的数据
 * @return 返回值为TRUE表示成功，返回值为FALSE表示失败。\n
 * 可能的失败原因：1.输入format为空指针；2.输入format参数格式校验失败；3.输入key为空指针。
 * @since 9
 * @version 1.0
 */
bool OH_AVFormat_SetDoubleValue(struct OH_AVFormat *format, const char *key, double value);

/**
 * @brief 向OH_AVFormat写入String数据。
 * 
 * @syscap SystemCapability.Multimedia.Media.Core
 * @param format 指向OH_AVFormat实例的指针
 * @param key 写入数据的键值
 * @param value 写入的数据
 * @return 返回值为TRUE表示成功，返回值为FALSE表示失败。\n
 * 可能的失败原因：1.输入format为空指针；2.输入format参数格式校验失败；3.输入key为空指针；4.输入value为空指针。
 * @since 9
 * @version 1.0
 */
bool OH_AVFormat_SetStringValue(struct OH_AVFormat *format, const char *key, const char *value);

/**
 * @brief 向OH_AVFormat写入一块指定长度的数据。
 * 
 * @syscap SystemCapability.Multimedia.Media.Core
 * @param format 指向OH_AVFormat实例的指针
 * @param key 写入数据的键值
 * @param addr 写入的数据地址
 * @param size 写入的数据长度
 * @return 返回值为TRUE表示成功，返回值为FALSE表示失败。\n
 * 可能的失败原因：1.输入format为空指针；2.输入format参数格式校验失败；3.输入key为空指针；4.输入addr为空指针；5.size为0。
 * @since 9
 * @version 1.0
 */
bool OH_AVFormat_SetBuffer(struct OH_AVFormat *format, const char *key, const uint8_t *addr, size_t size);

/**
 * @brief 从OH_AVFormat读取Int数据。
 * 
 * @syscap SystemCapability.Multimedia.Media.Core
 * @param format 指向OH_AVFormat实例的指针
 * @param key 读取数据的键值
 * @param out 读取的数据
 * @return 返回值为TRUE表示成功，返回值为FALSE表示失败。\n
 * 可能的失败原因：1.输入format为空指针；2.输入format参数格式校验失败；3.输入key为空指针；4.输入out为空指针。
 * @since 9
 * @version 1.0
 */
bool OH_AVFormat_GetIntValue(struct OH_AVFormat *format, const char *key, int32_t *out);

/**
 * @brief 从OH_AVFormat读取Long数据。
 * 
 * @syscap SystemCapability.Multimedia.Media.Core
 * @param format 指向OH_AVFormat实例的指针
 * @param key 读取数据的键值
 * @param out 读取的数据
 * @return 返回值为TRUE表示成功，返回值为FALSE表示失败。\n
 * 可能的失败原因：1.输入format为空指针；2.输入format参数格式校验失败；3.输入key为空指针；4.输入out为空指针。
 * @since 9
 * @version 1.0
 */
bool OH_AVFormat_GetLongValue(struct OH_AVFormat *format, const char *key, int64_t *out);

/**
 * @brief 从OH_AVFormat读取Float数据。
 * 
 * @syscap SystemCapability.Multimedia.Media.Core
 * @param format 指向OH_AVFormat实例的指针
 * @param key 读取数据的键值
 * @param out 读取的数据
 * @return 返回值为TRUE表示成功，返回值为FALSE表示失败。\n
 * 可能的失败原因：1.输入format为空指针；2.输入format参数格式校验失败；3.输入key为空指针；4.输入out为空指针。
 * @since 9
 * @version 1.0
 */
bool OH_AVFormat_GetFloatValue(struct OH_AVFormat *format, const char *key, float *out);

/**
 * @brief 从OH_AVFormat读取Double数据。
 * 
 * @syscap SystemCapability.Multimedia.Media.Core
 * @param format 指向OH_AVFormat实例的指针
 * @param key 读取数据的键值
 * @param out 读取的数据
 * @return 返回值为TRUE表示成功，返回值为FALSE表示失败。\n
 * 可能的失败原因：1.输入format为空指针；2.输入format参数格式校验失败；3.输入key为空指针；4.输入out为空指针。
 * @since 9
 * @version 1.0
 */
bool OH_AVFormat_GetDoubleValue(struct OH_AVFormat *format, const char *key, double *out);

/**
 * @brief 从OH_AVFormat读取Double数据。
 * 
 * @syscap SystemCapability.Multimedia.Media.Core
 * @param format 指向OH_AVFormat实例的指针
 * @param key 读取数据的键值
 * @param out 读取的字符串指针，指向的数据生命周期伴随GetString更新，伴随format销毁，如果调用者需要长期持有，必须进行内存拷贝
 * @return 返回值为TRUE表示成功，返回值为FALSE表示失败。\n
 * 可能的失败原因：1.输入format为空指针；2.输入format参数格式校验失败；3.输入key为空指针；4.输入out为空指针；5.malloc出的out字符串为空指针。
 * @since 9
 * @version 1.0
 */
bool OH_AVFormat_GetStringValue(struct OH_AVFormat *format, const char *key, const char **out);

/**
 * @brief 从OH_AVFormat读取一块指定长度的数据。
 * 
 * @syscap SystemCapability.Multimedia.Media.Core
 * @param format 指向OH_AVFormat实例的指针
 * @param key 读写数据的键值
 * @param addr 生命周期是format持有，伴随format销毁，如果调用者需要长期持有，必须进行内存拷贝
 * @param size 读写数据的长度
 * @return 返回值为TRUE表示成功，返回值为FALSE表示失败。\n
 * 可能的失败原因：1.输入format为空指针；2.输入format参数格式校验失败；3.输入key为空指针；4.输入addr为空指针；5.size为空指针。
 * @since 9
 * @version 1.0
 */
bool OH_AVFormat_GetBuffer(struct OH_AVFormat *format, const char *key, uint8_t **addr, size_t *size);

/**
 * @brief 以字符串的形式输出OH_AVFormat所包含的信息。
 * 
 * @syscap SystemCapability.Multimedia.Media.Core
 * @param format 指向OH_AVFormat实例的指针
 * @return 返回由键值和数据组成的字符串指针如果操作成功，否则返回空指针。\n
 * 可能的失败原因：1.输入format为空指针；2.malloc出的dump info为空指针。
 * @since 9
 * @version 1.0
 */
const char *OH_AVFormat_DumpInfo(struct OH_AVFormat *format);

#ifdef __cplusplus
}
#endif

#endif // NATIVE_AVFORMAT_H