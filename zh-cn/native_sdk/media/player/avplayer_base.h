/*
 * Copyright (C) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup AVPlayer
 * @{
 *
 * @brief 为媒体源提供播放能力的API
 *
 * @Syscap SystemCapability.Multimedia.Media.AVPlayer
 * @since 11
 * @version 1.0
 */

/**
 * @file avplayer_base.h
 *
 * @brief 定义AVPlayer的结构体和枚举
 *
 * @kit MediaKit
 * @library libavplayer.so
 * @since 11
 * @version 1.0
 */

#ifndef MULTIMEDIA_PLAYER_FRAMEWORK_NATIVE_AVPLAYER_BASH_H
#define MULTIMEDIA_PLAYER_FRAMEWORK_NATIVE_AVPLAYER_BASH_H

#include <stdint.h>

#include "native_avformat.h"

#ifdef __cplusplus
extern "C" {
#endif

typedef struct OH_AVPlayer OH_AVPlayer;

/**
 * @brief 播放状态
 * @syscap SystemCapability.Multimedia.Media.AVPlayer
 * @since 11
 * @version 1.0
 */
typedef enum AVPlayerState {
    /* 空闲 */
    AV_IDLE = 0,
    /* 初始化 */
    AV_INITIALIZED = 1,
    /* 准备 */
    AV_PREPARED = 2,
    /* 播放 */
    AV_PLAYING = 3,
    /* 暂停 */
    AV_PAUSED = 4,
    /* 停止 */
    AV_STOPPED = 5,
    /* 结束 */
    AV_COMPLETED = 6,
    /* 释放 */
    AV_RELEASED = 7,
    /* 错误 */
    AV_ERROR = 8,
} AVPlayerState;

/**
 * @brief 跳转模式
 * @syscap SystemCapability.Multimedia.Media.AVPlayer
 * @since 11
 * @version 1.0
 */
typedef enum AVPlayerSeekMode {
    /* 同步到时间点之后的关键帧 */
    AV_SEEK_NEXT_SYNC = 0,
    /* 同步到时间点之前的关键帧 */
    AV_SEEK_PREVIOUS_SYNC,
    /**
    * @brief 同步到距离指定时间点最近的帧
    * @since 12
    */
    AV_SEEK_CLOSEST = 2,
} AVPlayerSeekMode;

/**
 * @brief 播放速度
 * @syscap SystemCapability.Multimedia.Media.AVPlayer
 * @since 11
 * @version 1.0
 */
typedef enum AVPlaybackSpeed {
    /* 0.75倍速播放 */
    AV_SPEED_FORWARD_0_75_X,
    /* 正常播放 */
    AV_SPEED_FORWARD_1_00_X,
    /* 1.25倍速播放 */
    AV_SPEED_FORWARD_1_25_X,
    /* 1.75倍速播放 */
    AV_SPEED_FORWARD_1_75_X,
    /* 2.0倍速播放 */
    AV_SPEED_FORWARD_2_00_X,
    /**
     * @brief 0.5倍速播放
     * @since 12
     */
    AV_SPEED_FORWARD_0_50_X,
    /**
     * @brief 1.5倍速播放
     * @since 12
    */
    AV_SPEED_FORWARD_1_50_X,
} AVPlaybackSpeed;

/**
 * @brief OnInfo类型。
 * @syscap SystemCapability.Multimedia.Media.AVPlayer
 * @since 11
 * @version 1.0
 */
typedef enum AVPlayerOnInfoType {
    /** 上报Seek完成消息。 */
    AV_INFO_TYPE_SEEKDONE = 0,
    /** 上报倍速设置完成消息。 */
    AV_INFO_TYPE_SPEEDDONE = 1,
    /** 上报比特率选择完成消息。 */
    AV_INFO_TYPE_BITRATEDONE = 2,
    /** 上报播放完成消息。 */
    AV_INFO_TYPE_EOS = 3,
    /** 上报播放状态更新消息。 */
    AV_INFO_TYPE_STATE_CHANGE = 4,
    /** 上报播放进度消息。 */
    AV_INFO_TYPE_POSITION_UPDATE = 5,
    /** 上报播放器消息。 */
    AV_INFO_TYPE_MESSAGE = 6,
    /** 上报音量变更消息。 */
    AV_INFO_TYPE_VOLUME_CHANGE = 7,
    /** 上报视频分辨率消息。 */
    AV_INFO_TYPE_RESOLUTION_CHANGE = 8,
    /** 上报缓冲更新消息。 */
    AV_INFO_TYPE_BUFFERING_UPDATE = 9,
    /** 上报HLS视频比特率列表消息。
       上报时每个比特率已经被转为uint8_t字节数组，
       使用者需要将uint8_t字节数组强制转换为uint32_t整型数组。 */
    AV_INFO_TYPE_BITRATE_COLLECT = 10,
    /** 上报音频焦点变更消息。 */
    AV_INFO_TYPE_INTERRUPT_EVENT = 11,
    /** 上报媒体资源时长更新消息。 */
    AV_INFO_TYPE_DURATION_UPDATE = 12,
    /** 上报媒体资源是否为直播类型消息。 */
    AV_INFO_TYPE_IS_LIVE_STREAM = 13,
    /** 上报媒体资源播放轨道切换消息。 */
    AV_INFO_TYPE_TRACKCHANGE = 14,
    /** 上报媒体资源轨道信息。 */
    AV_INFO_TYPE_TRACK_INFO_UPDATE = 15,
    /** 上报媒体字幕更新信息。 */
    AV_INFO_TYPE_SUBTITLE_UPDATE = 16,
    /** 上报音频输出设备变更消息，数据类型为{@link OH_AudioStream_DeviceChangeReason}。 */
    AV_INFO_TYPE_AUDIO_OUTPUT_DEVICE_CHANGE = 17,
} AVPlayerOnInfoType;

/**
 * @brief 播放缓冲消息类型定义。
 * @syscap SystemCapability.Multimedia.Media.AVPlayer
 * @since 12
 * @version 1.0
 */
typedef enum AVPlayerBufferingType {
    /** 缓冲开始消息。 */
    AVPLAYER_BUFFERING_START = 1,

    /** 缓冲结束消息。 */
    AVPLAYER_BUFFERING_END,

    /** 缓冲执行进度百分比，取值范围：整数，[0, 100]。 */
    AVPLAYER_BUFFERING_PERCENT,

    /** 缓冲数据可播放时长，单位：毫秒。 */
    AVPLAYER_BUFFERING_CACHED_DURATION,
} AVPlayerBufferingType;

/**
 * @brief 获取播放状态的关键字, 对应值类型是int32_t。
 * @syscap SystemCapability.Multimedia.Media.AVPlayer
 * @since 12
 * @version 1.0
 */
extern const char* OH_PLAYER_STATE;

/**
 * @brief 获取播放状态变更原因的关键字, 对应值类型是int32_t。1：用户操作触发；2：系统变更触发。
 * @syscap SystemCapability.Multimedia.Media.AVPlayer
 * @since 12
 * @version 1.0
 */
extern const char* OH_PLAYER_STATE_CHANGE_REASON;

/**
 * @brief 获取音量的关键字, 对应值类型是float。
 * @syscap SystemCapability.Multimedia.Media.AVPlayer
 * @since 12
 * @version 1.0
 */
extern const char* OH_PLAYER_VOLUME;

/**
 * @brief 获取比特率列表的关键字, 对应值类型是uint8_t字节数组 {@link AV_INFO_TYPE_BITRATE_COLLECT}。
 * @syscap SystemCapability.Multimedia.Media.AVPlayer
 * @since 12
 * @version 1.0
 */
extern const char* OH_PLAYER_BITRATE_ARRAY;

/**
 * @brief 获取音频打断类型的关键字, 对应值类型是int32_t。
 * @syscap SystemCapability.Multimedia.Media.AVPlayer
 * @since 12
 * @version 1.0
 */
extern const char* OH_PLAYER_AUDIO_INTERRUPT_TYPE;

/**
 * @brief 获取音频打断FORCE类型的关键字, 对应值类型是int32_t。
 * @syscap SystemCapability.Multimedia.Media.AVPlayer
 * @since 12
 * @version 1.0
 */
extern const char* OH_PLAYER_AUDIO_INTERRUPT_FORCE;

/**
 * @brief 获取音频打断HINT类型的关键字, 对应值类型是int32_t。
 * @syscap SystemCapability.Multimedia.Media.AVPlayer
 * @since 12
 * @version 1.0
 */
extern const char* OH_PLAYER_AUDIO_INTERRUPT_HINT;

/**
 * @brief 获取音频音频设备变更原因的关键字, 对应值类型是int32_t。
 * @syscap SystemCapability.Multimedia.Media.AVPlayer
 * @since 12
 * @version 1.0
 */
extern const char* OH_PLAYER_AUDIO_DEVICE_CHANGE_REASON;

/**
 * @brief 获取缓冲更新消息类型的关键字, 对应值类型是AVPlayerBufferingType。
 * @syscap SystemCapability.Multimedia.Media.AVPlayer
 * @since 12
 * @version 1.0
 */
extern const char* OH_PLAYER_BUFFERING_TYPE;

/**
 * @brief 获取缓冲更新消息具体数值的关键字, 对应值类型是int32_t。{@link AVPLAYER_BUFFERING_PERCENT} {@link AVPLAYER_BUFFERING_CACHED_DURATION}。
 * @syscap SystemCapability.Multimedia.Media.AVPlayer
 * @since 12
 * @version 1.0
 */
extern const char* OH_PLAYER_BUFFERING_VALUE;

/**
 * @brief 获取Seek后播放进度信息的关键字, 对应值类型是int32_t。
 * @syscap SystemCapability.Multimedia.Media.AVPlayer
 * @since 12
 */
extern const char* OH_PLAYER_SEEK_POSITION;

/**
 * @brief 获取播放倍速信息的关键字, 对应值类型是AVPlaybackSpeed。
 * @syscap SystemCapability.Multimedia.Media.AVPlayer
 * @since 12
 */
extern const char* OH_PLAYER_PLAYBACK_SPEED;

/**
 * @brief 获取比特率信息的关键字, 对应值类型是uint32_t。
 * @syscap SystemCapability.Multimedia.Media.AVPlayer
 * @since 12
 */
extern const char* OH_PLAYER_BITRATE;

/**
 * @brief 获取播放进度信息的关键字, 对应值类型是int32_t。
 * @syscap SystemCapability.Multimedia.Media.AVPlayer
 * @since 12
 */
extern const char* OH_PLAYER_CURRENT_POSITION;

/**
 * @brief 获取媒体资源时长信息的关键字, 对应值类型是int64_t。
 * @syscap SystemCapability.Multimedia.Media.AVPlayer
 * @since 12
 */
extern const char* OH_PLAYER_DURATION;

/**
 * @brief 获取视频宽度信息的关键字, 对应值类型int32_t。
 * @syscap SystemCapability.Multimedia.Media.AVPlayer
 * @since 12
 */
extern const char* OH_PLAYER_VIDEO_WIDTH;

/**
 * @brief 获取视频高度信息的关键字, 对应值类型int32_t。
 * @syscap SystemCapability.Multimedia.Media.AVPlayer
 * @since 12
 */
extern const char* OH_PLAYER_VIDEO_HEIGHT;

/**
 * @brief 获取播放器消息信息的关键字, 对应值类型int32_t。1：视频帧开始渲染。
 * @syscap SystemCapability.Multimedia.Media.AVPlayer
 * @since 12
 */
extern const char* OH_PLAYER_MESSAGE_TYPE;

/**
 * @brief 获取媒体资源是否为直播类型信息的关键字, 对应值类型int32_t。1：直播。
 * @syscap SystemCapability.Multimedia.Media.AVPlayer
 * @since 12
 */
extern const char* OH_PLAYER_IS_LIVE_STREAM;

/**
 * @brief 收到播放器消息时调用
 * @syscap SystemCapability.Multimedia.Media.AVPlayer
 * @param player 指向OH_AVPlayer实例的指针
 * @param type 信息类型。具体请参见{@link AVPlayerOnInfoType}
 * @param extra 其他信息，如播放文件的开始时间位置
 * @since 11
 * @deprecated since 12
 * @useinstead {@link OH_AVPlayerOnInfoCallback}
 * @version 1.0
 */
typedef void (*OH_AVPlayerOnInfo)(OH_AVPlayer *player, AVPlayerOnInfoType type, int32_t extra);

/**
 * @brief 收到播放器消息时被调用。如果应用成功设置该回调，则不会回调OH_AVPlayerOnInfo函数。
 * @syscap SystemCapability.Multimedia.Media.AVPlayer
 * @param player 指向OH_AVPlayer实例的指针。
 * @param type 信息类型。具体请参见{@link AVPlayerOnInfoType}。
 * @param infoBody 指向携带具体消息的指针，具体类型参见{@link OH_AVFormat}，仅在该回调方法内有效。
 * @param userData 指向应用调用者设置该回调函数时提供的实例的指针。
 * @since 12
 */
typedef void (*OH_AVPlayerOnInfoCallback)(OH_AVPlayer *player, AVPlayerOnInfoType type, OH_AVFormat* infoBody,
    void *userData);

/**
 * @brief 在api9以上的版本发生错误时调用
 * @syscap SystemCapability.Multimedia.Media.AVPlayer
 * @param 指向OH_AVPlayer实例的指针
 * @param errorCode 错误码
 * @param errorMsg 错误消息
 * @since 11
 * @deprecated since 12
 * @useinstead {@link OH_AVPlayerOnInfoCallback} {@link OH_AVPlayerOnError}
 * @version 1.0
 */
typedef void (*OH_AVPlayerOnError)(OH_AVPlayer *player, int32_t errorCode, const char *errorMsg);

/**
 * @brief 发生错误时被调用。如果应用成功设置该回调，则不会回调OH_AVPlayerOnError函数。
 * @syscap SystemCapability.Multimedia.Media.AVPlayer
 * @param player 指向OH_AVPlayer实例的指针。
 * @param errorCode 错误码。
 * @param errorMsg 错误消息。
 * @param userData 指向应用调用者设置该回调函数时提供的实例的指针。
 * @since 12
 */
typedef void (*OH_AVPlayerOnErrorCallback)(OH_AVPlayer *player, int32_t errorCode, const char *errorMsg,
    void *userData);

/**
 * @brief OH_AVPlayer中所有回调函数指针的集合。注册此的实例结构体到OH_AVPlayer实例中，并对回调上报的信息进行处理，保证AVPlayer的正常运行。
 * @syscap SystemCapability.Multimedia.Media.AVPlayer
 * @param onInfo 监控AVPlayer过程信息，参考{@link OH_AVPlayerOnInfo}
 * @param onError 监控AVPlayer操作错误，参考{@link OH_AVPlayerOnError}
 * @since 11
 * @deprecated since 12
 * @useinstead {@link OH_AVPlayerOnInfoCallback} {@link OH_AVPlayerOnErrorCallback}
 * @version 1.0
 */
typedef struct AVPlayerCallback {
    OH_AVPlayerOnInfo onInfo;
    OH_AVPlayerOnError onError;
} AVPlayerCallback;

#ifdef __cplusplus
}
#endif
#endif // MULTIMEDIA_PLAYER_FRAMEWORK_NATIVE_AVPLAYER_BASH_H
