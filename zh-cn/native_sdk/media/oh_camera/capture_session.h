/*
 * Copyright (C) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup OH_Camera
 * @{
 *
 * @brief 为相机模块提供C接口的定义。
 *
 * @syscap SystemCapability.Multimedia.Camera.Core
 *
 * @since 11
 * @version 1.0
 */

/**
 * @file capture_session.h
 *
 * @brief 声明捕获会话概念。
 *
 * @library libohcamera.so
 * @syscap SystemCapability.Multimedia.Camera.Core
 * @since 11
 * @version 1.0
 */

#ifndef NATIVE_INCLUDE_CAMERA_CAMERA_SESSION_H
#define NATIVE_INCLUDE_CAMERA_CAMERA_SESSION_H

#include <stdint.h>
#include <stdio.h>
#include "camera.h"
#include "camera_input.h"
#include "preview_output.h"
#include "photo_output.h"
#include "video_output.h"
#include "metadata_output.h"
#include "native_buffer/native_buffer.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief 捕获会话对象
 *
 * 可以使用{@link OH_CameraManager_CreateCaptureSession}方法创建指针。
 *
 * @since 11
 * @version 1.0
 */
typedef struct Camera_CaptureSession Camera_CaptureSession;

/**
 * @brief 在{@link CaptureSession_Callbacks}中被调用的捕获会话焦点状态回调。
 *
 * @param session 传递回调的{@link Camera_CaptureSession}。
 * @param focusState 回调传递的{@link Camera_FocusState}。
 * @since 11
 */
typedef void (*OH_CaptureSession_OnFocusStateChange)(Camera_CaptureSession* session, Camera_FocusState focusState);

/**
 * @brief 在{@link CaptureSession_Callbacks}中被调用的捕获会话错误回调。
 *
 * @param session 传递回调的{@link Camera_CaptureSession}。
 * @param errorCode 捕获会话的{@link Camera_ErrorCode}。
 *
 * @see CAMERA_SERVICE_FATAL_ERROR
 * @since 11
 */
typedef void (*OH_CaptureSession_OnError)(Camera_CaptureSession* session, Camera_ErrorCode errorCode);

/**
 * @brief 拍照会话平滑变焦信息回调，触发平滑变焦后该回调会返回。
 *
 * @param session 传递回调的{@link Camera_CaptureSession}。
 * @param smoothZoomInfo 回调传递的{@link Camera_SmoothZoomInfo}。
 * @since 12
 */
typedef void (*OH_CaptureSession_OnSmoothZoomInfo)(Camera_CaptureSession* session,
    Camera_SmoothZoomInfo* smoothZoomInfo);

/**
 * @brief 捕获会话的回调。
 *
 * @see OH_CaptureSession_RegisterCallback
 * @since 11
 * @version 1.0
 */
typedef struct CaptureSession_Callbacks {
    /**
     * 捕获会话焦点状态更改事件。
     */
    OH_CaptureSession_OnFocusStateChange onFocusStateChange;

    /**
     * 捕获会话错误事件。
     */
    OH_CaptureSession_OnError onError;
} CaptureSession_Callbacks;

/**
 * @brief 注册捕获会话事件回调。
 *
 * @param session {@link Camera_CaptureSession}实例。
 * @param callback 要注册的{@link CaptureSession_Callbacks}。
 * @return {@link#CAMERA_OK}如果方法调用成功。
 *         {@link#INVALID_ARGUMENT}如果参数丢失或参数类型不正确。
 * @since 11
 */
Camera_ErrorCode OH_CaptureSession_RegisterCallback(Camera_CaptureSession* session,
    CaptureSession_Callbacks* callback);

/**
 * @brief 注销捕获会话事件回调。
 *
 * @param session {@link Camera_CaptureSession}实例。
 * @param callback 要注销的{@link CaptureSession_Callbacks}。
 * @return {@link#CAMERA_OK}如果方法调用成功。
 *         {@link#INVALID_ARGUMENT}如果参数丢失或参数类型不正确。
 * @since 11
 */
Camera_ErrorCode OH_CaptureSession_UnregisterCallback(Camera_CaptureSession* session,
    CaptureSession_Callbacks* callback);

/**
 * @brief 注册平滑变焦信息事件回调。
 *
 * @param session {@link Camera_CaptureSession}实例。
 * @param smoothZoomInfoCallback 要注册的{@link OH_CaptureSession_OnSmoothZoomInfo}。
 * @return {@link#CAMERA_OK}如果方法调用成功。
 *         {@link#CAMERA_INVALID_ARGUMENT}如果参数丢失或参数类型不正确。
 * @since 12
 */
Camera_ErrorCode OH_CaptureSession_RegisterSmoothZoomInfoCallback(Camera_CaptureSession* session,
    OH_CaptureSession_OnSmoothZoomInfo smoothZoomInfoCallback);

/**
 * @brief 注销平滑变焦信息事件回调。
 *
 * @param session {@link Camera_CaptureSession}实例。
 * @param smoothZoomInfoCallback 要注销的{@link OH_CaptureSession_OnSmoothZoomInfo}。
 * @return {@link#CAMERA_OK}如果方法调用成功。
 *         {@link#CAMERA_INVALID_ARGUMENT}如果参数丢失或参数类型不正确。
 * @since 12
 */
Camera_ErrorCode OH_CaptureSession_UnregisterSmoothZoomInfoCallback(Camera_CaptureSession* session,
    OH_CaptureSession_OnSmoothZoomInfo smoothZoomInfoCallback);

/**
 * @brief 设置会话模式。
 *
 * @param session {@link Camera_CaptureSession}实例。
 * @param sceneMode {@link Camera_SceneMode}实例。
 * @return {@link#CAMERA_OK}如果方法调用成功。
 *         {@link#INVALID_ARGUMENT}如果参数丢失或参数类型不正确。
 *         {@link#CAMERA_OPERATION_NOT_ALLOWED}如果不允许操作。
 *         {@link #CAMERA_SESSION_CONFIG_LOCKED}如果会话配置已锁定。
 * @since 12
 */
Camera_ErrorCode OH_CaptureSession_SetSessionMode(Camera_CaptureSession* session, Camera_SceneMode sceneMode);
 
/**
 * @brief 把其中一条PreviewOutput标记成安全输出。
 *
 * @param session {@link Camera_CaptureSession}实例。
 * @param previewOutput 要标记为安全输出的{@link Camera_PreviewOutput}。
 * @return {@link#CAMERA_OK}如果方法调用成功。
 *         {@link#INVALID_ARGUMENT}如果参数丢失或参数类型不正确。
 *         {@link#CAMERA_OPERATION_NOT_ALLOWED}如果不允许操作。
 *         {@link #CAMERA_SESSION_CONFIG_LOCKED}如果会话配置已锁定。
 * @since 12
 */
Camera_ErrorCode OH_CaptureSession_AddSecureOutput(Camera_CaptureSession* session, Camera_PreviewOutput* previewOutput);

/**
 * @brief 开始捕获会话配置。
 *
 * @param session {@link Camera_CaptureSession}实例。
 * @return {@link#CAMERA_OK}如果方法调用成功。
 *         {@link#INVALID_ARGUMENT}如果参数丢失或参数类型不正确。
 *         {@link#CAMERA_SESSION_CONFIG_LOCKED}如果会话配置已锁定。
 * @since 11
 */
Camera_ErrorCode OH_CaptureSession_BeginConfig(Camera_CaptureSession* session);

/**
 * @brief 提交捕获会话配置。
 *
 * @param session {@link Camera_CaptureSession}实例。
 * @return {@link#CAMERA_OK}如果方法调用成功。
 *         {@link#INVALID_ARGUMENT}如果参数丢失或参数类型不正确。
 *         {@link#CAMERA_OPERATION_NOT_ALLOWED}如果不允许操作。
 *         {@link#CAMERA_SERVICE_FATAL_ERROR}如果相机服务出现致命错误。
 * @since 11
 */
Camera_ErrorCode OH_CaptureSession_CommitConfig(Camera_CaptureSession* session);

/**
 * @brief 添加相机输入。
 *
 * @param session {@link Camera_CaptureSession}实例。
 * @param cameraInput 要添加的目标{@link Camera_Input}。
 * @return {@link#CAMERA_OK}如果方法调用成功。
 *         {@link#INVALID_ARGUMENT}如果参数丢失或参数类型不正确。
 *         {@link#CAMERA_OPERATION_NOT_ALLOWED}如果不允许操作。
 * @since 11
 */
Camera_ErrorCode OH_CaptureSession_AddInput(Camera_CaptureSession* session, Camera_Input* cameraInput);

/**
 * @brief 删除相机输入。
 *
 * @param session {@link Camera_CaptureSession}实例。
 * @param cameraInput 要删除的目标{@link Camera_Input}。
 * @return {@link#CAMERA_OK}如果方法调用成功。
 *         {@link#INVALID_ARGUMENT}如果参数丢失或参数类型不正确。
 *         {@link#CAMERA_OPERATION_NOT_ALLOWED}如果不允许操作。
 * @since 11
 */
Camera_ErrorCode OH_CaptureSession_RemoveInput(Camera_CaptureSession* session, Camera_Input* cameraInput);

/**
 * @brief 添加预览输出。
 *
 * @param session {@link Camera_CaptureSession}实例。
 * @param previewOutput 要添加的目标{@link Camera_PreviewOutput}。
 * @return {@link#CAMERA_OK}如果方法调用成功。
 *         {@link#INVALID_ARGUMENT}如果参数丢失或参数类型不正确。
 *         {@link#CAMERA_OPERATION_NOT_ALLOWED}如果不允许操作。
 * @since 11
 */
Camera_ErrorCode OH_CaptureSession_AddPreviewOutput(Camera_CaptureSession* session,
    Camera_PreviewOutput* previewOutput);

/**
 * @brief 删除预览输出。
 *
 * @param session {@link Camera_CaptureSession}实例。
 * @param previewOutput 要删除的目标{@link Camera_PreviewOutput}。
 * @return {@link#CAMERA_OK}如果方法调用成功。
 *         {@link#INVALID_ARGUMENT}如果参数丢失或参数类型不正确。
 *         {@link#CAMERA_OPERATION_NOT_ALLOWED}如果不允许操作。
 * @since 11
 */
Camera_ErrorCode OH_CaptureSession_RemovePreviewOutput(Camera_CaptureSession* session,
    Camera_PreviewOutput* previewOutput);

/**
 * @brief 添加拍照输出。
 *
 * @param session {@link Camera_CaptureSession}实例。
 * @param photoOutput 要添加的目标{@link Camera_PhotoOutput}。
 * @return {@link#CAMERA_OK}如果方法调用成功。
 *         {@link#INVALID_ARGUMENT}如果参数丢失或参数类型不正确。
 *         {@link#CAMERA_OPERATION_NOT_ALLOWED}如果不允许操作。
 * @since 11
 */
Camera_ErrorCode OH_CaptureSession_AddPhotoOutput(Camera_CaptureSession* session, Camera_PhotoOutput* photoOutput);

/**
 * @brief 删除拍照输出。
 *
 * @param session {@link Camera_CaptureSession}实例。
 * @param photoOutput 要删除的目标{@link Camera_PhotoOutput}。
 * @return {@link#CAMERA_OK}如果方法调用成功。
 *         {@link#INVALID_ARGUMENT}如果参数丢失或参数类型不正确。
 *         {@link#CAMERA_OPERATION_NOT_ALLOWED}如果不允许操作。
 * @since 11
 */
Camera_ErrorCode OH_CaptureSession_RemovePhotoOutput(Camera_CaptureSession* session, Camera_PhotoOutput* photoOutput);

/**
 * @brief 添加录像输出。
 *
 * @param session {@link Camera_CaptureSession}实例。
 * @param videoOutput 要添加的目标{@link Camera_VideoOutput}。
 * @return {@link#CAMERA_OK}如果方法调用成功。
 *         {@link#INVALID_ARGUMENT}如果参数丢失或参数类型不正确。
 *         {@link#CAMERA_OPERATION_NOT_ALLOWED}如果不允许操作。
 * @since 11
 */
Camera_ErrorCode OH_CaptureSession_AddVideoOutput(Camera_CaptureSession* session, Camera_VideoOutput* videoOutput);

/**
 * @brief 删除录像输出。
 *
 * @param session {@link Camera_CaptureSession}实例。
 * @param videoOutput 要删除的目标{@link Camera_VideoOutput}。
 * @return {@link#CAMERA_OK}如果方法调用成功。
 *         {@link#INVALID_ARGUMENT}如果参数丢失或参数类型不正确。
 *         {@link#CAMERA_OPERATION_NOT_ALLOWED}如果不允许操作。
 * @since 11
 */
Camera_ErrorCode OH_CaptureSession_RemoveVideoOutput(Camera_CaptureSession* session, Camera_VideoOutput* videoOutput);

/**
 * @brief 添加元数据输出。
 *
 * @param session {@link Camera_CaptureSession}实例。
 * @param metadataOutput 要添加的目标{@link Camera_MetadataOutput}。
 * @return {@link#CAMERA_OK}如果方法调用成功。
 *         {@link#INVALID_ARGUMENT}如果参数丢失或参数类型不正确。
 *         {@link#CAMERA_OPERATION_NOT_ALLOWED}如果不允许操作。
 * @since 11
 */
Camera_ErrorCode OH_CaptureSession_AddMetadataOutput(Camera_CaptureSession* session,
    Camera_MetadataOutput* metadataOutput);

/**
 * @brief 删除元数据输出。
 *
 * @param session {@link Camera_CaptureSession}实例。
 * @param metadataOutput 要删除的目标{@link Camera_MetadataOutput}。
 * @return {@link#CAMERA_OK}如果方法调用成功。
 *         {@link#INVALID_ARGUMENT}如果参数丢失或参数类型不正确。
 *         {@link#CAMERA_OPERATION_NOT_ALLOWED}如果不允许操作。
 * @since 11
 */
Camera_ErrorCode OH_CaptureSession_RemoveMetadataOutput(Camera_CaptureSession* session,
    Camera_MetadataOutput* metadataOutput);

/**
 * @brief 启动捕获会话。
 *
 * @param session 要启动的{@link Camera_CaptureSession}实例。
 * @return {@link#CAMERA_OK}如果方法调用成功。
 *         {@link#INVALID_ARGUMENT}如果参数丢失或参数类型不正确。
 *         {@link#CAMERA_SESSION_NOT_CONFIG}如果捕获会话未配置。
 *         {@link#CAMERA_SERVICE_FATAL_ERROR}如果相机服务出现致命错误。
 * @since 11
 */
Camera_ErrorCode OH_CaptureSession_Start(Camera_CaptureSession* session);

/**
 * @brief 停止捕获会话。
 *
 * @param session 要停止的{@link Camera_CaptureSession}实例。
 * @return {@link#CAMERA_OK}如果方法调用成功。
 *         {@link#INVALID_ARGUMENT}如果参数丢失或参数类型不正确。
 *         {@link#CAMERA_SERVICE_FATAL_ERROR}如果相机服务出现致命错误。
 * @since 11
 */
Camera_ErrorCode OH_CaptureSession_Stop(Camera_CaptureSession* session);

/**
 * @brief 释放捕获会话。
 *
 * @param session 要释放的{@link Camera_CaptureSession}实例。
 * @return {@link#CAMERA_OK}如果方法调用成功。
 *         {@link#INVALID_ARGUMENT}如果参数丢失或参数类型不正确。
 *         {@link#CAMERA_SERVICE_FATAL_ERROR}如果相机服务出现致命错误。
 * @since 11
 */
Camera_ErrorCode OH_CaptureSession_Release(Camera_CaptureSession* session);

/**
 * @brief 检查设备是否有闪光灯。
 *
 * @param session {@link Camera_CaptureSession}实例。
 * @param hasFlash 是否支持闪光灯的结果。
 * @return {@link#CAMERA_OK}如果方法调用成功。
 *         {@link#INVALID_ARGUMENT}如果参数丢失或参数类型不正确。
 *         {@link#CAMERA_SESSION_NOT_CONFIG}如果捕获会话未配置。
 * @since 11
 */
Camera_ErrorCode OH_CaptureSession_HasFlash(Camera_CaptureSession* session, bool* hasFlash);

/**
 * @brief 检查是否支持指定的闪光灯模式。
 *
 * @param session {@link Camera_CaptureSession}实例。
 * @param flashMode 要检查的{@link Camera_FlashMode}。
 * @param isSupported 是否支持闪光灯模式的结果。
 * @return {@link#CAMERA_OK}如果方法调用成功。
 *         {@link#INVALID_ARGUMENT}如果参数丢失或参数类型不正确。
 *         {@link#CAMERA_SESSION_NOT_CONFIG}如果捕获会话未配置。
 * @since 11
 */
Camera_ErrorCode OH_CaptureSession_IsFlashModeSupported(Camera_CaptureSession* session,
    Camera_FlashMode flashMode, bool* isSupported);

/**
 * @brief 获取当前闪光灯模式。
 *
 * @param session {@link Camera_CaptureSession}实例。
 * @param flashMode 当前{@link Camera_FlashMode}。
 * @return {@link#CAMERA_OK}如果方法调用成功。
 *         {@link#INVALID_ARGUMENT}如果参数丢失或参数类型不正确。
 *         {@link#CAMERA_SESSION_NOT_CONFIG}如果捕获会话未配置。
 * @since 11
 */
Camera_ErrorCode OH_CaptureSession_GetFlashMode(Camera_CaptureSession* session, Camera_FlashMode* flashMode);

/**
 * @brief 设置闪光灯模式。
 *
 * @param session {@link Camera_CaptureSession}实例。
 * @param flashMode 要设置的目标{@link Camera_FlashMode}。
 * @return {@link#CAMERA_OK}如果方法调用成功。
 *         {@link#INVALID_ARGUMENT}如果参数丢失或参数类型不正确。
 *         {@link#CAMERA_SESSION_NOT_CONFIG}如果捕获会话未配置。
 * @since 11
 */
Camera_ErrorCode OH_CaptureSession_SetFlashMode(Camera_CaptureSession* session, Camera_FlashMode flashMode);

/**
 * @brief 检查是否支持指定的曝光模式。
 *
 * @param session {@link Camera_CaptureSession}实例。
 * @param exposureMode 要检查的{@link Camera_ExposureMode}。
 * @param isSupported 是否支持曝光模式的结果。
 * @return {@link#CAMERA_OK}如果方法调用成功。
 *         {@link#INVALID_ARGUMENT}如果参数丢失或参数类型不正确。
 *         {@link#CAMERA_SESSION_NOT_CONFIG}如果捕获会话未配置。
 * @since 11
 */
Camera_ErrorCode OH_CaptureSession_IsExposureModeSupported(Camera_CaptureSession* session,
    Camera_ExposureMode exposureMode, bool* isSupported);

/**
 * @brief 获取当前曝光模式。
 *
 * @param session {@link Camera_CaptureSession}实例。
 * @param exposureMode 当前的{@link Camera_ExposureMode}。
 * @return {@link#CAMERA_OK}如果方法调用成功。
 *         {@link#INVALID_ARGUMENT}如果参数丢失或参数类型不正确。
 *         {@link#CAMERA_SESSION_NOT_CONFIG}如果捕获会话未配置。
 * @since 11
 */
Camera_ErrorCode OH_CaptureSession_GetExposureMode(Camera_CaptureSession* session, Camera_ExposureMode* exposureMode);

/**
 * @brief 设置曝光模式。
 *
 * @param session {@link Camera_CaptureSession}实例。
 * @param exposureMode 要设置的目标{@link Camera_ExposureMode}。
 * @return {@link#CAMERA_OK}如果方法调用成功。
 *         {@link#INVALID_ARGUMENT}如果参数丢失或参数类型不正确。
 *         {@link#CAMERA_SESSION_NOT_CONFIG}如果捕获会话未配置。
 * @since 11
 */
Camera_ErrorCode OH_CaptureSession_SetExposureMode(Camera_CaptureSession* session, Camera_ExposureMode exposureMode);

/**
 * @brief 获取当前测量点。
 *
 * @param session {@link Camera_CaptureSession}实例。
 * @param point 当前{@link Camera_Point}测量点。
 * @return {@link#CAMERA_OK}如果方法调用成功。
 *         {@link#INVALID_ARGUMENT}如果参数丢失或参数类型不正确。
 *         {@link#CAMERA_SESSION_NOT_CONFIG}如果捕获会话未配置。
 * @since 11
 */
Camera_ErrorCode OH_CaptureSession_GetMeteringPoint(Camera_CaptureSession* session, Camera_Point* point);

/**
 * @brief 设置计量区域的中心点。
 *
 * @param session {@link Camera_CaptureSession}实例。
 * @param point 要设置的目标{@link Camera_Point}。
 * @return {@link#CAMERA_OK}如果方法调用成功。
 *         {@link#INVALID_ARGUMENT}如果参数丢失或参数类型不正确。
 *         {@link#CAMERA_SESSION_NOT_CONFIG}如果捕获会话未配置。
 * @since 11
 */
Camera_ErrorCode OH_CaptureSession_SetMeteringPoint(Camera_CaptureSession* session, Camera_Point point);

/**
 * @brief 查询曝光补偿范围。
 *
 * @param session {@link Camera_CaptureSession}实例。
 * @param minExposureBias 曝光补偿的最小值。
 * @param maxExposureBias 曝光补偿的最大值。
 * @param step 每个级别之间的曝光补偿阶梯。
 * @return {@link#CAMERA_OK}如果方法调用成功。
 *         {@link#INVALID_ARGUMENT}如果参数丢失或参数类型不正确。
 *         {@link#CAMERA_SESSION_NOT_CONFIG}如果捕获会话未配置。
 * @since 11
 */
Camera_ErrorCode OH_CaptureSession_GetExposureBiasRange(Camera_CaptureSession* session, float* minExposureBias,
    float* maxExposureBias, float* step);

/**
 * @brief 设置曝光补偿。
 *
 * @param session {@link Camera_CaptureSession}实例。
 * @param exposureBias 要设置的目标曝光补偿。
 * @return {@link#CAMERA_OK}如果方法调用成功。
 *         {@link#INVALID_ARGUMENT}如果参数丢失或参数类型不正确。
 *         {@link#CAMERA_SESSION_NOT_CONFIG}如果捕获会话未配置。
 * @since 11
 */
Camera_ErrorCode OH_CaptureSession_SetExposureBias(Camera_CaptureSession* session, float exposureBias);

/**
 * @brief 获取当前曝光补偿。
 *
 * @param session {@link Camera_CaptureSession}实例。
 * @param exposureBias 当前的曝光补偿。
 * @return {@link#CAMERA_OK}如果方法调用成功。
 *         {@link#INVALID_ARGUMENT}如果参数丢失或参数类型不正确。
 *         {@link#CAMERA_SESSION_NOT_CONFIG}如果捕获会话未配置。
 * @since 11
 */
Camera_ErrorCode OH_CaptureSession_GetExposureBias(Camera_CaptureSession* session, float* exposureBias);

/**
 * @brief 检查是否支持指定的聚焦模式。
 *
 * @param session {@link Camera_CaptureSession}实例。
 * @param focusMode 要检查的{@link Camera_FocusMode}。
 * @param isSupported 是否支持聚焦模式的结果。
 * @return {@link#CAMERA_OK}如果方法调用成功。
 *         {@link#INVALID_ARGUMENT}如果参数丢失或参数类型不正确。
 *         {@link#CAMERA_SESSION_NOT_CONFIG}如果捕获会话未配置。
 * @since 11
 */
Camera_ErrorCode OH_CaptureSession_IsFocusModeSupported(Camera_CaptureSession* session,
    Camera_FocusMode focusMode, bool* isSupported);

/**
 * @brief 获取当前聚焦模式。
 *
 * @param session {@link Camera_CaptureSession}实例。
 * @param exposureBias 当前{@link Camera_FocusMode}。
 * @return {@link#CAMERA_OK}如果方法调用成功。
 *         {@link#INVALID_ARGUMENT}如果参数丢失或参数类型不正确。
 *         {@link#CAMERA_SESSION_NOT_CONFIG}如果捕获会话未配置。
 * @since 11
 */
Camera_ErrorCode OH_CaptureSession_GetFocusMode(Camera_CaptureSession* session, Camera_FocusMode* focusMode);

/**
 * @brief 设置聚焦模式。
 *
 * @param session {@link Camera_CaptureSession}实例。
 * @param focusMode 要设置的目标{@link Camera_FocusMode}。
 * @return {@link#CAMERA_OK}如果方法调用成功。
 *         {@link#INVALID_ARGUMENT}如果参数丢失或参数类型不正确。
 *         {@link#CAMERA_SESSION_NOT_CONFIG}如果捕获会话未配置。
 * @since 11
 */
Camera_ErrorCode OH_CaptureSession_SetFocusMode(Camera_CaptureSession* session, Camera_FocusMode focusMode);

/**
 * @brief 获取当前焦点。
 *
 * @param session {@link Camera_CaptureSession}实例。
 * @param focusPoint 当前{@link Camera_Point}。
 * @return {@link#CAMERA_OK}如果方法调用成功。
 *         {@link#INVALID_ARGUMENT}如果参数丢失或参数类型不正确。
 *         {@link#CAMERA_SESSION_NOT_CONFIG}如果捕获会话未配置。
 * @since 11
 */
Camera_ErrorCode OH_CaptureSession_GetFocusPoint(Camera_CaptureSession* session, Camera_Point* focusPoint);

/**
 * @brief 设置焦点。
 *
 * @param session {@link Camera_CaptureSession}实例。
 * @param focusPoint 要设置的目标{@link Camera_Point}。
 * @return {@link#CAMERA_OK}如果方法调用成功。
 *         {@link#INVALID_ARGUMENT}如果参数丢失或参数类型不正确。
 *         {@link#CAMERA_SESSION_NOT_CONFIG}如果捕获会话未配置。
 * @since 11
 */
Camera_ErrorCode OH_CaptureSession_SetFocusPoint(Camera_CaptureSession* session, Camera_Point focusPoint);

/**
 * @brief 获取所有支持的缩放比例范围。
 *
 * @param session {@link Camera_CaptureSession}实例。
 * @param minZoom 缩放比范围的最小值。
 * @param maxZoom 缩放比例范围的最大值。
 * @return {@link#CAMERA_OK}如果方法调用成功。
 *         {@link#INVALID_ARGUMENT}如果参数丢失或参数类型不正确。
 *         {@link#CAMERA_SESSION_NOT_CONFIG}如果捕获会话未配置。
 * @since 11
 */
Camera_ErrorCode OH_CaptureSession_GetZoomRatioRange(Camera_CaptureSession* session, float* minZoom, float* maxZoom);

/**
 * @brief 获取当前缩放比例。
 *
 * @param session {@link Camera_CaptureSession}实例。
 * @param zoom 当前缩放比例。
 * @return {@link#CAMERA_OK}如果方法调用成功。
 *         {@link#INVALID_ARGUMENT}如果参数丢失或参数类型不正确。
 *         {@link#CAMERA_SESSION_NOT_CONFIG}如果捕获会话未配置。
 * @since 11
 */
Camera_ErrorCode OH_CaptureSession_GetZoomRatio(Camera_CaptureSession* session, float* zoom);

/**
 * @brief 设置缩放比例。
 *
 * @param session {@link Camera_CaptureSession}实例。
 * @param zoom 要设置的目标缩放比。
 * @return {@link#CAMERA_OK}如果方法调用成功。
 *         {@link#INVALID_ARGUMENT}如果参数丢失或参数类型不正确。
 *         {@link#CAMERA_SESSION_NOT_CONFIG}如果捕获会话未配置。
 * @since 11
 */
Camera_ErrorCode OH_CaptureSession_SetZoomRatio(Camera_CaptureSession* session, float zoom);

/**
 * @brief 检查是否支持指定的录像防抖模式。
 *
 * @param session {@link Camera_CaptureSession}实例。
 * @param mode 要检查的{@link Camera_VideoStatizationMode}。
 * @param isSupported 是否支持录像防抖模式的结果。
 * @return {@link#CAMERA_OK}如果方法调用成功。
 *         {@link#INVALID_ARGUMENT}如果参数丢失或参数类型不正确。
 *         {@link#CAMERA_SESSION_NOT_CONFIG}如果捕获会话未配置。
 * @since 11
 */
Camera_ErrorCode OH_CaptureSession_IsVideoStabilizationModeSupported(Camera_CaptureSession* session,
    Camera_VideoStabilizationMode mode, bool* isSupported);

/**
 * @brief 获取当前录像防抖模式。
 *
 * @param session {@link Camera_CaptureSession}实例。
 * @param mode 当前{@link Camera_VideoStatizationMode}。
 * @return {@link#CAMERA_OK}如果方法调用成功。
 *         {@link#INVALID_ARGUMENT}如果参数丢失或参数类型不正确。
 *         {@link#CAMERA_SESSION_NOT_CONFIG}如果捕获会话未配置。
 * @since 11
 */
Camera_ErrorCode OH_CaptureSession_GetVideoStabilizationMode(Camera_CaptureSession* session,
    Camera_VideoStabilizationMode* mode);

/**
 * @brief 设置录像防抖模式。
 *
 * @param session {@link Camera_CaptureSession}实例。
 * @param mode 要设置的目标{@link Camera_VideoStatizationMode}。
 * @return {@link#CAMERA_OK}如果方法调用成功。
 *         {@link#INVALID_ARGUMENT}如果参数丢失或参数类型不正确。
 *         {@link#CAMERA_SESSION_NOT_CONFIG}如果捕获会话未配置。
 * @since 11
 */
Camera_ErrorCode OH_CaptureSession_SetVideoStabilizationMode(Camera_CaptureSession* session,
    Camera_VideoStabilizationMode mode);

/**
 * @brief 确定是否可以将相机输入添加到会话中。
 *
 * @param session {@link Camera_CaptureSession}实例。
 * @param cameraInput 要设置的{@link Camera_Input}。
 * @param isSuccessful 是否可以将相机输入添加到会话中的结果。
 * @return {@link#CAMERA_OK}如果方法调用成功。
 *         {@link#CAMERA_INVALID_ARGUMENT}如果参数丢失或参数类型不正确。
 * @since 12
 */
Camera_ErrorCode OH_CaptureSession_CanAddInput(Camera_CaptureSession* session,
    Camera_Input* cameraInput, bool* isSuccessful);

/**
 * @brief 确定是否可以将相机预览输出添加到会话中。
 *
 * @param session {@link Camera_CaptureSession}实例。
 * @param cameraOutput 要设置的{@link Camera_PreviewOutput}。
 * @param isSuccessful 是否可以将相机预览输出添加到会话中的结果。
 * @return {@link#CAMERA_OK}如果方法调用成功。
 *         {@link#CAMERA_INVALID_ARGUMENT}如果参数丢失或参数类型不正确。
 * @since 12
 */
Camera_ErrorCode OH_CaptureSession_CanAddPreviewOutput(Camera_CaptureSession* session,
    Camera_PreviewOutput* cameraOutput, bool* isSuccessful);

/**
 * @brief 确定是否可以将相机照片输出添加到会话中。
 *
 * @param session {@link Camera_CaptureSession}实例。
 * @param cameraOutput 要设置的{@link Camera_PhotoOutput}。
 * @param isSuccessful 相机照片输出是否可以添加到会话中的结果。
 * @return {@link#CAMERA_OK}如果方法调用成功。
 *         {@link#CAMERA_INVALID_ARGUMENT}如果参数丢失或参数类型不正确。
 * @since 12
 */
Camera_ErrorCode OH_CaptureSession_CanAddPhotoOutput(Camera_CaptureSession* session,
    Camera_PhotoOutput* cameraOutput, bool* isSuccessful);

/**
 * @brief 确定是否可以将相机视频输出添加到会话中。
 *
 * @param session {@link Camera_CaptureSession}实例。
 * @param cameraOutput 要添加的{@link Camera_VideoOutput}。
 * @param isSuccessful 相机视频输出是否可以添加到会话中的结果。
 * @return {@link#CAMERA_OK}如果方法调用成功。
 *         {@link#CAMERA_INVALID_ARGUMENT}如果参数丢失或参数类型不正确。
 * @since 12
 */
Camera_ErrorCode OH_CaptureSession_CanAddVideoOutput(Camera_CaptureSession* session,
    Camera_VideoOutput* cameraOutput, bool* isSuccessful);

/**
 * @brief 检查是否支持指定的预配置类型。
 *
 * @param session {@link Camera_CaptureSession}实例。
 * @param preconfigType 要检查的预配置类型{@link Camera_PreconfigType}。
 * @param canPreconfig 是否支持预配置的结果。
 * @return {@link#CAMERA_OK}如果方法调用成功。
 *         {@link#CAMERA_INVALID_ARGUMENT}如果参数丢失或参数类型不正确。
 * @since 12
 */
Camera_ErrorCode OH_CaptureSession_CanPreconfig(Camera_CaptureSession* session,
    Camera_PreconfigType preconfigType, bool* canPreconfig);

/**
 * @brief 检查是否支持带比例的预配置类型。
 *
 * @param session {@link Camera_CaptureSession}实例。
 * @param preconfigType 要检查支持的类型{@link Camera_PreconfigType}。
 * @param preconfigRatio 要检查支持的比例{@link Camera_PreconfigRatio}。
 * @param canPreconfig 是否支持预配置的结果。
 * @return {@link#CAMERA_OK}如果方法调用成功。
 *         {@link#CAMERA_INVALID_ARGUMENT}如果参数丢失或参数类型不正确。
 * @since 12
 */
Camera_ErrorCode OH_CaptureSession_CanPreconfigWithRatio(Camera_CaptureSession* session,
    Camera_PreconfigType preconfigType, Camera_PreconfigRatio preconfigRatio, bool* canPreconfig);

/**
 * @brief 设置预配置类型。
 *
 * @param session {@link Camera_CaptureSession}实例。
 * @param preconfigType 要检查支持的类型{@link Camera_PreconfigType}。
 * @return {@link#CAMERA_OK}如果方法调用成功。
 *         {@link#CAMERA_INVALID_ARGUMENT}如果参数丢失或参数类型不正确。
 *         {@link#CAMERA_SERVICE_FATAL_ERROR}如果相机服务出现致命错误。
 * @since 12
 */
Camera_ErrorCode OH_CaptureSession_Preconfig(Camera_CaptureSession* session,
    Camera_PreconfigType preconfigType);

/**
 * @brief 设置带有比例的预配置类型。
 *
 * @param session {@link Camera_CaptureSession}实例。
 * @param preconfigType 要检查支持的类型{@link Camera_PreconfigType}。
 * @param preconfigRatio 要检查支持的比例{@link Camera_PreconfigRatio}。
 * @return {@link#CAMERA_OK}如果方法调用成功。
 *         {@link#CAMERA_INVALID_ARGUMENT}如果参数丢失或参数类型不正确。
 *         {@link#CAMERA_SERVICE_FATAL_ERROR}如果相机服务出现致命错误。
 * @since 12
 */
Camera_ErrorCode OH_CaptureSession_PreconfigWithRatio(Camera_CaptureSession* session,
    Camera_PreconfigType preconfigType, Camera_PreconfigRatio preconfigRatio);

/**
 * @brief 查询曝光值。
 *
 * @param session {@link Camera_CaptureSession}实例。
 * @param exposureValue 当前的曝光值。
 * @return {@link#CAMERA_OK}如果方法调用成功。
 *         {@link#CAMERA_INVALID_ARGUMENT}如果参数丢失或参数类型不正确。
 *         {@link#CAMERA_SERVICE_FATAL_ERROR}如果相机服务出现致命错误。
 * @since 12
 */
Camera_ErrorCode OH_CaptureSession_GetExposureValue(Camera_CaptureSession* session, float* exposureValue);

/**
 * @brief 获取当前焦距值。
 *
 * @param session {@link Camera_CaptureSession}实例。
 * @param focalLength 当前焦距值。
 * @return {@link#CAMERA_OK}如果方法调用成功。
 *         {@link#CAMERA_INVALID_ARGUMENT}如果参数丢失或参数类型不正确。
 *         {@link#CAMERA_SESSION_NOT_CONFIG}如果捕获会话未配置。
 * @since 12
 */
Camera_ErrorCode OH_CaptureSession_GetFocalLength(Camera_CaptureSession* session, float* focalLength);

/**
 * @brief 触发平滑变焦。
 *
 * @param session {@link Camera_CaptureSession}实例。
 * @param targetZoom 要设置的目标变焦比。
 * @param smoothZoomMode {@link Camera_SmoothZoomMode}实例。
 * @return {@link#CAMERA_OK}如果方法调用成功。
 *         {@link#CAMERA_INVALID_ARGUMENT}如果参数丢失或参数类型不正确。
 *         {@link#CAMERA_SESSION_NOT_CONFIG}如果捕获会话未配置。
 * @since 12
 */
Camera_ErrorCode OH_CaptureSession_SetSmoothZoom(Camera_CaptureSession* session,
    float targetZoom, Camera_SmoothZoomMode smoothZoomMode);

/**
 * @brief 获取支持的色彩空间列表。
 *
 * @param session {@link Camera_CaptureSession}实例。
 * @param colorSpace 如果方法调用成功，则将记录支持的{@link OH_NativeBuffer_ColorSpace}列表。
 * @param size 如果方法调用成功，则将记录支持的{@link OH_NativeBuffer_ColorSpace}列表的大小。
 * @return {@link#CAMERA_OK}如果方法调用成功。
 *         {@link#CAMERA_INVALID_ARGUMENT}如果参数丢失或参数类型不正确。
 *         {@link#CAMERA_SESSION_NOT_CONFIG}如果捕获会话未配置。
 * @since 12
 */
Camera_ErrorCode OH_CaptureSession_GetSupportedColorSpaces(Camera_CaptureSession* session,
    OH_NativeBuffer_ColorSpace** colorSpace, uint32_t* size);

/**
 * @brief 删除色彩空间列表。
 *
 * @param session {@link Camera_CaptureSession}实例。
 * @param colorSpace 如果方法调用成功，将删除的目前{@link OH_NativeBuffer_ColorSpace}列表。
 * @return {@link#CAMERA_OK}如果方法调用成功。
 *         {@link#CAMERA_INVALID_ARGUMENT}如果参数丢失或参数类型不正确。
 * @since 12
 */
Camera_ErrorCode OH_CaptureSession_DeleteColorSpaces(Camera_CaptureSession* session,
    OH_NativeBuffer_ColorSpace* colorSpace);

/**
 * @brief 获取当前色彩空间。
 *
 * @param session {@link Camera_CaptureSession}实例。
 * @param colorSpace 当前的{@link OH_NativeBuffer_ColorSpace}。
 * @return {@link#CAMERA_OK}如果方法调用成功。
 *         {@link#CAMERA_INVALID_ARGUMENT}如果参数丢失或参数类型不正确。
 *         {@link#CAMERA_SESSION_NOT_CONFIG}如果捕获会话未配置。
 * @since 12
 */
Camera_ErrorCode OH_CaptureSession_GetActiveColorSpace(Camera_CaptureSession* session,
    OH_NativeBuffer_ColorSpace* colorSpace);

/**
 * @brief 设置当前色彩空间。
 *
 * @param session {@link Camera_CaptureSession}实例。
 * @param colorSpace 要设置的目标{@link OH_NativeBuffer_ColorSpace}。
 * @return {@link#CAMERA_OK}如果方法调用成功。
 *         {@link#CAMERA_INVALID_ARGUMENT}如果参数丢失或参数类型不正确。
 *         {@link#CAMERA_SESSION_NOT_CONFIG}如果捕获会话未配置。
 * @since 12
 */
Camera_ErrorCode OH_CaptureSession_SetActiveColorSpace(Camera_CaptureSession* session,
    OH_NativeBuffer_ColorSpace colorSpace);

#ifdef __cplusplus
}
#endif

#endif // NATIVE_INCLUDE_CAMERA_CAMERA_SESSION_H
/** @} */